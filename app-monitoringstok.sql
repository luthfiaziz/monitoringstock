/*
Navicat MySQL Data Transfer

Source Server         : root@localhost
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : app-monitoringstok

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2016-06-27 10:07:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `1nt3x_inv_adj`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_inv_adj`;
CREATE TABLE `1nt3x_inv_adj` (
  `adj_no_trx` varchar(30) NOT NULL,
  `adj_tgl` date NOT NULL,
  `adj_ket` text,
  `adj_tgl_buat` date NOT NULL,
  `adj_tgl_ubah` date NOT NULL,
  `adj_petugas` varchar(50) NOT NULL,
  `m_toko_kode` varchar(11) DEFAULT NULL,
  `stok_opname_no_trx` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`adj_no_trx`),
  KEY `stok_opname_no_trx` (`stok_opname_no_trx`),
  KEY `perusahaan_kode` (`m_toko_kode`) USING BTREE,
  CONSTRAINT `1nt3x_inv_adj_ibfk_1` FOREIGN KEY (`stok_opname_no_trx`) REFERENCES `1nt3x_inv_stok_opname` (`stok_opname_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_adj_ibfk_2` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_inv_adj
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_inv_adj_detail`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_inv_adj_detail`;
CREATE TABLE `1nt3x_inv_adj_detail` (
  `adj_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `adj_detail_ket` text,
  `adj_detail_qtyb` double NOT NULL COMMENT 'book',
  `adj_detail_qtyf` double NOT NULL COMMENT 'fisik',
  `adj_detail_qtys` double NOT NULL COMMENT 'selisih',
  `adj_detail_adjust` double NOT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  `adj_no_trx` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`adj_detail_id`),
  KEY `adj_no_trx` (`adj_no_trx`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_inv_adj_detail_ibfk_1` FOREIGN KEY (`adj_no_trx`) REFERENCES `1nt3x_inv_adj` (`adj_no_trx`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_adj_detail_ibfk_2` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_inv_adj_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_inv_penerimaan_barang`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_inv_penerimaan_barang`;
CREATE TABLE `1nt3x_inv_penerimaan_barang` (
  `inv_pen_brg_kode` varchar(15) NOT NULL DEFAULT '',
  `inv_pen_brg_date` date NOT NULL,
  `inv_pen_brg_no_srt_jln` varchar(15) NOT NULL,
  `inv_pen_brg_ket` text,
  `inv_pen_brg_tgl_buat` date NOT NULL,
  `inv_pen_brg_tgl_ubah` date NOT NULL,
  `inv_pen_brg_petugas` varchar(50) NOT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  `inv_po_no` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`inv_pen_brg_kode`),
  KEY `m_toko_kode` (`m_toko_kode`),
  KEY `inv_po_no` (`inv_po_no`),
  CONSTRAINT `1nt3x_inv_penerimaan_barang_ibfk_1` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_inv_penerimaan_barang
-- ----------------------------
INSERT INTO `1nt3x_inv_penerimaan_barang` VALUES ('PB/112015/00001', '2015-11-16', '', 'tes edit', '2015-11-16', '2015-11-16', 'admin', 'ABC', 'TF/KR/ABC/112015/00002');
INSERT INTO `1nt3x_inv_penerimaan_barang` VALUES ('PB/112015/00002', '2015-11-16', '', 'tes 2', '2015-11-16', '0000-00-00', 'admin', 'ABC', 'PO/112015/00001');
INSERT INTO `1nt3x_inv_penerimaan_barang` VALUES ('PB/112015/00003', '2015-11-16', '', '', '2015-11-16', '0000-00-00', 'admin', 'ABC', 'PO/112015/00001');
INSERT INTO `1nt3x_inv_penerimaan_barang` VALUES ('PB/112015/00004', '2015-11-16', '', '', '2015-11-16', '0000-00-00', 'admin', 'ABC', 'TF/KR/ABC/112015/00002');

-- ----------------------------
-- Table structure for `1nt3x_inv_penerimaan_barang_detail`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_inv_penerimaan_barang_detail`;
CREATE TABLE `1nt3x_inv_penerimaan_barang_detail` (
  `inv_pen_brg_detail_kode` int(11) NOT NULL AUTO_INCREMENT,
  `inv_pen_brg_detail_qty` double NOT NULL,
  `qty_po_sisa` double NOT NULL,
  `inv_pen_brg_kode` varchar(15) DEFAULT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`inv_pen_brg_detail_kode`),
  KEY `inv_pen_brg_kode` (`inv_pen_brg_kode`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_inv_penerimaan_barang_detail_ibfk_1` FOREIGN KEY (`inv_pen_brg_kode`) REFERENCES `1nt3x_inv_penerimaan_barang` (`inv_pen_brg_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_penerimaan_barang_detail_ibfk_2` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_inv_penerimaan_barang_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_inv_persediaan_awal`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_inv_persediaan_awal`;
CREATE TABLE `1nt3x_inv_persediaan_awal` (
  `persediaan_awal_no` varchar(30) NOT NULL,
  `persediaan_awal_bulan` int(2) NOT NULL,
  `persediaan_awal_tahun` int(4) NOT NULL,
  `persediaan_awal_tgl_buat` date NOT NULL,
  `persediaan_awal_tgl_ubah` date NOT NULL,
  `persediaan_awal_petugas` varchar(50) NOT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`persediaan_awal_no`),
  KEY `m_toko_kode` (`m_toko_kode`),
  CONSTRAINT `1nt3x_inv_persediaan_awal_ibfk_1` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_inv_persediaan_awal
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_inv_persediaan_awal_detail`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_inv_persediaan_awal_detail`;
CREATE TABLE `1nt3x_inv_persediaan_awal_detail` (
  `persediaan_awal_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `persediaan_awal_detail_qty` int(11) NOT NULL,
  `persediaan_awal_no` varchar(30) DEFAULT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  `batch_no` varchar(35) DEFAULT NULL,
  `tgl_produksi` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  PRIMARY KEY (`persediaan_awal_detail_id`),
  KEY `persediaan_awal_no` (`persediaan_awal_no`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_inv_persediaan_awal_detail_ibfk_1` FOREIGN KEY (`persediaan_awal_no`) REFERENCES `1nt3x_inv_persediaan_awal` (`persediaan_awal_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_persediaan_awal_detail_ibfk_2` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_inv_persediaan_awal_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_inv_po`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_inv_po`;
CREATE TABLE `1nt3x_inv_po` (
  `inv_po_no` varchar(30) NOT NULL,
  `inv_po_tgl` date NOT NULL,
  `inv_po_ket` text,
  `inv_po_total` double NOT NULL,
  `inv_po_grand_total` double NOT NULL,
  `inv_po_diskon` varchar(4) NOT NULL,
  `inv_po_pajak` varchar(4) NOT NULL,
  `inv_po_status` enum('2','1','0') NOT NULL DEFAULT '2' COMMENT '0: void, 1:closed, 2:proces',
  `inv_po_tgl_buat` date NOT NULL,
  `inv_po_tgl_ubah` date NOT NULL,
  `inv_po_petugas` varchar(50) NOT NULL,
  `m_suplier_kode` varchar(15) DEFAULT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`inv_po_no`),
  KEY `m_suplier_kode` (`m_suplier_kode`),
  KEY `m_toko_kode` (`m_toko_kode`),
  CONSTRAINT `1nt3x_inv_po_ibfk_1` FOREIGN KEY (`m_suplier_kode`) REFERENCES `1nt3x_m_suplier` (`m_suplier_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_po_ibfk_2` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_inv_po
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_inv_po_detail`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_inv_po_detail`;
CREATE TABLE `1nt3x_inv_po_detail` (
  `inv_po_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_po_detail_qty` int(11) NOT NULL,
  `inv_po_detail_harga` double NOT NULL,
  `inv_po_detail_total` double NOT NULL,
  `inv_po_detail_diskon` double NOT NULL,
  `inv_po_detail_grandtotal` double NOT NULL,
  `inv_po_detail_ket` text,
  `inv_po_no` varchar(30) DEFAULT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`inv_po_detail_id`),
  KEY `inv_po_no` (`inv_po_no`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_inv_po_detail_ibfk_1` FOREIGN KEY (`inv_po_no`) REFERENCES `1nt3x_inv_po` (`inv_po_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_po_detail_ibfk_2` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_inv_po_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_inv_return_pembelian`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_inv_return_pembelian`;
CREATE TABLE `1nt3x_inv_return_pembelian` (
  `return_po_no` varchar(30) NOT NULL,
  `return_po_tgl` date NOT NULL,
  `return_po_ket` text NOT NULL,
  `return_po_srt_jln` varchar(30) DEFAULT NULL,
  `return_po_tgl_buat` date NOT NULL,
  `return_po_tgl_ubah` date NOT NULL,
  `return_po_petugas` varchar(50) NOT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  `m_suplier_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`return_po_no`),
  KEY `m_toko_kode` (`m_toko_kode`),
  KEY `m_suplier_kode` (`m_suplier_kode`),
  CONSTRAINT `1nt3x_inv_return_pembelian_ibfk_1` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_return_pembelian_ibfk_2` FOREIGN KEY (`m_suplier_kode`) REFERENCES `1nt3x_m_suplier` (`m_suplier_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_inv_return_pembelian
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_inv_return_pembelian_detail`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_inv_return_pembelian_detail`;
CREATE TABLE `1nt3x_inv_return_pembelian_detail` (
  `return_po_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `return_po_detail_qty` int(11) NOT NULL,
  `return_po_no` varchar(30) DEFAULT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`return_po_detail_id`),
  KEY `return_po_no` (`return_po_no`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_inv_return_pembelian_detail_ibfk_1` FOREIGN KEY (`return_po_no`) REFERENCES `1nt3x_inv_return_pembelian` (`return_po_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_return_pembelian_detail_ibfk_2` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_inv_return_pembelian_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_inv_stok_berjalan`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_inv_stok_berjalan`;
CREATE TABLE `1nt3x_inv_stok_berjalan` (
  `stok_berjalan_id` int(11) NOT NULL AUTO_INCREMENT,
  `stok_berjalan_tgl` date NOT NULL,
  `stok_berjalan_qty_masuk` int(11) DEFAULT NULL,
  `stok_berjalan_qty_keluar` int(11) DEFAULT NULL,
  `stok_berjalan_ket` text,
  `stok_berjalan_no_trx` varchar(30) NOT NULL,
  `stok_berjalan_tgl_buat` date NOT NULL,
  `stok_berjalan_tgl_ubah` date NOT NULL,
  `stok_berjalan_petugas` varchar(50) NOT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`stok_berjalan_id`),
  KEY `m_toko_kode` (`m_toko_kode`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_inv_stok_berjalan_ibfk_1` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_stok_berjalan_ibfk_2` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_inv_stok_berjalan
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_inv_stok_opname`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_inv_stok_opname`;
CREATE TABLE `1nt3x_inv_stok_opname` (
  `stok_opname_no` varchar(30) NOT NULL,
  `stok_opname_tgl` date NOT NULL,
  `stok_opname_bulan` varchar(2) DEFAULT NULL,
  `stok_opname_tahun` varchar(4) DEFAULT NULL,
  `stok_opname_ket` text,
  `stok_opname_tgl_buat` date NOT NULL,
  `stok_opname_tgl_ubah` date NOT NULL,
  `stok_opname_petugas` varchar(50) NOT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`stok_opname_no`),
  KEY `m_toko_kode` (`m_toko_kode`),
  CONSTRAINT `1nt3x_inv_stok_opname_ibfk_1` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_inv_stok_opname
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_inv_stok_opname_detail`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_inv_stok_opname_detail`;
CREATE TABLE `1nt3x_inv_stok_opname_detail` (
  `stok_opname_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `stok_opname_detail_qty_sys` int(11) NOT NULL,
  `stok_opname_detail_qty_fisik` int(11) NOT NULL,
  `stok_opname_no` varchar(30) NOT NULL,
  `m_item_kode` varchar(15) NOT NULL,
  PRIMARY KEY (`stok_opname_detail_id`),
  KEY `stok_opname_no` (`stok_opname_no`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_inv_stok_opname_detail_ibfk_1` FOREIGN KEY (`stok_opname_no`) REFERENCES `1nt3x_inv_stok_opname` (`stok_opname_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_stok_opname_detail_ibfk_2` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_inv_stok_opname_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_inv_transfer_barang`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_inv_transfer_barang`;
CREATE TABLE `1nt3x_inv_transfer_barang` (
  `transfer_barang_no` varchar(30) NOT NULL,
  `transfer_barang_tgl` date NOT NULL,
  `transfer_barang_ket` text NOT NULL,
  `transfer_barang_asal` varchar(50) NOT NULL,
  `transfer_barang_tujuan` varchar(50) NOT NULL,
  `transfer_barang_tgl_buat` date NOT NULL,
  `transfer_barang_tgl_ubah` date NOT NULL,
  `transfer_barang_petugas` varchar(50) NOT NULL,
  PRIMARY KEY (`transfer_barang_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_inv_transfer_barang
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_inv_transfer_barang_detail`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_inv_transfer_barang_detail`;
CREATE TABLE `1nt3x_inv_transfer_barang_detail` (
  `transfer_barang_detail_no` int(11) NOT NULL AUTO_INCREMENT,
  `transfer_barang_detail_qty` int(11) NOT NULL,
  `transfer_barang_no` varchar(30) DEFAULT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`transfer_barang_detail_no`),
  KEY `transfer_barang_no` (`transfer_barang_no`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_inv_transfer_barang_detail_ibfk_1` FOREIGN KEY (`transfer_barang_no`) REFERENCES `1nt3x_inv_transfer_barang` (`transfer_barang_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_transfer_barang_detail_ibfk_2` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_inv_transfer_barang_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_kabupaten`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_kabupaten`;
CREATE TABLE `1nt3x_kabupaten` (
  `kabupaten_kode` varchar(11) NOT NULL,
  `kabupaten_ket` varchar(50) DEFAULT NULL,
  `kabupaten_tgl_buat` date DEFAULT NULL,
  `kabupaten_tgl_ubah` date DEFAULT NULL,
  `kabupaten_petugas` varchar(50) DEFAULT NULL,
  `kabupaten_status` enum('1','0') DEFAULT NULL,
  `provinsi_kode` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`kabupaten_kode`),
  KEY `provinsi_kode` (`provinsi_kode`),
  CONSTRAINT `1nt3x_kabupaten_ibfk_1` FOREIGN KEY (`provinsi_kode`) REFERENCES `1nt3x_provinsi` (`provinsi_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_kabupaten
-- ----------------------------
INSERT INTO `1nt3x_kabupaten` VALUES ('1101', 'Kab. Simeulue', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1102', 'Kab. Aceh Singkil', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1103', 'Kab. Aceh Selatan', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1104', 'Kab. Aceh Tenggara', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1105', 'Kab. Aceh Timur', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1106', 'Kab. Aceh Tengah', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1107', 'Kab. Aceh Barat', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1108', 'Kab. Aceh Besar', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1109', 'Kab. Pidie', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1110', 'Kab. Bireuen', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1111', 'Kab. Aceh Utara', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1112', 'Kab. Aceh Barat Daya', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1113', 'Kab. Gayo Lues', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1114', 'Kab. Aceh Tamiang', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1115', 'Kab. Nagan Raya', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1116', 'Kab. Aceh Jaya', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1117', 'Kab. Bener Meriah', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1118', 'Kab. Pidie Jaya', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1171', 'Kota Banda Aceh', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1172', 'Kota Sabang', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1173', 'Kota Langsa', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1174', 'Kota Lhokseumawe', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1175', 'Kota Subulussalam', null, null, 'system', '1', '11');
INSERT INTO `1nt3x_kabupaten` VALUES ('1201', 'Kab. Nias', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1202', 'Kab. Mandailing Natal', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1203', 'Kab. Tapanuli Selatan', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1204', 'Kab. Tapanuli Tengah', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1205', 'Kab. Tapanuli Utara', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1206', 'Kab. Toba Samosir', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1207', 'Kab. Labuhan Batu', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1208', 'Kab. Asahan', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1209', 'Kab. Simalungun', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1210', 'Kab. Dairi', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1211', 'Kab. Karo', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1212', 'Kab. Deli Serdang', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1213', 'Kab. Langkat', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1214', 'Kab. Nias Selatan', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1215', 'Kab. Humbang Hasundutan', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1216', 'Kab. Pakpak Bharat', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1217', 'Kab. Samosir', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1218', 'Kab. Serdang Bedagai', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1219', 'Kab. Batu Bara', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1220', 'Kab. Padang Lawas Utara', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1221', 'Kab. Padang Lawas', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1222', 'Kab. Labuhan Batu Selatan', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1223', 'Kab. Labuhan Batu Utara', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1224', 'Kab. Nias Utara', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1225', 'Kab. Nias Barat', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1271', 'Kota Sibolga', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1272', 'Kota Tanjung Balai', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1273', 'Kota Pematang Siantar', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1274', 'Kota Tebing Tinggi', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1275', 'Kota Medan', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1276', 'Kota Binjai', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1277', 'Kota Padangsidimpuan', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1278', 'Kota Gunungsitoli', null, null, 'system', '1', '12');
INSERT INTO `1nt3x_kabupaten` VALUES ('1301', 'Kab. Kepulauan Mentawai', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1302', 'Kab. Pesisir Selatan', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1303', 'Kab. Solok', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1304', 'Kab. Sijunjung', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1305', 'Kab. Tanah Datar', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1306', 'Kab. Padang Pariaman', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1307', 'Kab. Agam', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1308', 'Kab. Lima Puluh Kota', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1309', 'Kab. Pasaman', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1310', 'Kab. Solok Selatan', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1311', 'Kab. Dharmasraya', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1312', 'Kab. Pasaman Barat', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1371', 'Kota Padang', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1372', 'Kota Solok', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1373', 'Kota Sawah Lunto', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1374', 'Kota Padang Panjang', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1375', 'Kota Bukittinggi', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1376', 'Kota Payakumbuh', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1377', 'Kota Pariaman', null, null, 'system', '1', '13');
INSERT INTO `1nt3x_kabupaten` VALUES ('1401', 'Kab. Kuantan Singingi', null, null, 'system', '1', '14');
INSERT INTO `1nt3x_kabupaten` VALUES ('1402', 'Kab. Indragiri Hulu', null, null, 'system', '1', '14');
INSERT INTO `1nt3x_kabupaten` VALUES ('1403', 'Kab. Indragiri Hilir', null, null, 'system', '1', '14');
INSERT INTO `1nt3x_kabupaten` VALUES ('1404', 'Kab. Pelalawan', null, null, 'system', '1', '14');
INSERT INTO `1nt3x_kabupaten` VALUES ('1405', 'Kab. Siak', null, null, 'system', '1', '14');
INSERT INTO `1nt3x_kabupaten` VALUES ('1406', 'Kab. Kampar', null, null, 'system', '1', '14');
INSERT INTO `1nt3x_kabupaten` VALUES ('1407', 'Kab. Rokan Hulu', null, null, 'system', '1', '14');
INSERT INTO `1nt3x_kabupaten` VALUES ('1408', 'Kab. Bengkalis', null, null, 'system', '1', '14');
INSERT INTO `1nt3x_kabupaten` VALUES ('1409', 'Kab. Rokan Hilir', null, null, 'system', '1', '14');
INSERT INTO `1nt3x_kabupaten` VALUES ('1410', 'Kab. Kepulauan Meranti', null, null, 'system', '1', '14');
INSERT INTO `1nt3x_kabupaten` VALUES ('1471', 'Kota Pekanbaru', null, null, 'system', '1', '14');
INSERT INTO `1nt3x_kabupaten` VALUES ('1473', 'Kota Dumai', null, null, 'system', '1', '14');
INSERT INTO `1nt3x_kabupaten` VALUES ('1501', 'Kab. Kerinci', null, null, 'system', '1', '15');
INSERT INTO `1nt3x_kabupaten` VALUES ('1502', 'Kab. Merangin', null, null, 'system', '1', '15');
INSERT INTO `1nt3x_kabupaten` VALUES ('1503', 'Kab. Sarolangun', null, null, 'system', '1', '15');
INSERT INTO `1nt3x_kabupaten` VALUES ('1504', 'Kab. Batang Hari', null, null, 'system', '1', '15');
INSERT INTO `1nt3x_kabupaten` VALUES ('1505', 'Kab. Muaro Jambi', null, null, 'system', '1', '15');
INSERT INTO `1nt3x_kabupaten` VALUES ('1506', 'Kab. Tanjung Jabung Timur', null, null, 'system', '1', '15');
INSERT INTO `1nt3x_kabupaten` VALUES ('1507', 'Kab. Tanjung Jabung Barat', null, null, 'system', '1', '15');
INSERT INTO `1nt3x_kabupaten` VALUES ('1508', 'Kab. Tebo', null, null, 'system', '1', '15');
INSERT INTO `1nt3x_kabupaten` VALUES ('1509', 'Kab. Bungo', null, null, 'system', '1', '15');
INSERT INTO `1nt3x_kabupaten` VALUES ('1571', 'Kota Jambi', null, null, 'system', '1', '15');
INSERT INTO `1nt3x_kabupaten` VALUES ('1572', 'Kota Sungai Penuh', null, null, 'system', '1', '15');
INSERT INTO `1nt3x_kabupaten` VALUES ('1601', 'Kab. Ogan Komering Ulu', null, null, 'system', '1', '16');
INSERT INTO `1nt3x_kabupaten` VALUES ('1602', 'Kab. Ogan Komering Ilir', null, null, 'system', '1', '16');
INSERT INTO `1nt3x_kabupaten` VALUES ('1603', 'Kab. Muara Enim', null, null, 'system', '1', '16');
INSERT INTO `1nt3x_kabupaten` VALUES ('1604', 'Kab. Lahat', null, null, 'system', '1', '16');
INSERT INTO `1nt3x_kabupaten` VALUES ('1605', 'Kab. Musi Rawas', null, null, 'system', '1', '16');
INSERT INTO `1nt3x_kabupaten` VALUES ('1606', 'Kab. Musi Banyuasin', null, null, 'system', '1', '16');
INSERT INTO `1nt3x_kabupaten` VALUES ('1607', 'Kab. Banyu Asin', null, null, 'system', '1', '16');
INSERT INTO `1nt3x_kabupaten` VALUES ('1608', 'Kab. Ogan Komering Ulu Selatan', null, null, 'system', '1', '16');
INSERT INTO `1nt3x_kabupaten` VALUES ('1609', 'Kab. Ogan Komering Ulu Timur', null, null, 'system', '1', '16');
INSERT INTO `1nt3x_kabupaten` VALUES ('1610', 'Kab. Ogan Ilir', null, null, 'system', '1', '16');
INSERT INTO `1nt3x_kabupaten` VALUES ('1611', 'Kab. Empat Lawang', null, null, 'system', '1', '16');
INSERT INTO `1nt3x_kabupaten` VALUES ('1671', 'Kota Palembang', null, null, 'system', '1', '16');
INSERT INTO `1nt3x_kabupaten` VALUES ('1672', 'Kota Prabumulih', null, null, 'system', '1', '16');
INSERT INTO `1nt3x_kabupaten` VALUES ('1673', 'Kota Pagar Alam', null, null, 'system', '1', '16');
INSERT INTO `1nt3x_kabupaten` VALUES ('1674', 'Kota Lubuklinggau', null, null, 'system', '1', '16');
INSERT INTO `1nt3x_kabupaten` VALUES ('1701', 'Kab. Bengkulu Selatan', null, null, 'system', '1', '17');
INSERT INTO `1nt3x_kabupaten` VALUES ('1702', 'Kab. Rejang Lebong', null, null, 'system', '1', '17');
INSERT INTO `1nt3x_kabupaten` VALUES ('1703', 'Kab. Bengkulu Utara', null, null, 'system', '1', '17');
INSERT INTO `1nt3x_kabupaten` VALUES ('1704', 'Kab. Kaur', null, null, 'system', '1', '17');
INSERT INTO `1nt3x_kabupaten` VALUES ('1705', 'Kab. Seluma', null, null, 'system', '1', '17');
INSERT INTO `1nt3x_kabupaten` VALUES ('1706', 'Kab. Mukomuko', null, null, 'system', '1', '17');
INSERT INTO `1nt3x_kabupaten` VALUES ('1707', 'Kab. Lebong', null, null, 'system', '1', '17');
INSERT INTO `1nt3x_kabupaten` VALUES ('1708', 'Kab. Kepahiang', null, null, 'system', '1', '17');
INSERT INTO `1nt3x_kabupaten` VALUES ('1709', 'Kab. Bengkulu Tengah', null, null, 'system', '1', '17');
INSERT INTO `1nt3x_kabupaten` VALUES ('1771', 'Kota Bengkulu', null, null, 'system', '1', '17');
INSERT INTO `1nt3x_kabupaten` VALUES ('1801', 'Kab. Lampung Barat', null, null, 'system', '1', '18');
INSERT INTO `1nt3x_kabupaten` VALUES ('1802', 'Kab. Tanggamus', null, null, 'system', '1', '18');
INSERT INTO `1nt3x_kabupaten` VALUES ('1803', 'Kab. Lampung Selatan', null, null, 'system', '1', '18');
INSERT INTO `1nt3x_kabupaten` VALUES ('1804', 'Kab. Lampung Timur', null, null, 'system', '1', '18');
INSERT INTO `1nt3x_kabupaten` VALUES ('1805', 'Kab. Lampung Tengah', null, null, 'system', '1', '18');
INSERT INTO `1nt3x_kabupaten` VALUES ('1806', 'Kab. Lampung Utara', null, null, 'system', '1', '18');
INSERT INTO `1nt3x_kabupaten` VALUES ('1807', 'Kab. Way Kanan', null, null, 'system', '1', '18');
INSERT INTO `1nt3x_kabupaten` VALUES ('1808', 'Kab. Tulangbawang', null, null, 'system', '1', '18');
INSERT INTO `1nt3x_kabupaten` VALUES ('1809', 'Kab. Pesawaran', null, null, 'system', '1', '18');
INSERT INTO `1nt3x_kabupaten` VALUES ('1810', 'Kab. Pringsewu', null, null, 'system', '1', '18');
INSERT INTO `1nt3x_kabupaten` VALUES ('1811', 'Kab. Mesuji', null, null, 'system', '1', '18');
INSERT INTO `1nt3x_kabupaten` VALUES ('1812', 'Kab. Tulang Bawang Barat', null, null, 'system', '1', '18');
INSERT INTO `1nt3x_kabupaten` VALUES ('1813', 'Kab. Pesisir Barat', null, null, 'system', '1', '18');
INSERT INTO `1nt3x_kabupaten` VALUES ('1871', 'Kota Bandar Lampung', null, null, 'system', '1', '18');
INSERT INTO `1nt3x_kabupaten` VALUES ('1872', 'Kota Metro', null, null, 'system', '1', '18');
INSERT INTO `1nt3x_kabupaten` VALUES ('1901', 'Kab. Bangka', null, null, 'system', '1', '19');
INSERT INTO `1nt3x_kabupaten` VALUES ('1902', 'Kab. Belitung', null, null, 'system', '1', '19');
INSERT INTO `1nt3x_kabupaten` VALUES ('1903', 'Kab. Bangka Barat', null, null, 'system', '1', '19');
INSERT INTO `1nt3x_kabupaten` VALUES ('1904', 'Kab. Bangka Tengah', null, null, 'system', '1', '19');
INSERT INTO `1nt3x_kabupaten` VALUES ('1905', 'Kab. Bangka Selatan', null, null, 'system', '1', '19');
INSERT INTO `1nt3x_kabupaten` VALUES ('1906', 'Kab. Belitung Timur', null, null, 'system', '1', '19');
INSERT INTO `1nt3x_kabupaten` VALUES ('1971', 'Kota Pangkal Pinang', null, null, 'system', '1', '19');
INSERT INTO `1nt3x_kabupaten` VALUES ('2101', 'Kab. Karimun', null, null, 'system', '1', '21');
INSERT INTO `1nt3x_kabupaten` VALUES ('2102', 'Kab. Bintan', null, null, 'system', '1', '21');
INSERT INTO `1nt3x_kabupaten` VALUES ('2103', 'Kab. Natuna', null, null, 'system', '1', '21');
INSERT INTO `1nt3x_kabupaten` VALUES ('2104', 'Kab. Lingga', null, null, 'system', '1', '21');
INSERT INTO `1nt3x_kabupaten` VALUES ('2105', 'Kab. Kepulauan Anambas', null, null, 'system', '1', '21');
INSERT INTO `1nt3x_kabupaten` VALUES ('2171', 'Kota Batam', null, null, 'system', '1', '21');
INSERT INTO `1nt3x_kabupaten` VALUES ('2172', 'Kota Tanjung Pinang', null, null, 'system', '1', '21');
INSERT INTO `1nt3x_kabupaten` VALUES ('3101', 'Kab. Kepulauan Seribu', null, null, 'system', '1', '31');
INSERT INTO `1nt3x_kabupaten` VALUES ('3171', 'Kota Jakarta Selatan', null, null, 'system', '1', '31');
INSERT INTO `1nt3x_kabupaten` VALUES ('3172', 'Kota Jakarta Timur', null, null, 'system', '1', '31');
INSERT INTO `1nt3x_kabupaten` VALUES ('3173', 'Kota Jakarta Pusat', null, null, 'system', '1', '31');
INSERT INTO `1nt3x_kabupaten` VALUES ('3174', 'Kota Jakarta Barat', null, null, 'system', '1', '31');
INSERT INTO `1nt3x_kabupaten` VALUES ('3175', 'Kota Jakarta Utara', null, null, 'system', '1', '31');
INSERT INTO `1nt3x_kabupaten` VALUES ('3201', 'Kab. Bogor', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3202', 'Kab. Sukabumi', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3203', 'Kab. Cianjur', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3204', 'Kab. Bandung', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3205', 'Kab. Garut', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3206', 'Kab. Tasikmalaya', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3207', 'Kab. Ciamis', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3208', 'Kab. Kuningan', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3209', 'Kab. Cirebon', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3210', 'Kab. Majalengka', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3211', 'Kab. Sumedang', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3212', 'Kab. Indramayu', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3213', 'Kab. Subang', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3214', 'Kab. Purwakarta', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3215', 'Kab. Karawang', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3216', 'Kab. Bekasi', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3217', 'Kab. Bandung Barat', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3218', 'Kab. Pangandaran', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3271', 'Kota Bogor', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3272', 'Kota Sukabumi', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3273', 'Kota Bandung', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3274', 'Kota Cirebon', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3275', 'Kota Bekasi', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3276', 'Kota Depok', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3277', 'Kota Cimahi', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3278', 'Kota Tasikmalaya', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3279', 'Kota Banjar', null, null, 'system', '1', '32');
INSERT INTO `1nt3x_kabupaten` VALUES ('3301', 'Kab. Cilacap', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3302', 'Kab. Banyumas', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3303', 'Kab. Purbalingga', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3304', 'Kab. Banjarnegara', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3305', 'Kab. Kebumen', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3306', 'Kab. Purworejo', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3307', 'Kab. Wonosobo', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3308', 'Kab. Magelang', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3309', 'Kab. Boyolali', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3310', 'Kab. Klaten', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3311', 'Kab. Sukoharjo', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3312', 'Kab. Wonogiri', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3313', 'Kab. Karanganyar', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3314', 'Kab. Sragen', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3315', 'Kab. Grobogan', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3316', 'Kab. Blora', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3317', 'Kab. Rembang', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3318', 'Kab. Pati', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3319', 'Kab. Kudus', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3320', 'Kab. Jepara', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3321', 'Kab. Demak', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3322', 'Kab. Semarang', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3323', 'Kab. Temanggung', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3324', 'Kab. Kendal', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3325', 'Kab. Batang', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3326', 'Kab. Pekalongan', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3327', 'Kab. Pemalang', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3328', 'Kab. Tegal', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3329', 'Kab. Brebes', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3371', 'Kota Magelang', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3372', 'Kota Surakarta', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3373', 'Kota Salatiga', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3374', 'Kota Semarang', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3375', 'Kota Pekalongan', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3376', 'Kota Tegal', null, null, 'system', '1', '33');
INSERT INTO `1nt3x_kabupaten` VALUES ('3401', 'Kab. Kulon Progo', null, null, 'system', '1', '34');
INSERT INTO `1nt3x_kabupaten` VALUES ('3402', 'Kab. Bantul', null, null, 'system', '1', '34');
INSERT INTO `1nt3x_kabupaten` VALUES ('3403', 'Kab. Gunung Kidul', null, null, 'system', '1', '34');
INSERT INTO `1nt3x_kabupaten` VALUES ('3404', 'Kab. Sleman', null, null, 'system', '1', '34');
INSERT INTO `1nt3x_kabupaten` VALUES ('3471', 'Kota Yogyakarta', null, null, 'system', '1', '34');
INSERT INTO `1nt3x_kabupaten` VALUES ('3501', 'Kab. Pacitan', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3502', 'Kab. Ponorogo', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3503', 'Kab. Trenggalek', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3504', 'Kab. Tulungagung', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3505', 'Kab. Blitar', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3506', 'Kab. Kediri', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3507', 'Kab. Malang', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3508', 'Kab. Lumajang', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3509', 'Kab. Jember', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3510', 'Kab. Banyuwangi', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3511', 'Kab. Bondowoso', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3512', 'Kab. Situbondo', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3513', 'Kab. Probolinggo', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3514', 'Kab. Pasuruan', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3515', 'Kab. Sidoarjo', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3516', 'Kab. Mojokerto', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3517', 'Kab. Jombang', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3518', 'Kab. Nganjuk', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3519', 'Kab. Madiun', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3520', 'Kab. Magetan', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3521', 'Kab. Ngawi', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3522', 'Kab. Bojonegoro', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3523', 'Kab. Tuban', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3524', 'Kab. Lamongan', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3525', 'Kab. Gresik', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3526', 'Kab. Bangkalan', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3527', 'Kab. Sampang', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3528', 'Kab. Pamekasan', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3529', 'Kab. Sumenep', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3571', 'Kota Kediri', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3572', 'Kota Blitar', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3573', 'Kota Malang', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3574', 'Kota Probolinggo', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3575', 'Kota Pasuruan', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3576', 'Kota Mojokerto', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3577', 'Kota Madiun', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3578', 'Kota Surabaya', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3579', 'Kota Batu', null, null, 'system', '1', '35');
INSERT INTO `1nt3x_kabupaten` VALUES ('3601', 'Kab. Pandeglang', null, null, 'system', '1', '36');
INSERT INTO `1nt3x_kabupaten` VALUES ('3602', 'Kab. Lebak', null, null, 'system', '1', '36');
INSERT INTO `1nt3x_kabupaten` VALUES ('3603', 'Kab. Tangerang', null, null, 'system', '1', '36');
INSERT INTO `1nt3x_kabupaten` VALUES ('3604', 'Kab. Serang', null, null, 'system', '1', '36');
INSERT INTO `1nt3x_kabupaten` VALUES ('3671', 'Kota Tangerang', null, null, 'system', '1', '36');
INSERT INTO `1nt3x_kabupaten` VALUES ('3672', 'Kota Cilegon', null, null, 'system', '1', '36');
INSERT INTO `1nt3x_kabupaten` VALUES ('3673', 'Kota Serang', null, null, 'system', '1', '36');
INSERT INTO `1nt3x_kabupaten` VALUES ('3674', 'Kota Tangerang Selatan', null, null, 'system', '1', '36');
INSERT INTO `1nt3x_kabupaten` VALUES ('5101', 'Kab. Jembrana', null, null, 'system', '1', '51');
INSERT INTO `1nt3x_kabupaten` VALUES ('5102', 'Kab. Tabanan', null, null, 'system', '1', '51');
INSERT INTO `1nt3x_kabupaten` VALUES ('5103', 'Kab. Badung', null, null, 'system', '1', '51');
INSERT INTO `1nt3x_kabupaten` VALUES ('5104', 'Kab. Gianyar', null, null, 'system', '1', '51');
INSERT INTO `1nt3x_kabupaten` VALUES ('5105', 'Kab. Klungkung', null, null, 'system', '1', '51');
INSERT INTO `1nt3x_kabupaten` VALUES ('5106', 'Kab. Bangli', null, null, 'system', '1', '51');
INSERT INTO `1nt3x_kabupaten` VALUES ('5107', 'Kab. Karang Asem', null, null, 'system', '1', '51');
INSERT INTO `1nt3x_kabupaten` VALUES ('5108', 'Kab. Buleleng', null, null, 'system', '1', '51');
INSERT INTO `1nt3x_kabupaten` VALUES ('5171', 'Kota Denpasar', null, null, 'system', '1', '51');
INSERT INTO `1nt3x_kabupaten` VALUES ('5201', 'Kab. Lombok Barat', null, null, 'system', '1', '52');
INSERT INTO `1nt3x_kabupaten` VALUES ('5202', 'Kab. Lombok Tengah', null, null, 'system', '1', '52');
INSERT INTO `1nt3x_kabupaten` VALUES ('5203', 'Kab. Lombok Timur', null, null, 'system', '1', '52');
INSERT INTO `1nt3x_kabupaten` VALUES ('5204', 'Kab. Sumbawa', null, null, 'system', '1', '52');
INSERT INTO `1nt3x_kabupaten` VALUES ('5205', 'Kab. Dompu', null, null, 'system', '1', '52');
INSERT INTO `1nt3x_kabupaten` VALUES ('5206', 'Kab. Bima', null, null, 'system', '1', '52');
INSERT INTO `1nt3x_kabupaten` VALUES ('5207', 'Kab. Sumbawa Barat', null, null, 'system', '1', '52');
INSERT INTO `1nt3x_kabupaten` VALUES ('5208', 'Kab. Lombok Utara', null, null, 'system', '1', '52');
INSERT INTO `1nt3x_kabupaten` VALUES ('5271', 'Kota Mataram', null, null, 'system', '1', '52');
INSERT INTO `1nt3x_kabupaten` VALUES ('5272', 'Kota Bima', null, null, 'system', '1', '52');
INSERT INTO `1nt3x_kabupaten` VALUES ('5301', 'Kab. Sumba Barat', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5302', 'Kab. Sumba Timur', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5303', 'Kab. Kupang', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5304', 'Kab. Timor Tengah Selatan', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5305', 'Kab. Timor Tengah Utara', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5306', 'Kab. Belu', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5307', 'Kab. Alor', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5308', 'Kab. Lembata', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5309', 'Kab. Flores Timur', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5310', 'Kab. Sikka', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5311', 'Kab. Ende', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5312', 'Kab. Ngada', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5313', 'Kab. Manggarai', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5314', 'Kab. Rote Ndao', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5315', 'Kab. Manggarai Barat', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5316', 'Kab. Sumba Tengah', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5317', 'Kab. Sumba Barat Daya', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5318', 'Kab. Nagekeo', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5319', 'Kab. Manggarai Timur', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5320', 'Kab. Sabu Raijua', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('5371', 'Kota Kupang', null, null, 'system', '1', '53');
INSERT INTO `1nt3x_kabupaten` VALUES ('6101', 'Kab. Sambas', null, null, 'system', '1', '61');
INSERT INTO `1nt3x_kabupaten` VALUES ('6102', 'Kab. Bengkayang', null, null, 'system', '1', '61');
INSERT INTO `1nt3x_kabupaten` VALUES ('6103', 'Kab. Landak', null, null, 'system', '1', '61');
INSERT INTO `1nt3x_kabupaten` VALUES ('6104', 'Kab. Pontianak', null, null, 'system', '1', '61');
INSERT INTO `1nt3x_kabupaten` VALUES ('6105', 'Kab. Sanggau', null, null, 'system', '1', '61');
INSERT INTO `1nt3x_kabupaten` VALUES ('6106', 'Kab. Ketapang', null, null, 'system', '1', '61');
INSERT INTO `1nt3x_kabupaten` VALUES ('6107', 'Kab. Sintang', null, null, 'system', '1', '61');
INSERT INTO `1nt3x_kabupaten` VALUES ('6108', 'Kab. Kapuas Hulu', null, null, 'system', '1', '61');
INSERT INTO `1nt3x_kabupaten` VALUES ('6109', 'Kab. Sekadau', null, null, 'system', '1', '61');
INSERT INTO `1nt3x_kabupaten` VALUES ('6110', 'Kab. Melawi', null, null, 'system', '1', '61');
INSERT INTO `1nt3x_kabupaten` VALUES ('6111', 'Kab. Kayong Utara', null, null, 'system', '1', '61');
INSERT INTO `1nt3x_kabupaten` VALUES ('6112', 'Kab. Kubu Raya', null, null, 'system', '1', '61');
INSERT INTO `1nt3x_kabupaten` VALUES ('6171', 'Kota Pontianak', null, null, 'system', '1', '61');
INSERT INTO `1nt3x_kabupaten` VALUES ('6172', 'Kota Singkawang', null, null, 'system', '1', '61');
INSERT INTO `1nt3x_kabupaten` VALUES ('6201', 'Kab. Kotawaringin Barat', null, null, 'system', '1', '62');
INSERT INTO `1nt3x_kabupaten` VALUES ('6202', 'Kab. Kotawaringin Timur', null, null, 'system', '1', '62');
INSERT INTO `1nt3x_kabupaten` VALUES ('6203', 'Kab. Kapuas', null, null, 'system', '1', '62');
INSERT INTO `1nt3x_kabupaten` VALUES ('6204', 'Kab. Barito Selatan', null, null, 'system', '1', '62');
INSERT INTO `1nt3x_kabupaten` VALUES ('6205', 'Kab. Barito Utara', null, null, 'system', '1', '62');
INSERT INTO `1nt3x_kabupaten` VALUES ('6206', 'Kab. Sukamara', null, null, 'system', '1', '62');
INSERT INTO `1nt3x_kabupaten` VALUES ('6207', 'Kab. Lamandau', null, null, 'system', '1', '62');
INSERT INTO `1nt3x_kabupaten` VALUES ('6208', 'Kab. Seruyan', null, null, 'system', '1', '62');
INSERT INTO `1nt3x_kabupaten` VALUES ('6209', 'Kab. Katingan', null, null, 'system', '1', '62');
INSERT INTO `1nt3x_kabupaten` VALUES ('6210', 'Kab. Pulang Pisau', null, null, 'system', '1', '62');
INSERT INTO `1nt3x_kabupaten` VALUES ('6211', 'Kab. Gunung Mas', null, null, 'system', '1', '62');
INSERT INTO `1nt3x_kabupaten` VALUES ('6212', 'Kab. Barito Timur', null, null, 'system', '1', '62');
INSERT INTO `1nt3x_kabupaten` VALUES ('6213', 'Kab. Murung Raya', null, null, 'system', '1', '62');
INSERT INTO `1nt3x_kabupaten` VALUES ('6271', 'Kota Palangka Raya', null, null, 'system', '1', '62');
INSERT INTO `1nt3x_kabupaten` VALUES ('6301', 'Kab. Tanah Laut', null, null, 'system', '1', '63');
INSERT INTO `1nt3x_kabupaten` VALUES ('6302', 'Kab. Kota Baru', null, null, 'system', '1', '63');
INSERT INTO `1nt3x_kabupaten` VALUES ('6303', 'Kab. Banjar', null, null, 'system', '1', '63');
INSERT INTO `1nt3x_kabupaten` VALUES ('6304', 'Kab. Barito Kuala', null, null, 'system', '1', '63');
INSERT INTO `1nt3x_kabupaten` VALUES ('6305', 'Kab. Tapin', null, null, 'system', '1', '63');
INSERT INTO `1nt3x_kabupaten` VALUES ('6306', 'Kab. Hulu Sungai Selatan', null, null, 'system', '1', '63');
INSERT INTO `1nt3x_kabupaten` VALUES ('6307', 'Kab. Hulu Sungai Tengah', null, null, 'system', '1', '63');
INSERT INTO `1nt3x_kabupaten` VALUES ('6308', 'Kab. Hulu Sungai Utara', null, null, 'system', '1', '63');
INSERT INTO `1nt3x_kabupaten` VALUES ('6309', 'Kab. Tabalong', null, null, 'system', '1', '63');
INSERT INTO `1nt3x_kabupaten` VALUES ('6310', 'Kab. Tanah Bumbu', null, null, 'system', '1', '63');
INSERT INTO `1nt3x_kabupaten` VALUES ('6311', 'Kab. Balangan', null, null, 'system', '1', '63');
INSERT INTO `1nt3x_kabupaten` VALUES ('6371', 'Kota Banjarmasin', null, null, 'system', '1', '63');
INSERT INTO `1nt3x_kabupaten` VALUES ('6372', 'Kota Banjar Baru', null, null, 'system', '1', '63');
INSERT INTO `1nt3x_kabupaten` VALUES ('6401', 'Kab. Paser', null, null, 'system', '1', '64');
INSERT INTO `1nt3x_kabupaten` VALUES ('6402', 'Kab. Kutai Barat', null, null, 'system', '1', '64');
INSERT INTO `1nt3x_kabupaten` VALUES ('6403', 'Kab. Kutai Kartanegara', null, null, 'system', '1', '64');
INSERT INTO `1nt3x_kabupaten` VALUES ('6404', 'Kab. Kutai Timur', null, null, 'system', '1', '64');
INSERT INTO `1nt3x_kabupaten` VALUES ('6405', 'Kab. Berau', null, null, 'system', '1', '64');
INSERT INTO `1nt3x_kabupaten` VALUES ('6409', 'Kab. Penajam Paser Utara', null, null, 'system', '1', '64');
INSERT INTO `1nt3x_kabupaten` VALUES ('6471', 'Kota Balikpapan', null, null, 'system', '1', '64');
INSERT INTO `1nt3x_kabupaten` VALUES ('6472', 'Kota Samarinda', null, null, 'system', '1', '64');
INSERT INTO `1nt3x_kabupaten` VALUES ('6474', 'Kota Bontang', null, null, 'system', '1', '64');
INSERT INTO `1nt3x_kabupaten` VALUES ('6501', 'Kab. Malinau', null, null, 'system', '1', '65');
INSERT INTO `1nt3x_kabupaten` VALUES ('6502', 'Kab. Bulungan', null, null, 'system', '1', '65');
INSERT INTO `1nt3x_kabupaten` VALUES ('6503', 'Kab. Tana Tidung', null, null, 'system', '1', '65');
INSERT INTO `1nt3x_kabupaten` VALUES ('6504', 'Kab. Nunukan', null, null, 'system', '1', '65');
INSERT INTO `1nt3x_kabupaten` VALUES ('6571', 'Kota Tarakan', null, null, 'system', '1', '65');
INSERT INTO `1nt3x_kabupaten` VALUES ('7101', 'Kab. Bolaang Mongondow', null, null, 'system', '1', '71');
INSERT INTO `1nt3x_kabupaten` VALUES ('7102', 'Kab. Minahasa', null, null, 'system', '1', '71');
INSERT INTO `1nt3x_kabupaten` VALUES ('7103', 'Kab. Kepulauan Sangihe', null, null, 'system', '1', '71');
INSERT INTO `1nt3x_kabupaten` VALUES ('7104', 'Kab. Kepulauan Talaud', null, null, 'system', '1', '71');
INSERT INTO `1nt3x_kabupaten` VALUES ('7105', 'Kab. Minahasa Selatan', null, null, 'system', '1', '71');
INSERT INTO `1nt3x_kabupaten` VALUES ('7106', 'Kab. Minahasa Utara', null, null, 'system', '1', '71');
INSERT INTO `1nt3x_kabupaten` VALUES ('7107', 'Kab. Bolaang Mongondow Utara', null, null, 'system', '1', '71');
INSERT INTO `1nt3x_kabupaten` VALUES ('7108', 'Kab. Siau Tagulandang Biaro', null, null, 'system', '1', '71');
INSERT INTO `1nt3x_kabupaten` VALUES ('7109', 'Kab. Minahasa Tenggara', null, null, 'system', '1', '71');
INSERT INTO `1nt3x_kabupaten` VALUES ('7110', 'Kab. Bolaang Mongondow Selatan', null, null, 'system', '1', '71');
INSERT INTO `1nt3x_kabupaten` VALUES ('7111', 'Kab. Bolaang Mongondow Timur', null, null, 'system', '1', '71');
INSERT INTO `1nt3x_kabupaten` VALUES ('7171', 'Kota Manado', null, null, 'system', '1', '71');
INSERT INTO `1nt3x_kabupaten` VALUES ('7172', 'Kota Bitung', null, null, 'system', '1', '71');
INSERT INTO `1nt3x_kabupaten` VALUES ('7173', 'Kota Tomohon', null, null, 'system', '1', '71');
INSERT INTO `1nt3x_kabupaten` VALUES ('7174', 'Kota Kotamobagu', null, null, 'system', '1', '71');
INSERT INTO `1nt3x_kabupaten` VALUES ('7201', 'Kab. Banggai Kepulauan', null, null, 'system', '1', '72');
INSERT INTO `1nt3x_kabupaten` VALUES ('7202', 'Kab. Banggai', null, null, 'system', '1', '72');
INSERT INTO `1nt3x_kabupaten` VALUES ('7203', 'Kab. Morowali', null, null, 'system', '1', '72');
INSERT INTO `1nt3x_kabupaten` VALUES ('7204', 'Kab. Poso', null, null, 'system', '1', '72');
INSERT INTO `1nt3x_kabupaten` VALUES ('7205', 'Kab. Donggala', null, null, 'system', '1', '72');
INSERT INTO `1nt3x_kabupaten` VALUES ('7206', 'Kab. Toli-toli', null, null, 'system', '1', '72');
INSERT INTO `1nt3x_kabupaten` VALUES ('7207', 'Kab. Buol', null, null, 'system', '1', '72');
INSERT INTO `1nt3x_kabupaten` VALUES ('7208', 'Kab. Parigi Moutong', null, null, 'system', '1', '72');
INSERT INTO `1nt3x_kabupaten` VALUES ('7209', 'Kab. Tojo Una-una', null, null, 'system', '1', '72');
INSERT INTO `1nt3x_kabupaten` VALUES ('7210', 'Kab. Sigi', null, null, 'system', '1', '72');
INSERT INTO `1nt3x_kabupaten` VALUES ('7271', 'Kota Palu', null, null, 'system', '1', '72');
INSERT INTO `1nt3x_kabupaten` VALUES ('7301', 'Kab. Kepulauan Selayar', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7302', 'Kab. Bulukumba', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7303', 'Kab. Bantaeng', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7304', 'Kab. Jeneponto', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7305', 'Kab. Takalar', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7306', 'Kab. Gowa', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7307', 'Kab. Sinjai', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7308', 'Kab. Maros', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7309', 'Kab. Pangkajene Dan Kepulauan', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7310', 'Kab. Barru', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7311', 'Kab. Bone', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7312', 'Kab. Soppeng', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7313', 'Kab. Wajo', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7314', 'Kab. Sidenreng Rappang', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7315', 'Kab. Pinrang', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7316', 'Kab. Enrekang', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7317', 'Kab. Luwu', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7318', 'Kab. Tana Toraja', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7322', 'Kab. Luwu Utara', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7325', 'Kab. Luwu Timur', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7326', 'Kab. Toraja Utara', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7371', 'Kota Makassar', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7372', 'Kota Parepare', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7373', 'Kota Palopo', null, null, 'system', '1', '73');
INSERT INTO `1nt3x_kabupaten` VALUES ('7401', 'Kab. Buton', null, null, 'system', '1', '74');
INSERT INTO `1nt3x_kabupaten` VALUES ('7402', 'Kab. Muna', null, null, 'system', '1', '74');
INSERT INTO `1nt3x_kabupaten` VALUES ('7403', 'Kab. Konawe', null, null, 'system', '1', '74');
INSERT INTO `1nt3x_kabupaten` VALUES ('7404', 'Kab. Kolaka', null, null, 'system', '1', '74');
INSERT INTO `1nt3x_kabupaten` VALUES ('7405', 'Kab. Konawe Selatan', null, null, 'system', '1', '74');
INSERT INTO `1nt3x_kabupaten` VALUES ('7406', 'Kab. Bombana', null, null, 'system', '1', '74');
INSERT INTO `1nt3x_kabupaten` VALUES ('7407', 'Kab. Wakatobi', null, null, 'system', '1', '74');
INSERT INTO `1nt3x_kabupaten` VALUES ('7408', 'Kab. Kolaka Utara', null, null, 'system', '1', '74');
INSERT INTO `1nt3x_kabupaten` VALUES ('7409', 'Kab. Buton Utara', null, null, 'system', '1', '74');
INSERT INTO `1nt3x_kabupaten` VALUES ('7410', 'Kab. Konawe Utara', null, null, 'system', '1', '74');
INSERT INTO `1nt3x_kabupaten` VALUES ('7471', 'Kota Kendari', null, null, 'system', '1', '74');
INSERT INTO `1nt3x_kabupaten` VALUES ('7472', 'Kota Baubau', null, null, 'system', '1', '74');
INSERT INTO `1nt3x_kabupaten` VALUES ('7501', 'Kab. Boalemo', null, null, 'system', '1', '75');
INSERT INTO `1nt3x_kabupaten` VALUES ('7502', 'Kab. Gorontalo', null, null, 'system', '1', '75');
INSERT INTO `1nt3x_kabupaten` VALUES ('7503', 'Kab. Pohuwato', null, null, 'system', '1', '75');
INSERT INTO `1nt3x_kabupaten` VALUES ('7504', 'Kab. Bone Bolango', null, null, 'system', '1', '75');
INSERT INTO `1nt3x_kabupaten` VALUES ('7505', 'Kab. Gorontalo Utara', null, null, 'system', '1', '75');
INSERT INTO `1nt3x_kabupaten` VALUES ('7571', 'Kota Gorontalo', null, null, 'system', '1', '75');
INSERT INTO `1nt3x_kabupaten` VALUES ('7601', 'Kab. Majene', null, null, 'system', '1', '76');
INSERT INTO `1nt3x_kabupaten` VALUES ('7602', 'Kab. Polewali Mandar', null, null, 'system', '1', '76');
INSERT INTO `1nt3x_kabupaten` VALUES ('7603', 'Kab. Mamasa', null, null, 'system', '1', '76');
INSERT INTO `1nt3x_kabupaten` VALUES ('7604', 'Kab. Mamuju', null, null, 'system', '1', '76');
INSERT INTO `1nt3x_kabupaten` VALUES ('7605', 'Kab. Mamuju Utara', null, null, 'system', '1', '76');
INSERT INTO `1nt3x_kabupaten` VALUES ('8101', 'Kab. Maluku Tenggara Barat', null, null, 'system', '1', '81');
INSERT INTO `1nt3x_kabupaten` VALUES ('8102', 'Kab. Maluku Tenggara', null, null, 'system', '1', '81');
INSERT INTO `1nt3x_kabupaten` VALUES ('8103', 'Kab. Maluku Tengah', null, null, 'system', '1', '81');
INSERT INTO `1nt3x_kabupaten` VALUES ('8104', 'Kab. Buru', null, null, 'system', '1', '81');
INSERT INTO `1nt3x_kabupaten` VALUES ('8105', 'Kab. Kepulauan Aru', null, null, 'system', '1', '81');
INSERT INTO `1nt3x_kabupaten` VALUES ('8106', 'Kab. Seram Bagian Barat', null, null, 'system', '1', '81');
INSERT INTO `1nt3x_kabupaten` VALUES ('8107', 'Kab. Seram Bagian Timur', null, null, 'system', '1', '81');
INSERT INTO `1nt3x_kabupaten` VALUES ('8108', 'Kab. Maluku Barat Daya', null, null, 'system', '1', '81');
INSERT INTO `1nt3x_kabupaten` VALUES ('8109', 'Kab. Buru Selatan', null, null, 'system', '1', '81');
INSERT INTO `1nt3x_kabupaten` VALUES ('8171', 'Kota Ambon', null, null, 'system', '1', '81');
INSERT INTO `1nt3x_kabupaten` VALUES ('8172', 'Kota Tual', null, null, 'system', '1', '81');
INSERT INTO `1nt3x_kabupaten` VALUES ('8201', 'Kab. Halmahera Barat', null, null, 'system', '1', '82');
INSERT INTO `1nt3x_kabupaten` VALUES ('8202', 'Kab. Halmahera Tengah', null, null, 'system', '1', '82');
INSERT INTO `1nt3x_kabupaten` VALUES ('8203', 'Kab. Kepulauan Sula', null, null, 'system', '1', '82');
INSERT INTO `1nt3x_kabupaten` VALUES ('8204', 'Kab. Halmahera Selatan', null, null, 'system', '1', '82');
INSERT INTO `1nt3x_kabupaten` VALUES ('8205', 'Kab. Halmahera Utara', null, null, 'system', '1', '82');
INSERT INTO `1nt3x_kabupaten` VALUES ('8206', 'Kab. Halmahera Timur', null, null, 'system', '1', '82');
INSERT INTO `1nt3x_kabupaten` VALUES ('8207', 'Kab. Pulau Morotai', null, null, 'system', '1', '82');
INSERT INTO `1nt3x_kabupaten` VALUES ('8271', 'Kota Ternate', null, null, 'system', '1', '82');
INSERT INTO `1nt3x_kabupaten` VALUES ('8272', 'Kota Tidore Kepulauan', null, null, 'system', '1', '82');
INSERT INTO `1nt3x_kabupaten` VALUES ('9101', 'Kab. Fakfak', null, null, 'system', '1', '91');
INSERT INTO `1nt3x_kabupaten` VALUES ('9102', 'Kab. Kaimana', null, null, 'system', '1', '91');
INSERT INTO `1nt3x_kabupaten` VALUES ('9103', 'Kab. Teluk Wondama', null, null, 'system', '1', '91');
INSERT INTO `1nt3x_kabupaten` VALUES ('9104', 'Kab. Teluk Bintuni', null, null, 'system', '1', '91');
INSERT INTO `1nt3x_kabupaten` VALUES ('9105', 'Kab. Manokwari', null, null, 'system', '1', '91');
INSERT INTO `1nt3x_kabupaten` VALUES ('9106', 'Kab. Sorong Selatan', null, null, 'system', '1', '91');
INSERT INTO `1nt3x_kabupaten` VALUES ('9107', 'Kab. Sorong', null, null, 'system', '1', '91');
INSERT INTO `1nt3x_kabupaten` VALUES ('9108', 'Kab. Raja Ampat', null, null, 'system', '1', '91');
INSERT INTO `1nt3x_kabupaten` VALUES ('9109', 'Kab. Tambrauw', null, null, 'system', '1', '91');
INSERT INTO `1nt3x_kabupaten` VALUES ('9110', 'Kab. Maybrat', null, null, 'system', '1', '91');
INSERT INTO `1nt3x_kabupaten` VALUES ('9171', 'Kota Sorong', null, null, 'system', '1', '91');
INSERT INTO `1nt3x_kabupaten` VALUES ('9401', 'Kab. Merauke', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9402', 'Kab. Jayawijaya', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9403', 'Kab. Jayapura', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9404', 'Kab. Nabire', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9408', 'Kab. Kepulauan Yapen', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9409', 'Kab. Biak Numfor', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9410', 'Kab. Paniai', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9411', 'Kab. Puncak Jaya', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9412', 'Kab. Mimika', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9413', 'Kab. Boven Digoel', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9414', 'Kab. Mappi', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9415', 'Kab. Asmat', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9416', 'Kab. Yahukimo', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9417', 'Kab. Pegunungan Bintang', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9418', 'Kab. Tolikara', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9419', 'Kab. Sarmi', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9420', 'Kab. Keerom', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9426', 'Kab. Waropen', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9427', 'Kab. Supiori', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9428', 'Kab. Mamberamo Raya', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9429', 'Kab. Nduga', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9430', 'Kab. Lanny Jaya', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9431', 'Kab. Mamberamo Tengah', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9432', 'Kab. Yalimo', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9433', 'Kab. Puncak', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9434', 'Kab. Dogiyai', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9435', 'Kab. Intan Jaya', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9436', 'Kab. Deiyai', null, null, 'system', '1', '94');
INSERT INTO `1nt3x_kabupaten` VALUES ('9471', 'Kota Jayapura', null, null, 'system', '1', '94');

-- ----------------------------
-- Table structure for `1nt3x_log`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_log`;
CREATE TABLE `1nt3x_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log_user` varchar(50) DEFAULT NULL,
  `log_kegiatan` varchar(50) DEFAULT NULL,
  `log_waktu` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_log
-- ----------------------------
INSERT INTO `1nt3x_log` VALUES ('1', 'admin', 'Hapus data toko KR', '2016-06-27 00:37:18');
INSERT INTO `1nt3x_log` VALUES ('2', 'admin', 'Hapus data toko KP', '2016-06-27 00:37:22');
INSERT INTO `1nt3x_log` VALUES ('3', 'admin', 'Hapus data toko koa', '2016-06-27 00:37:25');
INSERT INTO `1nt3x_log` VALUES ('4', 'admin', 'Hapus data toko dsa', '2016-06-27 00:37:29');
INSERT INTO `1nt3x_log` VALUES ('5', 'admin', 'Rubah data toko ABC', '2016-06-27 00:40:27');
INSERT INTO `1nt3x_log` VALUES ('6', 'admin', 'Rubah data toko ABC', '2016-06-27 00:40:34');
INSERT INTO `1nt3x_log` VALUES ('7', 'admin', 'Rubah data toko ABC', '2016-06-27 00:40:45');

-- ----------------------------
-- Table structure for `1nt3x_menus`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_menus`;
CREATE TABLE `1nt3x_menus` (
  `menu_id` int(2) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(50) NOT NULL,
  `menu_url` text NOT NULL,
  `menu_parent` int(2) DEFAULT NULL,
  `menu_status` char(1) NOT NULL,
  `menu_position` int(2) NOT NULL,
  `menu_icon` varchar(25) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(25) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_menus
-- ----------------------------
INSERT INTO `1nt3x_menus` VALUES ('1', 'File', '#|file|1', '0', '1', '0', null, '2015-08-24 17:16:02', '2015-08-24 20:59:36', 'system');
INSERT INTO `1nt3x_menus` VALUES ('3', 'Exit', 'auth|logout|3', '1', '1', '2', null, '2015-08-24 17:40:02', '2015-08-24 20:18:56', 'system');
INSERT INTO `1nt3x_menus` VALUES ('4', 'Application Settings', '#|pengaturan|4', '0', '1', '1', null, '2015-08-25 09:22:56', '2015-09-22 02:36:23', 'system');
INSERT INTO `1nt3x_menus` VALUES ('5', 'Menu Management', 'pengaturan|menu|5', '4', '1', '4', null, '2015-08-25 09:23:45', '2015-08-24 19:23:48', 'system');
INSERT INTO `1nt3x_menus` VALUES ('6', 'Access Management', 'pengaturan|aturan|6', '4', '1', '5', null, '2015-08-25 09:24:08', '2015-08-24 19:24:17', 'system');
INSERT INTO `1nt3x_menus` VALUES ('7', 'User Management', 'pengaturan|pengguna|7', '4', '1', '6', null, '2015-08-25 09:24:36', '0000-00-00 00:00:00', 'system');
INSERT INTO `1nt3x_menus` VALUES ('8', 'Master', '#|master|8', '0', '1', '2', null, '2015-08-26 00:00:00', '2015-09-22 02:36:35', '');
INSERT INTO `1nt3x_menus` VALUES ('12', 'Application Profile', 'pengaturan|profil_aplikasi|12', '4', '1', '0', null, '2015-08-25 00:00:00', '2015-08-30 21:58:54', '');
INSERT INTO `1nt3x_menus` VALUES ('13', 'Wilayah', '#|wilayah|13', '8', '1', '0', null, '2015-08-31 00:00:00', '0000-00-00 00:00:00', '');
INSERT INTO `1nt3x_menus` VALUES ('14', 'Provinsi', 'master|provinsi|14', '13', '1', '0', null, '2015-08-31 00:00:00', '0000-00-00 00:00:00', '');
INSERT INTO `1nt3x_menus` VALUES ('15', 'Kabupaten / Kota', 'master|kabupaten|15', '13', '1', '1', null, '2015-08-31 00:00:00', '0000-00-00 00:00:00', '');
INSERT INTO `1nt3x_menus` VALUES ('18', 'Barang', '#|produk|18', '8', '1', '1', null, '2015-08-31 00:00:00', '2015-09-22 02:31:01', '');
INSERT INTO `1nt3x_menus` VALUES ('19', 'List Barang', 'master|list_item|19', '18', '1', '0', null, '2015-08-31 00:00:00', '2015-09-27 20:59:37', '');
INSERT INTO `1nt3x_menus` VALUES ('20', 'Type Barang', 'master|tipe|20', '18', '1', '1', null, '2015-08-31 00:00:00', '2015-09-22 22:11:27', '');
INSERT INTO `1nt3x_menus` VALUES ('21', 'Jenis Barang', 'master|jenis|21', '18', '1', '3', null, '2015-08-31 00:00:00', '2015-08-30 22:34:05', '');
INSERT INTO `1nt3x_menus` VALUES ('22', 'Kategori Barang', 'master|kategori|22', '18', '1', '4', null, '2015-08-31 00:00:00', '2015-09-22 02:34:22', '');
INSERT INTO `1nt3x_menus` VALUES ('23', 'Merek Barang', 'master|merek|23', '18', '1', '2', null, '2015-08-31 00:00:00', '2015-09-22 02:33:05', '');
INSERT INTO `1nt3x_menus` VALUES ('29', 'Satuan', 'master|satuan|29', '8', '1', '3', null, '2015-08-31 00:00:00', '2015-10-08 00:17:57', '');
INSERT INTO `1nt3x_menus` VALUES ('33', 'Toko', 'master|toko|33', '8', '1', '4', null, '2015-09-01 00:00:00', '2015-10-08 00:18:05', '');
INSERT INTO `1nt3x_menus` VALUES ('34', 'Rak', 'master|rak|34', '8', '1', '5', null, '2015-09-01 00:00:00', '2015-10-08 00:18:13', '');
INSERT INTO `1nt3x_menus` VALUES ('35', 'Penjualan', '#|penjualan|35', '0', '1', '3', null, '2015-09-22 00:00:00', '0000-00-00 00:00:00', '');
INSERT INTO `1nt3x_menus` VALUES ('36', 'Penjualan', 'penjualan|penjualan|36', '35', '1', '0', null, '2015-09-22 00:00:00', '0000-00-00 00:00:00', '');
INSERT INTO `1nt3x_menus` VALUES ('37', 'Laporan Penjualan', 'penjualan|laporan_penjualan|37', '35', '1', '1', null, '2015-09-22 00:00:00', '2015-09-29 02:52:28', '');
INSERT INTO `1nt3x_menus` VALUES ('38', 'Manajemen Harga', '#|manajemen_harga|38', '8', '1', '2', null, '2015-10-08 00:00:00', '2015-10-08 00:17:50', '');
INSERT INTO `1nt3x_menus` VALUES ('39', 'List Harga', 'master|list_harga|39', '38', '1', '0', null, '2015-10-08 00:00:00', '2015-10-08 00:37:06', '');
INSERT INTO `1nt3x_menus` VALUES ('40', 'Harga Promo', 'master|harga_promo|40', '38', '1', '1', null, '2015-10-08 00:00:00', '2015-10-08 00:37:15', '');
INSERT INTO `1nt3x_menus` VALUES ('41', 'Supplier', 'master|supplier|41', '8', '1', '6', null, '2015-10-09 00:00:00', '0000-00-00 00:00:00', '');
INSERT INTO `1nt3x_menus` VALUES ('42', 'Inventory', '#|inventory|42', '0', '1', '4', null, '2015-10-19 00:00:00', '0000-00-00 00:00:00', '');
INSERT INTO `1nt3x_menus` VALUES ('43', 'Persediaan Awal', 'inventory|persediaan_awal|43', '42', '1', '0', null, '2015-10-19 00:00:00', '0000-00-00 00:00:00', '');
INSERT INTO `1nt3x_menus` VALUES ('44', 'Purchase Order', 'inventory|purchase_order|44', '42', '1', '1', null, '2015-10-19 00:00:00', '0000-00-00 00:00:00', '');
INSERT INTO `1nt3x_menus` VALUES ('45', 'Transfer Barang', 'inventory|transfer_barang|45', '42', '1', '4', null, '2015-10-19 00:00:00', '2015-11-08 22:40:48', '');
INSERT INTO `1nt3x_menus` VALUES ('46', 'Penerimaan Barang', 'inventory|penerimaan_barang|46', '42', '1', '2', null, '2015-10-19 00:00:00', '2015-10-27 20:31:40', '');
INSERT INTO `1nt3x_menus` VALUES ('47', 'Stok Opname', 'inventory|stok_opname|47', '42', '1', '5', null, '2015-10-19 00:00:00', '2015-11-08 22:41:02', '');
INSERT INTO `1nt3x_menus` VALUES ('48', 'Adjustment Stok', 'inventory|adjustment_stok|48', '42', '1', '6', null, '2015-10-19 00:00:00', '2015-11-08 22:41:13', '');
INSERT INTO `1nt3x_menus` VALUES ('49', 'Stok Mutasi', 'inventory|stok_mutasi|49', '42', '1', '7', null, '2015-10-19 00:00:00', '2015-11-08 22:41:24', '');
INSERT INTO `1nt3x_menus` VALUES ('50', 'Minimum Stok', 'master|minimum_stok|50', '18', '1', '5', null, '2015-11-02 00:00:00', '2015-11-02 01:23:58', '');

-- ----------------------------
-- Table structure for `1nt3x_menu_groups`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_menu_groups`;
CREATE TABLE `1nt3x_menu_groups` (
  `group_id` int(2) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) DEFAULT NULL,
  `group_desc` text,
  `created_at` datetime DEFAULT NULL,
  `modified_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_menu_groups
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_menu_scut`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_menu_scut`;
CREATE TABLE `1nt3x_menu_scut` (
  `menu_id` int(2) DEFAULT NULL,
  `role_id` int(2) DEFAULT NULL,
  `user_id` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_menu_scut
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_m_bank`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_m_bank`;
CREATE TABLE `1nt3x_m_bank` (
  `m_bank_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_bank_nama` varchar(75) DEFAULT NULL,
  `m_bank_ket` text,
  `m_bank_tgl_buat` date DEFAULT NULL,
  `m_bank_tgl_ubah` date DEFAULT NULL,
  `m_bank_petugas` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`m_bank_id`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_m_bank
-- ----------------------------
INSERT INTO `1nt3x_m_bank` VALUES ('1', 'Bank Negara Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('2', 'Bank Rakyat Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('3', 'Bank Tabungan Negara', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('4', 'Bank Mandiri', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('5', 'Bank Mutiara', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('6', 'Bank Agroniaga', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('7', 'Bank Anda', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('8', 'Bank Artha Graha Internatiional', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('9', 'Bank Bukopin', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('10', 'Bank Bumi Arta', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('11', 'Bank Capital Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('12', 'Bank Central Asia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('13', 'Bank CIMB Niaga', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('14', 'Bank Danamon', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('15', 'Bank Ekonomi Raharja', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('16', 'Bank Ganesha', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('17', 'Bank Hana', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('18', 'Bank Himpunan Saudara 1906', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('19', 'Bank ICB Bumiputera', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('20', 'Bank ICBC Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('21', 'Bank Index Selindo', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('22', 'Bank Internasional Maybank', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('23', 'Bank Kesawan', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('24', 'Bank Maspion', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('25', 'Bank Mayapada', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('26', 'Bank Mega', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('27', 'Bank Mestika Dharma', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('28', 'Bank Metro Express', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('29', 'Bank Muamalat indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('30', 'Bank Nusantara Parahyangan', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('31', 'Bank OCBC NISP', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('32', 'Bank Permata', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('33', 'Bank SBI Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('34', 'Bank Sinarmas', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('35', 'Bank Swadesi', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('36', 'Bank Syariah Mandiri', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('37', 'Bank Syariah Mega Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('38', 'Bank Victoria Internasional', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('39', 'Bank Pan Indonesia Bank', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('40', 'Bank Anglomas Internasional Bank', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('41', 'Bank Andara', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('42', 'Bank Artos', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('43', 'Bank Barclays Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('44', 'Bank Bisnis Internasional', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('45', 'Bank BRI Syariah', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('46', 'Bank Central Asia Syariah', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('47', 'Bank Dipo Internasional', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('48', 'Bank Fama Internasional', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('49', 'Bank Ina Perdana', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('50', 'Bank Jasa Jakarta', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('51', 'Bank Kesehatan Ekonomi', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('52', 'Bank Liman Internasional', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('53', 'Bank Mayora', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('54', 'Bank ANZ Panin Bank', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('55', 'Bank Agris', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('56', 'Bank Commonwealth', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('57', 'Bank BNP Paribas Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('58', 'Bank DBS Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('59', 'Bank KEB Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('60', 'Bank Maybank Syariah Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('61', 'Bank Mizuho Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('62', 'Bank UOB Buana', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('63', 'Bank Rabobank Internasional Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('64', 'Bank Resona Perdania', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('65', 'Bank Windu Kentjana Internasional', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('66', 'Bank Woori Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('67', 'Bank Sumitomo Mitsui Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('68', 'Bank UFJ Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('69', 'Bank Mitraniaga', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('70', 'Bank Multi Arta Sentosa', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('71', 'Bank Nationalnobu', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('72', 'Bank Pundi Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('73', 'Bank Purba Danarta', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('74', 'Bank Royal Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('75', 'Bank Sinar Harapan Bali', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('76', 'Bank STMIK Binamulia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('77', 'Bank Syariah Bukopin', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('78', 'Bank Tabungan Pensiunan Nasional', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('79', 'Bank Victoria Syariah', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('80', 'Bank Yudha Bhakti', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('81', 'Bank Centratama Nasional Bank', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('82', 'Bank Pan Indonesia Bank Syariah', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('83', 'Bank Prima Master Bank', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('84', 'Bank Jambi', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('85', 'Bank Kalsel', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('86', 'Bank Kaltim', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('87', 'Bank Sultr', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('88', 'Bank BPD DIY', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('89', 'Bank Nagar', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('90', 'Bank DKI', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('91', 'Bank Lampung', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('92', 'Bank Kalteng', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('93', 'Bank BPD Aceh', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('94', 'Bank Sulse', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('95', 'Bank BJB', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('96', 'Bank Kalbar', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('97', 'Bank Maluku', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('98', 'Bank Bengkulu', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('99', 'Bank Jateng', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('100', 'Bank Jatim', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('101', 'Bank NTB', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('102', 'Bank NTT', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('103', 'Bank Sulteng', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('104', 'Bank Sulut', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('105', 'Bank BPD Bali', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('106', 'Bank Papua', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('107', 'Bank Riau Kepri', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('108', 'Bank Sumsel Babel', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('109', 'Bank Sumut', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('110', 'Bank BNI Syariah', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('111', 'Bank BRI Syariah', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('112', 'Bank Maybank Syariah Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('113', 'Bank Mega Syariah Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('114', 'Bank Muamalat Indonesia', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('115', 'Bank Syariah Bukopin', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('116', 'Bank Syariah Mandiri', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('117', 'Bank Victoria Syariah', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('118', 'Bank Pan Indonesia Bank Syariah', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('119', 'Bank CIMB Niaga Syariah', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('120', 'Bank OCBC NISP Syariah', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('121', 'Bank Danamon Syariah', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('122', 'Bank Riau Kepri Syariah', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('123', 'Bank BCA Syariah', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('124', 'Bank BJB Syariah', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');
INSERT INTO `1nt3x_m_bank` VALUES ('125', 'Bank Permata Syariah', 'Salah satu bank di indonesia', '2015-09-21', '2015-09-21', 'Admin');

-- ----------------------------
-- Table structure for `1nt3x_m_harga`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_m_harga`;
CREATE TABLE `1nt3x_m_harga` (
  `m_harga_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_harga_beli` double NOT NULL,
  `m_harga_jual` double NOT NULL,
  `m_harga_tgl_buat` date NOT NULL,
  `m_harga_tgl_ubah` date NOT NULL,
  `m_harga_petugas` varchar(50) NOT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`m_harga_id`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_m_harga_ibfk_1` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_m_harga
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_m_harga_promo`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_m_harga_promo`;
CREATE TABLE `1nt3x_m_harga_promo` (
  `m_harga_promo_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_harga_promo_harga` double NOT NULL,
  `m_harga_promo_periode_mulai` date NOT NULL,
  `m_harga_promo_periode_akhir` date NOT NULL,
  `m_harga_promo_qty_promo` int(11) NOT NULL,
  `m_harga_promo_tgl_buat` date NOT NULL,
  `m_harga_promo_tgl_ubah` date NOT NULL,
  `m_harga_promo_petugas` varchar(50) NOT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`m_harga_promo_id`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_m_harga_promo_ibfk_1` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_m_harga_promo
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_m_harga_promo_detail`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_m_harga_promo_detail`;
CREATE TABLE `1nt3x_m_harga_promo_detail` (
  `m_harga_promo_id` int(11) DEFAULT '0',
  `m_toko_kode` varchar(15) DEFAULT NULL,
  KEY `m_toko_kode` (`m_toko_kode`),
  KEY `m_harga_promo_id` (`m_harga_promo_id`),
  CONSTRAINT `1nt3x_m_harga_promo_detail_ibfk_2` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_harga_promo_detail_ibfk_3` FOREIGN KEY (`m_harga_promo_id`) REFERENCES `1nt3x_m_harga_promo` (`m_harga_promo_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_m_harga_promo_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_m_item`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_m_item`;
CREATE TABLE `1nt3x_m_item` (
  `m_item_kode` varchar(35) NOT NULL DEFAULT '0',
  `m_item_barcode` varchar(35) DEFAULT NULL,
  `m_item_nama` varchar(35) NOT NULL,
  `m_item_ket` text,
  `m_item_gambar` varchar(35) DEFAULT NULL,
  `m_item_tgl_buat` date NOT NULL,
  `m_item_tgl_ubah` date NOT NULL,
  `m_item_petugas` varchar(50) NOT NULL,
  `m_item_merk_kode` varchar(15) DEFAULT NULL,
  `m_item_tipe_kode` varchar(15) DEFAULT NULL,
  `m_item_jenis_kode` varchar(15) DEFAULT NULL,
  `m_item_kategori_kode` varchar(15) DEFAULT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  `m_rak_kode` varchar(15) DEFAULT NULL,
  `m_satuan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`m_item_kode`),
  KEY `m_item_merk_kode` (`m_item_merk_kode`),
  KEY `m_item_jenis_kode` (`m_item_jenis_kode`),
  KEY `m_item_kategori_kode` (`m_item_kategori_kode`),
  KEY `m_toko_kode` (`m_toko_kode`),
  KEY `m_rak_kode` (`m_rak_kode`),
  KEY `m_satuan_id` (`m_satuan_id`),
  CONSTRAINT `1nt3x_m_item_ibfk_1` FOREIGN KEY (`m_item_merk_kode`) REFERENCES `1nt3x_m_item_merk` (`m_item_merk_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_item_ibfk_2` FOREIGN KEY (`m_item_jenis_kode`) REFERENCES `1nt3x_m_item_jenis` (`m_item_jenis_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_item_ibfk_3` FOREIGN KEY (`m_item_kategori_kode`) REFERENCES `1nt3x_m_item_kategori` (`m_item_kategori_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_item_ibfk_4` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_item_ibfk_5` FOREIGN KEY (`m_rak_kode`) REFERENCES `1nt3x_m_rak` (`m_rak_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_item_ibfk_6` FOREIGN KEY (`m_satuan_id`) REFERENCES `1nt3x_m_satuan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_m_item
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_m_item_jenis`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_m_item_jenis`;
CREATE TABLE `1nt3x_m_item_jenis` (
  `m_item_jenis_kode` varchar(15) NOT NULL DEFAULT '0',
  `m_item_jenis_nama` varchar(35) DEFAULT NULL,
  `m_item_jenis_ket` text,
  `m_item_jenis_tgl_buat` date DEFAULT NULL,
  `m_item_jenis_tgl_ubah` date DEFAULT NULL,
  `m_item_jenis_petugas` varchar(50) DEFAULT NULL,
  `m_item_kategori_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`m_item_jenis_kode`),
  KEY `m_item_kategori_kode` (`m_item_kategori_kode`),
  CONSTRAINT `1nt3x_m_item_jenis_ibfk_1` FOREIGN KEY (`m_item_kategori_kode`) REFERENCES `1nt3x_m_item_kategori` (`m_item_kategori_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_m_item_jenis
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_m_item_kategori`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_m_item_kategori`;
CREATE TABLE `1nt3x_m_item_kategori` (
  `m_item_kategori_kode` varchar(15) NOT NULL DEFAULT '0',
  `m_item_kategori_nama` varchar(35) DEFAULT NULL,
  `m_item_kategori_ket` text,
  `m_item_kategori_tgl_buat` date DEFAULT NULL,
  `m_item_kategori_tgl_ubah` date DEFAULT NULL,
  `m_item_kategori_petugas` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`m_item_kategori_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_m_item_kategori
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_m_item_merk`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_m_item_merk`;
CREATE TABLE `1nt3x_m_item_merk` (
  `m_item_merk_kode` varchar(15) NOT NULL DEFAULT '0',
  `m_item_merk_nama` varchar(35) DEFAULT NULL,
  `m_item_merk_ket` text,
  `m_item_merk_tgl_buat` date DEFAULT NULL,
  `m_item_merk_tgl_ubah` date DEFAULT NULL,
  `m_item_merk_petugas` varchar(50) DEFAULT NULL,
  `m_item_jenis_kode` varchar(15) DEFAULT NULL,
  `m_item_kategori_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`m_item_merk_kode`),
  KEY `m_item_jenis_kode` (`m_item_jenis_kode`),
  KEY `m_item_kategori_kode` (`m_item_kategori_kode`),
  CONSTRAINT `1nt3x_m_item_merk_ibfk_1` FOREIGN KEY (`m_item_jenis_kode`) REFERENCES `1nt3x_m_item_jenis` (`m_item_jenis_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_item_merk_ibfk_2` FOREIGN KEY (`m_item_kategori_kode`) REFERENCES `1nt3x_m_item_kategori` (`m_item_kategori_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_m_item_merk
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_m_item_tipe`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_m_item_tipe`;
CREATE TABLE `1nt3x_m_item_tipe` (
  `m_item_tipe_kode` varchar(15) NOT NULL,
  `m_item_tipe_nama` varchar(35) DEFAULT NULL,
  `m_item_tipe_ket` text,
  `m_item_tipe_tgl_buat` date DEFAULT NULL,
  `m_item_tipe_tgl_ubah` date DEFAULT NULL,
  `m_item_tipe_petugas` varchar(50) DEFAULT NULL,
  `m_item_kategori_kode` varchar(15) DEFAULT NULL,
  `m_item_jenis_kode` varchar(15) DEFAULT NULL,
  `m_item_merk_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`m_item_tipe_kode`),
  KEY `m_item_kategori_kode` (`m_item_kategori_kode`),
  KEY `m_item_jenis_kode` (`m_item_jenis_kode`),
  KEY `m_item_merk_kode` (`m_item_merk_kode`),
  CONSTRAINT `1nt3x_m_item_tipe_ibfk_1` FOREIGN KEY (`m_item_kategori_kode`) REFERENCES `1nt3x_m_item_kategori` (`m_item_kategori_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_item_tipe_ibfk_2` FOREIGN KEY (`m_item_jenis_kode`) REFERENCES `1nt3x_m_item_jenis` (`m_item_jenis_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_item_tipe_ibfk_3` FOREIGN KEY (`m_item_merk_kode`) REFERENCES `1nt3x_m_item_merk` (`m_item_merk_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_m_item_tipe
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_m_rak`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_m_rak`;
CREATE TABLE `1nt3x_m_rak` (
  `m_rak_kode` varchar(15) NOT NULL DEFAULT '0',
  `m_rak_nama` varchar(35) DEFAULT NULL,
  `m_rak_ket` text,
  `m_rak_tgl_buat` date DEFAULT NULL,
  `m_rak_tgl_ubah` date DEFAULT NULL,
  `m_rak_petugas` varchar(50) DEFAULT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`m_rak_kode`),
  KEY `m_toko_kode` (`m_toko_kode`),
  CONSTRAINT `1nt3x_m_rak_ibfk_1` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_m_rak
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_m_rak_detail`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_m_rak_detail`;
CREATE TABLE `1nt3x_m_rak_detail` (
  `m_rak_kode` varchar(15) DEFAULT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_m_rak_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_m_satuan`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_m_satuan`;
CREATE TABLE `1nt3x_m_satuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_satuan` varchar(4) DEFAULT NULL,
  `nama_satuan` varchar(20) DEFAULT NULL,
  `aktif` varchar(1) DEFAULT '1' COMMENT '1 : Aktif | 0 : Tidak Aktif',
  `created` datetime DEFAULT NULL,
  `created_by` varchar(25) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_kode_satuan` (`kode_satuan`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_m_satuan
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_m_stok_min`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_m_stok_min`;
CREATE TABLE `1nt3x_m_stok_min` (
  `m_stok_min` int(11) DEFAULT NULL,
  `m_item_kode` varchar(35) DEFAULT NULL,
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_m_stok_min_ibfk_1` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_m_stok_min
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_m_suplier`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_m_suplier`;
CREATE TABLE `1nt3x_m_suplier` (
  `m_suplier_kode` varchar(15) NOT NULL DEFAULT '',
  `m_suplier_nama` varchar(50) DEFAULT NULL,
  `m_suplier_alamat` text,
  `m_suplier_no_tlp` varchar(15) DEFAULT NULL,
  `m_suplier_tgl_buat` date DEFAULT NULL,
  `m_suplier_tgl_ubah` date DEFAULT NULL,
  `m_suplier_petugas` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`m_suplier_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_m_suplier
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_m_toko`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_m_toko`;
CREATE TABLE `1nt3x_m_toko` (
  `m_toko_kode` varchar(15) NOT NULL DEFAULT '0',
  `m_toko_nama` varchar(35) NOT NULL,
  `m_toko_ket` text,
  `m_toko_no_tlp` varchar(20) NOT NULL,
  `m_toko_tgl_buat` date NOT NULL,
  `m_toko_tgl_ubah` date NOT NULL,
  `m_toko_petugas` varchar(50) NOT NULL,
  PRIMARY KEY (`m_toko_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_m_toko
-- ----------------------------
INSERT INTO `1nt3x_m_toko` VALUES ('ABC', 'CARDINAL CIMAHI', 'Di Daerah Cimahi', '', '2015-09-23', '2016-06-27', 'admin');

-- ----------------------------
-- Table structure for `1nt3x_profile`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_profile`;
CREATE TABLE `1nt3x_profile` (
  `profile_id` int(1) NOT NULL AUTO_INCREMENT,
  `profile_name` varchar(50) NOT NULL,
  `profile_npwp` varchar(25) DEFAULT NULL,
  `profile_phone` varchar(15) DEFAULT NULL,
  `profile_fax` varchar(15) DEFAULT NULL,
  `profile_email` varchar(50) DEFAULT NULL,
  `profile_address` text,
  `profile_logo` varchar(50) DEFAULT NULL,
  `profile_app_name` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(25) NOT NULL,
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_profile
-- ----------------------------
INSERT INTO `1nt3x_profile` VALUES ('1', 'Cardinal', '12321321', '021 7169 9550', '021 8430 7221', 'info@cardinal.co.id', 'Bandung', 'image_b9os9_20160606022253.png', 'Aplikasi Monitoring Stok', '2015-08-31 12:03:35', '2016-06-06 00:23:37', '');

-- ----------------------------
-- Table structure for `1nt3x_provinsi`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_provinsi`;
CREATE TABLE `1nt3x_provinsi` (
  `provinsi_kode` varchar(11) NOT NULL,
  `provinsi_nama` varchar(50) DEFAULT NULL,
  `provinsi_tgl_buat` date DEFAULT NULL,
  `provinsi_tgl_ubah` date DEFAULT NULL,
  `provinsi_petugas` varchar(50) DEFAULT NULL,
  `provinsi_status` enum('1','0') DEFAULT NULL,
  PRIMARY KEY (`provinsi_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_provinsi
-- ----------------------------
INSERT INTO `1nt3x_provinsi` VALUES ('11', 'Aceh', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('12', 'Sumatera Utara', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('13', 'Sumatera Barat', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('14', 'Riau', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('15', 'Jambi', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('16', 'Sumatera Selatan', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('17', 'Bengkulu', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('18', 'Lampung', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('19', 'Kepulauan Bangka Belitung', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('21', 'Kepulauan Riau', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('31', 'Dki Jakarta', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('32', 'Jawa Barat', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('33', 'Jawa Tengah', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('34', 'Di Yogyakarta', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('35', 'Jawa Timur', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('36', 'Banten', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('51', 'Bali', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('52', 'Nusa Tenggara Barat', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('53', 'Nusa Tenggara Timur', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('61', 'Kalimantan Barat', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('62', 'Kalimantan Tengah', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('63', 'Kalimantan Selatan', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('64', 'Kalimantan Timur', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('65', 'Kalimantan Utara', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('71', 'Sulawesi Utara', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('72', 'Sulawesi Tengah', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('73', 'Sulawesi Selatan', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('74', 'Sulawesi Tenggara', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('75', 'Gorontalo', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('76', 'Sulawesi Barat', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('81', 'Maluku', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('82', 'Maluku Utara', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('91', 'Papua Barat', '0000-00-00', null, 'sytem', '1');
INSERT INTO `1nt3x_provinsi` VALUES ('94', 'Papua', '0000-00-00', null, 'sytem', '1');

-- ----------------------------
-- Table structure for `1nt3x_roles`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_roles`;
CREATE TABLE `1nt3x_roles` (
  `role_id` int(2) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `role_desc` text,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(25) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_roles
-- ----------------------------
INSERT INTO `1nt3x_roles` VALUES ('1', 'admin', 'admin', '2015-08-24 17:03:53', '2015-08-24 20:41:13', 'admin');
INSERT INTO `1nt3x_roles` VALUES ('2', 'kasir', 'kasir', '0000-00-00 00:00:00', '2015-10-21 02:26:56', '');

-- ----------------------------
-- Table structure for `1nt3x_roles_access`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_roles_access`;
CREATE TABLE `1nt3x_roles_access` (
  `role_access_view` enum('0','1') NOT NULL DEFAULT '0',
  `role_access_add` enum('0','1') NOT NULL DEFAULT '0',
  `role_access_edit` enum('0','1') NOT NULL DEFAULT '0',
  `role_access_delete` enum('0','1') NOT NULL DEFAULT '0',
  `role_access_export` enum('0','1') NOT NULL DEFAULT '0',
  `role_access_import` enum('0','1') NOT NULL DEFAULT '0',
  `role_id` int(2) NOT NULL,
  `menu_id` int(2) NOT NULL,
  KEY `role_id` (`role_id`),
  KEY `menu_id` (`menu_id`),
  CONSTRAINT `1nt3x_roles_access_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `1nt3x_roles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_roles_access_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `1nt3x_menus` (`menu_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_roles_access
-- ----------------------------
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '1');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '3');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '4');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '12');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '5');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '6');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '7');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '8');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '1', '13');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '1', '14');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '1', '15');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '18');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '19');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '20');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '23');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '21');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '22');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '50');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '38');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '39');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '40');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '29');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '33');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '34');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '41');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '35');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '36');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '37');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '42');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '43');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '44');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '46');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '45');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '47');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '48');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '1', '49');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '2', '1');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '2', '3');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '4');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '12');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '5');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '6');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '7');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '8');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '13');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '14');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '15');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '18');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '19');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '20');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '23');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '21');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '22');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '50');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '38');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '39');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '40');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '29');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '33');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '34');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '41');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '2', '35');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '2', '36');
INSERT INTO `1nt3x_roles_access` VALUES ('1', '1', '1', '1', '1', '1', '2', '37');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '42');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '43');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '44');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '46');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '45');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '47');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '48');
INSERT INTO `1nt3x_roles_access` VALUES ('0', '0', '0', '0', '0', '0', '2', '49');

-- ----------------------------
-- Table structure for `1nt3x_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_sessions`;
CREATE TABLE `1nt3x_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_sessions
-- ----------------------------
INSERT INTO `1nt3x_sessions` VALUES ('b76e06102c3e09486dfa175f015c518a', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0', '1466961425', 'a:2:{s:9:\"user_data\";s:0:\"\";s:32:\"edbe3f7eac75e8303bc87d28d5ff1436\";a:18:{s:7:\"user_id\";s:1:\"3\";s:13:\"user_fullname\";s:19:\"Intex Administrator\";s:13:\"user_username\";s:5:\"admin\";s:13:\"user_password\";s:32:\"21232f297a57a5a743894a0e4a801fc3\";s:7:\"user_jk\";s:0:\"\";s:12:\"user_tgl_lhr\";N;s:11:\"user_alamat\";N;s:10:\"user_email\";s:17:\"admin@intex.co.id\";s:8:\"user_tlp\";s:11:\"02171699550\";s:15:\"user_wrong_pass\";s:1:\"0\";s:11:\"user_status\";s:1:\"1\";s:14:\"role_access_id\";N;s:16:\"role_access_nama\";N;s:15:\"role_access_ket\";N;s:7:\"role_id\";s:1:\"1\";s:9:\"role_name\";s:5:\"admin\";s:9:\"toko_kode\";s:3:\"ABC\";s:13:\"otoritas_menu\";a:36:{i:1;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:1:\"1\";}i:3;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:1:\"3\";}i:4;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:1:\"4\";}i:12;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"12\";}i:5;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:1:\"5\";}i:6;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:1:\"6\";}i:7;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:1:\"7\";}i:8;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:1:\"8\";}i:13;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"13\";}i:14;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"14\";}i:15;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"15\";}i:18;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"18\";}i:19;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"19\";}i:20;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"20\";}i:23;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"23\";}i:21;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"21\";}i:22;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"22\";}i:50;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"50\";}i:38;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"38\";}i:39;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"39\";}i:40;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"40\";}i:29;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"29\";}i:33;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"33\";}i:34;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"34\";}i:41;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"41\";}i:35;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"35\";}i:36;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"36\";}i:37;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"37\";}i:42;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"42\";}i:43;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"43\";}i:44;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"44\";}i:46;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"46\";}i:45;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"45\";}i:47;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"47\";}i:48;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"48\";}i:49;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"49\";}}}}');

-- ----------------------------
-- Table structure for `1nt3x_tmp_trx`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_tmp_trx`;
CREATE TABLE `1nt3x_tmp_trx` (
  `id_tmp` int(11) NOT NULL AUTO_INCREMENT,
  `no_tmp` varchar(30) DEFAULT NULL,
  `kd_item` varchar(30) DEFAULT NULL,
  `nm_item` varchar(100) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `sub_total` double DEFAULT NULL,
  `sales` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_tmp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_tmp_trx
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_trx_pos`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_trx_pos`;
CREATE TABLE `1nt3x_trx_pos` (
  `trx_pos_no` varchar(25) NOT NULL DEFAULT '0',
  `trx_pos_tgl` datetime DEFAULT NULL,
  `trx_pos_ket` text,
  `trx_pos_sub_total` double DEFAULT NULL,
  `trx_pos_diskon` int(11) DEFAULT NULL,
  `trx_pos_tax` int(11) DEFAULT NULL,
  `trx_pos_grand_total` double DEFAULT NULL,
  `trx_pos_kembalian` double DEFAULT NULL,
  `trx_pos_tgl_buat` date DEFAULT NULL,
  `trx_pos_tgl_ubah` date DEFAULT NULL,
  `trx_pos_petugas` varchar(50) DEFAULT NULL,
  `toko_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`trx_pos_no`),
  KEY `toko_kode` (`toko_kode`),
  CONSTRAINT `1nt3x_trx_pos_ibfk_2` FOREIGN KEY (`toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_trx_pos
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_trx_pos_detail`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_trx_pos_detail`;
CREATE TABLE `1nt3x_trx_pos_detail` (
  `trx_pos_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `trx_pos_detail_qty` int(11) DEFAULT NULL,
  `trx_pos_detail_harga` double DEFAULT NULL,
  `trx_pos_detail_disc` int(4) DEFAULT NULL,
  `trx_pos_detail_total` double DEFAULT NULL,
  `item_kode` varchar(35) DEFAULT NULL,
  `trx_pos_no` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`trx_pos_detail_id`),
  KEY `trx_pos_no` (`trx_pos_no`),
  KEY `item_kode` (`item_kode`),
  CONSTRAINT `1nt3x_trx_pos_detail_ibfk_1` FOREIGN KEY (`trx_pos_no`) REFERENCES `1nt3x_trx_pos` (`trx_pos_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_trx_pos_detail_ibfk_2` FOREIGN KEY (`item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_trx_pos_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_trx_pos_jns_pem`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_trx_pos_jns_pem`;
CREATE TABLE `1nt3x_trx_pos_jns_pem` (
  `trx_pos_jns_pem_id` int(11) NOT NULL AUTO_INCREMENT,
  `trx_pos_jns_pem_status` enum('credit','debit','cash') DEFAULT NULL,
  `trx_pos_jns_pem_no_kartu` varchar(20) DEFAULT NULL,
  `trx_pos_jns_pem_no_ref` varchar(30) DEFAULT NULL,
  `trx_pos_jns_pem_bayar` double DEFAULT NULL,
  `trx_pos_no` varchar(25) DEFAULT NULL,
  `m_bank_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`trx_pos_jns_pem_id`),
  KEY `trx_pos_no` (`trx_pos_no`),
  KEY `m_bank_id` (`m_bank_id`),
  CONSTRAINT `1nt3x_trx_pos_jns_pem_ibfk_1` FOREIGN KEY (`trx_pos_no`) REFERENCES `1nt3x_trx_pos` (`trx_pos_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_trx_pos_jns_pem_ibfk_2` FOREIGN KEY (`m_bank_id`) REFERENCES `1nt3x_m_bank` (`m_bank_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_trx_pos_jns_pem
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_trx_tmp_pos`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_trx_tmp_pos`;
CREATE TABLE `1nt3x_trx_tmp_pos` (
  `trx_tmp_pos_tgl` date NOT NULL,
  `trx_tmp_pos_item_kode` varchar(35) NOT NULL,
  `trx_tmp_pos_qty` int(11) NOT NULL,
  `trx_tmp_pos_harga` double NOT NULL,
  `toko_kode` varchar(15) NOT NULL,
  `trx_tmp_pos_petugas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_trx_tmp_pos
-- ----------------------------

-- ----------------------------
-- Table structure for `1nt3x_users`
-- ----------------------------
DROP TABLE IF EXISTS `1nt3x_users`;
CREATE TABLE `1nt3x_users` (
  `user_id` int(3) NOT NULL AUTO_INCREMENT,
  `user_fullname` varchar(50) NOT NULL,
  `user_username` varchar(25) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_password_length` int(11) NOT NULL DEFAULT '0',
  `user_jk` char(1) NOT NULL COMMENT 'Jenis Kelamin (L/K)',
  `user_email` varchar(255) DEFAULT NULL,
  `user_tlp` varchar(15) DEFAULT NULL,
  `user_wrong_pass` int(3) NOT NULL DEFAULT '0',
  `user_status` char(1) NOT NULL COMMENT '1/0',
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(25) NOT NULL,
  `role_id` int(2) NOT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `role_id` (`role_id`),
  KEY `m_toko_kode` (`m_toko_kode`),
  CONSTRAINT `1nt3x_users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `1nt3x_roles` (`role_id`) ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_users_ibfk_2` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1nt3x_users
-- ----------------------------
INSERT INTO `1nt3x_users` VALUES ('3', 'Administrator Cardinal', 'admin', '21232f297a57a5a743894a0e4a801fc3', '5', '', 'admin@cardinal.co.id', '02171699550', '0', '1', '0000-00-00 00:00:00', '2016-06-26 10:40:16', 'Intex Administrator', '1', 'ABC');

-- ----------------------------
-- Table structure for `format penomoran`
-- ----------------------------
DROP TABLE IF EXISTS `format penomoran`;
CREATE TABLE `format penomoran` (
  `nama proses` varchar(50) DEFAULT NULL,
  `format no` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of format penomoran
-- ----------------------------
INSERT INTO `format penomoran` VALUES ('adjusment stok', 'AS/bulantahun/auto  increment(5 digit)');
INSERT INTO `format penomoran` VALUES ('penerimaan barang', 'PB/bulantahun/auto increment(5 digit)');
INSERT INTO `format penomoran` VALUES ('persediaan awal', 'PA/bulantahun/auto increment(5 digit)');
INSERT INTO `format penomoran` VALUES ('purchase order', 'PO/bulantahun/auto increment(5 digit)');
INSERT INTO `format penomoran` VALUES ('return pembelian', 'RP/nopengiriman/bulantahun/auto increment(5 digit)');
INSERT INTO `format penomoran` VALUES ('stok opname', 'SO/bulantahun/auto increment(5 digit)');
INSERT INTO `format penomoran` VALUES ('transfer barang', 'TF/kodetokoasal/kodetokotujuan/bulantahun/auto inc');
INSERT INTO `format penomoran` VALUES ('kode toko', '2digitlokasi+auto increment(3digit)');
INSERT INTO `format penomoran` VALUES ('suplier', '2digitnamaperusahaan+auto increment(3digit)');
INSERT INTO `format penomoran` VALUES ('transaksi pos', 'kodetoko/bulantahun/no auto(5 digit)');

-- ----------------------------
-- View structure for `1nt3x_vbarang`
-- ----------------------------
DROP VIEW IF EXISTS `1nt3x_vbarang`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `1nt3x_vbarang` AS select `mi`.`m_item_kode` AS `m_item_kode`,`mi`.`m_item_barcode` AS `m_item_barcode`,`mi`.`m_item_nama` AS `m_item_nama`,`mi`.`m_item_kategori_kode` AS `m_item_kategori_kode`,`mi`.`m_item_ket` AS `m_item_ket`,`mi`.`m_item_gambar` AS `m_item_gambar`,`mh`.`m_harga_jual` AS `m_harga_jual` from (`1nt3x_m_item` `mi` join `1nt3x_m_harga` `mh` on((`mh`.`m_item_kode` = `mi`.`m_item_kode`)));

-- ----------------------------
-- View structure for `1nt3x_vharga_promo`
-- ----------------------------
DROP VIEW IF EXISTS `1nt3x_vharga_promo`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `1nt3x_vharga_promo` AS select `mhp`.`m_harga_promo_id` AS `m_harga_promo_id`,`mhp`.`m_harga_promo_harga` AS `m_harga_promo_harga`,`mhp`.`m_harga_promo_periode_mulai` AS `m_harga_promo_periode_mulai`,`mhp`.`m_harga_promo_periode_akhir` AS `m_harga_promo_periode_akhir`,`mhp`.`m_harga_promo_qty_promo` AS `m_harga_promo_qty_promo`,`mhp`.`m_harga_promo_tgl_buat` AS `m_harga_promo_tgl_buat`,`mhp`.`m_harga_promo_tgl_ubah` AS `m_harga_promo_tgl_ubah`,`mhp`.`m_harga_promo_petugas` AS `m_harga_promo_petugas`,`mhp`.`m_item_kode` AS `m_item_kode`,`mhpd`.`m_toko_kode` AS `m_toko_kode` from (`1nt3x_m_harga_promo` `mhp` join `1nt3x_m_harga_promo_detail` `mhpd` on((`mhpd`.`m_harga_promo_id` = `mhp`.`m_harga_promo_id`)));

-- ----------------------------
-- View structure for `1nt3x_vinfostok_blnini`
-- ----------------------------
DROP VIEW IF EXISTS `1nt3x_vinfostok_blnini`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `1nt3x_vinfostok_blnini` AS select `vbn`.`m_item_kode` AS `m_item_kode`,`vbn`.`m_toko_kode` AS `m_toko_kode`,`vbn`.`stok_keluar` AS `stok_keluar`,`vbn`.`stok_masuk` AS `stok_masuk`,ifnull(`pa`.`persediaan_awal_detail_qty`,0) AS `QTY` from (`1nt3x_vsb_peritem_blnini` `vbn` left join `1nt3x_vpersediaanawal` `pa` on((`vbn`.`m_item_kode` = `pa`.`m_item_kode`)));

-- ----------------------------
-- View structure for `1nt3x_vminstok`
-- ----------------------------
DROP VIEW IF EXISTS `1nt3x_vminstok`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `1nt3x_vminstok` AS select `a`.`m_item_kode` AS `m_item_kode`,`a`.`m_item_barcode` AS `m_item_barcode`,`a`.`m_item_nama` AS `m_item_nama`,`a`.`m_item_kategori_kode` AS `m_item_kategori_kode`,`a`.`m_item_ket` AS `m_item_ket`,`a`.`m_item_gambar` AS `m_item_gambar`,`a`.`m_harga_jual` AS `m_harga_jual`,`b`.`m_stok_min` AS `m_stok_min` from (`1nt3x_vbarang` `a` join `1nt3x_m_stok_min` `b` on((`b`.`m_item_kode` = `a`.`m_item_kode`)));

-- ----------------------------
-- View structure for `1nt3x_vpersediaanawal`
-- ----------------------------
DROP VIEW IF EXISTS `1nt3x_vpersediaanawal`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `1nt3x_vpersediaanawal` AS select `pa`.`persediaan_awal_no` AS `persediaan_awal_no`,`pa`.`persediaan_awal_bulan` AS `persediaan_awal_bulan`,`pa`.`persediaan_awal_tahun` AS `persediaan_awal_tahun`,`pa`.`m_toko_kode` AS `m_toko_kode`,`pd`.`m_item_kode` AS `m_item_kode`,`pd`.`batch_no` AS `batch_no`,`pd`.`persediaan_awal_detail_qty` AS `persediaan_awal_detail_qty`,`pd`.`tgl_produksi` AS `tgl_produksi`,`pd`.`exp_date` AS `exp_date` from (`1nt3x_inv_persediaan_awal_detail` `pd` join `1nt3x_inv_persediaan_awal` `pa` on((`pa`.`persediaan_awal_no` = `pd`.`persediaan_awal_no`))) where (`pa`.`persediaan_awal_bulan` = date_format(now(),'%m'));

-- ----------------------------
-- View structure for `1nt3x_vsb_peritem_blnini`
-- ----------------------------
DROP VIEW IF EXISTS `1nt3x_vsb_peritem_blnini`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `1nt3x_vsb_peritem_blnini` AS select `sbn`.`m_toko_kode` AS `m_toko_kode`,`sbn`.`m_item_kode` AS `m_item_kode`,sum(`sbn`.`stok_berjalan_qty_masuk`) AS `stok_masuk`,sum(`sbn`.`stok_berjalan_qty_keluar`) AS `stok_keluar` from `1nt3x_vstokberjalanbulanini` `sbn` group by `sbn`.`m_item_kode`;

-- ----------------------------
-- View structure for `1nt3x_vstokberjalanbulanini`
-- ----------------------------
DROP VIEW IF EXISTS `1nt3x_vstokberjalanbulanini`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `1nt3x_vstokberjalanbulanini` AS select `sb`.`stok_berjalan_id` AS `stok_berjalan_id`,`sb`.`m_toko_kode` AS `m_toko_kode`,`sb`.`m_item_kode` AS `m_item_kode`,`sb`.`stok_berjalan_tgl` AS `stok_berjalan_tgl`,`sb`.`stok_berjalan_qty_masuk` AS `stok_berjalan_qty_masuk`,`sb`.`stok_berjalan_qty_keluar` AS `stok_berjalan_qty_keluar` from `1nt3x_inv_stok_berjalan` `sb` where (`sb`.`stok_berjalan_tgl` between (curdate() - interval (dayofmonth(curdate()) - 1) day) and last_day((now() + interval 0 month)));
