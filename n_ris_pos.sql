-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: ris_pos
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `1nt3x_inv_adj`
--

DROP TABLE IF EXISTS `1nt3x_inv_adj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_inv_adj` (
  `adj_no_trx` varchar(30) NOT NULL,
  `adj_tgl` date NOT NULL,
  `adj_ket` text,
  `adj_tgl_buat` date NOT NULL,
  `adj_tgl_ubah` date NOT NULL,
  `adj_petugas` varchar(50) NOT NULL,
  `m_toko_kode` varchar(11) DEFAULT NULL,
  `stok_opname_no_trx` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`adj_no_trx`),
  KEY `stok_opname_no_trx` (`stok_opname_no_trx`),
  KEY `perusahaan_kode` (`m_toko_kode`) USING BTREE,
  CONSTRAINT `1nt3x_inv_adj_ibfk_1` FOREIGN KEY (`stok_opname_no_trx`) REFERENCES `1nt3x_inv_stok_opname` (`stok_opname_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_adj_ibfk_2` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_inv_adj`
--

LOCK TABLES `1nt3x_inv_adj` WRITE;
/*!40000 ALTER TABLE `1nt3x_inv_adj` DISABLE KEYS */;
/*!40000 ALTER TABLE `1nt3x_inv_adj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_inv_adj_detail`
--

DROP TABLE IF EXISTS `1nt3x_inv_adj_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_inv_adj_detail` (
  `adj_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `adj_detail_ket` text,
  `adj_detail_qtyb` double NOT NULL COMMENT 'book',
  `adj_detail_qtyf` double NOT NULL COMMENT 'fisik',
  `adj_detail_qtys` double NOT NULL COMMENT 'selisih',
  `adj_detail_adjust` double NOT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  `adj_no_trx` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`adj_detail_id`),
  KEY `adj_no_trx` (`adj_no_trx`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_inv_adj_detail_ibfk_1` FOREIGN KEY (`adj_no_trx`) REFERENCES `1nt3x_inv_adj` (`adj_no_trx`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_adj_detail_ibfk_2` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_inv_adj_detail`
--

LOCK TABLES `1nt3x_inv_adj_detail` WRITE;
/*!40000 ALTER TABLE `1nt3x_inv_adj_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `1nt3x_inv_adj_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_inv_penerimaan_barang`
--

DROP TABLE IF EXISTS `1nt3x_inv_penerimaan_barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_inv_penerimaan_barang` (
  `inv_pen_brg_kode` varchar(15) NOT NULL DEFAULT '',
  `inv_pen_brg_date` date NOT NULL,
  `inv_pen_brg_no_srt_jln` varchar(15) NOT NULL,
  `inv_pen_brg_ket` text,
  `inv_pen_brg_tgl_buat` date NOT NULL,
  `inv_pen_brg_tgl_ubah` date NOT NULL,
  `inv_pen_brg_petugas` varchar(50) NOT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  `inv_po_no` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`inv_pen_brg_kode`),
  KEY `m_toko_kode` (`m_toko_kode`),
  KEY `inv_po_no` (`inv_po_no`),
  CONSTRAINT `1nt3x_inv_penerimaan_barang_ibfk_1` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_inv_penerimaan_barang`
--

LOCK TABLES `1nt3x_inv_penerimaan_barang` WRITE;
/*!40000 ALTER TABLE `1nt3x_inv_penerimaan_barang` DISABLE KEYS */;
INSERT INTO `1nt3x_inv_penerimaan_barang` VALUES ('PB/112015/00001','2015-11-16','','tes edit','2015-11-16','2015-11-16','admin','ABC','TF/KR/ABC/112015/00002'),('PB/112015/00002','2015-11-16','','tes 2','2015-11-16','0000-00-00','admin','ABC','PO/112015/00001'),('PB/112015/00003','2015-11-16','','','2015-11-16','0000-00-00','admin','ABC','PO/112015/00001'),('PB/112015/00004','2015-11-16','','','2015-11-16','0000-00-00','admin','ABC','TF/KR/ABC/112015/00002'),('PB/112015/00005','2015-11-16','','Ok','2015-11-16','0000-00-00','nugraha','KR','TF/ABC/KR/112015/00001');
/*!40000 ALTER TABLE `1nt3x_inv_penerimaan_barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_inv_penerimaan_barang_detail`
--

DROP TABLE IF EXISTS `1nt3x_inv_penerimaan_barang_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_inv_penerimaan_barang_detail` (
  `inv_pen_brg_detail_kode` int(11) NOT NULL AUTO_INCREMENT,
  `inv_pen_brg_detail_qty` double NOT NULL,
  `qty_po_sisa` double NOT NULL,
  `inv_pen_brg_kode` varchar(15) DEFAULT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`inv_pen_brg_detail_kode`),
  KEY `inv_pen_brg_kode` (`inv_pen_brg_kode`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_inv_penerimaan_barang_detail_ibfk_1` FOREIGN KEY (`inv_pen_brg_kode`) REFERENCES `1nt3x_inv_penerimaan_barang` (`inv_pen_brg_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_penerimaan_barang_detail_ibfk_2` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_inv_penerimaan_barang_detail`
--

LOCK TABLES `1nt3x_inv_penerimaan_barang_detail` WRITE;
/*!40000 ALTER TABLE `1nt3x_inv_penerimaan_barang_detail` DISABLE KEYS */;
INSERT INTO `1nt3x_inv_penerimaan_barang_detail` VALUES (7,5,10,'PB/112015/00001','0012'),(8,5,10,'PB/112015/00001','0013'),(9,75,100,'PB/112015/00002','0012'),(10,75,100,'PB/112015/00002','0013'),(15,25,25,'PB/112015/00003','0012'),(16,25,25,'PB/112015/00003','0013'),(17,5,5,'PB/112015/00004','0012'),(18,5,5,'PB/112015/00004','0013'),(19,25,25,'PB/112015/00005','0012'),(20,25,25,'PB/112015/00005','0013');
/*!40000 ALTER TABLE `1nt3x_inv_penerimaan_barang_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_inv_persediaan_awal`
--

DROP TABLE IF EXISTS `1nt3x_inv_persediaan_awal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_inv_persediaan_awal` (
  `persediaan_awal_no` varchar(30) NOT NULL,
  `persediaan_awal_bulan` int(2) NOT NULL,
  `persediaan_awal_tahun` int(4) NOT NULL,
  `persediaan_awal_tgl_buat` date NOT NULL,
  `persediaan_awal_tgl_ubah` date NOT NULL,
  `persediaan_awal_petugas` varchar(50) NOT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`persediaan_awal_no`),
  KEY `m_toko_kode` (`m_toko_kode`),
  CONSTRAINT `1nt3x_inv_persediaan_awal_ibfk_1` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_inv_persediaan_awal`
--

LOCK TABLES `1nt3x_inv_persediaan_awal` WRITE;
/*!40000 ALTER TABLE `1nt3x_inv_persediaan_awal` DISABLE KEYS */;
INSERT INTO `1nt3x_inv_persediaan_awal` VALUES ('PA/102015/00001',11,2015,'2015-10-20','2015-11-05','admin','ABC'),('PA/112015/00002',11,2015,'2015-11-16','2015-11-16','nugraha','KR'),('PA/122010/00004',12,2010,'2015-12-14','0000-00-00','admin','ABC'),('PA/122015/00003',12,2015,'2015-12-14','0000-00-00','admin','ABC');
/*!40000 ALTER TABLE `1nt3x_inv_persediaan_awal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_inv_persediaan_awal_detail`
--

DROP TABLE IF EXISTS `1nt3x_inv_persediaan_awal_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_inv_persediaan_awal_detail` (
  `persediaan_awal_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `persediaan_awal_detail_qty` int(11) NOT NULL,
  `persediaan_awal_no` varchar(30) DEFAULT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  `batch_no` varchar(35) DEFAULT NULL,
  `tgl_produksi` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  PRIMARY KEY (`persediaan_awal_detail_id`),
  KEY `persediaan_awal_no` (`persediaan_awal_no`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_inv_persediaan_awal_detail_ibfk_1` FOREIGN KEY (`persediaan_awal_no`) REFERENCES `1nt3x_inv_persediaan_awal` (`persediaan_awal_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_persediaan_awal_detail_ibfk_2` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_inv_persediaan_awal_detail`
--

LOCK TABLES `1nt3x_inv_persediaan_awal_detail` WRITE;
/*!40000 ALTER TABLE `1nt3x_inv_persediaan_awal_detail` DISABLE KEYS */;
INSERT INTO `1nt3x_inv_persediaan_awal_detail` VALUES (18,100,'PA/102015/00001','0012','','0000-00-00','0000-00-00'),(19,100,'PA/102015/00001','0013','','0000-00-00','0000-00-00'),(20,100,'PA/102015/00001','0010','','0000-00-00','0000-00-00'),(21,100,'PA/102015/00001','0011','','0000-00-00','0000-00-00'),(26,50,'PA/112015/00002','0012','AX8','0000-00-00','0000-00-00'),(27,50,'PA/112015/00002','0013','AX9','0000-00-00','0000-00-00'),(28,50,'PA/112015/00002','0010','','0000-00-00','0000-00-00'),(29,50,'PA/112015/00002','0011','','0000-00-00','0000-00-00'),(30,20,'PA/112015/00002','0012','AX10','0000-00-00','0000-00-00'),(31,20,'PA/112015/00002','0013','AX11','0000-00-00','0000-00-00'),(32,100,'PA/122015/00003','0012','','0000-00-00','0000-00-00'),(33,100,'PA/122015/00003','0013','','0000-00-00','0000-00-00'),(34,100,'PA/122015/00003','0014','','0000-00-00','0000-00-00'),(35,100,'PA/122015/00003','0015','','0000-00-00','0000-00-00'),(36,100,'PA/122015/00003','0016','','0000-00-00','0000-00-00'),(37,100,'PA/122015/00003','0010','','0000-00-00','0000-00-00'),(38,100,'PA/122015/00003','0011','','0000-00-00','0000-00-00'),(39,100,'PA/122010/00004','0012','','0000-00-00','0000-00-00'),(40,100,'PA/122010/00004','0013','','0000-00-00','0000-00-00'),(41,100,'PA/122010/00004','0014','','0000-00-00','0000-00-00'),(42,100,'PA/122010/00004','0015','','0000-00-00','0000-00-00'),(43,100,'PA/122010/00004','0016','','0000-00-00','0000-00-00');
/*!40000 ALTER TABLE `1nt3x_inv_persediaan_awal_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_inv_po`
--

DROP TABLE IF EXISTS `1nt3x_inv_po`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_inv_po` (
  `inv_po_no` varchar(30) NOT NULL,
  `inv_po_tgl` date NOT NULL,
  `inv_po_ket` text,
  `inv_po_total` double NOT NULL,
  `inv_po_grand_total` double NOT NULL,
  `inv_po_diskon` varchar(4) NOT NULL,
  `inv_po_pajak` varchar(4) NOT NULL,
  `inv_po_status` enum('2','1','0') NOT NULL DEFAULT '2' COMMENT '0: void, 1:closed, 2:proces',
  `inv_po_tgl_buat` date NOT NULL,
  `inv_po_tgl_ubah` date NOT NULL,
  `inv_po_petugas` varchar(50) NOT NULL,
  `m_suplier_kode` varchar(15) DEFAULT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`inv_po_no`),
  KEY `m_suplier_kode` (`m_suplier_kode`),
  KEY `m_toko_kode` (`m_toko_kode`),
  CONSTRAINT `1nt3x_inv_po_ibfk_1` FOREIGN KEY (`m_suplier_kode`) REFERENCES `1nt3x_m_suplier` (`m_suplier_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_po_ibfk_2` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_inv_po`
--

LOCK TABLES `1nt3x_inv_po` WRITE;
/*!40000 ALTER TABLE `1nt3x_inv_po` DISABLE KEYS */;
INSERT INTO `1nt3x_inv_po` VALUES ('PO/112015/00001','2015-11-16','tes',250000,250000,'0','0','2','2015-11-16','0000-00-00','nugraha','BA001','ABC');
/*!40000 ALTER TABLE `1nt3x_inv_po` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_inv_po_detail`
--

DROP TABLE IF EXISTS `1nt3x_inv_po_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_inv_po_detail` (
  `inv_po_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_po_detail_qty` int(11) NOT NULL,
  `inv_po_detail_harga` double NOT NULL,
  `inv_po_detail_total` double NOT NULL,
  `inv_po_detail_diskon` double NOT NULL,
  `inv_po_detail_grandtotal` double NOT NULL,
  `inv_po_detail_ket` text,
  `inv_po_no` varchar(30) DEFAULT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`inv_po_detail_id`),
  KEY `inv_po_no` (`inv_po_no`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_inv_po_detail_ibfk_1` FOREIGN KEY (`inv_po_no`) REFERENCES `1nt3x_inv_po` (`inv_po_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_po_detail_ibfk_2` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_inv_po_detail`
--

LOCK TABLES `1nt3x_inv_po_detail` WRITE;
/*!40000 ALTER TABLE `1nt3x_inv_po_detail` DISABLE KEYS */;
INSERT INTO `1nt3x_inv_po_detail` VALUES (9,100,1000,100000,0,100000,'-','PO/112015/00001','0012'),(10,100,1500,150000,0,150000,'-','PO/112015/00001','0013');
/*!40000 ALTER TABLE `1nt3x_inv_po_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_inv_return_pembelian`
--

DROP TABLE IF EXISTS `1nt3x_inv_return_pembelian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_inv_return_pembelian` (
  `return_po_no` varchar(30) NOT NULL,
  `return_po_tgl` date NOT NULL,
  `return_po_ket` text NOT NULL,
  `return_po_srt_jln` varchar(30) DEFAULT NULL,
  `return_po_tgl_buat` date NOT NULL,
  `return_po_tgl_ubah` date NOT NULL,
  `return_po_petugas` varchar(50) NOT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  `m_suplier_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`return_po_no`),
  KEY `m_toko_kode` (`m_toko_kode`),
  KEY `m_suplier_kode` (`m_suplier_kode`),
  CONSTRAINT `1nt3x_inv_return_pembelian_ibfk_1` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_return_pembelian_ibfk_2` FOREIGN KEY (`m_suplier_kode`) REFERENCES `1nt3x_m_suplier` (`m_suplier_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_inv_return_pembelian`
--

LOCK TABLES `1nt3x_inv_return_pembelian` WRITE;
/*!40000 ALTER TABLE `1nt3x_inv_return_pembelian` DISABLE KEYS */;
/*!40000 ALTER TABLE `1nt3x_inv_return_pembelian` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_inv_return_pembelian_detail`
--

DROP TABLE IF EXISTS `1nt3x_inv_return_pembelian_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_inv_return_pembelian_detail` (
  `return_po_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `return_po_detail_qty` int(11) NOT NULL,
  `return_po_no` varchar(30) DEFAULT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`return_po_detail_id`),
  KEY `return_po_no` (`return_po_no`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_inv_return_pembelian_detail_ibfk_1` FOREIGN KEY (`return_po_no`) REFERENCES `1nt3x_inv_return_pembelian` (`return_po_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_return_pembelian_detail_ibfk_2` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_inv_return_pembelian_detail`
--

LOCK TABLES `1nt3x_inv_return_pembelian_detail` WRITE;
/*!40000 ALTER TABLE `1nt3x_inv_return_pembelian_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `1nt3x_inv_return_pembelian_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_inv_stok_berjalan`
--

DROP TABLE IF EXISTS `1nt3x_inv_stok_berjalan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_inv_stok_berjalan` (
  `stok_berjalan_id` int(11) NOT NULL AUTO_INCREMENT,
  `stok_berjalan_tgl` date NOT NULL,
  `stok_berjalan_qty_masuk` int(11) DEFAULT NULL,
  `stok_berjalan_qty_keluar` int(11) DEFAULT NULL,
  `stok_berjalan_ket` text,
  `stok_berjalan_no_trx` varchar(30) NOT NULL,
  `stok_berjalan_tgl_buat` date NOT NULL,
  `stok_berjalan_tgl_ubah` date NOT NULL,
  `stok_berjalan_petugas` varchar(50) NOT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`stok_berjalan_id`),
  KEY `m_toko_kode` (`m_toko_kode`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_inv_stok_berjalan_ibfk_1` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_stok_berjalan_ibfk_2` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=214 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_inv_stok_berjalan`
--

LOCK TABLES `1nt3x_inv_stok_berjalan` WRITE;
/*!40000 ALTER TABLE `1nt3x_inv_stok_berjalan` DISABLE KEYS */;
INSERT INTO `1nt3x_inv_stok_berjalan` VALUES (50,'2015-11-01',100,0,'Persediaan Awal','PA/102015/00001','2015-11-05','0000-00-00','admin','ABC','0012'),(51,'2015-11-01',100,0,'Persediaan Awal','PA/102015/00001','2015-11-05','0000-00-00','admin','ABC','0013'),(52,'2015-11-01',100,0,'Persediaan Awal','PA/102015/00001','2015-11-05','0000-00-00','admin','ABC','0010'),(53,'2015-11-01',100,0,'Persediaan Awal','PA/102015/00001','2015-11-05','0000-00-00','admin','ABC','0011'),(147,'2015-12-14',0,1,'POS toko','ABC/12/2015/00004','2015-12-14','0000-00-00','admin','ABC','0012'),(148,'2015-12-14',0,1,'POS toko','ABC/12/2015/00004','2015-12-14','0000-00-00','admin','ABC','0013'),(149,'2015-12-14',0,2,'POS toko','ABC/12/2015/00004','2015-12-14','0000-00-00','admin','ABC','0010'),(150,'2015-12-14',0,1,'POS toko','ABC/12/2015/00004','2015-12-14','0000-00-00','admin','ABC','0011'),(151,'2015-12-14',0,2,'POS Phone','ABC/12/2015/00005','2015-12-14','0000-00-00','intex_kasir','ABC','0010'),(152,'2015-12-14',0,9,'POS Phone','ABC/12/2015/00005','2015-12-14','0000-00-00','intex_kasir','ABC','0011'),(156,'2015-12-14',0,2,'POS Phone','ABC/12/2015/00006','2015-12-14','0000-00-00','intex_kasir','ABC','0010'),(157,'2015-12-14',0,5,'POS Phone','ABC/12/2015/00006','2015-12-14','0000-00-00','intex_kasir','ABC','0011'),(162,'2015-12-14',0,4,'POS Phone','ABC/12/2015/00008','2015-12-14','0000-00-00','intex_kasir','ABC','0010'),(163,'2015-12-14',0,5,'POS Phone','ABC/12/2015/00008','2015-12-14','0000-00-00','intex_kasir','ABC','0011'),(164,'2015-12-14',0,9,'POS Phone','ABC/12/2015/00008','2015-12-14','0000-00-00','intex_kasir','ABC','0012'),(165,'2015-12-14',0,10,'POS Phone','ABC/12/2015/00009','2015-12-14','0000-00-00','intex_kasir','ABC','0010'),(166,'2015-12-14',0,10,'POS Phone','ABC/12/2015/00009','2015-12-14','0000-00-00','intex_kasir','ABC','0012'),(167,'2015-12-14',0,15,'POS Phone','ABC/12/2015/00010','2015-12-14','0000-00-00','intex_kasir','ABC','0012'),(168,'2015-12-01',100,0,'Persediaan Awal','PA/122015/00003','2015-12-14','0000-00-00','admin','ABC','0012'),(169,'2015-12-01',100,0,'Persediaan Awal','PA/122015/00003','2015-12-14','0000-00-00','admin','ABC','0013'),(170,'2015-12-01',100,0,'Persediaan Awal','PA/122015/00003','2015-12-14','0000-00-00','admin','ABC','0014'),(171,'2015-12-01',100,0,'Persediaan Awal','PA/122015/00003','2015-12-14','0000-00-00','admin','ABC','0015'),(172,'2015-12-01',100,0,'Persediaan Awal','PA/122015/00003','2015-12-14','0000-00-00','admin','ABC','0016'),(173,'2015-12-01',100,0,'Persediaan Awal','PA/122015/00003','2015-12-14','0000-00-00','admin','ABC','0010'),(174,'2015-12-01',100,0,'Persediaan Awal','PA/122015/00003','2015-12-14','0000-00-00','admin','ABC','0011'),(179,'2015-12-14',0,5,'POS Phone','ABC/12/2015/00015','2015-12-14','0000-00-00','intex_kasir','ABC','0010'),(180,'2015-12-14',0,1,'POS Phone','ABC/12/2015/00016','2015-12-14','0000-00-00','intex_kasir','ABC','0010'),(181,'2015-12-14',0,5,'POS Phone','ABC/12/2015/00017','2015-12-14','0000-00-00','intex_kasir','ABC','0012'),(182,'2015-12-14',0,1,'POS Phone','ABC/12/2015/00018','2015-12-14','0000-00-00','intex_kasir','ABC','0010'),(183,'2010-12-01',100,0,'Persediaan Awal','PA/122010/00004','2015-12-14','0000-00-00','admin','ABC','0012'),(184,'2010-12-01',100,0,'Persediaan Awal','PA/122010/00004','2015-12-14','0000-00-00','admin','ABC','0013'),(185,'2010-12-01',100,0,'Persediaan Awal','PA/122010/00004','2015-12-14','0000-00-00','admin','ABC','0014'),(186,'2010-12-01',100,0,'Persediaan Awal','PA/122010/00004','2015-12-14','0000-00-00','admin','ABC','0015'),(187,'2010-12-01',100,0,'Persediaan Awal','PA/122010/00004','2015-12-14','0000-00-00','admin','ABC','0016'),(188,'2015-12-14',0,2,'POS Phone','ABC/12/2015/00019','2015-12-14','0000-00-00','intex_kasir','ABC','0011'),(189,'2015-12-15',0,6,'POS Phone','ABC/12/2015/00020','2015-12-15','0000-00-00','intex_kasir','ABC','0012'),(190,'2015-12-15',0,5,'POS Phone','ABC/12/2015/00020','2015-12-15','0000-00-00','intex_kasir','ABC','0011'),(191,'2015-12-15',0,9,'POS Phone','ABC/12/2015/00020','2015-12-15','0000-00-00','intex_kasir','ABC','0014'),(192,'2015-12-15',0,10,'POS Phone','ABC/12/2015/00020','2015-12-15','0000-00-00','intex_kasir','ABC','0015'),(193,'2015-12-15',0,1,'POS Phone','ABC/12/2015/00021','2015-12-15','0000-00-00','intex_kasir','ABC','0012'),(194,'2015-12-15',0,6,'POS Phone','ABC/12/2015/00021','2015-12-15','0000-00-00','intex_kasir','ABC','0011'),(195,'2015-12-15',0,5,'POS Phone','ABC/12/2015/00021','2015-12-15','0000-00-00','intex_kasir','ABC','0015'),(196,'2015-12-15',0,11,'POS Phone','ABC/12/2015/00021','2015-12-15','0000-00-00','intex_kasir','ABC','0016'),(197,'2015-12-15',0,12,'POS Phone','ABC/12/2015/00021','2015-12-15','0000-00-00','intex_kasir','ABC','0013'),(198,'2015-12-15',0,12,'POS Phone','ABC/12/2015/00021','2015-12-15','0000-00-00','intex_kasir','ABC','0013'),(199,'2015-12-15',0,1,'POS Phone','ABC/12/2015/00022','2015-12-15','0000-00-00','intex_kasir','ABC','0011'),(200,'2015-12-15',0,5,'POS Phone','ABC/12/2015/00023','2015-12-15','0000-00-00','intex_kasir','ABC','0010'),(201,'2015-12-15',0,8,'POS Phone','ABC/12/2015/00024','2015-12-15','0000-00-00','intex_kasir','ABC','0010'),(202,'2015-12-15',0,6,'POS Phone','ABC/12/2015/00024','2015-12-15','0000-00-00','intex_kasir','ABC','0011'),(203,'2015-12-16',0,28,'POS toko','ABC/12/2015/00025','2015-12-16','0000-00-00','admin','ABC','0010'),(204,'2015-12-16',0,2,'POS toko','ABC/12/2015/00026','2015-12-16','0000-00-00','admin','ABC','0010'),(205,'2015-12-16',0,1,'POS toko','ABC/12/2015/00027','2015-12-16','0000-00-00','admin','ABC','0010'),(206,'2015-12-17',0,10,'POS toko','ABC/12/2015/00028','2015-12-17','0000-00-00','intex_kasir','ABC','0012'),(207,'2015-12-17',0,1,'POS toko','ABC/12/2015/00028','2015-12-17','0000-00-00','intex_kasir','ABC','0010'),(208,'2015-12-17',0,15,'POS toko','ABC/12/2015/00028','2015-12-17','0000-00-00','intex_kasir','ABC','0011'),(209,'2016-01-02',0,8,'POS Phone','ABC/01/2016/00029','2016-01-02','0000-00-00','intex_kasir','ABC','0010'),(210,'2016-01-02',0,5,'POS Phone','ABC/01/2016/00029','2016-01-02','0000-00-00','intex_kasir','ABC','0011'),(211,'2016-01-02',0,3,'POS Phone','ABC/01/2016/00029','2016-01-02','0000-00-00','intex_kasir','ABC','0012'),(212,'2016-01-02',0,1,'POS Phone','ABC/01/2016/00029','2016-01-02','0000-00-00','intex_kasir','ABC','0014'),(213,'2016-01-02',0,5,'POS Phone','ABC/01/2016/00029','2016-01-02','0000-00-00','intex_kasir','ABC','0015');
/*!40000 ALTER TABLE `1nt3x_inv_stok_berjalan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_inv_stok_opname`
--

DROP TABLE IF EXISTS `1nt3x_inv_stok_opname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_inv_stok_opname` (
  `stok_opname_no` varchar(30) NOT NULL,
  `stok_opname_tgl` date NOT NULL,
  `stok_opname_bulan` varchar(2) DEFAULT NULL,
  `stok_opname_tahun` varchar(4) DEFAULT NULL,
  `stok_opname_ket` text,
  `stok_opname_tgl_buat` date NOT NULL,
  `stok_opname_tgl_ubah` date NOT NULL,
  `stok_opname_petugas` varchar(50) NOT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`stok_opname_no`),
  KEY `m_toko_kode` (`m_toko_kode`),
  CONSTRAINT `1nt3x_inv_stok_opname_ibfk_1` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_inv_stok_opname`
--

LOCK TABLES `1nt3x_inv_stok_opname` WRITE;
/*!40000 ALTER TABLE `1nt3x_inv_stok_opname` DISABLE KEYS */;
/*!40000 ALTER TABLE `1nt3x_inv_stok_opname` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_inv_stok_opname_detail`
--

DROP TABLE IF EXISTS `1nt3x_inv_stok_opname_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_inv_stok_opname_detail` (
  `stok_opname_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `stok_opname_detail_qty_sys` int(11) NOT NULL,
  `stok_opname_detail_qty_fisik` int(11) NOT NULL,
  `stok_opname_no` varchar(30) NOT NULL,
  `m_item_kode` varchar(15) NOT NULL,
  PRIMARY KEY (`stok_opname_detail_id`),
  KEY `stok_opname_no` (`stok_opname_no`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_inv_stok_opname_detail_ibfk_1` FOREIGN KEY (`stok_opname_no`) REFERENCES `1nt3x_inv_stok_opname` (`stok_opname_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_stok_opname_detail_ibfk_2` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_inv_stok_opname_detail`
--

LOCK TABLES `1nt3x_inv_stok_opname_detail` WRITE;
/*!40000 ALTER TABLE `1nt3x_inv_stok_opname_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `1nt3x_inv_stok_opname_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_inv_transfer_barang`
--

DROP TABLE IF EXISTS `1nt3x_inv_transfer_barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_inv_transfer_barang` (
  `transfer_barang_no` varchar(30) NOT NULL,
  `transfer_barang_tgl` date NOT NULL,
  `transfer_barang_ket` text NOT NULL,
  `transfer_barang_asal` varchar(50) NOT NULL,
  `transfer_barang_tujuan` varchar(50) NOT NULL,
  `transfer_barang_tgl_buat` date NOT NULL,
  `transfer_barang_tgl_ubah` date NOT NULL,
  `transfer_barang_petugas` varchar(50) NOT NULL,
  PRIMARY KEY (`transfer_barang_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_inv_transfer_barang`
--

LOCK TABLES `1nt3x_inv_transfer_barang` WRITE;
/*!40000 ALTER TABLE `1nt3x_inv_transfer_barang` DISABLE KEYS */;
INSERT INTO `1nt3x_inv_transfer_barang` VALUES ('TF/ABC/KR/112015/00001','2015-11-11','tes','ABC','KR','2015-11-11','0000-00-00','admin'),('TF/KR/ABC/112015/00002','2015-11-16','tes','KR','ABC','2015-11-16','0000-00-00','nugraha');
/*!40000 ALTER TABLE `1nt3x_inv_transfer_barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_inv_transfer_barang_detail`
--

DROP TABLE IF EXISTS `1nt3x_inv_transfer_barang_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_inv_transfer_barang_detail` (
  `transfer_barang_detail_no` int(11) NOT NULL AUTO_INCREMENT,
  `transfer_barang_detail_qty` int(11) NOT NULL,
  `transfer_barang_no` varchar(30) DEFAULT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`transfer_barang_detail_no`),
  KEY `transfer_barang_no` (`transfer_barang_no`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_inv_transfer_barang_detail_ibfk_1` FOREIGN KEY (`transfer_barang_no`) REFERENCES `1nt3x_inv_transfer_barang` (`transfer_barang_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_inv_transfer_barang_detail_ibfk_2` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_inv_transfer_barang_detail`
--

LOCK TABLES `1nt3x_inv_transfer_barang_detail` WRITE;
/*!40000 ALTER TABLE `1nt3x_inv_transfer_barang_detail` DISABLE KEYS */;
INSERT INTO `1nt3x_inv_transfer_barang_detail` VALUES (7,25,'TF/ABC/KR/112015/00001','0012'),(8,25,'TF/ABC/KR/112015/00001','0013'),(9,10,'TF/KR/ABC/112015/00002','0012'),(10,10,'TF/KR/ABC/112015/00002','0013');
/*!40000 ALTER TABLE `1nt3x_inv_transfer_barang_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_kabupaten`
--

DROP TABLE IF EXISTS `1nt3x_kabupaten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_kabupaten` (
  `kabupaten_kode` varchar(11) NOT NULL,
  `kabupaten_ket` varchar(50) DEFAULT NULL,
  `kabupaten_tgl_buat` date DEFAULT NULL,
  `kabupaten_tgl_ubah` date DEFAULT NULL,
  `kabupaten_petugas` varchar(50) DEFAULT NULL,
  `kabupaten_status` enum('1','0') DEFAULT NULL,
  `provinsi_kode` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`kabupaten_kode`),
  KEY `provinsi_kode` (`provinsi_kode`),
  CONSTRAINT `1nt3x_kabupaten_ibfk_1` FOREIGN KEY (`provinsi_kode`) REFERENCES `1nt3x_provinsi` (`provinsi_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_kabupaten`
--

LOCK TABLES `1nt3x_kabupaten` WRITE;
/*!40000 ALTER TABLE `1nt3x_kabupaten` DISABLE KEYS */;
INSERT INTO `1nt3x_kabupaten` VALUES ('1101','Kab. Simeulue',NULL,NULL,'system','1','11'),('1102','Kab. Aceh Singkil',NULL,NULL,'system','1','11'),('1103','Kab. Aceh Selatan',NULL,NULL,'system','1','11'),('1104','Kab. Aceh Tenggara',NULL,NULL,'system','1','11'),('1105','Kab. Aceh Timur',NULL,NULL,'system','1','11'),('1106','Kab. Aceh Tengah',NULL,NULL,'system','1','11'),('1107','Kab. Aceh Barat',NULL,NULL,'system','1','11'),('1108','Kab. Aceh Besar',NULL,NULL,'system','1','11'),('1109','Kab. Pidie',NULL,NULL,'system','1','11'),('1110','Kab. Bireuen',NULL,NULL,'system','1','11'),('1111','Kab. Aceh Utara',NULL,NULL,'system','1','11'),('1112','Kab. Aceh Barat Daya',NULL,NULL,'system','1','11'),('1113','Kab. Gayo Lues',NULL,NULL,'system','1','11'),('1114','Kab. Aceh Tamiang',NULL,NULL,'system','1','11'),('1115','Kab. Nagan Raya',NULL,NULL,'system','1','11'),('1116','Kab. Aceh Jaya',NULL,NULL,'system','1','11'),('1117','Kab. Bener Meriah',NULL,NULL,'system','1','11'),('1118','Kab. Pidie Jaya',NULL,NULL,'system','1','11'),('1171','Kota Banda Aceh',NULL,NULL,'system','1','11'),('1172','Kota Sabang',NULL,NULL,'system','1','11'),('1173','Kota Langsa',NULL,NULL,'system','1','11'),('1174','Kota Lhokseumawe',NULL,NULL,'system','1','11'),('1175','Kota Subulussalam',NULL,NULL,'system','1','11'),('1201','Kab. Nias',NULL,NULL,'system','1','12'),('1202','Kab. Mandailing Natal',NULL,NULL,'system','1','12'),('1203','Kab. Tapanuli Selatan',NULL,NULL,'system','1','12'),('1204','Kab. Tapanuli Tengah',NULL,NULL,'system','1','12'),('1205','Kab. Tapanuli Utara',NULL,NULL,'system','1','12'),('1206','Kab. Toba Samosir',NULL,NULL,'system','1','12'),('1207','Kab. Labuhan Batu',NULL,NULL,'system','1','12'),('1208','Kab. Asahan',NULL,NULL,'system','1','12'),('1209','Kab. Simalungun',NULL,NULL,'system','1','12'),('1210','Kab. Dairi',NULL,NULL,'system','1','12'),('1211','Kab. Karo',NULL,NULL,'system','1','12'),('1212','Kab. Deli Serdang',NULL,NULL,'system','1','12'),('1213','Kab. Langkat',NULL,NULL,'system','1','12'),('1214','Kab. Nias Selatan',NULL,NULL,'system','1','12'),('1215','Kab. Humbang Hasundutan',NULL,NULL,'system','1','12'),('1216','Kab. Pakpak Bharat',NULL,NULL,'system','1','12'),('1217','Kab. Samosir',NULL,NULL,'system','1','12'),('1218','Kab. Serdang Bedagai',NULL,NULL,'system','1','12'),('1219','Kab. Batu Bara',NULL,NULL,'system','1','12'),('1220','Kab. Padang Lawas Utara',NULL,NULL,'system','1','12'),('1221','Kab. Padang Lawas',NULL,NULL,'system','1','12'),('1222','Kab. Labuhan Batu Selatan',NULL,NULL,'system','1','12'),('1223','Kab. Labuhan Batu Utara',NULL,NULL,'system','1','12'),('1224','Kab. Nias Utara',NULL,NULL,'system','1','12'),('1225','Kab. Nias Barat',NULL,NULL,'system','1','12'),('1271','Kota Sibolga',NULL,NULL,'system','1','12'),('1272','Kota Tanjung Balai',NULL,NULL,'system','1','12'),('1273','Kota Pematang Siantar',NULL,NULL,'system','1','12'),('1274','Kota Tebing Tinggi',NULL,NULL,'system','1','12'),('1275','Kota Medan',NULL,NULL,'system','1','12'),('1276','Kota Binjai',NULL,NULL,'system','1','12'),('1277','Kota Padangsidimpuan',NULL,NULL,'system','1','12'),('1278','Kota Gunungsitoli',NULL,NULL,'system','1','12'),('1301','Kab. Kepulauan Mentawai',NULL,NULL,'system','1','13'),('1302','Kab. Pesisir Selatan',NULL,NULL,'system','1','13'),('1303','Kab. Solok',NULL,NULL,'system','1','13'),('1304','Kab. Sijunjung',NULL,NULL,'system','1','13'),('1305','Kab. Tanah Datar',NULL,NULL,'system','1','13'),('1306','Kab. Padang Pariaman',NULL,NULL,'system','1','13'),('1307','Kab. Agam',NULL,NULL,'system','1','13'),('1308','Kab. Lima Puluh Kota',NULL,NULL,'system','1','13'),('1309','Kab. Pasaman',NULL,NULL,'system','1','13'),('1310','Kab. Solok Selatan',NULL,NULL,'system','1','13'),('1311','Kab. Dharmasraya',NULL,NULL,'system','1','13'),('1312','Kab. Pasaman Barat',NULL,NULL,'system','1','13'),('1371','Kota Padang',NULL,NULL,'system','1','13'),('1372','Kota Solok',NULL,NULL,'system','1','13'),('1373','Kota Sawah Lunto',NULL,NULL,'system','1','13'),('1374','Kota Padang Panjang',NULL,NULL,'system','1','13'),('1375','Kota Bukittinggi',NULL,NULL,'system','1','13'),('1376','Kota Payakumbuh',NULL,NULL,'system','1','13'),('1377','Kota Pariaman',NULL,NULL,'system','1','13'),('1401','Kab. Kuantan Singingi',NULL,NULL,'system','1','14'),('1402','Kab. Indragiri Hulu',NULL,NULL,'system','1','14'),('1403','Kab. Indragiri Hilir',NULL,NULL,'system','1','14'),('1404','Kab. Pelalawan',NULL,NULL,'system','1','14'),('1405','Kab. Siak',NULL,NULL,'system','1','14'),('1406','Kab. Kampar',NULL,NULL,'system','1','14'),('1407','Kab. Rokan Hulu',NULL,NULL,'system','1','14'),('1408','Kab. Bengkalis',NULL,NULL,'system','1','14'),('1409','Kab. Rokan Hilir',NULL,NULL,'system','1','14'),('1410','Kab. Kepulauan Meranti',NULL,NULL,'system','1','14'),('1471','Kota Pekanbaru',NULL,NULL,'system','1','14'),('1473','Kota Dumai',NULL,NULL,'system','1','14'),('1501','Kab. Kerinci',NULL,NULL,'system','1','15'),('1502','Kab. Merangin',NULL,NULL,'system','1','15'),('1503','Kab. Sarolangun',NULL,NULL,'system','1','15'),('1504','Kab. Batang Hari',NULL,NULL,'system','1','15'),('1505','Kab. Muaro Jambi',NULL,NULL,'system','1','15'),('1506','Kab. Tanjung Jabung Timur',NULL,NULL,'system','1','15'),('1507','Kab. Tanjung Jabung Barat',NULL,NULL,'system','1','15'),('1508','Kab. Tebo',NULL,NULL,'system','1','15'),('1509','Kab. Bungo',NULL,NULL,'system','1','15'),('1571','Kota Jambi',NULL,NULL,'system','1','15'),('1572','Kota Sungai Penuh',NULL,NULL,'system','1','15'),('1601','Kab. Ogan Komering Ulu',NULL,NULL,'system','1','16'),('1602','Kab. Ogan Komering Ilir',NULL,NULL,'system','1','16'),('1603','Kab. Muara Enim',NULL,NULL,'system','1','16'),('1604','Kab. Lahat',NULL,NULL,'system','1','16'),('1605','Kab. Musi Rawas',NULL,NULL,'system','1','16'),('1606','Kab. Musi Banyuasin',NULL,NULL,'system','1','16'),('1607','Kab. Banyu Asin',NULL,NULL,'system','1','16'),('1608','Kab. Ogan Komering Ulu Selatan',NULL,NULL,'system','1','16'),('1609','Kab. Ogan Komering Ulu Timur',NULL,NULL,'system','1','16'),('1610','Kab. Ogan Ilir',NULL,NULL,'system','1','16'),('1611','Kab. Empat Lawang',NULL,NULL,'system','1','16'),('1671','Kota Palembang',NULL,NULL,'system','1','16'),('1672','Kota Prabumulih',NULL,NULL,'system','1','16'),('1673','Kota Pagar Alam',NULL,NULL,'system','1','16'),('1674','Kota Lubuklinggau',NULL,NULL,'system','1','16'),('1701','Kab. Bengkulu Selatan',NULL,NULL,'system','1','17'),('1702','Kab. Rejang Lebong',NULL,NULL,'system','1','17'),('1703','Kab. Bengkulu Utara',NULL,NULL,'system','1','17'),('1704','Kab. Kaur',NULL,NULL,'system','1','17'),('1705','Kab. Seluma',NULL,NULL,'system','1','17'),('1706','Kab. Mukomuko',NULL,NULL,'system','1','17'),('1707','Kab. Lebong',NULL,NULL,'system','1','17'),('1708','Kab. Kepahiang',NULL,NULL,'system','1','17'),('1709','Kab. Bengkulu Tengah',NULL,NULL,'system','1','17'),('1771','Kota Bengkulu',NULL,NULL,'system','1','17'),('1801','Kab. Lampung Barat',NULL,NULL,'system','1','18'),('1802','Kab. Tanggamus',NULL,NULL,'system','1','18'),('1803','Kab. Lampung Selatan',NULL,NULL,'system','1','18'),('1804','Kab. Lampung Timur',NULL,NULL,'system','1','18'),('1805','Kab. Lampung Tengah',NULL,NULL,'system','1','18'),('1806','Kab. Lampung Utara',NULL,NULL,'system','1','18'),('1807','Kab. Way Kanan',NULL,NULL,'system','1','18'),('1808','Kab. Tulangbawang',NULL,NULL,'system','1','18'),('1809','Kab. Pesawaran',NULL,NULL,'system','1','18'),('1810','Kab. Pringsewu',NULL,NULL,'system','1','18'),('1811','Kab. Mesuji',NULL,NULL,'system','1','18'),('1812','Kab. Tulang Bawang Barat',NULL,NULL,'system','1','18'),('1813','Kab. Pesisir Barat',NULL,NULL,'system','1','18'),('1871','Kota Bandar Lampung',NULL,NULL,'system','1','18'),('1872','Kota Metro',NULL,NULL,'system','1','18'),('1901','Kab. Bangka',NULL,NULL,'system','1','19'),('1902','Kab. Belitung',NULL,NULL,'system','1','19'),('1903','Kab. Bangka Barat',NULL,NULL,'system','1','19'),('1904','Kab. Bangka Tengah',NULL,NULL,'system','1','19'),('1905','Kab. Bangka Selatan',NULL,NULL,'system','1','19'),('1906','Kab. Belitung Timur',NULL,NULL,'system','1','19'),('1971','Kota Pangkal Pinang',NULL,NULL,'system','1','19'),('2101','Kab. Karimun',NULL,NULL,'system','1','21'),('2102','Kab. Bintan',NULL,NULL,'system','1','21'),('2103','Kab. Natuna',NULL,NULL,'system','1','21'),('2104','Kab. Lingga',NULL,NULL,'system','1','21'),('2105','Kab. Kepulauan Anambas',NULL,NULL,'system','1','21'),('2171','Kota Batam',NULL,NULL,'system','1','21'),('2172','Kota Tanjung Pinang',NULL,NULL,'system','1','21'),('3101','Kab. Kepulauan Seribu',NULL,NULL,'system','1','31'),('3171','Kota Jakarta Selatan',NULL,NULL,'system','1','31'),('3172','Kota Jakarta Timur',NULL,NULL,'system','1','31'),('3173','Kota Jakarta Pusat',NULL,NULL,'system','1','31'),('3174','Kota Jakarta Barat',NULL,NULL,'system','1','31'),('3175','Kota Jakarta Utara',NULL,NULL,'system','1','31'),('3201','Kab. Bogor',NULL,NULL,'system','1','32'),('3202','Kab. Sukabumi',NULL,NULL,'system','1','32'),('3203','Kab. Cianjur',NULL,NULL,'system','1','32'),('3204','Kab. Bandung',NULL,NULL,'system','1','32'),('3205','Kab. Garut',NULL,NULL,'system','1','32'),('3206','Kab. Tasikmalaya',NULL,NULL,'system','1','32'),('3207','Kab. Ciamis',NULL,NULL,'system','1','32'),('3208','Kab. Kuningan',NULL,NULL,'system','1','32'),('3209','Kab. Cirebon',NULL,NULL,'system','1','32'),('3210','Kab. Majalengka',NULL,NULL,'system','1','32'),('3211','Kab. Sumedang',NULL,NULL,'system','1','32'),('3212','Kab. Indramayu',NULL,NULL,'system','1','32'),('3213','Kab. Subang',NULL,NULL,'system','1','32'),('3214','Kab. Purwakarta',NULL,NULL,'system','1','32'),('3215','Kab. Karawang',NULL,NULL,'system','1','32'),('3216','Kab. Bekasi',NULL,NULL,'system','1','32'),('3217','Kab. Bandung Barat',NULL,NULL,'system','1','32'),('3218','Kab. Pangandaran',NULL,NULL,'system','1','32'),('3271','Kota Bogor',NULL,NULL,'system','1','32'),('3272','Kota Sukabumi',NULL,NULL,'system','1','32'),('3273','Kota Bandung',NULL,NULL,'system','1','32'),('3274','Kota Cirebon',NULL,NULL,'system','1','32'),('3275','Kota Bekasi',NULL,NULL,'system','1','32'),('3276','Kota Depok',NULL,NULL,'system','1','32'),('3277','Kota Cimahi',NULL,NULL,'system','1','32'),('3278','Kota Tasikmalaya',NULL,NULL,'system','1','32'),('3279','Kota Banjar',NULL,NULL,'system','1','32'),('3301','Kab. Cilacap',NULL,NULL,'system','1','33'),('3302','Kab. Banyumas',NULL,NULL,'system','1','33'),('3303','Kab. Purbalingga',NULL,NULL,'system','1','33'),('3304','Kab. Banjarnegara',NULL,NULL,'system','1','33'),('3305','Kab. Kebumen',NULL,NULL,'system','1','33'),('3306','Kab. Purworejo',NULL,NULL,'system','1','33'),('3307','Kab. Wonosobo',NULL,NULL,'system','1','33'),('3308','Kab. Magelang',NULL,NULL,'system','1','33'),('3309','Kab. Boyolali',NULL,NULL,'system','1','33'),('3310','Kab. Klaten',NULL,NULL,'system','1','33'),('3311','Kab. Sukoharjo',NULL,NULL,'system','1','33'),('3312','Kab. Wonogiri',NULL,NULL,'system','1','33'),('3313','Kab. Karanganyar',NULL,NULL,'system','1','33'),('3314','Kab. Sragen',NULL,NULL,'system','1','33'),('3315','Kab. Grobogan',NULL,NULL,'system','1','33'),('3316','Kab. Blora',NULL,NULL,'system','1','33'),('3317','Kab. Rembang',NULL,NULL,'system','1','33'),('3318','Kab. Pati',NULL,NULL,'system','1','33'),('3319','Kab. Kudus',NULL,NULL,'system','1','33'),('3320','Kab. Jepara',NULL,NULL,'system','1','33'),('3321','Kab. Demak',NULL,NULL,'system','1','33'),('3322','Kab. Semarang',NULL,NULL,'system','1','33'),('3323','Kab. Temanggung',NULL,NULL,'system','1','33'),('3324','Kab. Kendal',NULL,NULL,'system','1','33'),('3325','Kab. Batang',NULL,NULL,'system','1','33'),('3326','Kab. Pekalongan',NULL,NULL,'system','1','33'),('3327','Kab. Pemalang',NULL,NULL,'system','1','33'),('3328','Kab. Tegal',NULL,NULL,'system','1','33'),('3329','Kab. Brebes',NULL,NULL,'system','1','33'),('3371','Kota Magelang',NULL,NULL,'system','1','33'),('3372','Kota Surakarta',NULL,NULL,'system','1','33'),('3373','Kota Salatiga',NULL,NULL,'system','1','33'),('3374','Kota Semarang',NULL,NULL,'system','1','33'),('3375','Kota Pekalongan',NULL,NULL,'system','1','33'),('3376','Kota Tegal',NULL,NULL,'system','1','33'),('3401','Kab. Kulon Progo',NULL,NULL,'system','1','34'),('3402','Kab. Bantul',NULL,NULL,'system','1','34'),('3403','Kab. Gunung Kidul',NULL,NULL,'system','1','34'),('3404','Kab. Sleman',NULL,NULL,'system','1','34'),('3471','Kota Yogyakarta',NULL,NULL,'system','1','34'),('3501','Kab. Pacitan',NULL,NULL,'system','1','35'),('3502','Kab. Ponorogo',NULL,NULL,'system','1','35'),('3503','Kab. Trenggalek',NULL,NULL,'system','1','35'),('3504','Kab. Tulungagung',NULL,NULL,'system','1','35'),('3505','Kab. Blitar',NULL,NULL,'system','1','35'),('3506','Kab. Kediri',NULL,NULL,'system','1','35'),('3507','Kab. Malang',NULL,NULL,'system','1','35'),('3508','Kab. Lumajang',NULL,NULL,'system','1','35'),('3509','Kab. Jember',NULL,NULL,'system','1','35'),('3510','Kab. Banyuwangi',NULL,NULL,'system','1','35'),('3511','Kab. Bondowoso',NULL,NULL,'system','1','35'),('3512','Kab. Situbondo',NULL,NULL,'system','1','35'),('3513','Kab. Probolinggo',NULL,NULL,'system','1','35'),('3514','Kab. Pasuruan',NULL,NULL,'system','1','35'),('3515','Kab. Sidoarjo',NULL,NULL,'system','1','35'),('3516','Kab. Mojokerto',NULL,NULL,'system','1','35'),('3517','Kab. Jombang',NULL,NULL,'system','1','35'),('3518','Kab. Nganjuk',NULL,NULL,'system','1','35'),('3519','Kab. Madiun',NULL,NULL,'system','1','35'),('3520','Kab. Magetan',NULL,NULL,'system','1','35'),('3521','Kab. Ngawi',NULL,NULL,'system','1','35'),('3522','Kab. Bojonegoro',NULL,NULL,'system','1','35'),('3523','Kab. Tuban',NULL,NULL,'system','1','35'),('3524','Kab. Lamongan',NULL,NULL,'system','1','35'),('3525','Kab. Gresik',NULL,NULL,'system','1','35'),('3526','Kab. Bangkalan',NULL,NULL,'system','1','35'),('3527','Kab. Sampang',NULL,NULL,'system','1','35'),('3528','Kab. Pamekasan',NULL,NULL,'system','1','35'),('3529','Kab. Sumenep',NULL,NULL,'system','1','35'),('3571','Kota Kediri',NULL,NULL,'system','1','35'),('3572','Kota Blitar',NULL,NULL,'system','1','35'),('3573','Kota Malang',NULL,NULL,'system','1','35'),('3574','Kota Probolinggo',NULL,NULL,'system','1','35'),('3575','Kota Pasuruan',NULL,NULL,'system','1','35'),('3576','Kota Mojokerto',NULL,NULL,'system','1','35'),('3577','Kota Madiun',NULL,NULL,'system','1','35'),('3578','Kota Surabaya',NULL,NULL,'system','1','35'),('3579','Kota Batu',NULL,NULL,'system','1','35'),('3601','Kab. Pandeglang',NULL,NULL,'system','1','36'),('3602','Kab. Lebak',NULL,NULL,'system','1','36'),('3603','Kab. Tangerang',NULL,NULL,'system','1','36'),('3604','Kab. Serang',NULL,NULL,'system','1','36'),('3671','Kota Tangerang',NULL,NULL,'system','1','36'),('3672','Kota Cilegon',NULL,NULL,'system','1','36'),('3673','Kota Serang',NULL,NULL,'system','1','36'),('3674','Kota Tangerang Selatan',NULL,NULL,'system','1','36'),('5101','Kab. Jembrana',NULL,NULL,'system','1','51'),('5102','Kab. Tabanan',NULL,NULL,'system','1','51'),('5103','Kab. Badung',NULL,NULL,'system','1','51'),('5104','Kab. Gianyar',NULL,NULL,'system','1','51'),('5105','Kab. Klungkung',NULL,NULL,'system','1','51'),('5106','Kab. Bangli',NULL,NULL,'system','1','51'),('5107','Kab. Karang Asem',NULL,NULL,'system','1','51'),('5108','Kab. Buleleng',NULL,NULL,'system','1','51'),('5171','Kota Denpasar',NULL,NULL,'system','1','51'),('5201','Kab. Lombok Barat',NULL,NULL,'system','1','52'),('5202','Kab. Lombok Tengah',NULL,NULL,'system','1','52'),('5203','Kab. Lombok Timur',NULL,NULL,'system','1','52'),('5204','Kab. Sumbawa',NULL,NULL,'system','1','52'),('5205','Kab. Dompu',NULL,NULL,'system','1','52'),('5206','Kab. Bima',NULL,NULL,'system','1','52'),('5207','Kab. Sumbawa Barat',NULL,NULL,'system','1','52'),('5208','Kab. Lombok Utara',NULL,NULL,'system','1','52'),('5271','Kota Mataram',NULL,NULL,'system','1','52'),('5272','Kota Bima',NULL,NULL,'system','1','52'),('5301','Kab. Sumba Barat',NULL,NULL,'system','1','53'),('5302','Kab. Sumba Timur',NULL,NULL,'system','1','53'),('5303','Kab. Kupang',NULL,NULL,'system','1','53'),('5304','Kab. Timor Tengah Selatan',NULL,NULL,'system','1','53'),('5305','Kab. Timor Tengah Utara',NULL,NULL,'system','1','53'),('5306','Kab. Belu',NULL,NULL,'system','1','53'),('5307','Kab. Alor',NULL,NULL,'system','1','53'),('5308','Kab. Lembata',NULL,NULL,'system','1','53'),('5309','Kab. Flores Timur',NULL,NULL,'system','1','53'),('5310','Kab. Sikka',NULL,NULL,'system','1','53'),('5311','Kab. Ende',NULL,NULL,'system','1','53'),('5312','Kab. Ngada',NULL,NULL,'system','1','53'),('5313','Kab. Manggarai',NULL,NULL,'system','1','53'),('5314','Kab. Rote Ndao',NULL,NULL,'system','1','53'),('5315','Kab. Manggarai Barat',NULL,NULL,'system','1','53'),('5316','Kab. Sumba Tengah',NULL,NULL,'system','1','53'),('5317','Kab. Sumba Barat Daya',NULL,NULL,'system','1','53'),('5318','Kab. Nagekeo',NULL,NULL,'system','1','53'),('5319','Kab. Manggarai Timur',NULL,NULL,'system','1','53'),('5320','Kab. Sabu Raijua',NULL,NULL,'system','1','53'),('5371','Kota Kupang',NULL,NULL,'system','1','53'),('6101','Kab. Sambas',NULL,NULL,'system','1','61'),('6102','Kab. Bengkayang',NULL,NULL,'system','1','61'),('6103','Kab. Landak',NULL,NULL,'system','1','61'),('6104','Kab. Pontianak',NULL,NULL,'system','1','61'),('6105','Kab. Sanggau',NULL,NULL,'system','1','61'),('6106','Kab. Ketapang',NULL,NULL,'system','1','61'),('6107','Kab. Sintang',NULL,NULL,'system','1','61'),('6108','Kab. Kapuas Hulu',NULL,NULL,'system','1','61'),('6109','Kab. Sekadau',NULL,NULL,'system','1','61'),('6110','Kab. Melawi',NULL,NULL,'system','1','61'),('6111','Kab. Kayong Utara',NULL,NULL,'system','1','61'),('6112','Kab. Kubu Raya',NULL,NULL,'system','1','61'),('6171','Kota Pontianak',NULL,NULL,'system','1','61'),('6172','Kota Singkawang',NULL,NULL,'system','1','61'),('6201','Kab. Kotawaringin Barat',NULL,NULL,'system','1','62'),('6202','Kab. Kotawaringin Timur',NULL,NULL,'system','1','62'),('6203','Kab. Kapuas',NULL,NULL,'system','1','62'),('6204','Kab. Barito Selatan',NULL,NULL,'system','1','62'),('6205','Kab. Barito Utara',NULL,NULL,'system','1','62'),('6206','Kab. Sukamara',NULL,NULL,'system','1','62'),('6207','Kab. Lamandau',NULL,NULL,'system','1','62'),('6208','Kab. Seruyan',NULL,NULL,'system','1','62'),('6209','Kab. Katingan',NULL,NULL,'system','1','62'),('6210','Kab. Pulang Pisau',NULL,NULL,'system','1','62'),('6211','Kab. Gunung Mas',NULL,NULL,'system','1','62'),('6212','Kab. Barito Timur',NULL,NULL,'system','1','62'),('6213','Kab. Murung Raya',NULL,NULL,'system','1','62'),('6271','Kota Palangka Raya',NULL,NULL,'system','1','62'),('6301','Kab. Tanah Laut',NULL,NULL,'system','1','63'),('6302','Kab. Kota Baru',NULL,NULL,'system','1','63'),('6303','Kab. Banjar',NULL,NULL,'system','1','63'),('6304','Kab. Barito Kuala',NULL,NULL,'system','1','63'),('6305','Kab. Tapin',NULL,NULL,'system','1','63'),('6306','Kab. Hulu Sungai Selatan',NULL,NULL,'system','1','63'),('6307','Kab. Hulu Sungai Tengah',NULL,NULL,'system','1','63'),('6308','Kab. Hulu Sungai Utara',NULL,NULL,'system','1','63'),('6309','Kab. Tabalong',NULL,NULL,'system','1','63'),('6310','Kab. Tanah Bumbu',NULL,NULL,'system','1','63'),('6311','Kab. Balangan',NULL,NULL,'system','1','63'),('6371','Kota Banjarmasin',NULL,NULL,'system','1','63'),('6372','Kota Banjar Baru',NULL,NULL,'system','1','63'),('6401','Kab. Paser',NULL,NULL,'system','1','64'),('6402','Kab. Kutai Barat',NULL,NULL,'system','1','64'),('6403','Kab. Kutai Kartanegara',NULL,NULL,'system','1','64'),('6404','Kab. Kutai Timur',NULL,NULL,'system','1','64'),('6405','Kab. Berau',NULL,NULL,'system','1','64'),('6409','Kab. Penajam Paser Utara',NULL,NULL,'system','1','64'),('6471','Kota Balikpapan',NULL,NULL,'system','1','64'),('6472','Kota Samarinda',NULL,NULL,'system','1','64'),('6474','Kota Bontang',NULL,NULL,'system','1','64'),('6501','Kab. Malinau',NULL,NULL,'system','1','65'),('6502','Kab. Bulungan',NULL,NULL,'system','1','65'),('6503','Kab. Tana Tidung',NULL,NULL,'system','1','65'),('6504','Kab. Nunukan',NULL,NULL,'system','1','65'),('6571','Kota Tarakan',NULL,NULL,'system','1','65'),('7101','Kab. Bolaang Mongondow',NULL,NULL,'system','1','71'),('7102','Kab. Minahasa',NULL,NULL,'system','1','71'),('7103','Kab. Kepulauan Sangihe',NULL,NULL,'system','1','71'),('7104','Kab. Kepulauan Talaud',NULL,NULL,'system','1','71'),('7105','Kab. Minahasa Selatan',NULL,NULL,'system','1','71'),('7106','Kab. Minahasa Utara',NULL,NULL,'system','1','71'),('7107','Kab. Bolaang Mongondow Utara',NULL,NULL,'system','1','71'),('7108','Kab. Siau Tagulandang Biaro',NULL,NULL,'system','1','71'),('7109','Kab. Minahasa Tenggara',NULL,NULL,'system','1','71'),('7110','Kab. Bolaang Mongondow Selatan',NULL,NULL,'system','1','71'),('7111','Kab. Bolaang Mongondow Timur',NULL,NULL,'system','1','71'),('7171','Kota Manado',NULL,NULL,'system','1','71'),('7172','Kota Bitung',NULL,NULL,'system','1','71'),('7173','Kota Tomohon',NULL,NULL,'system','1','71'),('7174','Kota Kotamobagu',NULL,NULL,'system','1','71'),('7201','Kab. Banggai Kepulauan',NULL,NULL,'system','1','72'),('7202','Kab. Banggai',NULL,NULL,'system','1','72'),('7203','Kab. Morowali',NULL,NULL,'system','1','72'),('7204','Kab. Poso',NULL,NULL,'system','1','72'),('7205','Kab. Donggala',NULL,NULL,'system','1','72'),('7206','Kab. Toli-toli',NULL,NULL,'system','1','72'),('7207','Kab. Buol',NULL,NULL,'system','1','72'),('7208','Kab. Parigi Moutong',NULL,NULL,'system','1','72'),('7209','Kab. Tojo Una-una',NULL,NULL,'system','1','72'),('7210','Kab. Sigi',NULL,NULL,'system','1','72'),('7271','Kota Palu',NULL,NULL,'system','1','72'),('7301','Kab. Kepulauan Selayar',NULL,NULL,'system','1','73'),('7302','Kab. Bulukumba',NULL,NULL,'system','1','73'),('7303','Kab. Bantaeng',NULL,NULL,'system','1','73'),('7304','Kab. Jeneponto',NULL,NULL,'system','1','73'),('7305','Kab. Takalar',NULL,NULL,'system','1','73'),('7306','Kab. Gowa',NULL,NULL,'system','1','73'),('7307','Kab. Sinjai',NULL,NULL,'system','1','73'),('7308','Kab. Maros',NULL,NULL,'system','1','73'),('7309','Kab. Pangkajene Dan Kepulauan',NULL,NULL,'system','1','73'),('7310','Kab. Barru',NULL,NULL,'system','1','73'),('7311','Kab. Bone',NULL,NULL,'system','1','73'),('7312','Kab. Soppeng',NULL,NULL,'system','1','73'),('7313','Kab. Wajo',NULL,NULL,'system','1','73'),('7314','Kab. Sidenreng Rappang',NULL,NULL,'system','1','73'),('7315','Kab. Pinrang',NULL,NULL,'system','1','73'),('7316','Kab. Enrekang',NULL,NULL,'system','1','73'),('7317','Kab. Luwu',NULL,NULL,'system','1','73'),('7318','Kab. Tana Toraja',NULL,NULL,'system','1','73'),('7322','Kab. Luwu Utara',NULL,NULL,'system','1','73'),('7325','Kab. Luwu Timur',NULL,NULL,'system','1','73'),('7326','Kab. Toraja Utara',NULL,NULL,'system','1','73'),('7371','Kota Makassar',NULL,NULL,'system','1','73'),('7372','Kota Parepare',NULL,NULL,'system','1','73'),('7373','Kota Palopo',NULL,NULL,'system','1','73'),('7401','Kab. Buton',NULL,NULL,'system','1','74'),('7402','Kab. Muna',NULL,NULL,'system','1','74'),('7403','Kab. Konawe',NULL,NULL,'system','1','74'),('7404','Kab. Kolaka',NULL,NULL,'system','1','74'),('7405','Kab. Konawe Selatan',NULL,NULL,'system','1','74'),('7406','Kab. Bombana',NULL,NULL,'system','1','74'),('7407','Kab. Wakatobi',NULL,NULL,'system','1','74'),('7408','Kab. Kolaka Utara',NULL,NULL,'system','1','74'),('7409','Kab. Buton Utara',NULL,NULL,'system','1','74'),('7410','Kab. Konawe Utara',NULL,NULL,'system','1','74'),('7471','Kota Kendari',NULL,NULL,'system','1','74'),('7472','Kota Baubau',NULL,NULL,'system','1','74'),('7501','Kab. Boalemo',NULL,NULL,'system','1','75'),('7502','Kab. Gorontalo',NULL,NULL,'system','1','75'),('7503','Kab. Pohuwato',NULL,NULL,'system','1','75'),('7504','Kab. Bone Bolango',NULL,NULL,'system','1','75'),('7505','Kab. Gorontalo Utara',NULL,NULL,'system','1','75'),('7571','Kota Gorontalo',NULL,NULL,'system','1','75'),('7601','Kab. Majene',NULL,NULL,'system','1','76'),('7602','Kab. Polewali Mandar',NULL,NULL,'system','1','76'),('7603','Kab. Mamasa',NULL,NULL,'system','1','76'),('7604','Kab. Mamuju',NULL,NULL,'system','1','76'),('7605','Kab. Mamuju Utara',NULL,NULL,'system','1','76'),('8101','Kab. Maluku Tenggara Barat',NULL,NULL,'system','1','81'),('8102','Kab. Maluku Tenggara',NULL,NULL,'system','1','81'),('8103','Kab. Maluku Tengah',NULL,NULL,'system','1','81'),('8104','Kab. Buru',NULL,NULL,'system','1','81'),('8105','Kab. Kepulauan Aru',NULL,NULL,'system','1','81'),('8106','Kab. Seram Bagian Barat',NULL,NULL,'system','1','81'),('8107','Kab. Seram Bagian Timur',NULL,NULL,'system','1','81'),('8108','Kab. Maluku Barat Daya',NULL,NULL,'system','1','81'),('8109','Kab. Buru Selatan',NULL,NULL,'system','1','81'),('8171','Kota Ambon',NULL,NULL,'system','1','81'),('8172','Kota Tual',NULL,NULL,'system','1','81'),('8201','Kab. Halmahera Barat',NULL,NULL,'system','1','82'),('8202','Kab. Halmahera Tengah',NULL,NULL,'system','1','82'),('8203','Kab. Kepulauan Sula',NULL,NULL,'system','1','82'),('8204','Kab. Halmahera Selatan',NULL,NULL,'system','1','82'),('8205','Kab. Halmahera Utara',NULL,NULL,'system','1','82'),('8206','Kab. Halmahera Timur',NULL,NULL,'system','1','82'),('8207','Kab. Pulau Morotai',NULL,NULL,'system','1','82'),('8271','Kota Ternate',NULL,NULL,'system','1','82'),('8272','Kota Tidore Kepulauan',NULL,NULL,'system','1','82'),('9101','Kab. Fakfak',NULL,NULL,'system','1','91'),('9102','Kab. Kaimana',NULL,NULL,'system','1','91'),('9103','Kab. Teluk Wondama',NULL,NULL,'system','1','91'),('9104','Kab. Teluk Bintuni',NULL,NULL,'system','1','91'),('9105','Kab. Manokwari',NULL,NULL,'system','1','91'),('9106','Kab. Sorong Selatan',NULL,NULL,'system','1','91'),('9107','Kab. Sorong',NULL,NULL,'system','1','91'),('9108','Kab. Raja Ampat',NULL,NULL,'system','1','91'),('9109','Kab. Tambrauw',NULL,NULL,'system','1','91'),('9110','Kab. Maybrat',NULL,NULL,'system','1','91'),('9171','Kota Sorong',NULL,NULL,'system','1','91'),('9401','Kab. Merauke',NULL,NULL,'system','1','94'),('9402','Kab. Jayawijaya',NULL,NULL,'system','1','94'),('9403','Kab. Jayapura',NULL,NULL,'system','1','94'),('9404','Kab. Nabire',NULL,NULL,'system','1','94'),('9408','Kab. Kepulauan Yapen',NULL,NULL,'system','1','94'),('9409','Kab. Biak Numfor',NULL,NULL,'system','1','94'),('9410','Kab. Paniai',NULL,NULL,'system','1','94'),('9411','Kab. Puncak Jaya',NULL,NULL,'system','1','94'),('9412','Kab. Mimika',NULL,NULL,'system','1','94'),('9413','Kab. Boven Digoel',NULL,NULL,'system','1','94'),('9414','Kab. Mappi',NULL,NULL,'system','1','94'),('9415','Kab. Asmat',NULL,NULL,'system','1','94'),('9416','Kab. Yahukimo',NULL,NULL,'system','1','94'),('9417','Kab. Pegunungan Bintang',NULL,NULL,'system','1','94'),('9418','Kab. Tolikara',NULL,NULL,'system','1','94'),('9419','Kab. Sarmi',NULL,NULL,'system','1','94'),('9420','Kab. Keerom',NULL,NULL,'system','1','94'),('9426','Kab. Waropen',NULL,NULL,'system','1','94'),('9427','Kab. Supiori',NULL,NULL,'system','1','94'),('9428','Kab. Mamberamo Raya',NULL,NULL,'system','1','94'),('9429','Kab. Nduga',NULL,NULL,'system','1','94'),('9430','Kab. Lanny Jaya',NULL,NULL,'system','1','94'),('9431','Kab. Mamberamo Tengah',NULL,NULL,'system','1','94'),('9432','Kab. Yalimo',NULL,NULL,'system','1','94'),('9433','Kab. Puncak',NULL,NULL,'system','1','94'),('9434','Kab. Dogiyai',NULL,NULL,'system','1','94'),('9435','Kab. Intan Jaya',NULL,NULL,'system','1','94'),('9436','Kab. Deiyai',NULL,NULL,'system','1','94'),('9471','Kota Jayapura',NULL,NULL,'system','1','94');
/*!40000 ALTER TABLE `1nt3x_kabupaten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_log`
--

DROP TABLE IF EXISTS `1nt3x_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log_user` varchar(50) DEFAULT NULL,
  `log_kegiatan` varchar(50) DEFAULT NULL,
  `log_waktu` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_log`
--

LOCK TABLES `1nt3x_log` WRITE;
/*!40000 ALTER TABLE `1nt3x_log` DISABLE KEYS */;
INSERT INTO `1nt3x_log` VALUES (34,'admin','Rubah data rak 001','2015-10-28 16:29:47'),(35,'admin','Rubah data rak 001','2015-10-28 16:30:11'),(36,'intex_kasir','Simpan data POS','2015-10-28 16:36:36'),(37,'intex_kasir','Simpan data POS','2015-10-28 16:38:45'),(38,'intex_kasir','Simpan data POS','2015-10-28 16:49:55'),(39,'intex_kasir','Simpan data POS','2015-10-28 16:54:40'),(40,'intex_kasir','Simpan data POS','2015-10-28 16:54:57'),(41,'intex_kasir','Simpan data POS','2015-10-28 16:56:42'),(42,'intex_kasir','Simpan data POS','2015-10-28 16:57:45'),(43,'intex_kasir','Simpan data POS','2015-10-28 16:59:22'),(44,'intex_kasir','Simpan data POS','2015-10-28 17:08:36'),(45,'intex_kasir','Simpan data POS','2015-10-28 17:09:04'),(46,'intex_kasir','Simpan data POS','2015-10-28 17:10:33'),(47,'intex_kasir','Simpan data POS','2015-10-28 17:11:50'),(48,'intex_kasir','Simpan data POS','2015-10-28 17:14:13'),(49,'intex_kasir','Simpan data POS','2015-10-28 17:14:41'),(50,'intex_kasir','Simpan data POS','2015-10-28 17:15:48'),(51,'intex_kasir','Simpan data POS','2015-10-28 17:16:40'),(52,'intex_kasir','Simpan data POS','2015-10-28 17:17:00'),(53,'intex_kasir','Simpan data POS','2015-10-29 11:58:56'),(54,'intex_kasir','Simpan data POS','2015-10-29 13:59:14'),(55,'intex_kasir','Simpan data POS','2015-10-29 14:37:44'),(56,'intex_kasir','Simpan data POS','2015-10-29 14:38:53'),(57,'intex_kasir','Simpan data POS','2015-10-29 14:40:00'),(58,'intex_kasir','Simpan data POS','2015-10-29 14:53:41'),(59,'intex_kasir','Simpan data POS','2015-10-29 15:22:35'),(60,'intex_kasir','Simpan data POS','2015-11-02 09:53:20'),(61,'admin','Simpan data POS toko ABC/11/2015/00036','2015-11-02 11:27:34'),(62,'intex_kasir','Simpan data POS','2015-11-03 10:20:42'),(63,'intex_kasir','Simpan data POS','2015-11-03 10:25:11'),(64,'admin','Rubah data toko ABC','2015-11-03 10:29:46'),(65,'intex_kasir','Simpan data POS','2015-11-03 10:31:04'),(66,'nugraha','Simpan data harga promo 1','2015-11-03 10:57:44'),(67,'nugraha','Simpan data POS toko KR/11/2015/00001','2015-11-03 10:58:11'),(68,'intex_kasir','Simpan data POS','2015-11-03 10:59:58'),(69,'admin','Simpan data harga promo 2','2015-11-05 10:20:55'),(70,'admin','Rubah data harga promo 2','2015-11-05 10:21:16'),(71,'admin','Rubah data harga promo 2','2015-11-05 10:21:32'),(72,'admin','Rubah data harga promo 2','2015-11-05 10:27:39'),(73,'admin','Rubah data harga promo 2','2015-11-05 10:27:51'),(74,'admin','Simpan data POS toko ABC/11/2015/00048','2015-11-05 10:35:13'),(75,'intex_kasir','Simpan data POS','2015-11-05 11:45:17'),(76,'intex_kasir','Simpan data POS','2015-11-05 16:19:19'),(77,'admin','Rubah data persediaan awal PA/102015/00001','2015-11-05 17:11:55'),(78,'admin','Simpan data Transfer Barang TF/ABC/Array/112015/00','2015-11-11 16:43:39'),(79,'admin','Rubah data Transfer Barang TF/ABC/Array/112015/000','2015-11-11 16:49:38'),(80,'admin','Hapus data Transfer BarangTF/ABC/Array/112015/0000','2015-11-11 16:49:47'),(81,'admin','Simpan data Transfer Barang TF/ABC/2015-11-11/1120','2015-11-11 16:50:32'),(82,'admin','Hapus data Transfer BarangTF/ABC/2015-11-11/112015','2015-11-11 16:53:35'),(83,'admin','Simpan data Transfer Barang TF/ABC/KR/112015/00001','2015-11-11 16:56:02'),(84,'nugraha','Simpan data PO PO/112015/00001','2015-11-16 11:20:18'),(85,'nugraha','Simpan data Transfer Barang TF/KR/ABC/112015/00002','2015-11-16 12:03:47'),(86,'admin','Simpan data Transfer Barang PB/112015/00001','2015-11-16 13:44:38'),(87,'admin','Rubah data Transfer Barang PB/112015/00001','2015-11-16 13:54:01'),(88,'admin','Rubah data Transfer Barang PB/112015/00001','2015-11-16 13:57:53'),(89,'admin','Rubah data Transfer Barang PB/112015/00001','2015-11-16 13:58:53'),(90,'admin','Simpan data Transfer Barang PB/112015/00002','2015-11-16 14:07:33'),(91,'admin','Simpan data Transfer Barang PB/112015/00003','2015-11-16 14:08:15'),(92,'admin','Simpan data Transfer Barang PB/112015/00004','2015-11-16 14:08:40'),(93,'admin','Hapus data Penerimaan BarangPB/112015/00004','2015-11-16 14:10:30'),(94,'admin','Hapus data Penerimaan BarangPB/112015/00003','2015-11-16 14:10:51'),(95,'admin','Simpan data Transfer Barang PB/112015/00003','2015-11-16 14:11:10'),(96,'admin','Simpan data Transfer Barang PB/112015/00004','2015-11-16 14:11:26'),(97,'nugraha','Simpan data Transfer Barang PB/112015/00005','2015-11-16 14:16:57'),(98,'nugraha','Simpan data persediaan awal PA/112015/00002','2015-11-16 14:18:31'),(99,'nugraha','Rubah data persediaan awal PA/112015/00002','2015-11-16 14:25:17'),(100,'nugraha','Logout dari system','2015-11-16 14:32:04'),(101,'admin','Logout dari system','2015-11-16 14:35:07'),(102,'admin','Logout dari system','2015-11-16 14:37:15'),(103,'intex_kasir','Logout dari system','2015-11-23 15:09:52'),(104,'admin','Simpan data toko koa','2015-11-25 10:16:16'),(105,'admin','Simpan data toko dsa','2015-11-25 10:17:03'),(106,'admin','Logout dari system','2015-11-25 10:19:56'),(107,'luthfi','Logout dari system','2015-11-25 10:20:30'),(108,'admin','Logout dari system','2015-11-25 10:21:17'),(109,'luthfi','Logout dari system','2015-11-25 10:21:58'),(110,'admin','Logout dari system','2015-11-25 10:30:40'),(111,'asad','Logout dari system','2015-11-25 10:31:00'),(112,'admin','Logout dari system','2015-11-25 10:32:09'),(113,'asad','Logout dari system','2015-11-25 10:43:17'),(114,'bayu','Logout dari system','2015-11-25 10:43:28'),(115,'admin','Logout dari system','2015-11-25 10:44:39'),(116,'bayu','Simpan data POS toko KR/11/2015/00001','2015-11-25 10:53:33'),(117,'sharif','Simpan data POS toko KP/11/2015/00001','2015-11-25 10:54:30'),(118,'bayu','Simpan data POS toko KR/11/2015/00002','2015-11-25 10:54:46'),(119,'sharif','Simpan data POS toko KP/11/2015/00002','2015-11-25 10:55:33'),(120,'sharif','Simpan data POS toko KP/11/2015/00003','2015-11-25 10:55:37'),(121,'sharif','Simpan data POS toko KP/11/2015/00004','2015-11-25 10:55:47'),(122,'sharif','Simpan data POS toko KP/11/2015/00005','2015-11-25 10:57:24'),(123,'luthfi','Simpan data POS toko ABC/11/2015/00002','2015-11-25 10:58:40'),(124,'bayu','Simpan data POS toko KR/11/2015/00003','2015-11-25 11:01:44'),(125,'vredy','Simpan data POS toko dsa/11/2015/00001','2015-11-25 11:02:39'),(126,'vredy','Logout dari system','2015-11-25 11:04:02'),(127,'asad','Logout dari system','2015-11-25 11:04:57'),(128,'admin','Logout dari system','2015-11-25 11:14:31'),(129,'admin','Logout dari system','2015-11-25 11:14:43'),(130,'asad','Logout dari system','2015-11-25 11:29:09'),(131,'bayu','Logout dari system','2015-11-25 11:53:57'),(132,'intex_kasir','Logout dari system','2015-12-10 15:25:12'),(133,'intex_kasir','Logout dari system','2015-12-10 16:50:09'),(134,'intex_kasir','Logout dari system','2015-12-10 17:01:28'),(135,'intex_kasir','Logout dari system','2015-12-11 09:34:43'),(136,'admin','Logout dari system','2015-12-11 10:27:47'),(137,'admin','Logout dari system','2015-12-11 10:29:45'),(138,'intex_kasir','Logout dari system','2015-12-11 16:59:16'),(139,'intex_kasir','Logout dari system','2015-12-11 20:10:17'),(140,'admin','Simpan data POS toko ABC/12/2015/00004','2015-12-14 09:51:01'),(141,'intex_kasir','Logout dari system','2015-12-14 10:51:52'),(142,'intex_kasir','Logout dari system','2015-12-14 14:56:12'),(143,'admin','Simpan data merek 5','2015-12-14 16:16:13'),(144,'admin','Simpan data merek 6','2015-12-14 16:30:16'),(145,'admin','Simpan data merek 7','2015-12-14 16:31:02'),(146,'admin','Simpan data merek 8','2015-12-14 16:31:38'),(147,'admin','Simpan data item 0014','2015-12-14 16:38:56'),(148,'admin','Simpan data item 0015','2015-12-14 16:39:32'),(149,'admin','Simpan data item 0016','2015-12-14 16:39:57'),(150,'admin','Simpan data persediaan awal PA/122015/00003','2015-12-14 16:41:22'),(151,'admin','Simpan data list harga 0014','2015-12-14 16:42:56'),(152,'admin','Simpan data list harga 0015','2015-12-14 16:43:11'),(153,'admin','Simpan data list harga 0016','2015-12-14 16:43:19'),(154,'admin','Simpan data POS toko ABC/12/2015/00013','2015-12-14 16:43:50'),(155,'admin','Simpan data POS toko ABC/12/2015/00014','2015-12-14 16:44:30'),(156,'intex_kasir','Logout dari system','2015-12-14 17:11:54'),(157,'intex_kasir','Logout dari system','2015-12-14 17:11:54'),(158,'intex_kasir','Logout dari system','2015-12-14 17:57:05'),(159,'admin','Simpan data persediaan awal PA/122010/00004','2015-12-14 18:10:22'),(160,'intex_kasir','Logout dari system','2015-12-14 21:14:46'),(161,'intex_kasir','Logout dari system','2015-12-15 08:10:46'),(162,'intex_kasir','Logout dari system','2015-12-15 10:06:23'),(163,'admin','Logout dari system','2015-12-15 11:45:04'),(164,'admin','Rubah data harga promo 1','2015-12-16 11:25:50'),(165,'admin','Simpan data POS toko ABC/12/2015/00025','2015-12-16 11:27:20'),(166,'admin','Simpan data POS toko ABC/12/2015/00026','2015-12-16 11:27:43'),(167,'admin','Simpan data POS toko ABC/12/2015/00027','2015-12-16 11:28:10'),(168,'intex_kasir','Simpan data POS toko ABC/12/2015/00028','2015-12-17 19:59:12'),(169,'intex_kasir','Logout dari system','2015-12-23 09:12:56');
/*!40000 ALTER TABLE `1nt3x_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_m_bank`
--

DROP TABLE IF EXISTS `1nt3x_m_bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_m_bank` (
  `m_bank_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_bank_nama` varchar(75) DEFAULT NULL,
  `m_bank_ket` text,
  `m_bank_tgl_buat` date DEFAULT NULL,
  `m_bank_tgl_ubah` date DEFAULT NULL,
  `m_bank_petugas` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`m_bank_id`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_m_bank`
--

LOCK TABLES `1nt3x_m_bank` WRITE;
/*!40000 ALTER TABLE `1nt3x_m_bank` DISABLE KEYS */;
INSERT INTO `1nt3x_m_bank` VALUES (1,'Bank Negara Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(2,'Bank Rakyat Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(3,'Bank Tabungan Negara','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(4,'Bank Mandiri','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(5,'Bank Mutiara','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(6,'Bank Agroniaga','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(7,'Bank Anda','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(8,'Bank Artha Graha Internatiional','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(9,'Bank Bukopin','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(10,'Bank Bumi Arta','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(11,'Bank Capital Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(12,'Bank Central Asia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(13,'Bank CIMB Niaga','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(14,'Bank Danamon','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(15,'Bank Ekonomi Raharja','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(16,'Bank Ganesha','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(17,'Bank Hana','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(18,'Bank Himpunan Saudara 1906','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(19,'Bank ICB Bumiputera','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(20,'Bank ICBC Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(21,'Bank Index Selindo','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(22,'Bank Internasional Maybank','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(23,'Bank Kesawan','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(24,'Bank Maspion','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(25,'Bank Mayapada','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(26,'Bank Mega','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(27,'Bank Mestika Dharma','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(28,'Bank Metro Express','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(29,'Bank Muamalat indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(30,'Bank Nusantara Parahyangan','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(31,'Bank OCBC NISP','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(32,'Bank Permata','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(33,'Bank SBI Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(34,'Bank Sinarmas','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(35,'Bank Swadesi','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(36,'Bank Syariah Mandiri','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(37,'Bank Syariah Mega Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(38,'Bank Victoria Internasional','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(39,'Bank Pan Indonesia Bank','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(40,'Bank Anglomas Internasional Bank','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(41,'Bank Andara','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(42,'Bank Artos','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(43,'Bank Barclays Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(44,'Bank Bisnis Internasional','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(45,'Bank BRI Syariah','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(46,'Bank Central Asia Syariah','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(47,'Bank Dipo Internasional','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(48,'Bank Fama Internasional','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(49,'Bank Ina Perdana','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(50,'Bank Jasa Jakarta','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(51,'Bank Kesehatan Ekonomi','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(52,'Bank Liman Internasional','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(53,'Bank Mayora','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(54,'Bank ANZ Panin Bank','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(55,'Bank Agris','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(56,'Bank Commonwealth','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(57,'Bank BNP Paribas Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(58,'Bank DBS Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(59,'Bank KEB Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(60,'Bank Maybank Syariah Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(61,'Bank Mizuho Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(62,'Bank UOB Buana','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(63,'Bank Rabobank Internasional Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(64,'Bank Resona Perdania','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(65,'Bank Windu Kentjana Internasional','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(66,'Bank Woori Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(67,'Bank Sumitomo Mitsui Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(68,'Bank UFJ Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(69,'Bank Mitraniaga','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(70,'Bank Multi Arta Sentosa','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(71,'Bank Nationalnobu','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(72,'Bank Pundi Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(73,'Bank Purba Danarta','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(74,'Bank Royal Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(75,'Bank Sinar Harapan Bali','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(76,'Bank STMIK Binamulia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(77,'Bank Syariah Bukopin','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(78,'Bank Tabungan Pensiunan Nasional','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(79,'Bank Victoria Syariah','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(80,'Bank Yudha Bhakti','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(81,'Bank Centratama Nasional Bank','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(82,'Bank Pan Indonesia Bank Syariah','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(83,'Bank Prima Master Bank','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(84,'Bank Jambi','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(85,'Bank Kalsel','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(86,'Bank Kaltim','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(87,'Bank Sultr','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(88,'Bank BPD DIY','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(89,'Bank Nagar','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(90,'Bank DKI','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(91,'Bank Lampung','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(92,'Bank Kalteng','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(93,'Bank BPD Aceh','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(94,'Bank Sulse','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(95,'Bank BJB','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(96,'Bank Kalbar','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(97,'Bank Maluku','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(98,'Bank Bengkulu','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(99,'Bank Jateng','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(100,'Bank Jatim','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(101,'Bank NTB','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(102,'Bank NTT','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(103,'Bank Sulteng','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(104,'Bank Sulut','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(105,'Bank BPD Bali','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(106,'Bank Papua','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(107,'Bank Riau Kepri','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(108,'Bank Sumsel Babel','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(109,'Bank Sumut','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(110,'Bank BNI Syariah','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(111,'Bank BRI Syariah','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(112,'Bank Maybank Syariah Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(113,'Bank Mega Syariah Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(114,'Bank Muamalat Indonesia','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(115,'Bank Syariah Bukopin','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(116,'Bank Syariah Mandiri','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(117,'Bank Victoria Syariah','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(118,'Bank Pan Indonesia Bank Syariah','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(119,'Bank CIMB Niaga Syariah','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(120,'Bank OCBC NISP Syariah','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(121,'Bank Danamon Syariah','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(122,'Bank Riau Kepri Syariah','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(123,'Bank BCA Syariah','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(124,'Bank BJB Syariah','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin'),(125,'Bank Permata Syariah','Salah satu bank di indonesia','2015-09-21','2015-09-21','Admin');
/*!40000 ALTER TABLE `1nt3x_m_bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_m_harga`
--

DROP TABLE IF EXISTS `1nt3x_m_harga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_m_harga` (
  `m_harga_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_harga_beli` double NOT NULL,
  `m_harga_jual` double NOT NULL,
  `m_harga_tgl_buat` date NOT NULL,
  `m_harga_tgl_ubah` date NOT NULL,
  `m_harga_petugas` varchar(50) NOT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`m_harga_id`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_m_harga_ibfk_1` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_m_harga`
--

LOCK TABLES `1nt3x_m_harga` WRITE;
/*!40000 ALTER TABLE `1nt3x_m_harga` DISABLE KEYS */;
INSERT INTO `1nt3x_m_harga` VALUES (7,1000,2000,'2015-10-22','0000-00-00','admin','0012'),(8,1500,3000,'2015-10-22','0000-00-00','admin','0013'),(9,9500,9900,'2015-10-28','0000-00-00','admin','0010'),(10,18500,19000,'2015-10-28','0000-00-00','admin','0011'),(11,4000,5000,'2015-12-14','0000-00-00','admin','0014'),(12,7000,8000,'2015-12-14','0000-00-00','admin','0015'),(13,9000,10000,'2015-12-14','0000-00-00','admin','0016');
/*!40000 ALTER TABLE `1nt3x_m_harga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_m_harga_promo`
--

DROP TABLE IF EXISTS `1nt3x_m_harga_promo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_m_harga_promo` (
  `m_harga_promo_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_harga_promo_harga` double NOT NULL,
  `m_harga_promo_periode_mulai` date NOT NULL,
  `m_harga_promo_periode_akhir` date NOT NULL,
  `m_harga_promo_qty_promo` int(11) NOT NULL,
  `m_harga_promo_tgl_buat` date NOT NULL,
  `m_harga_promo_tgl_ubah` date NOT NULL,
  `m_harga_promo_petugas` varchar(50) NOT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`m_harga_promo_id`),
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_m_harga_promo_ibfk_1` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_m_harga_promo`
--

LOCK TABLES `1nt3x_m_harga_promo` WRITE;
/*!40000 ALTER TABLE `1nt3x_m_harga_promo` DISABLE KEYS */;
INSERT INTO `1nt3x_m_harga_promo` VALUES (1,1500,'2015-11-01','2015-12-31',2,'2015-11-03','2015-12-16','admin','0010'),(2,2000,'2015-11-01','2015-11-30',3,'2015-11-05','2015-11-05','admin','0012');
/*!40000 ALTER TABLE `1nt3x_m_harga_promo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_m_harga_promo_detail`
--

DROP TABLE IF EXISTS `1nt3x_m_harga_promo_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_m_harga_promo_detail` (
  `m_harga_promo_id` int(11) DEFAULT '0',
  `m_toko_kode` varchar(15) DEFAULT NULL,
  KEY `m_toko_kode` (`m_toko_kode`),
  KEY `m_harga_promo_id` (`m_harga_promo_id`),
  CONSTRAINT `1nt3x_m_harga_promo_detail_ibfk_2` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_harga_promo_detail_ibfk_3` FOREIGN KEY (`m_harga_promo_id`) REFERENCES `1nt3x_m_harga_promo` (`m_harga_promo_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_m_harga_promo_detail`
--

LOCK TABLES `1nt3x_m_harga_promo_detail` WRITE;
/*!40000 ALTER TABLE `1nt3x_m_harga_promo_detail` DISABLE KEYS */;
INSERT INTO `1nt3x_m_harga_promo_detail` VALUES (2,'ABC'),(2,'KP'),(2,'KR'),(1,'ABC'),(1,'KP'),(1,'KR');
/*!40000 ALTER TABLE `1nt3x_m_harga_promo_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_m_item`
--

DROP TABLE IF EXISTS `1nt3x_m_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_m_item` (
  `m_item_kode` varchar(35) NOT NULL DEFAULT '0',
  `m_item_barcode` varchar(35) DEFAULT NULL,
  `m_item_nama` varchar(35) NOT NULL,
  `m_item_ket` text,
  `m_item_gambar` varchar(35) DEFAULT NULL,
  `m_item_tgl_buat` date NOT NULL,
  `m_item_tgl_ubah` date NOT NULL,
  `m_item_petugas` varchar(50) NOT NULL,
  `m_item_merk_kode` varchar(15) DEFAULT NULL,
  `m_item_tipe_kode` varchar(15) DEFAULT NULL,
  `m_item_jenis_kode` varchar(15) DEFAULT NULL,
  `m_item_kategori_kode` varchar(15) DEFAULT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  `m_rak_kode` varchar(15) DEFAULT NULL,
  `m_satuan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`m_item_kode`),
  KEY `m_item_merk_kode` (`m_item_merk_kode`),
  KEY `m_item_jenis_kode` (`m_item_jenis_kode`),
  KEY `m_item_kategori_kode` (`m_item_kategori_kode`),
  KEY `m_toko_kode` (`m_toko_kode`),
  KEY `m_rak_kode` (`m_rak_kode`),
  KEY `m_satuan_id` (`m_satuan_id`),
  CONSTRAINT `1nt3x_m_item_ibfk_1` FOREIGN KEY (`m_item_merk_kode`) REFERENCES `1nt3x_m_item_merk` (`m_item_merk_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_item_ibfk_2` FOREIGN KEY (`m_item_jenis_kode`) REFERENCES `1nt3x_m_item_jenis` (`m_item_jenis_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_item_ibfk_3` FOREIGN KEY (`m_item_kategori_kode`) REFERENCES `1nt3x_m_item_kategori` (`m_item_kategori_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_item_ibfk_4` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_item_ibfk_5` FOREIGN KEY (`m_rak_kode`) REFERENCES `1nt3x_m_rak` (`m_rak_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_item_ibfk_6` FOREIGN KEY (`m_satuan_id`) REFERENCES `1nt3x_m_satuan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_m_item`
--

LOCK TABLES `1nt3x_m_item` WRITE;
/*!40000 ALTER TABLE `1nt3x_m_item` DISABLE KEYS */;
INSERT INTO `1nt3x_m_item` VALUES ('0010','8992695203441','Obat Tetes Mata','',NULL,'2015-10-28','0000-00-00','admin','4',NULL,'20','2','ABC','001',1),('0011','8887549436963','Batu baterai A2 Evolta','',NULL,'2015-10-28','0000-00-00','admin','2',NULL,'35','6','ABC','001',1),('0012','8996001440124','Energen Vanilla','',NULL,'2015-10-22','0000-00-00','admin','1',NULL,'1','1','ABC','001',1),('0013','8998866200745','Top Kopi','',NULL,'2015-10-22','0000-00-00','admin','1',NULL,'1','1','ABC','001',1),('0014','12321313','madurasa','',NULL,'2015-12-14','0000-00-00','admin','6',NULL,'7','1','ABC','001',1),('0015','12313123123','budy jam','',NULL,'2015-12-14','0000-00-00','admin','7',NULL,'7','1','ABC','001',1),('0016','12313123','ceres','',NULL,'2015-12-14','0000-00-00','admin','6',NULL,'7','1','ABC','001',1);
/*!40000 ALTER TABLE `1nt3x_m_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_m_item_jenis`
--

DROP TABLE IF EXISTS `1nt3x_m_item_jenis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_m_item_jenis` (
  `m_item_jenis_kode` varchar(15) NOT NULL DEFAULT '0',
  `m_item_jenis_nama` varchar(35) DEFAULT NULL,
  `m_item_jenis_ket` text,
  `m_item_jenis_tgl_buat` date DEFAULT NULL,
  `m_item_jenis_tgl_ubah` date DEFAULT NULL,
  `m_item_jenis_petugas` varchar(50) DEFAULT NULL,
  `m_item_kategori_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`m_item_jenis_kode`),
  KEY `m_item_kategori_kode` (`m_item_kategori_kode`),
  CONSTRAINT `1nt3x_m_item_jenis_ibfk_1` FOREIGN KEY (`m_item_kategori_kode`) REFERENCES `1nt3x_m_item_kategori` (`m_item_kategori_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_m_item_jenis`
--

LOCK TABLES `1nt3x_m_item_jenis` WRITE;
/*!40000 ALTER TABLE `1nt3x_m_item_jenis` DISABLE KEYS */;
INSERT INTO `1nt3x_m_item_jenis` VALUES ('1','makanan instan','makanan instan','2015-09-22',NULL,'admin','1'),('10','biskuit dan wafer','biskuit dan wafer','2015-09-22',NULL,'admin','1'),('11','permen, okelat dan jelly','permen, okelat dan jelly','2015-09-22',NULL,'admin','1'),('12','susu','susu','2015-09-22',NULL,'admin','1'),('13','minuman ringan','minuman ringan','2015-09-22',NULL,'admin','1'),('14','perawatan rambut','perawatan rambut','2015-09-22',NULL,'admin','2'),('15','perawatan wajah dan kosmetik','perawatan wajah dan kosmetik','2015-09-22',NULL,'admin','2'),('16','perawatan gigi dan mulut','perawatan gigi dan mulut','2015-09-22',NULL,'admin','2'),('17','perawatan kulit dan tubuh','perawatan kulit dan tubuh','2015-09-22',NULL,'admin','2'),('18','alat kesehatan dan kecantikan','alat kesehatan dan kecantikan','2015-09-22',NULL,'admin','2'),('19','pembalut dan popok dewasa','pembalut dan popok dewasa','2015-09-22',NULL,'admin','2'),('2','makanan kaleng','makanan kaleng','2015-09-22',NULL,'admin','1'),('20','obat dan vitamin','obat dan vitamin','2015-09-22',NULL,'admin','2'),('21','perawatan dan pembersih','perawatan dan pembersih','2015-09-22',NULL,'admin','3'),('22','laundry dan perawatan kain','laundry dan perawatan kain','2015-09-22',NULL,'admin','3'),('23','kamar mandi','kamar mandi','2015-09-22',NULL,'admin','3'),('24','handuk','handuk','2015-09-22',NULL,'admin','3'),('25','alat tulis dan perlengkapan kantor','alat tulis dan perlengkapan kantor','2015-09-22',NULL,'admin','3'),('26','perkakas dapur','perkakas dapur','2015-09-22',NULL,'admin','3'),('27','perlengkapan listrik dan lampu','perlengkapan listrik dan lampu','2015-09-22',NULL,'admin','3'),('28','susu formula dan makanan bayi','susu formula dan makanan bayi','2015-09-22',NULL,'admin','4'),('29','popok','popok','2015-09-22',NULL,'admin','4'),('3','bahan masakan','bahan masakan','2015-09-22',NULL,'admin','1'),('30','botol susu dan perlengkapan bayi','botol susu dan perlengkapan bayi','2015-09-22',NULL,'admin','4'),('31','perawatan kulit dan mandi bayi','perawatan kulit dan mandi bayi','2015-09-22',NULL,'admin','4'),('32','otomoti','otomoti','2015-09-22',NULL,'admin','5'),('33','fashion','fashion','2015-09-22',NULL,'admin','5'),('34','mainan','mainan','2015-09-22',NULL,'admin','5'),('35','perlengkapan elektronik','perlengkapan elektronik','2015-09-22',NULL,'admin','6'),('36','handphone','handphone','2015-09-22',NULL,'admin','6'),('37','aksesoris gadget','aksesoris gadget','2015-09-22',NULL,'admin','6'),('4','rokok dan tembakau','rokok dan tembakau','2015-09-22',NULL,'admin','1'),('5','kopi, teh dan minuman bubuk','kopi, teh dan minuman bubuk','2015-09-22',NULL,'admin','1'),('6','makanan segar','makanan segar','2015-09-22',NULL,'admin','1'),('7','sarapan','sarapan','2015-09-22',NULL,'admin','1'),('8','snack','snack','2015-09-22',NULL,'admin','1'),('9','perawatan pria','perawatan pria','2015-09-22',NULL,'admin','2');
/*!40000 ALTER TABLE `1nt3x_m_item_jenis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_m_item_kategori`
--

DROP TABLE IF EXISTS `1nt3x_m_item_kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_m_item_kategori` (
  `m_item_kategori_kode` varchar(15) NOT NULL DEFAULT '0',
  `m_item_kategori_nama` varchar(35) DEFAULT NULL,
  `m_item_kategori_ket` text,
  `m_item_kategori_tgl_buat` date DEFAULT NULL,
  `m_item_kategori_tgl_ubah` date DEFAULT NULL,
  `m_item_kategori_petugas` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`m_item_kategori_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_m_item_kategori`
--

LOCK TABLES `1nt3x_m_item_kategori` WRITE;
/*!40000 ALTER TABLE `1nt3x_m_item_kategori` DISABLE KEYS */;
INSERT INTO `1nt3x_m_item_kategori` VALUES ('1','Makanan dan minuman','makanan dan minuman','2015-09-22','2015-09-22','admin'),('2','Kesehatan dan kecantikan','Kesehatan dan kecantikan','2015-09-22','2015-09-22','admin'),('3','perlengkapan rumah','perlengkapan rumah','2015-09-22','2015-09-22','admin'),('4','bayi dan anak','bayi dan anak','2015-09-22','2015-09-22','admin'),('5','hoby dan lifestyle','hoby dan lifestyle','2015-09-22','2015-09-22','admin'),('6','Gadget dan elektronik','Gadget dan elektronik','2015-09-22','2015-09-22','admin');
/*!40000 ALTER TABLE `1nt3x_m_item_kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_m_item_merk`
--

DROP TABLE IF EXISTS `1nt3x_m_item_merk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_m_item_merk` (
  `m_item_merk_kode` varchar(15) NOT NULL DEFAULT '0',
  `m_item_merk_nama` varchar(35) DEFAULT NULL,
  `m_item_merk_ket` text,
  `m_item_merk_tgl_buat` date DEFAULT NULL,
  `m_item_merk_tgl_ubah` date DEFAULT NULL,
  `m_item_merk_petugas` varchar(50) DEFAULT NULL,
  `m_item_jenis_kode` varchar(15) DEFAULT NULL,
  `m_item_kategori_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`m_item_merk_kode`),
  KEY `m_item_jenis_kode` (`m_item_jenis_kode`),
  KEY `m_item_kategori_kode` (`m_item_kategori_kode`),
  CONSTRAINT `1nt3x_m_item_merk_ibfk_1` FOREIGN KEY (`m_item_jenis_kode`) REFERENCES `1nt3x_m_item_jenis` (`m_item_jenis_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_item_merk_ibfk_2` FOREIGN KEY (`m_item_kategori_kode`) REFERENCES `1nt3x_m_item_kategori` (`m_item_kategori_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_m_item_merk`
--

LOCK TABLES `1nt3x_m_item_merk` WRITE;
/*!40000 ALTER TABLE `1nt3x_m_item_merk` DISABLE KEYS */;
INSERT INTO `1nt3x_m_item_merk` VALUES ('1','Wings Food','tes','2015-09-23',NULL,'admin','1','1'),('2','Panasonic','','2015-10-02',NULL,'admin','35','6'),('3','Artline','spidol','2015-10-02',NULL,'admin','35','6'),('4','Insto reguler','Obat tetes mata 7.5 ml','2015-10-28',NULL,'admin','20','2'),('5','luwak white coffe','sachestan','2015-12-14',NULL,'admin','5','1'),('6','madurasa','','2015-12-14',NULL,'admin','7','1'),('7','budy jam','coklat kental manis 200 gr','2015-12-14',NULL,'admin','7','1'),('8','ceres','ceres 200 gr','2015-12-14',NULL,'admin','7','1'),('8998838290262','Kenko','','2015-10-01',NULL,'admin','25','3');
/*!40000 ALTER TABLE `1nt3x_m_item_merk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_m_item_tipe`
--

DROP TABLE IF EXISTS `1nt3x_m_item_tipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_m_item_tipe` (
  `m_item_tipe_kode` varchar(15) NOT NULL,
  `m_item_tipe_nama` varchar(35) DEFAULT NULL,
  `m_item_tipe_ket` text,
  `m_item_tipe_tgl_buat` date DEFAULT NULL,
  `m_item_tipe_tgl_ubah` date DEFAULT NULL,
  `m_item_tipe_petugas` varchar(50) DEFAULT NULL,
  `m_item_kategori_kode` varchar(15) DEFAULT NULL,
  `m_item_jenis_kode` varchar(15) DEFAULT NULL,
  `m_item_merk_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`m_item_tipe_kode`),
  KEY `m_item_kategori_kode` (`m_item_kategori_kode`),
  KEY `m_item_jenis_kode` (`m_item_jenis_kode`),
  KEY `m_item_merk_kode` (`m_item_merk_kode`),
  CONSTRAINT `1nt3x_m_item_tipe_ibfk_1` FOREIGN KEY (`m_item_kategori_kode`) REFERENCES `1nt3x_m_item_kategori` (`m_item_kategori_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_item_tipe_ibfk_2` FOREIGN KEY (`m_item_jenis_kode`) REFERENCES `1nt3x_m_item_jenis` (`m_item_jenis_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_m_item_tipe_ibfk_3` FOREIGN KEY (`m_item_merk_kode`) REFERENCES `1nt3x_m_item_merk` (`m_item_merk_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_m_item_tipe`
--

LOCK TABLES `1nt3x_m_item_tipe` WRITE;
/*!40000 ALTER TABLE `1nt3x_m_item_tipe` DISABLE KEYS */;
INSERT INTO `1nt3x_m_item_tipe` VALUES ('1','BKS','tes','2015-09-28',NULL,'admin','1','1','1');
/*!40000 ALTER TABLE `1nt3x_m_item_tipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_m_rak`
--

DROP TABLE IF EXISTS `1nt3x_m_rak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_m_rak` (
  `m_rak_kode` varchar(15) NOT NULL DEFAULT '0',
  `m_rak_nama` varchar(35) DEFAULT NULL,
  `m_rak_ket` text,
  `m_rak_tgl_buat` date DEFAULT NULL,
  `m_rak_tgl_ubah` date DEFAULT NULL,
  `m_rak_petugas` varchar(50) DEFAULT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`m_rak_kode`),
  KEY `m_toko_kode` (`m_toko_kode`),
  CONSTRAINT `1nt3x_m_rak_ibfk_1` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_m_rak`
--

LOCK TABLES `1nt3x_m_rak` WRITE;
/*!40000 ALTER TABLE `1nt3x_m_rak` DISABLE KEYS */;
INSERT INTO `1nt3x_m_rak` VALUES ('001','atas','test','2015-09-23','2015-10-28','admin','ABC');
/*!40000 ALTER TABLE `1nt3x_m_rak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_m_rak_detail`
--

DROP TABLE IF EXISTS `1nt3x_m_rak_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_m_rak_detail` (
  `m_rak_kode` varchar(15) DEFAULT NULL,
  `m_item_kode` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_m_rak_detail`
--

LOCK TABLES `1nt3x_m_rak_detail` WRITE;
/*!40000 ALTER TABLE `1nt3x_m_rak_detail` DISABLE KEYS */;
INSERT INTO `1nt3x_m_rak_detail` VALUES ('001','0010'),('001','0011'),('001','0012'),('001','0013');
/*!40000 ALTER TABLE `1nt3x_m_rak_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_m_satuan`
--

DROP TABLE IF EXISTS `1nt3x_m_satuan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_m_satuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_satuan` varchar(4) DEFAULT NULL,
  `nama_satuan` varchar(20) DEFAULT NULL,
  `aktif` varchar(1) DEFAULT '1' COMMENT '1 : Aktif | 0 : Tidak Aktif',
  `created` datetime DEFAULT NULL,
  `created_by` varchar(25) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_kode_satuan` (`kode_satuan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_m_satuan`
--

LOCK TABLES `1nt3x_m_satuan` WRITE;
/*!40000 ALTER TABLE `1nt3x_m_satuan` DISABLE KEYS */;
INSERT INTO `1nt3x_m_satuan` VALUES (1,'PCS','PIECE','1',NULL,NULL,'2014-10-13 11:24:36','admin'),(2,'SET','SET','1',NULL,NULL,NULL,NULL),(5,'KD','KODI','1','2014-10-08 15:41:19','admin','2014-10-08 15:41:19','admin'),(6,'PAIR','PAIR','1','2014-10-13 11:24:04','admin','2014-10-13 11:24:04','admin'),(10,'BX','Box','1','2014-10-20 11:21:07','admin','2014-10-20 11:22:01','admin'),(11,'BTL','Botol','1','2015-03-18 11:08:46','admin','2015-03-18 11:08:46','admin'),(12,'SCT','Sachet','1','2015-03-18 11:09:06','admin','2015-03-18 11:09:06','admin'),(13,'BLS','Blister','1','2015-03-18 11:09:23','admin','2015-03-18 11:09:23','admin'),(14,'STR','Strip','1','2015-03-18 11:09:46','admin','2015-03-18 11:09:46','admin'),(15,'BTR','Butir','1','2015-03-18 11:10:00','admin','2015-03-18 11:10:00','admin'),(16,'TUB','Tube','1','2015-03-18 11:10:10','admin','2015-03-18 11:10:10','admin'),(17,'UNT','UNIT','1','2015-06-11 11:37:27','intex','2015-06-11 11:37:27','intex'),(18,'BH','BUAH','1','2015-06-11 11:37:38','intex','2015-06-11 11:37:38','intex');
/*!40000 ALTER TABLE `1nt3x_m_satuan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_m_stok_min`
--

DROP TABLE IF EXISTS `1nt3x_m_stok_min`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_m_stok_min` (
  `m_stok_min` int(11) DEFAULT NULL,
  `m_item_kode` varchar(35) DEFAULT NULL,
  KEY `m_item_kode` (`m_item_kode`),
  CONSTRAINT `1nt3x_m_stok_min_ibfk_1` FOREIGN KEY (`m_item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_m_stok_min`
--

LOCK TABLES `1nt3x_m_stok_min` WRITE;
/*!40000 ALTER TABLE `1nt3x_m_stok_min` DISABLE KEYS */;
INSERT INTO `1nt3x_m_stok_min` VALUES (3,'0010'),(3,'0011'),(3,'0012'),(3,'0013');
/*!40000 ALTER TABLE `1nt3x_m_stok_min` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_m_suplier`
--

DROP TABLE IF EXISTS `1nt3x_m_suplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_m_suplier` (
  `m_suplier_kode` varchar(15) NOT NULL DEFAULT '',
  `m_suplier_nama` varchar(50) DEFAULT NULL,
  `m_suplier_alamat` text,
  `m_suplier_no_tlp` varchar(15) DEFAULT NULL,
  `m_suplier_tgl_buat` date DEFAULT NULL,
  `m_suplier_tgl_ubah` date DEFAULT NULL,
  `m_suplier_petugas` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`m_suplier_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_m_suplier`
--

LOCK TABLES `1nt3x_m_suplier` WRITE;
/*!40000 ALTER TABLE `1nt3x_m_suplier` DISABLE KEYS */;
INSERT INTO `1nt3x_m_suplier` VALUES ('BA001','Bayu Nugraha','Bekasi','087780832545','2015-10-20','2015-10-20','admin');
/*!40000 ALTER TABLE `1nt3x_m_suplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_m_toko`
--

DROP TABLE IF EXISTS `1nt3x_m_toko`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_m_toko` (
  `m_toko_kode` varchar(15) NOT NULL DEFAULT '0',
  `m_toko_nama` varchar(35) NOT NULL,
  `m_toko_ket` text,
  `m_toko_no_tlp` varchar(20) NOT NULL,
  `m_toko_tgl_buat` date NOT NULL,
  `m_toko_tgl_ubah` date NOT NULL,
  `m_toko_petugas` varchar(50) NOT NULL,
  PRIMARY KEY (`m_toko_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_m_toko`
--

LOCK TABLES `1nt3x_m_toko` WRITE;
/*!40000 ALTER TABLE `1nt3x_m_toko` DISABLE KEYS */;
INSERT INTO `1nt3x_m_toko` VALUES ('ABC','MINI STOP','Di depan Gandaria City, samping SEVEL','','2015-09-23','2015-11-03','admin'),('dsa','diba surya abadi','Keranggan permai','','2015-11-25','0000-00-00','admin'),('koa','koko abah','jalan proklamasi','','2015-11-25','0000-00-00','admin'),('KP','Kampung Rambutan','Toko yang berada di kampung rambutan','','2015-10-09','0000-00-00','admin'),('KR','Kranggan','Ruko Kranggan No. 8A\nJl. Kranggan - Jati Sampurna\nCibubur 17433','','2015-10-09','2015-10-13','admin');
/*!40000 ALTER TABLE `1nt3x_m_toko` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_menu_groups`
--

DROP TABLE IF EXISTS `1nt3x_menu_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_menu_groups` (
  `group_id` int(2) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) DEFAULT NULL,
  `group_desc` text,
  `created_at` datetime DEFAULT NULL,
  `modified_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_menu_groups`
--

LOCK TABLES `1nt3x_menu_groups` WRITE;
/*!40000 ALTER TABLE `1nt3x_menu_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `1nt3x_menu_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_menu_scut`
--

DROP TABLE IF EXISTS `1nt3x_menu_scut`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_menu_scut` (
  `menu_id` int(2) DEFAULT NULL,
  `role_id` int(2) DEFAULT NULL,
  `user_id` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_menu_scut`
--

LOCK TABLES `1nt3x_menu_scut` WRITE;
/*!40000 ALTER TABLE `1nt3x_menu_scut` DISABLE KEYS */;
/*!40000 ALTER TABLE `1nt3x_menu_scut` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_menus`
--

DROP TABLE IF EXISTS `1nt3x_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_menus` (
  `menu_id` int(2) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(50) NOT NULL,
  `menu_url` text NOT NULL,
  `menu_parent` int(2) DEFAULT NULL,
  `menu_status` char(1) NOT NULL,
  `menu_position` int(2) NOT NULL,
  `menu_icon` varchar(25) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(25) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_menus`
--

LOCK TABLES `1nt3x_menus` WRITE;
/*!40000 ALTER TABLE `1nt3x_menus` DISABLE KEYS */;
INSERT INTO `1nt3x_menus` VALUES (1,'File','#|file|1',0,'1',0,NULL,'2015-08-24 17:16:02','2015-08-25 03:59:36','system'),(2,'User Account','file|akun|2',1,'1',1,NULL,'2015-08-24 17:39:33','2015-08-25 02:23:05','system'),(3,'Exit','auth|logout|3',1,'1',2,NULL,'2015-08-24 17:40:02','2015-08-25 03:18:56','system'),(4,'Application Settings','#|pengaturan|4',0,'1',1,NULL,'2015-08-25 09:22:56','2015-09-22 09:36:23','system'),(5,'Menu Management','pengaturan|menu|5',4,'1',4,NULL,'2015-08-25 09:23:45','2015-08-25 02:23:48','system'),(6,'Access Management','pengaturan|aturan|6',4,'1',5,NULL,'2015-08-25 09:24:08','2015-08-25 02:24:17','system'),(7,'User Management','pengaturan|pengguna|7',4,'1',6,NULL,'2015-08-25 09:24:36','0000-00-00 00:00:00','system'),(8,'Master','#|master|8',0,'1',2,NULL,'2015-08-26 00:00:00','2015-09-22 09:36:35',''),(12,'Application Profile','pengaturan|profil_aplikasi|12',4,'1',0,NULL,'2015-08-25 00:00:00','2015-08-31 04:58:54',''),(13,'Wilayah','#|wilayah|13',8,'1',0,NULL,'2015-08-31 00:00:00','0000-00-00 00:00:00',''),(14,'Provinsi','master|provinsi|14',13,'1',0,NULL,'2015-08-31 00:00:00','0000-00-00 00:00:00',''),(15,'Kabupaten / Kota','master|kabupaten|15',13,'1',1,NULL,'2015-08-31 00:00:00','0000-00-00 00:00:00',''),(18,'Barang','#|produk|18',8,'1',1,NULL,'2015-08-31 00:00:00','2015-09-22 09:31:01',''),(19,'List Barang','master|list_item|19',18,'1',0,NULL,'2015-08-31 00:00:00','2015-09-28 03:59:37',''),(20,'Type Barang','master|tipe|20',18,'1',1,NULL,'2015-08-31 00:00:00','2015-09-23 05:11:27',''),(21,'Jenis Barang','master|jenis|21',18,'1',3,NULL,'2015-08-31 00:00:00','2015-08-31 05:34:05',''),(22,'Kategori Barang','master|kategori|22',18,'1',4,NULL,'2015-08-31 00:00:00','2015-09-22 09:34:22',''),(23,'Merek Barang','master|merek|23',18,'1',2,NULL,'2015-08-31 00:00:00','2015-09-22 09:33:05',''),(29,'Satuan','master|satuan|29',8,'1',3,NULL,'2015-08-31 00:00:00','2015-10-08 07:17:57',''),(33,'Toko','master|toko|33',8,'1',4,NULL,'2015-09-01 00:00:00','2015-10-08 07:18:05',''),(34,'Rak','master|rak|34',8,'1',5,NULL,'2015-09-01 00:00:00','2015-10-08 07:18:13',''),(35,'Penjualan','#|penjualan|35',0,'1',3,NULL,'2015-09-22 00:00:00','0000-00-00 00:00:00',''),(36,'Penjualan','penjualan|penjualan|36',35,'1',0,NULL,'2015-09-22 00:00:00','0000-00-00 00:00:00',''),(37,'Laporan Penjualan','penjualan|laporan_penjualan|37',35,'1',1,NULL,'2015-09-22 00:00:00','2015-09-29 09:52:28',''),(38,'Manajemen Harga','#|manajemen_harga|38',8,'1',2,NULL,'2015-10-08 00:00:00','2015-10-08 07:17:50',''),(39,'List Harga','master|list_harga|39',38,'1',0,NULL,'2015-10-08 00:00:00','2015-10-08 07:37:06',''),(40,'Harga Promo','master|harga_promo|40',38,'1',1,NULL,'2015-10-08 00:00:00','2015-10-08 07:37:15',''),(41,'Supplier','master|supplier|41',8,'1',6,NULL,'2015-10-09 00:00:00','0000-00-00 00:00:00',''),(42,'Inventory','#|inventory|42',0,'1',4,NULL,'2015-10-19 00:00:00','0000-00-00 00:00:00',''),(43,'Persediaan Awal','inventory|persediaan_awal|43',42,'1',0,NULL,'2015-10-19 00:00:00','0000-00-00 00:00:00',''),(44,'Purchase Order','inventory|purchase_order|44',42,'1',1,NULL,'2015-10-19 00:00:00','0000-00-00 00:00:00',''),(45,'Transfer Barang','inventory|transfer_barang|45',42,'1',4,NULL,'2015-10-19 00:00:00','2015-11-09 06:40:48',''),(46,'Penerimaan Barang','inventory|penerimaan_barang|46',42,'1',2,NULL,'2015-10-19 00:00:00','2015-10-28 03:31:40',''),(47,'Stok Opname','inventory|stok_opname|47',42,'1',5,NULL,'2015-10-19 00:00:00','2015-11-09 06:41:02',''),(48,'Adjustment Stok','inventory|adjustment_stok|48',42,'1',6,NULL,'2015-10-19 00:00:00','2015-11-09 06:41:13',''),(49,'Stok Mutasi','inventory|stok_mutasi|49',42,'1',7,NULL,'2015-10-19 00:00:00','2015-11-09 06:41:24',''),(50,'Minimum Stok','master|minimum_stok|50',18,'1',5,NULL,'2015-11-02 00:00:00','2015-11-02 09:23:58','');
/*!40000 ALTER TABLE `1nt3x_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_profile`
--

DROP TABLE IF EXISTS `1nt3x_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_profile` (
  `profile_id` int(1) NOT NULL AUTO_INCREMENT,
  `profile_name` varchar(50) NOT NULL,
  `profile_npwp` varchar(25) DEFAULT NULL,
  `profile_phone` varchar(15) DEFAULT NULL,
  `profile_fax` varchar(15) DEFAULT NULL,
  `profile_email` varchar(50) DEFAULT NULL,
  `profile_address` text,
  `profile_logo` varchar(50) DEFAULT NULL,
  `profile_app_name` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(25) NOT NULL,
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_profile`
--

LOCK TABLES `1nt3x_profile` WRITE;
/*!40000 ALTER TABLE `1nt3x_profile` DISABLE KEYS */;
INSERT INTO `1nt3x_profile` VALUES (1,'PT. Marui Intex','12321321','021 7169 9550','021 8430 7221','info@intex.co.id','Ruko Kranggan No 8A Jl. Kranggan - Jati Sampura Cibubur 17433','image_ZWHFk_20150831120332.png','POS','2015-08-31 12:03:35','2015-09-22 09:29:40','');
/*!40000 ALTER TABLE `1nt3x_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_provinsi`
--

DROP TABLE IF EXISTS `1nt3x_provinsi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_provinsi` (
  `provinsi_kode` varchar(11) NOT NULL,
  `provinsi_nama` varchar(50) DEFAULT NULL,
  `provinsi_tgl_buat` date DEFAULT NULL,
  `provinsi_tgl_ubah` date DEFAULT NULL,
  `provinsi_petugas` varchar(50) DEFAULT NULL,
  `provinsi_status` enum('1','0') DEFAULT NULL,
  PRIMARY KEY (`provinsi_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_provinsi`
--

LOCK TABLES `1nt3x_provinsi` WRITE;
/*!40000 ALTER TABLE `1nt3x_provinsi` DISABLE KEYS */;
INSERT INTO `1nt3x_provinsi` VALUES ('11','Aceh','0000-00-00',NULL,'sytem','1'),('12','Sumatera Utara','0000-00-00',NULL,'sytem','1'),('13','Sumatera Barat','0000-00-00',NULL,'sytem','1'),('14','Riau','0000-00-00',NULL,'sytem','1'),('15','Jambi','0000-00-00',NULL,'sytem','1'),('16','Sumatera Selatan','0000-00-00',NULL,'sytem','1'),('17','Bengkulu','0000-00-00',NULL,'sytem','1'),('18','Lampung','0000-00-00',NULL,'sytem','1'),('19','Kepulauan Bangka Belitung','0000-00-00',NULL,'sytem','1'),('21','Kepulauan Riau','0000-00-00',NULL,'sytem','1'),('31','Dki Jakarta','0000-00-00',NULL,'sytem','1'),('32','Jawa Barat','0000-00-00',NULL,'sytem','1'),('33','Jawa Tengah','0000-00-00',NULL,'sytem','1'),('34','Di Yogyakarta','0000-00-00',NULL,'sytem','1'),('35','Jawa Timur','0000-00-00',NULL,'sytem','1'),('36','Banten','0000-00-00',NULL,'sytem','1'),('51','Bali','0000-00-00',NULL,'sytem','1'),('52','Nusa Tenggara Barat','0000-00-00',NULL,'sytem','1'),('53','Nusa Tenggara Timur','0000-00-00',NULL,'sytem','1'),('61','Kalimantan Barat','0000-00-00',NULL,'sytem','1'),('62','Kalimantan Tengah','0000-00-00',NULL,'sytem','1'),('63','Kalimantan Selatan','0000-00-00',NULL,'sytem','1'),('64','Kalimantan Timur','0000-00-00',NULL,'sytem','1'),('65','Kalimantan Utara','0000-00-00',NULL,'sytem','1'),('71','Sulawesi Utara','0000-00-00',NULL,'sytem','1'),('72','Sulawesi Tengah','0000-00-00',NULL,'sytem','1'),('73','Sulawesi Selatan','0000-00-00',NULL,'sytem','1'),('74','Sulawesi Tenggara','0000-00-00',NULL,'sytem','1'),('75','Gorontalo','0000-00-00',NULL,'sytem','1'),('76','Sulawesi Barat','0000-00-00',NULL,'sytem','1'),('81','Maluku','0000-00-00',NULL,'sytem','1'),('82','Maluku Utara','0000-00-00',NULL,'sytem','1'),('91','Papua Barat','0000-00-00',NULL,'sytem','1'),('94','Papua','0000-00-00',NULL,'sytem','1');
/*!40000 ALTER TABLE `1nt3x_provinsi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_roles`
--

DROP TABLE IF EXISTS `1nt3x_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_roles` (
  `role_id` int(2) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `role_desc` text,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(25) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_roles`
--

LOCK TABLES `1nt3x_roles` WRITE;
/*!40000 ALTER TABLE `1nt3x_roles` DISABLE KEYS */;
INSERT INTO `1nt3x_roles` VALUES (1,'admin','admin','2015-08-24 17:03:53','2015-08-25 03:41:13','admin'),(2,'kasir','kasir','0000-00-00 00:00:00','2015-10-21 09:26:56','');
/*!40000 ALTER TABLE `1nt3x_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_roles_access`
--

DROP TABLE IF EXISTS `1nt3x_roles_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_roles_access` (
  `role_access_view` enum('0','1') NOT NULL DEFAULT '0',
  `role_access_add` enum('0','1') NOT NULL DEFAULT '0',
  `role_access_edit` enum('0','1') NOT NULL DEFAULT '0',
  `role_access_delete` enum('0','1') NOT NULL DEFAULT '0',
  `role_access_export` enum('0','1') NOT NULL DEFAULT '0',
  `role_access_import` enum('0','1') NOT NULL DEFAULT '0',
  `role_id` int(2) NOT NULL,
  `menu_id` int(2) NOT NULL,
  KEY `role_id` (`role_id`),
  KEY `menu_id` (`menu_id`),
  CONSTRAINT `1nt3x_roles_access_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `1nt3x_roles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_roles_access_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `1nt3x_menus` (`menu_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_roles_access`
--

LOCK TABLES `1nt3x_roles_access` WRITE;
/*!40000 ALTER TABLE `1nt3x_roles_access` DISABLE KEYS */;
INSERT INTO `1nt3x_roles_access` VALUES ('1','1','1','1','1','1',1,1),('1','1','1','1','1','1',1,2),('1','1','1','1','1','1',1,3),('1','1','1','1','1','1',1,4),('1','1','1','1','1','1',1,12),('1','1','1','1','1','1',1,5),('1','1','1','1','1','1',1,6),('1','1','1','1','1','1',1,7),('1','1','1','1','1','1',1,8),('0','0','0','0','0','0',1,13),('0','0','0','0','0','0',1,14),('0','0','0','0','0','0',1,15),('1','1','1','1','1','1',1,18),('1','1','1','1','1','1',1,19),('1','1','1','1','1','1',1,20),('1','1','1','1','1','1',1,23),('1','1','1','1','1','1',1,21),('1','1','1','1','1','1',1,22),('1','1','1','1','1','1',1,50),('1','1','1','1','1','1',1,38),('1','1','1','1','1','1',1,39),('1','1','1','1','1','1',1,40),('1','1','1','1','1','1',1,29),('1','1','1','1','1','1',1,33),('1','1','1','1','1','1',1,34),('1','1','1','1','1','1',1,41),('1','1','1','1','1','1',1,35),('1','1','1','1','1','1',1,36),('1','1','1','1','1','1',1,37),('1','1','1','1','1','1',1,42),('1','1','1','1','1','1',1,43),('1','1','1','1','1','1',1,44),('1','1','1','1','1','1',1,46),('1','1','1','1','1','1',1,45),('1','1','1','1','1','1',1,47),('1','1','1','1','1','1',1,48),('1','1','1','1','1','1',1,49),('1','1','1','1','1','1',2,1),('1','1','1','1','1','1',2,2),('1','1','1','1','1','1',2,3),('0','0','0','0','0','0',2,4),('0','0','0','0','0','0',2,12),('0','0','0','0','0','0',2,5),('0','0','0','0','0','0',2,6),('0','0','0','0','0','0',2,7),('0','0','0','0','0','0',2,8),('0','0','0','0','0','0',2,13),('0','0','0','0','0','0',2,14),('0','0','0','0','0','0',2,15),('0','0','0','0','0','0',2,18),('0','0','0','0','0','0',2,19),('0','0','0','0','0','0',2,20),('0','0','0','0','0','0',2,23),('0','0','0','0','0','0',2,21),('0','0','0','0','0','0',2,22),('0','0','0','0','0','0',2,50),('0','0','0','0','0','0',2,38),('0','0','0','0','0','0',2,39),('0','0','0','0','0','0',2,40),('0','0','0','0','0','0',2,29),('0','0','0','0','0','0',2,33),('0','0','0','0','0','0',2,34),('0','0','0','0','0','0',2,41),('1','1','1','1','1','1',2,35),('1','1','1','1','1','1',2,36),('1','1','1','1','1','1',2,37),('0','0','0','0','0','0',2,42),('0','0','0','0','0','0',2,43),('0','0','0','0','0','0',2,44),('0','0','0','0','0','0',2,46),('0','0','0','0','0','0',2,45),('0','0','0','0','0','0',2,47),('0','0','0','0','0','0',2,48),('0','0','0','0','0','0',2,49);
/*!40000 ALTER TABLE `1nt3x_roles_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_sessions`
--

DROP TABLE IF EXISTS `1nt3x_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_sessions`
--

LOCK TABLES `1nt3x_sessions` WRITE;
/*!40000 ALTER TABLE `1nt3x_sessions` DISABLE KEYS */;
INSERT INTO `1nt3x_sessions` VALUES ('0271e5b18aca77cb4248c4a643703183','202.78.195.56','Mozilla/5.0 (Linux; Android 4.4.2; HM NOTE 1W Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Mobile Sa',1451733020,''),('3409b8ddde89bad73e5294e9bf5e2ce2','202.78.195.56','Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; HM NOTE 1W Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0',1451733057,'a:2:{s:9:\"user_data\";s:0:\"\";s:32:\"edbe3f7eac75e8303bc87d28d5ff1436\";a:18:{s:7:\"user_id\";s:1:\"5\";s:13:\"user_fullname\";s:11:\"intex_kasir\";s:13:\"user_username\";s:11:\"intex_kasir\";s:13:\"user_password\";s:32:\"a857e217f23c9ed675709880715e6690\";s:7:\"user_jk\";s:0:\"\";s:12:\"user_tgl_lhr\";N;s:11:\"user_alamat\";N;s:10:\"user_email\";s:23:\"kasir_kasir@intex.co.id\";s:8:\"user_tlp\";s:13:\"0987782810101\";s:15:\"user_wrong_pass\";s:1:\"0\";s:11:\"user_status\";s:1:\"1\";s:14:\"role_access_id\";N;s:16:\"role_access_nama\";N;s:15:\"role_access_ket\";N;s:7:\"role_id\";s:1:\"2\";s:9:\"role_name\";s:5:\"kasir\";s:9:\"toko_kode\";s:3:\"ABC\";s:13:\"otoritas_menu\";a:37:{i:1;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:1:\"1\";}i:2;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:1:\"2\";}i:3;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:1:\"3\";}i:4;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:1:\"4\";}i:12;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"12\";}i:5;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:1:\"5\";}i:6;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:1:\"6\";}i:7;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:1:\"7\";}i:8;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:1:\"8\";}i:13;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"13\";}i:14;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"14\";}i:15;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"15\";}i:18;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"18\";}i:19;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"19\";}i:20;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"20\";}i:23;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"23\";}i:21;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"21\";}i:22;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"22\";}i:50;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"50\";}i:38;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"38\";}i:39;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"39\";}i:40;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"40\";}i:29;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"29\";}i:33;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"33\";}i:34;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"34\";}i:41;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"41\";}i:35;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"35\";}i:36;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"36\";}i:37;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"37\";}i:42;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"42\";}i:43;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"43\";}i:44;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"44\";}i:46;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"46\";}i:45;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"45\";}i:47;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"47\";}i:48;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"48\";}i:49;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"49\";}}}}'),('be04bef2e3cd971b8b435addbccc8ad1','202.78.195.56','Mozilla/5.0 (Linux; Android 4.4.2; H8 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 M',1451638557,''),('dfeef2bc92efcfb7d25b03d820c87560','202.78.195.56','Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; HM NOTE 1W Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0',1451703427,'a:2:{s:9:\"user_data\";s:0:\"\";s:32:\"edbe3f7eac75e8303bc87d28d5ff1436\";a:18:{s:7:\"user_id\";s:1:\"5\";s:13:\"user_fullname\";s:11:\"intex_kasir\";s:13:\"user_username\";s:11:\"intex_kasir\";s:13:\"user_password\";s:32:\"a857e217f23c9ed675709880715e6690\";s:7:\"user_jk\";s:0:\"\";s:12:\"user_tgl_lhr\";N;s:11:\"user_alamat\";N;s:10:\"user_email\";s:23:\"kasir_kasir@intex.co.id\";s:8:\"user_tlp\";s:13:\"0987782810101\";s:15:\"user_wrong_pass\";s:1:\"0\";s:11:\"user_status\";s:1:\"1\";s:14:\"role_access_id\";N;s:16:\"role_access_nama\";N;s:15:\"role_access_ket\";N;s:7:\"role_id\";s:1:\"2\";s:9:\"role_name\";s:5:\"kasir\";s:9:\"toko_kode\";s:3:\"ABC\";s:13:\"otoritas_menu\";a:37:{i:1;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:1:\"1\";}i:2;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:1:\"2\";}i:3;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:1:\"3\";}i:4;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:1:\"4\";}i:12;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"12\";}i:5;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:1:\"5\";}i:6;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:1:\"6\";}i:7;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:1:\"7\";}i:8;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:1:\"8\";}i:13;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"13\";}i:14;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"14\";}i:15;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"15\";}i:18;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"18\";}i:19;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"19\";}i:20;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"20\";}i:23;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"23\";}i:21;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"21\";}i:22;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"22\";}i:50;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"50\";}i:38;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"38\";}i:39;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"39\";}i:40;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"40\";}i:29;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"29\";}i:33;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"33\";}i:34;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"34\";}i:41;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"41\";}i:35;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"35\";}i:36;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"36\";}i:37;a:7:{s:16:\"role_access_view\";s:1:\"1\";s:15:\"role_access_add\";s:1:\"1\";s:16:\"role_access_edit\";s:1:\"1\";s:18:\"role_access_delete\";s:1:\"1\";s:18:\"role_access_export\";s:1:\"1\";s:18:\"role_access_import\";s:1:\"1\";s:7:\"menu_id\";s:2:\"37\";}i:42;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"42\";}i:43;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"43\";}i:44;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"44\";}i:46;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"46\";}i:45;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"45\";}i:47;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"47\";}i:48;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"48\";}i:49;a:7:{s:16:\"role_access_view\";s:1:\"0\";s:15:\"role_access_add\";s:1:\"0\";s:16:\"role_access_edit\";s:1:\"0\";s:18:\"role_access_delete\";s:1:\"0\";s:18:\"role_access_export\";s:1:\"0\";s:18:\"role_access_import\";s:1:\"0\";s:7:\"menu_id\";s:2:\"49\";}}}}');
/*!40000 ALTER TABLE `1nt3x_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_tmp_trx`
--

DROP TABLE IF EXISTS `1nt3x_tmp_trx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_tmp_trx` (
  `id_tmp` int(11) NOT NULL AUTO_INCREMENT,
  `no_tmp` varchar(30) DEFAULT NULL,
  `kd_item` varchar(30) DEFAULT NULL,
  `nm_item` varchar(100) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `sub_total` double DEFAULT NULL,
  `sales` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_tmp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_tmp_trx`
--

LOCK TABLES `1nt3x_tmp_trx` WRITE;
/*!40000 ALTER TABLE `1nt3x_tmp_trx` DISABLE KEYS */;
/*!40000 ALTER TABLE `1nt3x_tmp_trx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_trx_pos`
--

DROP TABLE IF EXISTS `1nt3x_trx_pos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_trx_pos` (
  `trx_pos_no` varchar(25) NOT NULL DEFAULT '0',
  `trx_pos_tgl` datetime DEFAULT NULL,
  `trx_pos_ket` text,
  `trx_pos_sub_total` double DEFAULT NULL,
  `trx_pos_diskon` int(11) DEFAULT NULL,
  `trx_pos_tax` int(11) DEFAULT NULL,
  `trx_pos_grand_total` double DEFAULT NULL,
  `trx_pos_kembalian` double DEFAULT NULL,
  `trx_pos_tgl_buat` date DEFAULT NULL,
  `trx_pos_tgl_ubah` date DEFAULT NULL,
  `trx_pos_petugas` varchar(50) DEFAULT NULL,
  `toko_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`trx_pos_no`),
  KEY `toko_kode` (`toko_kode`),
  CONSTRAINT `1nt3x_trx_pos_ibfk_2` FOREIGN KEY (`toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_trx_pos`
--

LOCK TABLES `1nt3x_trx_pos` WRITE;
/*!40000 ALTER TABLE `1nt3x_trx_pos` DISABLE KEYS */;
INSERT INTO `1nt3x_trx_pos` VALUES ('ABC/01/2016/00029','2016-01-02 18:13:39','',225200,0,0,225200,74800,'2016-01-02','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00001','2015-12-11 17:20:53','',95000,0,0,95000,5000,'2015-12-11','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00002','2015-12-11 20:07:38','',152000,0,0,152000,48000,'2015-12-11','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00003','2015-12-11 20:08:56','',95000,0,0,95000,5000,'2015-12-11','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00004','2015-12-14 09:51:01','POS toko oleh admin',43800,0,4380,43800,6200,'2015-12-14',NULL,'admin','ABC'),('ABC/12/2015/00005','2015-12-14 10:04:42','',190800,0,0,190800,9200,'2015-12-14','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00006','2015-12-14 10:52:53','',114800,0,0,114800,85200,'2015-12-14','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00007','2015-12-14 10:52:54','',114800,0,0,114800,85200,'2015-12-14','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00008','2015-12-14 13:40:42','',152600,0,0,152600,47400,'2015-12-14','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00009','2015-12-14 13:44:17','',119000,0,0,119000,1000,'2015-12-14','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00010','2015-12-14 16:39:41','',30000,0,0,30000,20000,'2015-12-14','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00011','2015-12-14 16:42:16','',38000,0,0,38000,0,'2015-12-14','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00012','2015-12-14 16:43:21','',38000,0,0,38000,0,'2015-12-14','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00013','2015-12-14 16:43:50','POS toko oleh admin',1200000,0,120000,1200000,0,'2015-12-14',NULL,'admin','ABC'),('ABC/12/2015/00014','2015-12-14 16:44:30','POS toko oleh admin',10000000,0,1000000,10000000,0,'2015-12-14',NULL,'admin','ABC'),('ABC/12/2015/00015','2015-12-14 16:47:04','',49500,0,0,49500,500,'2015-12-14','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00016','2015-12-14 17:03:21','',9900,0,0,9900,10100,'2015-12-14','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00017','2015-12-14 17:06:08','',10000,0,0,10000,40000,'2015-12-14','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00018','2015-12-14 17:09:35','',9900,0,0,9900,0,'2015-12-14','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00019','2015-12-14 21:13:11','',38000,0,0,38000,12000,'2015-12-14','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00020','2015-12-15 06:51:20','',232000,0,0,232000,18000,'2015-12-15','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00021','2015-12-15 10:12:07','',338000,0,0,338000,0,'2015-12-15','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00022','2015-12-15 10:17:32','',19000,0,0,19000,0,'2015-12-15','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00023','2015-12-15 10:18:38','',49500,0,0,49500,500,'2015-12-15','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00024','2015-12-15 12:01:28','',0,0,0,0,0,'2015-12-15','0000-00-00','intex_kasir','ABC'),('ABC/12/2015/00025','2015-12-16 11:27:20','POS toko oleh admin',277200,21000,27720,256200,543800,'2015-12-16',NULL,'admin','ABC'),('ABC/12/2015/00026','2015-12-16 11:27:43','POS toko oleh admin',19800,1500,1980,18300,0,'2015-12-16',NULL,'admin','ABC'),('ABC/12/2015/00027','2015-12-16 11:28:10','POS toko oleh admin',9900,0,990,9900,0,'2015-12-16',NULL,'admin','ABC'),('ABC/12/2015/00028','2015-12-17 19:59:12','POS toko oleh intex_kasir',314900,0,31490,314900,3185100,'2015-12-17',NULL,'intex_kasir','ABC');
/*!40000 ALTER TABLE `1nt3x_trx_pos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_trx_pos_detail`
--

DROP TABLE IF EXISTS `1nt3x_trx_pos_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_trx_pos_detail` (
  `trx_pos_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `trx_pos_detail_qty` int(11) DEFAULT NULL,
  `trx_pos_detail_harga` double DEFAULT NULL,
  `trx_pos_detail_disc` int(4) DEFAULT NULL,
  `trx_pos_detail_total` double DEFAULT NULL,
  `item_kode` varchar(35) DEFAULT NULL,
  `trx_pos_no` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`trx_pos_detail_id`),
  KEY `trx_pos_no` (`trx_pos_no`),
  KEY `item_kode` (`item_kode`),
  CONSTRAINT `1nt3x_trx_pos_detail_ibfk_1` FOREIGN KEY (`trx_pos_no`) REFERENCES `1nt3x_trx_pos` (`trx_pos_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_trx_pos_detail_ibfk_2` FOREIGN KEY (`item_kode`) REFERENCES `1nt3x_m_item` (`m_item_kode`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=358 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_trx_pos_detail`
--

LOCK TABLES `1nt3x_trx_pos_detail` WRITE;
/*!40000 ALTER TABLE `1nt3x_trx_pos_detail` DISABLE KEYS */;
INSERT INTO `1nt3x_trx_pos_detail` VALUES (300,5,19000,0,95000,'0011','ABC/12/2015/00001'),(301,8,19000,0,152000,'0011','ABC/12/2015/00002'),(302,5,19000,0,95000,'0011','ABC/12/2015/00003'),(303,1,2000,0,2000,'0012','ABC/12/2015/00004'),(304,1,3000,0,3000,'0013','ABC/12/2015/00004'),(305,2,9900,0,19800,'0010','ABC/12/2015/00004'),(306,1,19000,0,19000,'0011','ABC/12/2015/00004'),(307,2,9900,0,19800,'0010','ABC/12/2015/00005'),(308,9,19000,0,171000,'0011','ABC/12/2015/00005'),(312,2,9900,0,19800,'0010','ABC/12/2015/00006'),(313,5,19000,0,95000,'0011','ABC/12/2015/00006'),(318,4,9900,0,39600,'0010','ABC/12/2015/00008'),(319,5,19000,0,95000,'0011','ABC/12/2015/00008'),(320,9,2000,0,18000,'0012','ABC/12/2015/00008'),(321,10,9900,0,99000,'0010','ABC/12/2015/00009'),(322,10,2000,0,20000,'0012','ABC/12/2015/00009'),(323,15,2000,0,30000,'0012','ABC/12/2015/00010'),(324,2,19000,0,38000,'0011','ABC/12/2015/00011'),(325,5,9900,0,49500,'0010','ABC/12/2015/00012'),(326,120,10000,0,1200000,'0016','ABC/12/2015/00013'),(327,1000,10000,0,10000000,'0016','ABC/12/2015/00014'),(328,5,9900,0,49500,'0010','ABC/12/2015/00015'),(329,1,9900,0,9900,'0010','ABC/12/2015/00016'),(330,5,2000,0,10000,'0012','ABC/12/2015/00017'),(331,1,9900,0,9900,'0010','ABC/12/2015/00018'),(332,2,19000,0,38000,'0011','ABC/12/2015/00019'),(333,6,2000,0,12000,'0012','ABC/12/2015/00020'),(334,5,19000,0,95000,'0011','ABC/12/2015/00020'),(335,9,5000,0,45000,'0014','ABC/12/2015/00020'),(336,10,8000,0,80000,'0015','ABC/12/2015/00020'),(337,1,2000,0,2000,'0012','ABC/12/2015/00021'),(338,6,19000,0,114000,'0011','ABC/12/2015/00021'),(339,5,8000,0,40000,'0015','ABC/12/2015/00021'),(340,11,10000,0,110000,'0016','ABC/12/2015/00021'),(341,12,3000,0,36000,'0013','ABC/12/2015/00021'),(342,12,3000,0,36000,'0013','ABC/12/2015/00021'),(343,1,19000,0,19000,'0011','ABC/12/2015/00022'),(344,5,9900,0,49500,'0010','ABC/12/2015/00023'),(345,8,9900,0,79200,'0010','ABC/12/2015/00024'),(346,6,19000,0,114000,'0011','ABC/12/2015/00024'),(347,28,9900,21000,277200,'0010','ABC/12/2015/00025'),(348,2,9900,1500,19800,'0010','ABC/12/2015/00026'),(349,1,9900,0,9900,'0010','ABC/12/2015/00027'),(350,10,2000,0,20000,'0012','ABC/12/2015/00028'),(351,1,9900,0,9900,'0010','ABC/12/2015/00028'),(352,15,19000,0,285000,'0011','ABC/12/2015/00028'),(353,8,9900,0,79200,'0010','ABC/01/2016/00029'),(354,5,19000,0,95000,'0011','ABC/01/2016/00029'),(355,3,2000,0,6000,'0012','ABC/01/2016/00029'),(356,1,5000,0,5000,'0014','ABC/01/2016/00029'),(357,5,8000,0,40000,'0015','ABC/01/2016/00029');
/*!40000 ALTER TABLE `1nt3x_trx_pos_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_trx_pos_jns_pem`
--

DROP TABLE IF EXISTS `1nt3x_trx_pos_jns_pem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_trx_pos_jns_pem` (
  `trx_pos_jns_pem_id` int(11) NOT NULL AUTO_INCREMENT,
  `trx_pos_jns_pem_status` enum('credit','debit','cash') DEFAULT NULL,
  `trx_pos_jns_pem_no_kartu` varchar(20) DEFAULT NULL,
  `trx_pos_jns_pem_no_ref` varchar(30) DEFAULT NULL,
  `trx_pos_jns_pem_bayar` double DEFAULT NULL,
  `trx_pos_no` varchar(25) DEFAULT NULL,
  `m_bank_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`trx_pos_jns_pem_id`),
  KEY `trx_pos_no` (`trx_pos_no`),
  KEY `m_bank_id` (`m_bank_id`),
  CONSTRAINT `1nt3x_trx_pos_jns_pem_ibfk_1` FOREIGN KEY (`trx_pos_no`) REFERENCES `1nt3x_trx_pos` (`trx_pos_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_trx_pos_jns_pem_ibfk_2` FOREIGN KEY (`m_bank_id`) REFERENCES `1nt3x_m_bank` (`m_bank_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=237 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_trx_pos_jns_pem`
--

LOCK TABLES `1nt3x_trx_pos_jns_pem` WRITE;
/*!40000 ALTER TABLE `1nt3x_trx_pos_jns_pem` DISABLE KEYS */;
INSERT INTO `1nt3x_trx_pos_jns_pem` VALUES (100,'cash','','',NULL,NULL,NULL),(101,'cash','','',NULL,NULL,NULL),(105,'cash','','',NULL,NULL,NULL),(113,'cash','','',NULL,NULL,NULL),(114,'cash','','',NULL,NULL,NULL),(115,'cash','','',NULL,NULL,NULL),(116,'cash','','',NULL,NULL,NULL),(117,'cash','','',NULL,NULL,NULL),(118,'cash','','',NULL,NULL,NULL),(119,'cash','','',NULL,NULL,NULL),(120,'cash','','',NULL,NULL,NULL),(121,'cash','','',NULL,NULL,NULL),(122,'cash','','',NULL,NULL,NULL),(123,'cash','','',NULL,NULL,NULL),(124,'cash','','',NULL,NULL,NULL),(125,'cash','','',NULL,NULL,NULL),(132,'cash','','',NULL,NULL,NULL),(133,'cash','','',NULL,NULL,NULL),(135,'cash','','',NULL,NULL,NULL),(136,'cash','','',NULL,NULL,NULL),(137,'cash','','',NULL,NULL,NULL),(148,'cash','','',NULL,NULL,NULL),(162,'cash','','',NULL,NULL,NULL),(163,'cash','','',NULL,NULL,NULL),(164,'cash','','',NULL,NULL,NULL),(167,'cash','','',NULL,NULL,NULL),(179,'cash','','',NULL,NULL,NULL),(180,'cash','','',NULL,NULL,NULL),(184,'cash','','',NULL,NULL,NULL),(185,'cash','','',NULL,NULL,NULL),(196,'cash','','',NULL,NULL,NULL),(197,'cash','','',NULL,NULL,NULL),(198,'cash','','',NULL,NULL,NULL),(199,'cash','','',NULL,NULL,NULL),(200,'cash','','',NULL,NULL,NULL),(201,'cash','','',NULL,NULL,NULL),(204,'cash','','',NULL,NULL,NULL),(205,'cash','','',NULL,NULL,NULL),(206,'cash','','',NULL,NULL,NULL),(207,'cash',NULL,NULL,50000,'ABC/12/2015/00004',NULL),(208,'cash','','',NULL,NULL,NULL),(212,'cash','','',NULL,NULL,NULL),(213,'cash','','',NULL,NULL,NULL),(215,'cash','','',NULL,NULL,NULL),(216,'cash','','',NULL,NULL,NULL),(217,'cash','','',NULL,NULL,NULL),(218,'debit','124596387','254718963',38000,'ABC/12/2015/00011',4),(219,'credit','5847269300','54000521111',38000,'ABC/12/2015/00012',2),(220,'cash',NULL,NULL,1200000,'ABC/12/2015/00013',NULL),(221,'cash',NULL,NULL,10000000,'ABC/12/2015/00014',NULL),(222,'cash','','',NULL,NULL,NULL),(223,'cash','','',NULL,NULL,NULL),(224,'cash','','',NULL,NULL,NULL),(225,'debit','43879087000','12345689',9900,'ABC/12/2015/00018',9),(226,'cash','','',NULL,NULL,NULL),(227,'cash','','',NULL,NULL,NULL),(228,'cash','','',NULL,NULL,NULL),(229,'cash','','',NULL,NULL,NULL),(230,'cash','','',NULL,NULL,NULL),(231,'cash','','',NULL,NULL,NULL),(232,'cash',NULL,NULL,800000,'ABC/12/2015/00025',NULL),(233,'cash',NULL,NULL,18300,'ABC/12/2015/00026',NULL),(234,'cash',NULL,NULL,9900,'ABC/12/2015/00027',NULL),(235,'cash',NULL,NULL,3500000,'ABC/12/2015/00028',NULL),(236,'cash','','',NULL,NULL,NULL);
/*!40000 ALTER TABLE `1nt3x_trx_pos_jns_pem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_trx_tmp_pos`
--

DROP TABLE IF EXISTS `1nt3x_trx_tmp_pos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_trx_tmp_pos` (
  `trx_tmp_pos_tgl` date NOT NULL,
  `trx_tmp_pos_item_kode` varchar(35) NOT NULL,
  `trx_tmp_pos_qty` int(11) NOT NULL,
  `trx_tmp_pos_harga` double NOT NULL,
  `toko_kode` varchar(15) NOT NULL,
  `trx_tmp_pos_petugas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_trx_tmp_pos`
--

LOCK TABLES `1nt3x_trx_tmp_pos` WRITE;
/*!40000 ALTER TABLE `1nt3x_trx_tmp_pos` DISABLE KEYS */;
/*!40000 ALTER TABLE `1nt3x_trx_tmp_pos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `1nt3x_users`
--

DROP TABLE IF EXISTS `1nt3x_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1nt3x_users` (
  `user_id` int(3) NOT NULL AUTO_INCREMENT,
  `user_fullname` varchar(50) NOT NULL,
  `user_username` varchar(25) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_password_length` int(11) NOT NULL DEFAULT '0',
  `user_jk` char(1) NOT NULL COMMENT 'Jenis Kelamin (L/K)',
  `user_email` varchar(255) DEFAULT NULL,
  `user_tlp` varchar(15) DEFAULT NULL,
  `user_wrong_pass` int(3) NOT NULL DEFAULT '0',
  `user_status` char(1) NOT NULL COMMENT '1/0',
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(25) NOT NULL,
  `role_id` int(2) NOT NULL,
  `m_toko_kode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `role_id` (`role_id`),
  KEY `m_toko_kode` (`m_toko_kode`),
  CONSTRAINT `1nt3x_users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `1nt3x_roles` (`role_id`) ON UPDATE CASCADE,
  CONSTRAINT `1nt3x_users_ibfk_2` FOREIGN KEY (`m_toko_kode`) REFERENCES `1nt3x_m_toko` (`m_toko_kode`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1nt3x_users`
--

LOCK TABLES `1nt3x_users` WRITE;
/*!40000 ALTER TABLE `1nt3x_users` DISABLE KEYS */;
INSERT INTO `1nt3x_users` VALUES (3,'Intex Administrator','admin','21232f297a57a5a743894a0e4a801fc3',5,'','admin@intex.co.id','02171699550',0,'1','0000-00-00 00:00:00','2015-09-28 04:58:23','Intex Administrator',1,'ABC'),(4,'Bayu Nugraha','nugraha','53d951605e0267f7e10b5567c6644571',7,'','bayu.nugraha@intex.co.id','089761719939',0,'1','0000-00-00 00:00:00','2015-10-13 07:57:28','Intex Administrator',1,'KR'),(5,'intex_kasir','intex_kasir','a857e217f23c9ed675709880715e6690',15,'','kasir_kasir@intex.co.id','0987782810101',0,'1','2015-10-21 00:00:00','2015-10-21 09:28:04','',2,'ABC'),(6,'luthfi','luthfi','81dc9bdb52d04dc20036dbd8313ed055',4,'','luthfi@gmail.com','13123123',0,'1','2015-11-25 00:00:00','2015-11-25 03:18:07','',2,'ABC'),(7,'vredy','vredy','81dc9bdb52d04dc20036dbd8313ed055',4,'','vredy@gmail.com','13123132',0,'1','2015-11-25 00:00:00','2015-11-25 03:18:34','',2,'dsa'),(8,'asad','asad','81dc9bdb52d04dc20036dbd8313ed055',4,'','asad@gmail.com','12312313',0,'1','2015-11-25 00:00:00','2015-11-25 03:19:02','',2,'koa'),(9,'sharif','sharif','81dc9bdb52d04dc20036dbd8313ed055',4,'','sharif@gmail.com','1231231',0,'1','2015-11-25 00:00:00','2015-11-25 03:19:26','',2,'KP'),(10,'bayu','bayu','81dc9bdb52d04dc20036dbd8313ed055',4,'','bayu@gmail.com','123213',0,'1','2015-11-25 00:00:00','2015-11-25 03:19:49','',2,'KR');
/*!40000 ALTER TABLE `1nt3x_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `1nt3x_vbarang`
--

DROP TABLE IF EXISTS `1nt3x_vbarang`;
/*!50001 DROP VIEW IF EXISTS `1nt3x_vbarang`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `1nt3x_vbarang` (
  `m_item_kode` tinyint NOT NULL,
  `m_item_barcode` tinyint NOT NULL,
  `m_item_nama` tinyint NOT NULL,
  `m_item_kategori_kode` tinyint NOT NULL,
  `m_item_ket` tinyint NOT NULL,
  `m_item_gambar` tinyint NOT NULL,
  `m_harga_jual` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `1nt3x_vharga_promo`
--

DROP TABLE IF EXISTS `1nt3x_vharga_promo`;
/*!50001 DROP VIEW IF EXISTS `1nt3x_vharga_promo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `1nt3x_vharga_promo` (
  `m_harga_promo_id` tinyint NOT NULL,
  `m_harga_promo_harga` tinyint NOT NULL,
  `m_harga_promo_periode_mulai` tinyint NOT NULL,
  `m_harga_promo_periode_akhir` tinyint NOT NULL,
  `m_harga_promo_qty_promo` tinyint NOT NULL,
  `m_harga_promo_tgl_buat` tinyint NOT NULL,
  `m_harga_promo_tgl_ubah` tinyint NOT NULL,
  `m_harga_promo_petugas` tinyint NOT NULL,
  `m_item_kode` tinyint NOT NULL,
  `m_toko_kode` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `1nt3x_vinfostok_blnini`
--

DROP TABLE IF EXISTS `1nt3x_vinfostok_blnini`;
/*!50001 DROP VIEW IF EXISTS `1nt3x_vinfostok_blnini`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `1nt3x_vinfostok_blnini` (
  `m_item_kode` tinyint NOT NULL,
  `m_toko_kode` tinyint NOT NULL,
  `stok_keluar` tinyint NOT NULL,
  `stok_masuk` tinyint NOT NULL,
  `QTY` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `1nt3x_vminstok`
--

DROP TABLE IF EXISTS `1nt3x_vminstok`;
/*!50001 DROP VIEW IF EXISTS `1nt3x_vminstok`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `1nt3x_vminstok` (
  `m_item_kode` tinyint NOT NULL,
  `m_item_barcode` tinyint NOT NULL,
  `m_item_nama` tinyint NOT NULL,
  `m_item_kategori_kode` tinyint NOT NULL,
  `m_item_ket` tinyint NOT NULL,
  `m_item_gambar` tinyint NOT NULL,
  `m_harga_jual` tinyint NOT NULL,
  `m_stok_min` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `1nt3x_vpersediaanawal`
--

DROP TABLE IF EXISTS `1nt3x_vpersediaanawal`;
/*!50001 DROP VIEW IF EXISTS `1nt3x_vpersediaanawal`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `1nt3x_vpersediaanawal` (
  `persediaan_awal_no` tinyint NOT NULL,
  `persediaan_awal_bulan` tinyint NOT NULL,
  `persediaan_awal_tahun` tinyint NOT NULL,
  `m_toko_kode` tinyint NOT NULL,
  `m_item_kode` tinyint NOT NULL,
  `batch_no` tinyint NOT NULL,
  `persediaan_awal_detail_qty` tinyint NOT NULL,
  `tgl_produksi` tinyint NOT NULL,
  `exp_date` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `1nt3x_vsb_peritem_blnini`
--

DROP TABLE IF EXISTS `1nt3x_vsb_peritem_blnini`;
/*!50001 DROP VIEW IF EXISTS `1nt3x_vsb_peritem_blnini`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `1nt3x_vsb_peritem_blnini` (
  `m_toko_kode` tinyint NOT NULL,
  `m_item_kode` tinyint NOT NULL,
  `stok_masuk` tinyint NOT NULL,
  `stok_keluar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `1nt3x_vstokberjalanbulanini`
--

DROP TABLE IF EXISTS `1nt3x_vstokberjalanbulanini`;
/*!50001 DROP VIEW IF EXISTS `1nt3x_vstokberjalanbulanini`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `1nt3x_vstokberjalanbulanini` (
  `stok_berjalan_id` tinyint NOT NULL,
  `m_toko_kode` tinyint NOT NULL,
  `m_item_kode` tinyint NOT NULL,
  `stok_berjalan_tgl` tinyint NOT NULL,
  `stok_berjalan_qty_masuk` tinyint NOT NULL,
  `stok_berjalan_qty_keluar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `format penomoran`
--

DROP TABLE IF EXISTS `format penomoran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `format penomoran` (
  `nama proses` varchar(50) DEFAULT NULL,
  `format no` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `format penomoran`
--

LOCK TABLES `format penomoran` WRITE;
/*!40000 ALTER TABLE `format penomoran` DISABLE KEYS */;
INSERT INTO `format penomoran` VALUES ('adjusment stok','AS/bulantahun/auto  increment(5 digit)'),('penerimaan barang','PB/bulantahun/auto increment(5 digit)'),('persediaan awal','PA/bulantahun/auto increment(5 digit)'),('purchase order','PO/bulantahun/auto increment(5 digit)'),('return pembelian','RP/nopengiriman/bulantahun/auto increment(5 digit)'),('stok opname','SO/bulantahun/auto increment(5 digit)'),('transfer barang','TF/kodetokoasal/kodetokotujuan/bulantahun/auto inc'),('kode toko','2digitlokasi+auto increment(3digit)'),('suplier','2digitnamaperusahaan+auto increment(3digit)'),('transaksi pos','kodetoko/bulantahun/no auto(5 digit)');
/*!40000 ALTER TABLE `format penomoran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `1nt3x_vbarang`
--

/*!50001 DROP TABLE IF EXISTS `1nt3x_vbarang`*/;
/*!50001 DROP VIEW IF EXISTS `1nt3x_vbarang`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `1nt3x_vbarang` AS select `mi`.`m_item_kode` AS `m_item_kode`,`mi`.`m_item_barcode` AS `m_item_barcode`,`mi`.`m_item_nama` AS `m_item_nama`,`mi`.`m_item_kategori_kode` AS `m_item_kategori_kode`,`mi`.`m_item_ket` AS `m_item_ket`,`mi`.`m_item_gambar` AS `m_item_gambar`,`mh`.`m_harga_jual` AS `m_harga_jual` from (`1nt3x_m_item` `mi` join `1nt3x_m_harga` `mh` on((`mh`.`m_item_kode` = `mi`.`m_item_kode`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `1nt3x_vharga_promo`
--

/*!50001 DROP TABLE IF EXISTS `1nt3x_vharga_promo`*/;
/*!50001 DROP VIEW IF EXISTS `1nt3x_vharga_promo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `1nt3x_vharga_promo` AS select `mhp`.`m_harga_promo_id` AS `m_harga_promo_id`,`mhp`.`m_harga_promo_harga` AS `m_harga_promo_harga`,`mhp`.`m_harga_promo_periode_mulai` AS `m_harga_promo_periode_mulai`,`mhp`.`m_harga_promo_periode_akhir` AS `m_harga_promo_periode_akhir`,`mhp`.`m_harga_promo_qty_promo` AS `m_harga_promo_qty_promo`,`mhp`.`m_harga_promo_tgl_buat` AS `m_harga_promo_tgl_buat`,`mhp`.`m_harga_promo_tgl_ubah` AS `m_harga_promo_tgl_ubah`,`mhp`.`m_harga_promo_petugas` AS `m_harga_promo_petugas`,`mhp`.`m_item_kode` AS `m_item_kode`,`mhpd`.`m_toko_kode` AS `m_toko_kode` from (`1nt3x_m_harga_promo` `mhp` join `1nt3x_m_harga_promo_detail` `mhpd` on((`mhpd`.`m_harga_promo_id` = `mhp`.`m_harga_promo_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `1nt3x_vinfostok_blnini`
--

/*!50001 DROP TABLE IF EXISTS `1nt3x_vinfostok_blnini`*/;
/*!50001 DROP VIEW IF EXISTS `1nt3x_vinfostok_blnini`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `1nt3x_vinfostok_blnini` AS select `vbn`.`m_item_kode` AS `m_item_kode`,`vbn`.`m_toko_kode` AS `m_toko_kode`,`vbn`.`stok_keluar` AS `stok_keluar`,`vbn`.`stok_masuk` AS `stok_masuk`,ifnull(`pa`.`persediaan_awal_detail_qty`,0) AS `QTY` from (`1nt3x_vsb_peritem_blnini` `vbn` left join `1nt3x_vpersediaanawal` `pa` on((`vbn`.`m_item_kode` = `pa`.`m_item_kode`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `1nt3x_vminstok`
--

/*!50001 DROP TABLE IF EXISTS `1nt3x_vminstok`*/;
/*!50001 DROP VIEW IF EXISTS `1nt3x_vminstok`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `1nt3x_vminstok` AS select `a`.`m_item_kode` AS `m_item_kode`,`a`.`m_item_barcode` AS `m_item_barcode`,`a`.`m_item_nama` AS `m_item_nama`,`a`.`m_item_kategori_kode` AS `m_item_kategori_kode`,`a`.`m_item_ket` AS `m_item_ket`,`a`.`m_item_gambar` AS `m_item_gambar`,`a`.`m_harga_jual` AS `m_harga_jual`,`b`.`m_stok_min` AS `m_stok_min` from (`1nt3x_vbarang` `a` join `1nt3x_m_stok_min` `b` on((`b`.`m_item_kode` = `a`.`m_item_kode`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `1nt3x_vpersediaanawal`
--

/*!50001 DROP TABLE IF EXISTS `1nt3x_vpersediaanawal`*/;
/*!50001 DROP VIEW IF EXISTS `1nt3x_vpersediaanawal`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `1nt3x_vpersediaanawal` AS select `pa`.`persediaan_awal_no` AS `persediaan_awal_no`,`pa`.`persediaan_awal_bulan` AS `persediaan_awal_bulan`,`pa`.`persediaan_awal_tahun` AS `persediaan_awal_tahun`,`pa`.`m_toko_kode` AS `m_toko_kode`,`pd`.`m_item_kode` AS `m_item_kode`,`pd`.`batch_no` AS `batch_no`,`pd`.`persediaan_awal_detail_qty` AS `persediaan_awal_detail_qty`,`pd`.`tgl_produksi` AS `tgl_produksi`,`pd`.`exp_date` AS `exp_date` from (`1nt3x_inv_persediaan_awal_detail` `pd` join `1nt3x_inv_persediaan_awal` `pa` on((`pa`.`persediaan_awal_no` = `pd`.`persediaan_awal_no`))) where (`pa`.`persediaan_awal_bulan` = date_format(now(),'%m')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `1nt3x_vsb_peritem_blnini`
--

/*!50001 DROP TABLE IF EXISTS `1nt3x_vsb_peritem_blnini`*/;
/*!50001 DROP VIEW IF EXISTS `1nt3x_vsb_peritem_blnini`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `1nt3x_vsb_peritem_blnini` AS select `sbn`.`m_toko_kode` AS `m_toko_kode`,`sbn`.`m_item_kode` AS `m_item_kode`,sum(`sbn`.`stok_berjalan_qty_masuk`) AS `stok_masuk`,sum(`sbn`.`stok_berjalan_qty_keluar`) AS `stok_keluar` from `1nt3x_vstokberjalanbulanini` `sbn` group by `sbn`.`m_item_kode` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `1nt3x_vstokberjalanbulanini`
--

/*!50001 DROP TABLE IF EXISTS `1nt3x_vstokberjalanbulanini`*/;
/*!50001 DROP VIEW IF EXISTS `1nt3x_vstokberjalanbulanini`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `1nt3x_vstokberjalanbulanini` AS select `sb`.`stok_berjalan_id` AS `stok_berjalan_id`,`sb`.`m_toko_kode` AS `m_toko_kode`,`sb`.`m_item_kode` AS `m_item_kode`,`sb`.`stok_berjalan_tgl` AS `stok_berjalan_tgl`,`sb`.`stok_berjalan_qty_masuk` AS `stok_berjalan_qty_masuk`,`sb`.`stok_berjalan_qty_keluar` AS `stok_berjalan_qty_keluar` from `1nt3x_inv_stok_berjalan` `sb` where (`sb`.`stok_berjalan_tgl` between (curdate() - interval (dayofmonth(curdate()) - 1) day) and last_day((now() + interval 0 month))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-05  5:59:39
