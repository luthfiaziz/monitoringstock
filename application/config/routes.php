<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "cpanel";
$route['404_override'] = '';


$route['auth'] 				= 'auth/app_auth';
$route['auth_confirm']	    = 'auth/app_auth/login_auth';
$route['auth/logout']		= 'cpanel/cpanel/logout';

$route['auth/tab']			= 'auth_mobile/app_auth';
$route['auth_confirm/tab']	= 'auth_mobile/app_auth/login_auth';
$route['auth/logout/tab']	= 'cpanel/cpanel/logout';


$route['auth/phone']			= 'auth_phone/app_auth';
$route['auth_confirm/phone']	= 'auth_phone/app_auth/login_auth';
$route['auth/logout/phone']		= 'cpanel/cpanel/logout';


// POS MOBILE

$route['pos/gettype']				= 'auth/app_auth/login_checking_type';

$route['mb.pos']					= 'mobile_pos/mobile_index';
$route['mb.pos/transaksi']			= 'mobile_pos/transaksi';
$route['mb.pos/laporan_penjualan']	= 'mobile_pos/laporan_penjualan';
$route['mb.pos/minimum_stok']		= 'mobile_pos/minimum_stok';
$route['mb.pos/pengaturan']			= 'mobile_pos/pengaturan';
$route['mb.pos/phone/load_trx']		= 'mobile_pos/phone/phone_load_transaksi';

$route['mb.pos/tab']		 		= 'mobile_pos/tab_pos/mobile_tab_home';
$route['mb.pos/tab/transaction'] 	= 'mobile_pos/tab_pos/mobile_tab_transaction';
$route['mb.pos/tab/search_ktg']	 	= 'mobile_pos/tab_pos/mobile_search_kategori';
$route['mb.pos/tab/search_brg']	 	= 'mobile_pos/tab_pos/mobile_search_brg';
$route['mb.pos/tab/simpan-data'] 	= 'mobile_pos/tab_pos/prosesCash';
$route['mb.pos/tab/minstok'] 		= 'mobile_pos/tab_pos/minimumstok';
$route['mb.pos/tab/minstok-detail/(:any)'] = 'mobile_pos/tab_pos/minimumstok_detail/$1';
$route['mb.pos/tab/lapjual'] 		= 'mobile_pos/tab_pos/laporanpenjualan';

$route['mb.pos/tes']		= 'mobile_pos/mobile_tes';
$route['mb.pos/get-data']	= 'mobile_pos/get_data';

/* End of file routes.php */
/* Location: ./application/config/routes.php */