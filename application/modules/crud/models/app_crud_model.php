<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Luthfi Aziz Nugraha 
 * Create Date : 26/07/2014 - 00:43:05
 * Module Name : CRUD Model ( For Template )
 */

class app_crud_model extends CI_Model {
    //-----------------------------------------------------------------------------------------------------------
    // SAVE
    //-----------------------------------------------------------------------------------------------------------
	public function save_as_new($table, $data){
        $this->db->trans_begin();

        $this->db->insert($table, $data);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
	}

    // public function save_as_new($table, $data){
    //     $queryString = $this->generateDELAYEDQUERY($table,$data);

    //     return $this->db->query($queryString);
    // }

    public function save_as_new_nrb($table, $data){
        return $this->db->insert($table, $data);
    }


    //-----------------------------------------------------------------------------------------------------------
    // UPDATE
    //-----------------------------------------------------------------------------------------------------------
    public function save($table, $data, $key) {
        $this->db->trans_begin();
        $this->db->update($table, $data, $key);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function save_nrb($table, $data, $key) {
        return $this->db->update($table, $data, $key);
    }

    
    //-----------------------------------------------------------------------------------------------------------
    // SINGLE DELETE
    //-----------------------------------------------------------------------------------------------------------
    public function delete($table,$key) {
        $this->db->trans_begin();

        $this->db->delete($table, $key);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function delete_nrb($table,$key) {
        return $this->db->delete($table, $key);
    }

    public function generateDELAYEDQUERY($table,$data){
        // FIELDNAME
        $fieldname  = "(";
        $ftot       = 1;
        foreach ($data as $key => $values) {
            if($ftot == count($data)){
                $fieldname .= $key;
            }else{
                $fieldname .= $key . ",";
            }

            $ftot++;
        }
        $fieldname .= ")";

        // VALUE
        $value      = "(";
        $vtot       = 1;
        foreach ($data as $key => $values) {
            if($vtot == count($data)){
                $value .= "'" . $values . "'";
            }else{
                $value .= "'" . $values . "',";
            }

            $vtot++;
        }
        $value .= ")";

        return $query = "INSERT DELAYED INTO {$table} {$fieldname} VALUES {$value}";
    }

}