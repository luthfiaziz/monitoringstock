<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_management
 *
 * @author Luthfi Aziz Nugraha
 */

class app_crud extends MX_Controller {
	
	public function __construct() {
            parent::__construct();
            $this->load->model("app_crud_model","crud");
	}

    //-----------------------------------------------------------------------------------------------------------
    // SAVE NEW
    //-----------------------------------------------------------------------------------------------------------
	public function save_as_new($table, $data = array()){
		return $this->crud->save_as_new($table,$data);
	}

    //-----------------------------------------------------------------------------------------------------------
    // SAVE TANPA ROLLBACK
    //-----------------------------------------------------------------------------------------------------------
	public function save_as_new_nrb($table, $data = array()){
		//return $this->crud->save_as_new($table,$data);
        return $this->crud->save_as_new_nrb($table,$data);
	}

    //-----------------------------------------------------------------------------------------------------------
    // UPDATE
    //-----------------------------------------------------------------------------------------------------------
	public function save($table, $data = array(), $id){
		return $this->crud->save($table,$data,$id);
	}

	//-----------------------------------------------------------------------------------------------------------
    // UPDATE TANPA ROLLBACK
    //-----------------------------------------------------------------------------------------------------------
	public function save_nrb($table, $data = array(), $id){
		return $this->crud->save_nrb($table,$data,$id);
	}

    //-----------------------------------------------------------------------------------------------------------
    // DELETE
    //-----------------------------------------------------------------------------------------------------------
    public function delete($table, $id){
        return $this->crud->delete($table,$id);
    }

    public function delete_nrb($table, $id){
        return $this->crud->delete_nrb($table,$id);
    }
}