<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_management
 *
 * @author Luthfi Aziz Nugraha
 */

class Cpanel extends MX_Controller {
    // VARIABEL SESSION TEMP
    private $SESSION;

    public function __construct() {
        parent::__construct();
        
        $this->_module = 'cpanel/cpanel';
        $this->load->module("config/app_core");

        $this->load->model("cpanel_model","cpanel");

        $this->app_auth->checking_user_session();

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

	public function index(){
        $setting      = $this->app_setting;

        //ADD by Bayu Nugraha----------------------------------------------
        $dataPerusahaan         = $this->cpanel->getPerusahaanProfile();
        $data["title"]          = empty($dataPerusahaan->profile_app_name)?"RIS V.1":$dataPerusahaan->profile_app_name;
        $icon                   = empty($dataPerusahaan->profile_app_icon)?"icon_intex.png":$dataPerusahaan->profile_app_icon;
        $data['icon']           = base_url().'assets/img/logo/'.$icon;
        $logo                   = empty($dataPerusahaan->profile_logo)?"logo_intex.png":$dataPerusahaan->profile_logo;
        $data['logo']           = base_url().'assets/img/logo/'.$logo;
        //-----------------------------------------------------------------

        $data["page_content"]   = $setting->get_layout("layout-style-2");
        $data["dashboard_load"] = base_url() . "dashboard";
        $data["top_source"]    = base_url("config/app_menu/get_top/" . $this->SESSION["role_id"]);
        echo Modules::run("app_core/core", $data);
    }

    public function dashboard(){
    }


    public function header($path_module = ''){
        $data["toolbar_info_source"] =  base_url() . $this->_module . "/toolbar_info";
        $data["user"]                = "Anda Masuk Sebagai ".ucwords($this->SESSION["m_user_nama"]).", Di Lokasi ".ucwords(strtolower($this->SESSION["m_samsat_nama"]));
        
        $key = array("m_role_id" => $this->SESSION["m_role_id"],"m_user_id"=>$this->SESSION["m_user_id"]);
        $data_icon                   = $this->cpanel->data($key)->get()->result();

        $data["result"]              = $data_icon;
        $path = explode("-", $path_module);
        $this->load->view($path[0] . "/" . $path[1] . "/header",$data);
    }

    public function toolbar_info(){
        xml::xml_header();
        $xml = '<toolbar>
                    <item id="help" text="Bantuan" type="button" img="help.png" imgdis="help.png"/>
                    <item id="sep" type="separator"/>
                    <item 
                       id="page_setup" 
                       type="buttonSelect" 
                       img="config.png" 
                       imgdis="config.png"
                       text="' . $this->SESSION["m_role_nama"] . '"
                    > 
                        <item type="button" id="infoakun" img="informasi_akun.png" imgdis="informasi_akun.png" text="Informasi Akun"/>
                        <item type="button" id="logout" img="logout.png" imgdis="logout.png"  text="Keluar Aplikasi"/>
                    </item>
                </toolbar>';

        echo $xml;
    }

    public function logout(){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;
        $dataLog    = array(
            'log_user'      => $this->SESSION['user_username'],
            'log_kegiatan'  => "Logout dari system ",
            'log_waktu'     => date('Y-m-d H:i:s')
        );
        $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
        $this->session->sess_destroy();
        return true;
    }

}