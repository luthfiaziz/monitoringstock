<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Luthfi Aziz Nugraha 
 * Create Date : 26/07/2014 - 00:43:05
 * Module Name : Menu Model ( For Template )
 */

class cpanel_model extends CI_Model {
	private $_table1 = "m_menu_click_count";
    private $_table2 = "m_menu";

     public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("m_kecamatan_id" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->from($this->_table1 . " a");
        $this->db->join($this->_table2 . " b","b.m_menu_id=a.m_menu_id");
        $this->db->order_by("a.m_click_count","DESC");
        $this->db->limit(8);
        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    // ADD by Bayu Nugraha----------------------
    public function getPerusahaanProfile(){
        $this->db->from('profile');
        $this->db->where(array('profile_id'=>'1'));

        return $this->db->get()->row();
    }
}
