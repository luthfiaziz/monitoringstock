<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_management
 *
 * @author Luthfi Aziz Nugraha
 */

class app_auth extends MX_Controller {
 	public function __construct() {
        parent::__construct();
        
        $this->_module = 'auth/app_auth';

        $this->load->module("config/app_setting");
        $this->load->model("auth/app_auth_model");

        //ADD by Bayu Nugraha----------------------------------------------
        $this->load->model("cpanel/cpanel_model","cpanel");
    }

    public function index(){
        $e = $this->app_setting;

        $e->set_plugin(array(
            'jquery','jquery-ui', 'foundation', 'custom'
        ));

        $data["js_header" ] = $e->get_js();
        $data["css_header"] = $e->get_css();

        //ADD by Bayu Nugraha----------------------------------------------
        $dataPerusahaan         = $this->cpanel->getPerusahaanProfile();
        $data["app_title"]      = empty($dataPerusahaan->profile_app_name)?"RIS V.1":$dataPerusahaan->profile_app_name;
        $icon                   = empty($dataPerusahaan->profile_app_icon)?"icon_intex.png":$dataPerusahaan->profile_app_icon;
        $data['icon']           = base_url().'assets/img/logo/'.$icon;
        //-----------------------------------------------------------------

//        $data["app_title"]  = "Samsat Bangka Belitung";

        $this->load->view("layout/ministop/login_mobile",$data);
    }

    public function login_auth(){
        $this->form_validation->set_rules('samsat_username', 'Username', 'required');
        $this->form_validation->set_rules('samsat_password', 'Password', 'required');

        $location       = "auth/tab";

        $data["username"] = $this->input->post("samsat_username");;  
        $data["password"] = $this->input->post("samsat_password");; 

        if($this->form_validation->run()){
        	$result = $this->login_process(trim($data["username"]), trim($data["password"]));

        	if($result == TRUE){
        		$data["status"] = "<p>Login Berhasil</p>";
        		$location       = "mb.pos/tab";
        	}else{
        		$data["status"] = "<p>Login Gagal, <u><b>Username</b></u> / <u><b>Password</b></u> yang anda masukan salah</p>";
        	}
        }else{
            $data["status"] = validation_errors();
        }
	    
    	$this->session->set_flashdata($data);
	    redirect(base_url($location));
    }

    private function login_process($usr = '', $pass = ''){
    	$key = array(
    			"a.user_username" => $usr,
    			"a.user_password" => md5($pass)
    		);

    	$data       = $this->set_data_session($this->app_auth_model->data($key)->get());

        $data_count = $this->app_auth_model->data($key)->get();

    	if($data_count->num_rows() > 0){
    		$this->app_auth_model->register_session($data);

    		return TRUE;
    	}else{
    		return FALSE;
    	}
    }

    private function set_data_session($param = array()){
        $data   = $param->result();
        $result = array();

        foreach ($data as $key => $value) {
            // SET SESSION NAME
            $key_id = APPKEY;

            $result[$key_id]["user_id"]               = $value->user_id;
            $result[$key_id]["user_fullname"]         = $value->user_fullname;
            $result[$key_id]["user_username"]         = $value->user_username;
            $result[$key_id]["user_password"]         = $value->user_password;
            $result[$key_id]["user_jk"]               = $value->user_jk;
            $result[$key_id]["user_tgl_lhr"]          = $value->user_tgl_lhr;
            $result[$key_id]["user_alamat"]           = $value->user_alamat;
            $result[$key_id]["user_email"]            = $value->user_email;
            $result[$key_id]["user_tlp"]              = $value->user_tlp;
            $result[$key_id]["user_wrong_pass"]       = $value->user_wrong_pass;
            $result[$key_id]["user_status"]           = $value->user_status;
            $result[$key_id]["role_access_id"]        = $value->role_access_id;
            $result[$key_id]["role_access_nama"]      = $value->role_access_nama;
            $result[$key_id]["role_access_ket"]       = $value->role_access_desc;
            $result[$key_id]["role_id"]               = $value->role_id;
            $result[$key_id]["role_name"]             = $value->role_name;
            $result[$key_id]["toko_kode"]             = $value->m_toko_kode;

            $menu_id = $value->menu_id;

            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_view"]     = $value->role_access_view;
            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_add"]      = $value->role_access_add;
            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_edit"]     = $value->role_access_edit;
            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_delete"]   = $value->role_access_delete;
            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_export"]   = $value->role_access_export;
            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_import"]   = $value->role_access_import;
            $result[$key_id]["otoritas_menu"][$menu_id]["menu_id"]              = $value->menu_id;
        }

        $result_final = $result;

        return $result_final;
    }

    public function checking_user_session(){
        $GET_SESSION = $this->session->userdata(APPKEY);

        if(empty($GET_SESSION["user_username"]) || empty($GET_SESSION["user_password"])){
            return redirect(base_url('auth/tab'));
            exit();
        }else{
           if($GET_SESSION["role_name"] != 'kasir'){
                return redirect(base_url('auth/tab')); 
                exit();
           }
        }
    }

    public function login_checking_type(){
        $detect = new Mobile_Detect();
        if($detect->isMobile()){
            redirect(base_url('mb.pos')); 
        }else if($detect->isTablet()){
            redirect(base_url('mb.pos/tab/home')); 
        }else{
            redirect(base_url('penjualan/pos')); 
        }
    }

    public function sessionCEK(){
        $GET_SESSION = $this->session->userdata(APPKEY);
        $data = array(false);

        if(empty($GET_SESSION["user_username"]) || empty($GET_SESSION["user_password"])){
            $data = array(true);
        }

        echo json_encode($data);
    }
}