<!DOCTYPE html>
<html>
<link rel="icon" href="<?php echo $icon; ?>" type="image/logo">
<head>
	<title>Control Panel Footin 1.0</title>
	<?php 
		echo $js_header;
		echo $css_header;
	?>
</head>
<body>
	<div id="wrapper-login"> 
		<div id="logo">
			<img src="<?php echo base_url(); ?>/fw.assets/fw.img/footin_logo.png" style="height:50px">
		</div>
		<div style="margin-top:10px">
			<form action='auth_confirm' method='post'/>
			<div class="row">
	    		<div class="large-12 columns">
	      			<label style="text-align:left; color:#FFFFFF; font-size:13px; font-family:verdana"><b>Username</b></label>
	      				<?php echo form_input('december_username', !empty($this->session->flashdata('username'))? $this->session->flashdata('username') : '', ' class="input-login"  placeholder="input disini ..." id="december_username"'); ?>
	    		</div>
	  		</div>
			<div class="row">
	    		<div class="large-12 columns">
	      			<label style="text-align:left; color:#FFFFFF; font-size:13px; font-family:verdana"><b>Password</b></label>
	      				<?php echo form_password('december_password', !empty($this->session->flashdata('password'))? $this->session->flashdata('password') : '', 'class="input-login"  placeholder="input disini ..." id="december_password"'); ?>
					
	    		</div>
	  		</div>
			<div class="row">
	    		<div class="large-12 columns" style='text-align:right'>
					<input  class="button small" type='submit' name='submit' value='LOGIN' style='line-height:0px !important;'></label>
	    		</div>
	  		</div>
			</form>
		</div>
	</div>
	<br/>
	<?php 
		if($this->session->flashdata('status') != '' || $this->session->flashdata('status') != null){
	?>
	<div style="width:500px; positon:relative; margin:0px auto; padding:0px;" class="autohidden">
		<div class="alert-box alert" style="padding:10px !important">
			<b style="font-size:11px !important;">[ ERROR ]</b>
		<?php 
			echo $this->session->flashdata('status');
		?>
		</div>
	</div>
	<?php
		}
	?>
	<div id="copyright">
		<img src="<?php echo base_url(); ?>fw.assets/fw.img/intex-small.png" style="width:80px;">
		<br/>
		Copyrights &copy; <b>PT. Marui Intex</b> 2014. All rights reserved.
	</div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$("#december_username").select();

		setTimeout(function() {
			$(".autohidden").fadeOut();
		}, 3500);
	});
</script>
	