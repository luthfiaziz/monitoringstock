<div id="menuloading" class="imageloading">
	<img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:40%;"/>
</div>
<div class="form-wrapper">
    <?php
    $hidden_form = array('id' => !empty($id) ? $id : '');
    echo form_open_multipart($form_action, array('id' => 'menu'), $hidden_form);
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span><b>Nama</b> <i>*</i></span>
                <?php echo form_input('menu_name', !empty($default->menu_name) ? $default->menu_name : '', 'id="menu_name" class="form_web" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Parent</b> <i></i></span>
                <?php echo form_dropdown('menu_parent', $grup_options, !empty($default->menu_parent) ? $default->menu_parent : '', 'id="menu_parent" class="full"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>URL</b> <i>*</i></span>
                <?php echo form_input('menu_url', !empty($default->menu_url) ? arrman::showURL($default->menu_url) : '', 'id="menu_url" class="form_web" placeholder="wajib diisi"'); ?>
                <p style="font-size:11px; margin:0px; padding:0px; background:#FFE9A1; margin:5px; padding:5px; border:1px solid red ">
                    <?php
                        echo "format penulisan url untuk meload halaman<br/> <b style='margin-left:5px; font-size:12px'>nama_module/nama_controller</b><br/><br/>";
                        echo "format penulisan url yang tidak meload halaman<br/> <b style='margin-left:5px; font-size:12px'>#/nama_controller</b>";
                    ?>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Posisi</b> <i>*</i></span>
                <?php echo form_input('menu_position', !empty($default->menu_position) ? $default->menu_position : 0, 'id="menu_position" class="form_web" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Status</b> <i>*</i></span>
                <?php echo form_dropdown('menu_status', array(""=>"--Pilih Data--","1"=>"Aktif", "0"=>"Tidak Aktif"), !empty($default->menu_status) ? $default->menu_status : '', 'id="menu_status" class="full"'); ?>
            </td>
        </tr>

    </table>
    <?php 
        echo form_close(); 
        echo arrman::getRequired();
    ?>
</div>
<script type="text/javascript">
    $("#menu_parent").select2();
    $("#menu_status").select2();
    $("#menu_name").focus();

    function simpan(id){
        var toolbar = ["menuWindows","savemenu","cancelmenu"];

        SimpanData('#menu','#menuloading',toolbar);
    }

    function refresh(){
        close_form_modal(menuWindows,"menuWindows");
        refreshGrid(gridmenu,url + "pengaturan/menu/loadDaftarMenu","json");
    }

    function bersihmenu(){
        var data = [
                        ["menu_parent","","combo"],
                        ["menu_status","","combo"],
                    ];

        var focus = ["menu_name", "input"];

        refreshForm(data, "#menu", focus);
    }   
</script>