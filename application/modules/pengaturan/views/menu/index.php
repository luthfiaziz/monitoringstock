<script type="text/javascript">
    var menuAkses    = "<?php echo $aksesEdit; ?>";
    var menuWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    mainTab.cells("maintabbar").detachToolbar();
    toolbarmenu = mainTab.cells("maintabbar").attachToolbar();
    toolbarmenu.setSkin("dhx_skyblue");
    toolbarmenu.setIconsPath(url + "assets/img/btn/");
    toolbarmenu.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarmenu.attachEvent("onClick", cmenu);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function cmenu(id){
        var module  = url + "pengaturan/menu";
        var idRow   = gridmenu.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(menuWindows,"menuWindows",["Menu",500,500], module + "/loadFormmenuToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(menuWindows,"menuWindows",["Menu",500,500], module + "/loadFormmenuToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({                
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    }

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({ 
                                    type: "alert", 
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridmenu, module + "/loadDaftarMenu","json");
                                            }
                                        }
                                    }
                                });
                            },
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridmenu,url + "pengaturan/menu/loadDaftarMenu","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridmenu = mainTab.cells("maintabbar").attachGrid();
    gridmenu.selMultiRows = true;
    gridmenu.imgURL = "<?php echo base_url(); ?>assets/dhtmlx/imgs/icons_greenfolders/";
    gridmenu.setSkin("dhx_skyblue");
    gridmenu.setHeader("<b>MENU</b>,<b>POSISI</b>,<b>STATUS</b>,<b>#MENUID</b>");
    gridmenu.attachHeader("#text_filter,#rspan,#rspan,#rspan");
    gridmenu.setColAlign("left,left,center,center");
    gridmenu.setColTypes("tree,ro,ro,ro");
    gridmenu.setInitWidths("*,70,70,70");
    gridmenu.setColSorting("str,str,str,str");
    gridmenu.setColumnHidden(3,true);
    gridmenu.init();
    gridmenu.enableDistributedParsing(true,10,300); 
    gridmenu.load("<?php echo $data_source; ?>","json");
    
    if(menuAkses == "TRUE"){
        gridmenu.attachEvent("onRowDblClicked",gridmenuDBLClick);
    }
    // [ACTION] GRID ACTION
    function gridmenuDBLClick(){
        var module  = url + "pengaturan/menu";
        form_modal(menuWindows,"menuWindows",["Menu",500,500], module + "/loadFormmenuToolbar", module + "/loadform/" + gridmenu.getSelectedRowId());
    }
</script>