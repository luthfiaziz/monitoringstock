<div id="aturanloading" class="imageloading" align="center">
	<img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-50%; top:40%;"/>
</div>


<table class="table-form" border="0">
    <tr>
        <td height="10%">
            <div class="form-wrapper full90">
              <?php
                $hidden_form = array('id' => !empty($id) ? $id : '');
                echo form_open_multipart($form_action, array('id' => 'fm_aturan'), $hidden_form);
                ?>
                <table class="form" width="100%">
                    <tr>
                        <td>
                            <span><b>Nama Roles</b><i>*</i></span>
                            <?php echo form_input('role_name', !empty($default->role_name) ? $default->role_name : '', 'id="role_name" class="form_web" placeholder="wajib diisi"'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span><b>Keterangan Role</b> <i>*</i></span>
                            <?php echo form_input('role_desc', !empty($default->role_desc) ? $default->role_desc : '', 'id="role_desc" class="form_web" placeholder="wajib diisi"'); ?>
                        </td>
                    </tr>
              </table>
                <?php
                echo form_close();
                echo arrman::getRequired();
                ?>
          </div>
        </td>
    </tr>
    <tr>
        <td valign="top"><div id="wrapGridAturanForm" class="grid"></div></td>
    </tr>
</table>
<script type="text/javascript">
    $("#role_name").focus();

    function simpan(id){
        var toolbar = ["aturanWindows","saveaturan","cancelaturan"];
        var data = {
            grid : $.csv.toArrays(GridAturanForm.serializeToCSV())
        }

        SimpanData('#fm_aturan','#aturanloading',toolbar,data);
    }

    function refresh(){
        close_form_modal(aturanWindows,"aturanWindows");
        refreshGrid(gridaturan,url + "pengaturan/aturan/listdata","json");
    }

    function bersihaturan(){
        var data = [];

        var focus = ["role_name", "input"];

        refreshForm(data, "#fm_aturan", focus);
    }

    function KontrolAturan(id){
        if(id == "checkallaturan"){
            onofcheckboxGrid(1);
        }

        if(id == "uncheckallaturan"){
            onofcheckboxGrid(0);
        }
    }

    function onofcheckboxGrid(value){
        GridAturanForm.forEachRow(function(id){
            GridAturanForm.cells(id,1).setValue(value);
            GridAturanForm.cells(id,2).setValue(value);
            GridAturanForm.cells(id,3).setValue(value);
            GridAturanForm.cells(id,4).setValue(value);
            GridAturanForm.cells(id,5).setValue(value);
            GridAturanForm.cells(id,6).setValue(value);
        });
    }

    GridAturanForm = new dhtmlXGridObject("wrapGridAturanForm");
    GridAturanForm.selMultiRows = true;
    GridAturanForm.imgURL = "<?php echo base_url(); ?>assets/dhtmlx/imgs/icons_greenfolders/";
    GridAturanForm.setSkin("dhx_skyblue");
    GridAturanForm.setHeader("<b>MENU</b>,<b>LIHAT</b>,<b>TAMBAH</b>,<b>EDIT</b>,<b>HAPUS</b>,<b>EXPORT</b>,<b>IMPORT</b>,<b>#MENUID</b>");
    GridAturanForm.setColAlign("left,center,center,center,center,center,center,center");
    GridAturanForm.setColTypes("tree,ch,ch,ch,ch,ch,ch,ro");
    GridAturanForm.setInitWidths("*,80,80,80,80,80,80,80");
    GridAturanForm.setColSorting("str,str,str,str,str,str,str,str");
    GridAturanForm.setColumnHidden(7,true);
    GridAturanForm.init();
    GridAturanForm.load("<?php echo $menu_pengguna_source; ?>","json");
</script>
