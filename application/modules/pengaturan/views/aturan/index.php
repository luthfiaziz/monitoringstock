<script type="text/javascript">
    var aturanAkses    = "<?php echo $aksesEdit; ?>";
    var aturanWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    mainTab.cells("maintabbar").detachToolbar();
    
    toolbarAturan = mainTab.cells("maintabbar").attachToolbar();
    toolbarAturan.setSkin("dhx_skyblue");
    toolbarAturan.setIconsPath(url + "assets/img/btn/");
    toolbarAturan.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarAturan.attachEvent("onClick", caturan);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function caturan(id){
        var module  = url + "pengaturan/aturan";
        var idRow   = gridaturan.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(aturanWindows,"aturanWindows",["Aturan",900,600], module + "/loadFormaturanToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(aturanWindows,"aturanWindows",["Aturan",900,600], module + "/loadFormaturanToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({                
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    }

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({ 
                                    type: "alert", 
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridaturan, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            },
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridaturan,url + "pengaturan/aturan/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridaturan = mainTab.cells("maintabbar").attachGrid();
    gridaturan.setHeader("Role Nama, Keterangan Role");
    gridaturan.attachHeader("#text_filter,#text_filter"); 
    gridaturan.setColAlign("left,left");
    gridaturan.setColTypes("ro,ro");
    gridaturan.setInitWidths("*,*");
    gridaturan.init();    
    gridaturan.load("<?php echo $data_source; ?>","json");

    if(aturanAkses == "TRUE"){
        gridaturan.attachEvent("onRowDblClicked",gridaturanDBLClick);
    }

    // [ACTION] GRID ACTION
    function gridaturanDBLClick(){
        var module  = url + "pengaturan/aturan";
        form_modal(aturanWindows,"aturanWindows",["Aturan",900,600], module + "/loadFormaturanToolbar", module + "/loadform/" + gridaturan.getSelectedRowId());
    }
</script>