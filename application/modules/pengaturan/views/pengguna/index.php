<script type="text/javascript">
    var penggunaAkses    = "<?php echo $aksesEdit; ?>";
    var penggunaWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
	// [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    mainTab.cells("maintabbar").detachToolbar();
    
    toolbarPengguna = mainTab.cells("maintabbar").attachToolbar();
    toolbarPengguna.setSkin("dhx_skyblue");
 	toolbarPengguna.setIconsPath(url + "assets/img/btn/");
 	toolbarPengguna.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarPengguna.attachEvent("onClick", cPengguna);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function cPengguna(id){
        var module  = url + "pengaturan/pengguna";
        var idRow   = gridPengguna.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(penggunaWindows,"penggunaWindows",["Pengguna",500,530], module + "/loadFormPenggunaToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(penggunaWindows,"penggunaWindows",["Pengguna",500,530], module + "/loadFormPenggunaToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({                
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    }

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({ 
                                    type: "alert", 
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridPengguna, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            },
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridPengguna,url + "pengaturan/pengguna/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridPengguna = mainTab.cells("maintabbar").attachGrid();
    gridPengguna.setImagePath(url + "assets/dhtmlx/imgs/");
    gridPengguna.setHeader("Username,Nama, Jenis Akses, Email, Telpon, Toko, User Status");
	gridPengguna.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#combo_filter,#combo_filter");
	gridPengguna.setColAlign("left,left,left,left,left,left,center");
	gridPengguna.setColTypes("ro,ro,ro,ro,ro,ro,ro");
	gridPengguna.setInitWidths("*,*,*,*,*,*,*");
	gridPengguna.init(); 
	gridPengguna.enableDistributedParsing(true,10,300);   
	gridPengguna.load("<?php echo $data_source; ?>","json");

    if(penggunaAkses == "TRUE"){
    	gridPengguna.attachEvent("onRowDblClicked",gridPenggunaDBLClick);
    }

    // [ACTION] GRID ACTION
	function gridPenggunaDBLClick(){
        var module  = url + "pengaturan/pengguna";
        form_modal(penggunaWindows,"penggunaWindows",["Pengguna",500,530], module + "/loadFormPenggunaToolbar", module + "/loadform/" + gridPengguna.getSelectedRowId());
    }
</script>