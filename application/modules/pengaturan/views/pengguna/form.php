<div id="penggunaloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:40%;"/>
</div>
<div class="form-wrapper">
    <?php
    $hidden_form = array('id' => !empty($id) ? $id : '');
    echo form_open_multipart($form_action, array('id' => 'fm_pengguna'), $hidden_form);
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span><b>Nama</b> <i>*</i></span>
                <?php echo form_input('user_fullname', !empty($default->user_fullname) ? $default->user_fullname : '', 'id="user_fullname" class="form_web" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Username</b> <i>*</i></span>
                <?php echo form_input('user_username',!empty($default->user_username) ? $default->user_username : '', 'id="user_username" class="form_web disabledspace" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Password</b> <i>*</i></span>
                <?php echo form_password('user_password',!empty($default->user_password_length) ? arrman::PAS_SYMBOL("*",$default->user_password_length) : '', 'id="user_password" class="form_web" placeholder="wajib diisi"'); ?>
                <?php echo form_input('user_password_length',!empty($default->user_password_length) ? $default->user_password_length : '', 'id="user_password_length" class="form_web" placeholder="wajib diisi" style="display:none"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Akses</b> <i>*</i></span>
                <?php echo form_dropdown('role_id', $grup_options, !empty($default->role_id) ? $default->role_id : '', 'id="role_id" class="full"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Telpon</b> <i>*</i></span>
                <?php echo form_input('user_tlp', !empty($default->user_tlp) ? $default->user_tlp : '', 'id="user_tlp" class="form_web onlynumber" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Email</b> <i>*</i></span>
                <?php echo form_input('user_email', !empty($default->user_email) ? $default->user_email : '', 'id="user_email" class="form_web" placeholder="isikan email dengan benar"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Toko</b> <i>*</i></span>
                <?php echo form_dropdown('m_toko_kode', $toko_options, !empty($default->m_toko_kode) ? $default->m_toko_kode : '', 'id="m_toko_kode" class="full"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Status</b> <i>*</i></span>
                <?php echo form_dropdown('user_status', array(""=>"--Pilih Data--","1"=>"Aktif", "0"=>"Tidak Aktif"), !empty($default->user_status) ? $default->user_status : '', 'id="user_status" class="full"'); ?>
            </td>
        </tr>
        <tr>
            <td><?php echo arrman::getRequired(); ?></td>
        </tr>

    </table>
    <?php 
        echo form_close();
    ?>
</div>
<script type="text/javascript">
    $("#role_id").select2();
    $("#m_samsat_id").select2();
    $("#user_status").select2();
    $("#user_fullname").focus();
    $("#m_toko_kode").select2();

    function simpan(id){
        var toolbar = ["penggunaWindows","savePengguna","cancelPengguna"];

        SimpanData('#fm_pengguna','#penggunaloading',toolbar);
    }

    function refresh(){
        close_form_modal(penggunaWindows,"penggunaWindows");
        refreshGrid(gridPengguna,url + "pengaturan/pengguna/listdata","json");
    }
    
    function bersihPengguna(){
        var data = [
                        ["role_id","","combo"],
                        ["m_samsat_id","","combo"],
                        ["user_status","","combo"],
                    ];

        var focus = ["user_fullname", "input"];

        refreshForm(data, "#fm_pengguna", focus);
    }   
</script>