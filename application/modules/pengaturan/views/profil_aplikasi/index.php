<script type="text/javascript">
    mainTab.cells("maintabbar").detachToolbar();
</script>
<div id="profilapploading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:40%;"/>
</div>
<div style="padding:20px; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
	<?php if($button_view == "true"){ ?> <img src="<?php echo base_url(); ?>assets/img/btn/document_edit.png" class="editapp" onclick="editApp();" id="imagebutton"> <?php } ?>
	<div id="menuname" style="display:none"><?php echo $menuname; ?></div>
	<div id="menudid" style="display:none"><?php echo $menudid; ?></div>
    <?php
    $hidden_form = array('id' => !empty($id) ? $id : '');
    echo form_open_multipart($form_action, array('id' => 'fm_profile_app'), $hidden_form);
    ?>
	<fieldset style="width:97%">
		<legend>Informasi Profil Aplikasi</legend><br/>
		<table style="padding:10px; border-collapse:collapse" border="0" width="100%">
			<tbody id="tableinfo">
				<tr>
					<td colspan="2" width="50%">
						<span class="title_app">Nama Aplikasi</span><br/>
						<span class="title_content" id="nama_aplikasi"><?php echo $data_perusahaan->profile_app_name; ?></span>
					</td>
					<td rowspan="6" valign="top">
						<span class="title_app">Logo Perusahaan</span><br/><br/>
		                <?php $img = !empty($data_perusahaan->profile_logo) ? $data_perusahaan->profile_logo :'no_image.png'; ?>
		                <span class="title_content">
			                <img id="uploadPreview2" src="<?php echo base_url(); ?>assets/img/logo/<?php echo $img; ?>" class="upload"/><br />  
			            </span>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<span class="title_app">Nama Perusahaan</span><br/>
						<span class="title_content" id="nama_perusahaan"><?php echo $data_perusahaan->profile_name; ?></span>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<span class="title_app">NPWP</span><br/>
						<span class="title_content" id="npwp"><?php echo $data_perusahaan->profile_npwp; ?></span>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<span class="title_app">Alamat</span><br/>
						<span class="title_content" id="alamat"><?php echo $data_perusahaan->profile_address; ?></span>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<span class="title_app">Telepon</span><br/>
						<span class="title_content" id="telepon"><?php echo $data_perusahaan->profile_phone; ?></span>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<span class="title_app">Fax</span><br/>
						<span class="title_content" id="fax"><?php echo $data_perusahaan->profile_fax; ?></span>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<span class="title_app">Email</span><br/>
						<span class="title_content">
							<?php $mail = $data_perusahaan->profile_email; ?>
							<a href="mailto:<?php echo $mail; ?>" class="mail" id="email"><?php echo $mail; ?></a>
						</span>
					</td>
				</tr>
			</tbody>
		</table>
	</fieldset>
    <?php 
        echo form_close(); 
    ?>
	<div id="tempGambar" style="display:none"><?php echo $img; ?></div>
	<div id="tempIcon" style="display:none"><?php echo $imgicon; ?></div>
	<div id="tempHtml" style="display:none"></div>
</div>

<script type="text/javascript">
	var oldHtml = "";

	function editApp(){
		var nama_aplikasi 	= '<input class="input_profile"  name="nama_aplikasi"  	id="nama_aplikasi" 	 value="' + $("#nama_aplikasi").html() + '"/>';
		var nama_perusahaan = '<input class="input_profile"  name="nama_perusahaan" id="nama_perusahaan" value="' + $("#nama_perusahaan").html() + '"/>';
		var	npwp 			= '<input class="input_profile"  name="npwp" 			id="npwp" 			 value="' + $("#npwp").html() + '"/>';
		var	alamat 			= '<input class="input_profile"  name="alamat" 			id="alamat" 		 value="' + $("#alamat").html() + '"/>';
		var	telepon 		= '<input class="input_profile"  name="telepon" 		id="telepon" 		 value="' + $("#telepon").html() + '"/>';
		var	fax 			= '<input class="input_profile"  name="fax" 			id="fax" 			 value="' + $("#fax").html() + '"/>';
		var	email 			= '<input class="input_profile"  name="email" 			id="email" 			 value="' + $("#email").html() + '"/>';
		var image           = $("#tempGambar").html();
		var icon            = $("#tempIcon").html();

		oldHtml         	= $("#tableinfo").html();

		$("#tempHtml").html(oldHtml);
		    html  = '<tr>' + 
						'<td colspan="2" width="50%">' +
							'<span class="title_app">Nama Aplikasi</span><br/>' +
							'<span class="title_content">' + nama_aplikasi + '</span>' +
						'</td>' +
						'<td rowspan="6" valign="top">' +
							'<span class="title_app">Logo Perusahaan</span><br/><br/>' +
					        '<span class="title_content">' +
					            '<img id="uploadPreview2" src="' + url + 'assets/img/logo/' + image + '" class="upload"/><br /><br />' +   
					            '<input id="uploadImage2" type="file" name="images[]" onchange="PreviewImage(2);" />' + 
					        '</span>' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td colspan="2">' +
							'<span class="title_app">Nama Perusahaan</span><br/>' +
							'<span class="title_content">' + nama_perusahaan + '</span>' +
						'</td>' +
					'</tr>' +
					'<tr>' + 
						'<td colspan="2">' +
							'<span class="title_app">NPWP</span><br/>' +
							'<span class="title_content">' + npwp + '</span>' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td colspan="2">' +
							'<span class="title_app">Alamat</span><br/>' +
							'<span class="title_content">' + alamat + '</span>' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td colspan="2">' +
							'<span class="title_app">Telepon</span><br/>' +
							'<span class="title_content">' + telepon +'</span>' +
						'</td>' +
					'</tr>' +
					'<tr>' + 
						'<td colspan="2">' +
							'<span class="title_app">Fax</span><br/>' +
							'<span class="title_content">' + fax + '</span>' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td colspan="2">' +
							'<span class="title_app">Email</span><br/>' +
							'<span class="title_content">' + email + '</span>' +
						'</td>' +
						'<td align="right" colspan="2">' +
							'<a href="#" class="buttonsimpans" onclick="SimpanDataProfile()">' + 'Simpan' + '</a>' +
						'</td>' +
					'</tr>';

					$("#tableinfo").html(html);

					$("#imagebutton").attr("src", url + "assets/img/btn/cancelbutton.png");
					$("#imagebutton").attr("onclick", "cancelApp()");

					$("#nama_aplikasi").focus();
	}

	function cancelApp(){
		$("#tableinfo").html($("#tempHtml").html());
		$("#tempHtml").empty();

		$("#imagebutton").attr("src", url + "assets/img/btn/document_edit.png");
		$("#imagebutton").attr("onclick", "editApp()");
	}

	function SimpanDataProfile(){
        SimpanData('#fm_profile_app','#profilapploading', 'tanpatoolbar');
	}

	function refresh(){	   
	 	var text 			= $("#menuname").html();
	    var textLength 		= text.length;
	    var idTabb 			= $("#menudid").html();

	    if(parseInt(textLength) >= 12) {
	        var panjang = parseInt(textLength) * 9;
	    } else {
	        var panjang = 100;
	    }

		mainTab.clearAll();
		mainTab.addTab("maintabbar", text, panjang); 
		mainTab.cells("maintabbar").attachURL(url + "pengaturan/profil_aplikasi?id=" + idTabb,true);
		mainTab.setTabActive("maintabbar");
	}
</script>