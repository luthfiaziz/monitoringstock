<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 25/08/15
 * Time: 15:26
 */
?>
<script type="text/javascript">
    var appProfileAkses    = "<?php echo $aksesEdit; ?>";
    var appProfileWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    mainTab.cells("maintabbar").detachToolbar();

    toolbarappProfile = mainTab.cells("maintabbar").attachToolbar();
    toolbarappProfile.setSkin("dhx_skyblue");
    toolbarappProfile.setIconsPath(url + "assets/img/btn/");
    toolbarappProfile.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarappProfile.attachEvent("onClick", cappProfile);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function cappProfile(id){
        if(id == "simpan"){
            simpan();
        }
    }

    function simpan(id){
        var toolbar = ["simpan"];
        SimpanDataTF('#fm_application_profile','#appProfileloading',toolbar);
    }
</script>
<div id="appProfileloading" class="imageloading" align="center">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-50%; top:40%;"/>
</div>
<div>
    <?php
    $form_hidden    = array(
        "profile_logo"      =>(!empty($default->profile_logo) ? $default->profile_logo : ''),
        "profile_app_icon"  =>(!empty($default->profile_app_icon) ? $default->profile_app_icon : '')
    );
    echo form_open_multipart($form_action, array('id' => 'fm_application_profile'),$form_hidden);
    ?>
    <table width="98%">
        <tr>
            <td width="65%" valign="top">
                <fieldset style="width: 95%;">
                    <legend>Profile</legend>
                    <table width="100%">
                        <tr>
                            <td>
                                <span><b>Nama Perusahaan</b></span>
                                <?php echo form_input('profile_name', !empty($default->profile_name) ? $default->profile_name : '', 'id="profile_name" class="form_web" '.$disabled); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span><b>NPWP</b></span>
                                <?php echo form_input('profile_npwp', !empty($default->profile_npwp) ? $default->profile_npwp : '', 'id="profile_npwp" class="form_web" '.$disabled); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span><b>Telphone</b></span>
                                <?php echo form_input('profile_phone', !empty($default->profile_phone) ? $default->profile_phone : '', 'id="profile_phone" class="form_web" '.$disabled); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span><b>Fax</b></span>
                                <?php echo form_input('profile_fax', !empty($default->profile_fax) ? $default->profile_fax : '', 'id="profile_fax" class="form_web" '.$disabled); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span><b>E-mail</b></span>
                                <?php echo form_input('profile_email', !empty($default->profile_email) ? $default->profile_email : '', 'id="profile_email" class="form_web" '.$disabled); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span><b>Application Name</b></span>
                                <?php echo form_input('profile_app_name', !empty($default->profile_app_name) ? $default->profile_app_name : '', 'id="profile_app_name" class="form_web" '.$disabled); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span><b>Alamat</b></span>
                                <?php echo form_textarea('profile_address', !empty($default->profile_address) ? $default->profile_address : '', 'id="profile_address" class="form_web uppercase" style="height:60px" '.$disabled); ?>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td width="35%" align="right" valign="top">
                <fieldset style="width: 95%;">
                    <legend>Logo Perusahaan</legend>
                    <table width="100%">
                        <tr>
                            <td>
                                <?php $img = !empty($default->profile_logo) ? $default->profile_logo :'logo_intex.png'; ?>
                                <img width="80%" height="150" id="uploadPreview2" src="<?php echo base_url(); ?>assets/img/logo/<?php echo $img; ?>" class="upload"/>
                                <input id="uploadImage2" type="file" name="images[]" onchange="PreviewImage(2);" <?php echo  $disabled;?>/><br />
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset>
                    <legend>Icon Aplikasi</legend>
                    <table width="100%">
                        <tr>
                            <td align>
                                <?php $img = !empty($default->profile_app_icon) ? $default->profile_app_icon :'icon_intex.png'; ?>
                                <img width="75" height="75" id="uploadPreview3" src="<?php echo base_url(); ?>assets/img/logo/<?php echo $img; ?>" class="upload"/>
                                <input id="uploadImage3" type="file" name="images[]" onchange="PreviewImage(3);" <?php echo  $disabled;?>/><br />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    <?php echo form_close();?>
</div>