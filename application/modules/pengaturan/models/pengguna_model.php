<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Luthfi Aziz Nugraha 
 * Create Date : 26/07/2014 - 00:43:05
 * Module Name : Menu Model ( For Template )
 */

class pengguna_model extends CI_Model {
	private $_table1 = "users";
    private $_table2 = "roles";
    private $_table3 = "m_toko";

     public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("user_id" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->select("a.*,b.*,c.m_toko_nama");
        $this->db->from($this->_table1 . " a");
        $this->db->join($this->_table2 . " b","b.role_id = a.role_id");
        $this->db->join($this->_table3 . " c","c.m_toko_kode = a.m_toko_kode","left");
        $this->db->order_by("a.user_username","ASC");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData(){
    	$result = $this->data()->get();	
    	$data   = array();

    	foreach ($result->result() as $value) {
			$data[] = array(
							"id" => $value->user_id, 
							"data"=>array(
                                           $value->user_username,
										   $value->user_fullname,
										   $value->role_name,
										   $value->user_email,
                                           $value->user_tlp,
                                           $value->m_toko_nama,
										   (($value->user_status == 1)? 'Aktif' : 'Tidak Aktif'),
								         )
						    );
    	}

    	return $data;
    }

    public function options($default = '--Pilih Data--') {
        $option = array();
        $list   = $this->db->get($this->_table2);

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->role_id] = $row->role_name;
        }

        return $option;
    }

    
    //------------------------------------------------------------------------------------------------------------
    // GET MAX ID
    //------------------------------------------------------------------------------------------------------------
    public function getMax_ID($field = ""){
        $this->db->select_max($field);
        $this->db->from($this->_table1);

        return $this->db->get()->row();
    }   


    public function hitungJikaAda($key = array())
    {
        $this->db->from($this->_table1);
        
        if(is_array($key))
            $this->db->where($key);

        $total=$this->db->count_all_results();

        return $total;
    }
}
