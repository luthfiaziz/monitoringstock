<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 25/08/15
 * Time: 15:59
 */
class application_profile_model extends CI_Model {
    private $_table1 = "profile";

    public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("profile_id" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->from($this->_table1);

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }
}