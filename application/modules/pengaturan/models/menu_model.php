<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Luthfi Aziz Nugraha 
 * Create Date : 26/07/2014 - 00:43:05
 * Module Name : Menu Model ( For Template )
 */

class menu_model extends CI_Model {
    private $_table1 = "menus";

     public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("menu_id" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->from($this->_table1 . " a");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }
    
    //------------------------------------------------------------------------------------------------------------
    // LOAD TREEGRID MENU
    //------------------------------------------------------------------------------------------------------------
    public function data_menu($id = ''){
        $sql = "SELECT *
                FROM
                    (
                        SELECT
                            `a`.*, `child`.`have_child`
                        FROM
                            (`1nt3x_menus` a)
                        LEFT OUTER JOIN(
                            SELECT
                                menu_parent,
                                COUNT(*)AS have_child
                            FROM
                                `1nt3x_menus`
                            GROUP BY
                                menu_parent
                        )child ON `a`.menu_id = `child`.menu_parent
                    )my_menu
                WHERE 
                    `my_menu`.`menu_parent` = '{$id}' ORDER BY `my_menu`.`menu_position` ASC
                ";

        return $this->db->query($sql);
    }

    //------------------------------------------------------------------------------------------------------------
    // LOAD TREEGRID JSON FORMAT1
    //------------------------------------------------------------------------------------------------------------
    public function loadDataMenu_JSONFORMAT1($id = ''){
        $result = $this->data_menu($id); 
        $data   = array();

        if($result->num_rows()>0){
            $i = 0;
            foreach($result->result() as $rs) {
                if($rs->have_child > 0){  
                    $data[$i] = array(
                                "id" => $rs->menu_id, 
                                "data"=>array(
                                               array("value"=>$rs->menu_name, "image"=>"folder.gif"),
                                               !empty($rs->menu_position)?$rs->menu_position:0,
                                               ($rs->menu_status == 1) ? "Tidak Aktif" : "Aktif",
                                               !empty($rs->menu_id)? $rs->menu_id : "",
                                             ),
                                "xmlkids"=>$rs->have_child,
                                "open"=>true,
                                "rows"=>$this->loadDataMenu_JSONFORMAT1($rs->menu_id)
                                ); 
                }else if(empty($rs->have_child)){
                    $data[$i] = array(
                                "id" => $rs->menu_id, 
                                "data"=>array(
                                               $rs->menu_name,
                                               !empty($rs->menu_position)?$rs->menu_position:0,
                                               ($rs->menu_status == 1) ? "Tidak Aktif" : "Aktif",
                                               !empty($rs->menu_id)? $rs->menu_id : "",
                                             )
                                ); 
                }else{

                }
            $i++;
            }           
        }

        return $data;
    }

    //------------------------------------------------------------------------------------------------------------
    // GET MAX ID
    //------------------------------------------------------------------------------------------------------------
    public function getMax_ID($field = ""){
        $this->db->select_max($field);
        $this->db->from($this->_table1);

        return $this->db->get()->row();
    }   

    public function options($default = '--Pilih Data--') {
        $option = array();
        $list   = $this->db->get($this->_table1);

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->menu_id] = $row->menu_name;
        }

        return $option;
    }
}
