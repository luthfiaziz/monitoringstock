<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Luthfi Aziz Nugraha 
 * Create Date : 26/07/2014 - 00:43:05
 * Module Name : Menu Model ( For Template )
 */

class profil_aplikasi_model extends CI_Model {
    private $_table1 = "profile";
    private $_table2 = "menus";

     public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("a.role_id" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->from($this->_table1 . " a");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function getMenuText($key = ""){
        $this->db->from($this->_table2 . " a");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;

    }
}
