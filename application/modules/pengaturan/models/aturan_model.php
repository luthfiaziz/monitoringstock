<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Luthfi Aziz Nugraha 
 * Create Date : 26/07/2014 - 00:43:05
 * Module Name : Menu Model ( For Template )
 */

class aturan_model extends CI_Model {
    private $_table1 = "roles";
    private $_table2 = "roles_access";

     public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("a.role_id" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->from($this->_table1 . " a");
        $this->db->join($this->_table2 . " b","b.role_id = a.role_id");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }
    
    //------------------------------------------------------------------------------------------------------------
    // LOAD HEADER
    //------------------------------------------------------------------------------------------------------------
    public function loadData(){
        $result = $this->data()->get(); 
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                            "id" => $value->role_id, 
                            "data"=>array(
                                           $value->role_name,
                                           $value->role_desc,
                                         )
                            );
        }

        return $data;
    }

    //------------------------------------------------------------------------------------------------------------
    // LOAD TREEGRID
    //------------------------------------------------------------------------------------------------------------
    public function data_menu($id = ''){
        $sql = "SELECT *
                FROM
                    (
                        SELECT
                            `a`.*, `child`.`have_child`
                        FROM
                            (`1nt3x_menus` a)
                        LEFT OUTER JOIN(
                            SELECT
                                menu_parent,
                                COUNT(*)AS have_child
                            FROM
                                `1nt3x_menus`
                            GROUP BY
                                menu_parent
                        )child ON `a`.menu_id = `child`.menu_parent
                    )my_menu
                WHERE 
                    `my_menu`.`menu_parent` = '{$id}' ORDER BY `my_menu`.`menu_position` ASC";

        return $this->db->query($sql);
    }

    //------------------------------------------------------------------------------------------------------------
    // LOAD TREEGRID JSON FORMAT1
    //------------------------------------------------------------------------------------------------------------
    public function loadDataMenu_JSONFORMAT1($id = '',$role_id = ''){
        $result = $this->data_menu($id); 
        $data   = array();
        if($result->num_rows()>0){
            $i = 0;
            foreach($result->result() as $rs) {
                $role = $this->role_menu($rs->menu_id, $role_id);
                
                if($rs->have_child > 0){    
                    $data[] = array(
                                "id" => $rs->menu_id, 
                                "data"=>array(
                                               array("value"=>$rs->menu_name, "image"=>"folder.gif"),
                                               !empty($role->role_access_view)? $role->role_access_view : 0,
                                               !empty($role->role_access_add)? $role->role_access_add : 0,
                                               !empty($role->role_access_edit)? $role->role_access_edit : 0,
                                               !empty($role->role_access_delete)? $role->role_access_delete : 0,
                                               !empty($role->role_access_import)? $role->role_access_import : 0,
                                               !empty($role->role_access_export)? $role->role_access_export : 0,
                                               !empty($rs->menu_id)? $rs->menu_id : "",
                                             ),
                                "xmlkids"=>$rs->have_child,
                                "open"=>true,
                                "rows"=>$this->loadDataMenu_JSONFORMAT1($rs->menu_id,$role_id)
                                ); 
                }else if(empty($rs->have_child)){
                    $data[] = array(
                                "id" => $rs->menu_id, 
                                "data"=>array(
                                               $rs->menu_name,
                                               !empty($role->role_access_view)? $role->role_access_view : 0,
                                               !empty($role->role_access_add)? $role->role_access_add : 0,
                                               !empty($role->role_access_edit)? $role->role_access_edit : 0,
                                               !empty($role->role_access_delete)? $role->role_access_delete : 0,
                                               !empty($role->role_access_import)? $role->role_access_import : 0,
                                               !empty($role->role_access_export)? $role->role_access_export : 0,
                                               !empty($rs->menu_id)? $rs->menu_id : "",
                                             )
                                ); 
                }else{

                }
            $i++;
            }           
        }

        return $data;
    }

    //------------------------------------------------------------------------------------------------------------
    // GET ROLE ON EACH MENU
    //------------------------------------------------------------------------------------------------------------
    public function role_menu($menu_id = '',$role_id = ''){
        $this->db->from($this->_table2);
        $this->db->where(array("role_id"=>$role_id, "menu_id"=>$menu_id));

        return $this->db->get()->row();
    } 

    //------------------------------------------------------------------------------------------------------------
    // LOAD TREEGRID JSON FORMAT2
    //------------------------------------------------------------------------------------------------------------
    public function loadDataMenu_JSONFORMAT2($id = ''){
        $result = $this->data_menu($id); 
        $data   = array();

        if($result->num_rows()>0){
            $i = 0;
            foreach($result->result() as $rs) {
                if($rs->have_child > 0){  
                    $data[$i]["id"]                 = $rs->menu_id;
                    $data[$i]["tree"]               = array(
                                                        "value" => $rs->menu_name,
                                                        "image" => ""
                                                        );
                    $data[$i]["role_access_add"]         = !empty($rs->role_access_add)? $rs->role_access_add : 0;
                    $data[$i]["role_access_add"]         = !empty($rs->role_access_add)? $rs->role_access_add : 0;
                    $data[$i]["role_access_edit"]        = !empty($rs->role_access_edit)? $rs->role_access_edit : 0;
                    $data[$i]["role_access_delete"]      = !empty($rs->role_access_delete)? $rs->role_access_delete : 0;
                    $data[$i]["role_access_import"]      = !empty($rs->role_access_import)? $rs->role_access_import : 0;
                    $data[$i]["role_access_export"]      = !empty($rs->role_access_export)? $rs->role_access_export : 0;
                    $data[$i]["rows"]               = $this->loadDataMenu($rs->menu_id); 
                }else if(empty($rs->have_child)){
                    $data[$i]["id"]                 = $rs->menu_id;
                    $data[$i]["tree"]               = $rs->menu_name;
                    $data[$i]["role_access_view"]        = !empty($rs->role_access_export)? $rs->role_access_export : 0;
                    $data[$i]["role_access_add"]         = !empty($rs->role_access_add)? $rs->role_access_add : 0;
                    $data[$i]["role_access_edit"]        = !empty($rs->role_access_edit)? $rs->role_access_edit : 0;
                    $data[$i]["role_access_delete"]      = !empty($rs->role_access_delete)? $rs->role_access_delete : 0;
                    $data[$i]["role_access_import"]      = !empty($rs->role_access_delete)? $rs->role_access_delete : 0;
                    $data[$i]["role_access_export"]      = !empty($rs->role_access_export)? $rs->role_access_export : 0;
                }else{

                }
            $i++;
            }           
        }

        return $data;
    }

    //------------------------------------------------------------------------------------------------------------
    // GET MAX ID
    //------------------------------------------------------------------------------------------------------------
    public function getMax_ID($field = ""){
        $this->db->select_max($field);
        $this->db->from($this->_table1);

        return $this->db->get()->row();
    }   

    //------------------------------------------------------------------------------------------------------------
    // LOAD COMBO OPTIONS
    //------------------------------------------------------------------------------------------------------------
    public function options($default = '--Pilih Data--') {
        $option = array();
        $list   = $this->db->get($this->_table1);

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->role_id] = $row->role_name;
        }

        return $option;
    }
}
