<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 25/08/15
 * Time: 15:23
 */
/**
 * Description of application_profile
 */

class application_profile extends MX_Controller {
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    private $_attachment_path;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT aturan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();

        // Set Module Location
        $this->_module = 'pengaturan/application_profile';
        // Load Module Core
        $this->load->module('config/app_core');
        // Load Model
        $this->load->model('application_profile_model','profile');
        $this->load->model("cpanel/cpanel_model","cpanel");

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);

        $this->_attachment_path = FCPATH .'assets/img/logo/';
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX aturan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];

        $data["default"]                = $this->profile->data('1')->get()->row();
        $data['form_action']            = base_url() . $this->_module . '/proses';
        $data['main_toolbar_source']    = base_url() . $this->_module . '/loadMainToolbar/' . $menu_id;
        $data['data_source']            = base_url() . $this->_module . '/listdata';
        $data['disabled']               = ($this->SESSION["otoritas_menu"][$menu_id]["role_access_edit"] == 1)? "":'disabled="disabled"';
        $data['aksesEdit']              = ($this->SESSION["otoritas_menu"][$menu_id]["role_access_edit"] == 1)? "TRUE" : "FALSE";

        $this->load->view($this->_module . '/index',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR MAIN menu
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadMainToolbar($id = ''){
        xml::xml_header();

        $xml  = '<toolbar>';
        if($this->SESSION["otoritas_menu"][$id]["role_access_edit"] == 1){
            $xml .= '<item id="simpan" text="Simpan" type="button" img="save.png" imgdis="save.png"/>';
        }
        $xml .= "</toolbar>";

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - SAVE DATA aturan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function proses(){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $this->form_validation->set_rules('profile_npwp', 'NPWP', 'trim|numeric');
        $this->form_validation->set_rules('profile_phone', 'Phone', 'trim|numeric');
        $this->form_validation->set_rules('profile_fax', 'Fax', 'trim|numeric');
        $this->form_validation->set_rules('profile_email', 'E-mail', 'trim|valid_email');

        if ($this->form_validation->run($this)) {

            $data   = array();
            $data["profile_name"]   = $this->input->post("profile_name");
            $data["profile_npwp"]   = $this->input->post("profile_npwp");
            $data["profile_phone"]  = $this->input->post("profile_phone");
            $data["profile_fax"]    = $this->input->post("profile_fax");
            $data["profile_email"]  = $this->input->post("profile_email");
            $data["profile_address"]= $this->input->post("profile_address");
            $data["profile_app_name"]= $this->input->post("profile_app_name");

            $this->upload->initialize(array(
                "upload_path"   => $this->_attachment_path,
                "allowed_types" => "png|jpg|jpeg|gif",
                "overwrite"     => TRUE,
                "encrypt_name"  => TRUE,
                "remove_spaces" => TRUE,
                "max_size"      => 3000,
                "xss_clean"     => FALSE,
                "file_name"     => array("image_" . $this->random() . "_" . date('Ymdhis'))
            ));

            /* START TRANSACTION */
            if ($this->upload->do_multi_upload("images")) {
                $return = $this->upload->get_multi_upload_data();
                $data['profile_logo']       = $return[0]["orig_name"];
                $data['profile_app_icon']   = $return[1]["orig_name"];
            }

            $dataPerusahaan         = $this->cpanel->getPerusahaanProfile();
            if($dataPerusahaan->profile_logo!=""){
                unlink($this->_attachment_path . $dataPerusahaan->profile_logo);
            }
            if($dataPerusahaan->profile_app_icon!=""){
                unlink($this->_attachment_path . $dataPerusahaan->profile_app_icon);
            }

            $this->db->trans_begin();
            $fk = array('profile_id'=>'1');
            $crud->save_nrb("profile", $data, $fk);

            if ($this->db->trans_status() === FALSE) {
                $message = array(true, 'Proses Gagal', 'Proses penyimpanan data gagal ( data di rollback ).', '');
                $this->db->trans_rollback();
            } else {
                $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', '');
                $this->db->trans_commit();
            }
            /* END TRANSACTION */

        } else {
            $message = array(false, 'Proses gagal', validation_errors("<span class='error_mess'>","</span>"), "");
        }

        echo json_encode($message,true);
    }

    public function random($len = 5){
        $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $base = strlen($charset);
        $result = '';

        $now = explode(' ', microtime())[1];
        while ($now >= $base){
            $i = $now % $base;
            $result = $charset[$i] . $result;
            $now /= $base;
        }

        return substr($result, -5);
    }
}