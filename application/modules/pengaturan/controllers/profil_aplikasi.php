<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_management
 *
 * @author Luthfi Aziz Nugraha
 */

class profil_aplikasi extends MX_Controller {
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    private $_attachment_path;
    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT profil_aplikasi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();
        
        // Set Module Location
        $this->_module = 'pengaturan/profil_aplikasi';
        // Load Module Core
        $this->load->module('config/app_core');
        // Load Model
        $this->load->model('profil_aplikasi_model','profil_aplikasi');
        
        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);

        $this->_attachment_path = FCPATH .'assets/img/logo/';
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX profil_aplikasi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];

        $key = array("menu_id"=>$menu_id);
        $data["menuname"]        = $this->profil_aplikasi->getMenuText($key)->get()->row()->menu_name;
        $data["menudid"]         = $menu_id;
        $data["data_perusahaan"] = $this->profil_aplikasi->data()->get()->row();
        $data["form_action"]     = base_url() . $this->_module . "/proses";
        $data["button_view"]     = "false";
        
        if($this->SESSION["otoritas_menu"][$menu_id]["role_access_edit"] == 1){ 
            $data["button_view"] = "true";
        }

        $this->load->view($this->_module . '/index',$data);
    }

    public function proses(){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $this->form_validation->set_rules('nama_aplikasi', 'Nama Aplikasi', 'trim');
        $this->form_validation->set_rules('nama_perusahaan', 'Nama Perusahaan', 'tri');
        $this->form_validation->set_rules('npwp', 'NPWP', 'trim');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim');
        $this->form_validation->set_rules('telepon', 'Telepon', 'trim');
        $this->form_validation->set_rules('fax', 'Fax', 'trim');
        $this->form_validation->set_rules('email', 'E-mail', 'trim');

        if ($this->form_validation->run($this)) {

            $this->upload->initialize(array(
                "upload_path"   => $this->_attachment_path,
                "allowed_types" => "png|jpg|jpeg|gif",
                "overwrite"     => TRUE,
                "encrypt_name"  => TRUE,
                "remove_spaces" => TRUE,
                "max_size"      => 3000,
                "xss_clean"     => FALSE,
                "file_name"     => array("image_" . $this->random() . "_" . date('Ymdhis'))
            ));

            $data   = array();
            $data["profile_name"]   = $this->input->post("nama_perusahaan");
            $data["profile_npwp"]   = $this->input->post("npwp");
            $data["profile_phone"]  = $this->input->post("telepon");
            $data["profile_fax"]    = $this->input->post("fax");
            $data["profile_email"]  = $this->input->post("email");
            $data["profile_address"]= $this->input->post("alamat");
            $data["profile_app_name"]= $this->input->post("nama_aplikasi");

            if(!empty($_FILES)){
                if ($this->upload->do_multi_upload("images")) {
                    $return = $this->upload->get_multi_upload_data();
                    $data['profile_logo']       = $return[0]["orig_name"];
                }

                $dataPerusahaan         = $this->profil_aplikasi->data()->get()->row();
                if(!empty($dataPerusahaan->profile_logo)){
                    unlink($this->_attachment_path . $dataPerusahaan->profile_logo);
                }
            }

            /* START TRANSACTION */
            $this->db->trans_begin();

                $fk = array('profile_id'=>'1');
                $crud->save_nrb("profile", $data, $fk);

            if ($this->db->trans_status() === FALSE) {
                $message = array(true, 'Proses Gagal', 'Proses penyimpanan data gagal ( data di rollback ).', '');
                $this->db->trans_rollback();
            } else {
                $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh()');
                $this->db->trans_commit();
            }
            /* END TRANSACTION */

        } else {
            $message = array(false, 'Proses gagal', validation_errors("<span class='error_mess'>","</span>"), "");
        }

        echo json_encode($message,true);
    }

    /*RANDOM NUMBER - GENERATE RANDOM NUMBER*/
    public function random($len = 5){
        $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $base = strlen($charset);
        $result = '';

        $now = explode(' ', microtime())[1];
        while ($now >= $base){
            $i = $now % $base;
            $result = $charset[$i] . $result;
            $now /= $base;
        }

        return substr($result, -5);
    }
}

?>