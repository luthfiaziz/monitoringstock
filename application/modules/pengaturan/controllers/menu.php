<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_management
 *
 * @author Luthfi Aziz Nugraha
 */

class menu extends MX_Controller {
    // VARIABLE MODUL
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT menu
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();
        
        // Set Module Location
        $this->_module = 'pengaturan/menu';
        // Load Module Core
        $this->load->module('config/app_core');
        // Load Model
        $this->load->model('menu_model','menu');

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX menu
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];

        $data['load_form']              = base_url() . $this->_module . '/loadform';
        $data['main_toolbar_source']    = base_url() . $this->_module . '/loadMainToolbar/' . $menu_id;
        $data['data_source']            = base_url() . $this->_module . '/loadDaftarMenu';
        $data['aksesEdit']              = ($this->SESSION["otoritas_menu"][$menu_id]["role_access_edit"] == 1)? "TRUE" : "FALSE";
        $this->load->view($this->_module . '/index',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR MAIN menu
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadMainToolbar($id = ''){
        xml::xml_header();

        $xml  = '<toolbar>';

        if($this->SESSION["otoritas_menu"][$id]["role_access_add"] == 1){
            $xml .= '<item id="add" text="Tambah" type="button" img="add.png" imgdis="add.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_edit"] == 1){
            $xml .= '<item id="edit" text="Ubah" type="button" img="edit.png" imgdis="edit.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_delete"] == 1){
            $xml .= '<item id="delete" text="Hapus" type="button" img="delete.png" imgdis="delete.png"/>';
        }

        $xml .= '<item id="sep" type="separator"/>';
        $xml .= '<item id="refresh" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD DATA menu
    //-------------------------------------------------------------------------------------------------------------------------------
    public function listdata(){
       $data   = $this->menu->loadData();
       
       $result = array("rows" => $data);

       echo json_encode($result);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR FORM menu
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadFormmenuToolbar(){
        xml::xml_header();

        $xml  = '<toolbar>';
        $xml .= '<item id="savemenu" text="Simpan" type="button" img="save.png" imgdis="save.png" action="simpan"/>';
        $xml .= '<item id="cancelmenu" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png" action="bersihmenu"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }

    public function loadDaftarMenu(){
       $data   = $this->menu->loadDataMenu_JSONFORMAT1(0);
       
       $result = array("rows" => $data);

       echo json_encode($result);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD FORM menu
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadform($id = ''){
        if (!empty($id)) {
            $data["id"]      = $id;
            $data["default"] = $this->menu->data($id)->get()->row();
        }

        $data['grup_options'] = $this->menu->options();
        $data['form_action'] = base_url() . $this->_module . '/proses';
        $this->load->view($this->_module . '/form',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - SAVE DATA menu
    //-------------------------------------------------------------------------------------------------------------------------------
    public function proses(){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $this->form_validation->set_rules('menu_name', 'Nama Menu', 'trim|required');
        $this->form_validation->set_rules('menu_url', 'URL Menu', 'trim|required');
        $this->form_validation->set_rules('menu_position', 'Position Menu', 'trim|required');
        $this->form_validation->set_rules('menu_status', 'Status Menu', 'trim|required');
        
        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', 'refresh()');
            
            $id = $this->input->post('id');

            $musers = array();
            $musers['menu_name']      = $this->input->post('menu_name');
            $musers['menu_parent']    = $this->input->post('menu_parent');
            $musers['menu_position']  = $this->input->post('menu_position');
            $musers['menu_status']    = $this->input->post('menu_status');

            if ($id == '') {
                $musers['menu_url']       = str_replace("/", "|", $this->input->post("menu_url")) . "|" .  ( $this->menu->getMax_ID("menu_id")->menu_id + 1 );
                $musers['created_at']   = date("Y-m-d");

                if ($crud->save_as_new('menus', arrman::StripTagFilter($musers))) {
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh()');
                }
            } else {
                $musers['menu_url']       = str_replace("/", "|", $this->input->post("menu_url")) . "|" . $id;

                $key = array("menu_id"=> $id);
                if ($crud->save('menus', arrman::StripTagFilter($musers), $key)) {
                    $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh()');
                }
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors("<span class='error_mess'>","</span>"), "refresh()");
        }

        echo json_encode($message,true);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - DELETE DATA menu
    //-------------------------------------------------------------------------------------------------------------------------------
    public function delete() {
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        $key = array("menu_id"=> $this->input->post("id"));
        if ($crud->delete("menus",$key)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '');
        }

        echo json_encode($message);
    }
}

?>