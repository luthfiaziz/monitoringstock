<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_management
 *
 * @author Luthfi Aziz Nugraha
 */

class aturan extends MX_Controller {
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT aturan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();
        
        // Set Module Location
        $this->_module = 'pengaturan/aturan';
        // Load Module Core
        $this->load->module('config/app_core');
        // Load Model
        $this->load->model('aturan_model','aturan');
        
        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX aturan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];

        $data['load_form']              = base_url() . $this->_module . '/loadform';
        $data['main_toolbar_source']    = base_url() . $this->_module . '/loadMainToolbar/' . $menu_id;
        $data['data_source']            = base_url() . $this->_module . '/listdata';
        $data['aksesEdit']              = ($this->SESSION["otoritas_menu"][$menu_id]["role_access_edit"] == 1)? "TRUE" : "FALSE";

        $this->load->view($this->_module . '/index',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR MAIN menu
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadMainToolbar($id = ''){
        xml::xml_header();

        $xml  = '<toolbar>';

        if($this->SESSION["otoritas_menu"][$id]["role_access_add"] == 1){
            $xml .= '<item id="add" text="Tambah" type="button" img="add.png" imgdis="add.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_edit"] == 1){
            $xml .= '<item id="edit" text="Ubah" type="button" img="edit.png" imgdis="edit.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_add"] == 1){
            $xml .= '<item id="delete" text="Hapus" type="button" img="delete.png" imgdis="delete.png"/>';
        }

        $xml .= '<item id="sep" type="separator"/>';
        $xml .= '<item id="refresh" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }


    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD DATA aturan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function listdata(){
       $data   = $this->aturan->loadData();
       
       $result = array("rows" => $data);

       echo json_encode($result);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR FORM aturan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadFormaturanToolbar(){
        xml::xml_header();

        $xml  = '<toolbar>';
        $xml .= '<item id="saveaturan" text="Simpan" type="button" img="save.png" imgdis="save.png" action="simpan"/>';
        $xml .= '<item id="cancelaturan" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png" action="bersihaturan"/>';
        $xml .= '<item id="sep" type="separator"/>';
        $xml .= '<item id="checkallaturan" text="Tandai semua" type="button" img="check.png" imgdis="check.png" action="KontrolAturan"/>';
        $xml .= '<item id="uncheckallaturan" text="Hapus semua tanda" type="button" img="uncheck.png" imgdis="uncheck.png" action="KontrolAturan"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD FORM aturan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadform($id = ''){
        if (!empty($id)) {
            $data["id"]      = $id;
            $data["default"] = $this->aturan->data($id)->get()->row();
        }

        $data['grup_options']           = $this->aturan->options();
        $data['menu_pengguna_source']   = base_url() . $this->_module . '/loadDaftarMenu/' . $id;
        $data['form_action']            = base_url() . $this->_module . '/proses';
        $this->load->view($this->_module . '/form',$data);
    }

    public function loadDaftarMenu($role_id = ''){
       $data   = $this->aturan->loadDataMenu_JSONFORMAT1(0,$role_id);
       
       $result = array("rows" => $data);

       echo json_encode($result);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - SAVE DATA aturan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function proses(){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $this->form_validation->set_rules('role_name', 'Nama Role', 'trim|required');
        $this->form_validation->set_rules('role_desc', 'Keterangan Role', 'trim|required');
        
        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', 'refresh()');
            
            $id     = $this->input->post('id');

            $data   = array();
            $data["role_name"]        = $this->input->post("role_name");
            $data["role_desc"]        = $this->input->post("role_desc");

            //-------------------------------------------------------------------------------------------
            // CHILD DATA
            //-------------------------------------------------------------------------------------------
            $grid  = array_filter($this->input->post("grid"));

            /* START TRANSACTION */
            $this->db->trans_begin();

                $data_child = array();
                
                if ($id == '') {
                    if ($crud->save_as_new_nrb("roles", arrman::StripTagFilter($data))) {
                        foreach ($grid as $key => $value) {
                            $data_child["role_access_view"]   =  $value[1];
                            $data_child["role_access_add"]    =  $value[2];
                            $data_child["role_access_edit"]   =  $value[3];
                            $data_child["role_access_delete"] =  $value[4];
                            $data_child["role_access_export"] =  $value[5];
                            $data_child["role_access_import"] =  $value[6];
                            $data_child["menu_id"]            =  $value[7];
                            $data_child["role_id"]            =  $this->aturan->getMax_ID("role_id")->role_id;

                            $crud->save_as_new_nrb("roles_access", arrman::StripTagFilter($data_child));
                        }
                    }
                } else {
                    $key = array("role_id"=>$id);
                    if ($crud->save("roles", arrman::StripTagFilter($data), $key)) {

                        $this->db->delete("roles_access", $key);

                        foreach ($grid as $key => $value) {
                            $data_child["role_access_view"]     =  $value[1];
                            $data_child["role_access_add"]      =  $value[2];
                            $data_child["role_access_edit"]     =  $value[3];
                            $data_child["role_access_delete"]   =  $value[4];
                            $data_child["role_access_export"]   =  $value[5];
                            $data_child["role_access_import"]   =  $value[6];
                            $data_child["menu_id"]              =  $value[7];
                            $data_child["role_id"]              =  $id;
                            
                            $crud->save_as_new_nrb("roles_access", arrman::StripTagFilter($data_child));
                        }
                    }
                }

            if ($this->db->trans_status() === FALSE) {
                $message = array(true, 'Proses Gagal', 'Proses penyimpanan data gagal ( data di rollback ).', 'refresh()');
                $this->db->trans_rollback();
            } else {
                $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh()');
                $this->db->trans_commit();
            }
            /* END TRANSACTION */

        } else {
            $message = array(false, 'Proses gagal', validation_errors("<span class='error_mess'>","</span>"), "refresh()");
        }

        echo json_encode($message,true);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - DELETE DATA aturan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function delete() {
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        $key = array("role_id"=> $this->input->post("id"));
        if ($crud->delete("roles",$key)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '');
        }

        echo json_encode($message);
    }
}

?>