<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_management
 *
 * @author Luthfi Aziz Nugraha
 */

class Pengguna extends MX_Controller {
    // VARIABLE MODUL
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT PENGGUNA
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();
        
        // Set Module Location
        $this->_module = 'pengaturan/pengguna';
        // Load Module Core
        $this->load->module('config/app_core');
        // Load Model
        $this->load->model('pengguna_model','pengguna');
        $this->load->model('aturan_model','aturan');
        $this->load->model('master/toko_model','toko');
        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX PENGGUNA
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];

        $data['load_form']              = base_url() . $this->_module . '/loadform';
        $data['main_toolbar_source']    = base_url() . $this->_module . '/loadMainToolbar/' . $menu_id;
        $data['data_source']            = base_url() . $this->_module . '/listdata';
        $data['aksesEdit']              = ($this->SESSION["otoritas_menu"][$menu_id]["role_access_edit"] == 1)? "TRUE" : "FALSE";
    	$this->load->view($this->_module . '/index',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR MAIN PENGGUNA
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadMainToolbar($id = ''){
        xml::xml_header();
        $xml  = '<toolbar>';

        if($this->SESSION["otoritas_menu"][$id]["role_access_add"] == 1){
            $xml .= '<item id="add" text="Tambah" type="button" img="add.png" imgdis="add.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_edit"] == 1){
            $xml .= '<item id="edit" text="Ubah" type="button" img="edit.png" imgdis="edit.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_add"] == 1){
            $xml .= '<item id="delete" text="Hapus" type="button" img="delete.png" imgdis="delete.png"/>';
        }

        $xml .= '<item id="sep" type="separator"/>';
        $xml .= '<item id="refresh" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD DATA PENGGUNA
    //-------------------------------------------------------------------------------------------------------------------------------
    public function listdata(){
       $data   = $this->pengguna->loadData();
       
       $result = array("rows" => $data);

       echo json_encode($result);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR FORM PENGGUNA
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadFormPenggunaToolbar(){
        xml::xml_header();

        $xml  = '<toolbar>';
        $xml .= '<item id="savePengguna" text="Simpan" type="button" img="save.png" imgdis="save.png" action="simpan"/>';
        $xml .= '<item id="cancelPengguna" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png" action="bersihPengguna"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD FORM PENGGUNA
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadform($id = ''){
        if (!empty($id)) {
            $data["id"]         = $id;
            $data["default"]    = $this->pengguna->data($id)->get()->row();
        }

        $data['grup_options']   = $this->pengguna->options();
        $data['toko_options']   = $this->toko->options();
        $data['form_action']    = base_url() . $this->_module . '/proses';
        $this->load->view($this->_module . '/form',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - SAVE DATA PENGGUNA
    //-------------------------------------------------------------------------------------------------------------------------------
    public function proses(){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;
            
        $id = $this->input->post('id');

        $this->form_validation->set_rules('user_fullname', 'Nama', 'trim|required');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required');
        $this->form_validation->set_rules('role_id', 'Grup Akses', 'trim|required');

        if(empty($id)){
            $this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email|callback_ketersediaan_email');
            $this->form_validation->set_rules('user_username', 'Username', 'trim|required|alpha_dash|callback_ketersediaan_check');
        }else{
            $this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('user_username', 'Username', 'trim|required|alpha_dash');
        }

        $this->form_validation->set_rules('user_tlp', 'Telpon', 'trim|required');
        $this->form_validation->set_rules('user_status', 'Status', 'trim|required');
        $this->form_validation->set_rules('m_toko_kode', 'Toko', 'trim|required');

        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', 'refresh()');

            $key = array("a.role_id" => $this->input->post('role_id'));
            $data_menu = $this->aturan->data($key)->get();

            $musers = array();
            $musers['user_username']            = $this->input->post('user_username');
            $musers['user_fullname']            = $this->input->post('user_fullname');
            $musers['role_id']                  = $this->input->post('role_id');
            $musers['user_email']               = $this->input->post('user_email');
            $musers['user_tlp']                 = $this->input->post('user_tlp');
            $musers['created_at']               = $this->SESSION["user_fullname"];
            $musers['user_status']              = $this->input->post('user_status');
            $musers['m_toko_kode']              = $this->input->post('m_toko_kode');
            $musers['user_password_length']     = strlen($this->input->post('user_password'));

            if ($id == '') {
                $musers['user_password']       = md5($this->input->post('user_password'));
                $musers['created_at']          = date("Y-m-d");

                if ($crud->save_as_new('users', $musers)) {

                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh()');
                }
            } else {
                $jumlahbintang = arrman::PAS_SYMBOL("*", $this->input->post("user_password_length"));
                $oldpassword   = $this->input->post("user_password");

                if($oldpassword != $jumlahbintang){
                    $musers['user_password']       = md5($this->input->post('user_password'));
                }

                $musers['author']    = $this->SESSION["user_fullname"];

                $key = array("user_id"=> $id);
                if ($crud->save('users', $musers, $key)) {

                    $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh()');
                }
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors("<span class='error_mess'>","</span>"), "refresh()");
        }

        echo json_encode($message,true);
    }

         /*CUSTOM VALIDATION - cek id jika ada*/
    public function ketersediaan_check()
    {   
        $username = $this->input->post('user_username');
        
        $key = array("user_username"=>$username);
        $ada = $this->pengguna->hitungJikaAda($key);

        if ($ada > 0)
        {   
            $message = 'Username <b><u>' . $username . '</u></b> sudah ada';
            $this->form_validation->set_message('ketersediaan_check', $message);
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

             /*CUSTOM VALIDATION - cek id jika ada*/
    public function ketersediaan_email()
    {   
        $email = $this->input->post('user_email');
        
        $key = array("user_email"=>$email);
        $ada = $this->pengguna->hitungJikaAda($key);

        if ($ada > 0)
        {   
            $message = 'Email <b><u>' . $email . '</u></b> sudah ada';
            $this->form_validation->set_message('ketersediaan_email', $message);
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - DELETE DATA PENGGUNA
    //-------------------------------------------------------------------------------------------------------------------------------
    public function delete() {
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        $key = array("user_id"=> $this->input->post("id"));
        if ($crud->delete("users",$key)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '');
        }

        echo json_encode($message);
    }
}

?>