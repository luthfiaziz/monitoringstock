<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 23/09/15
 * Time: 10:36
 */
?>
<script type="text/javascript">
    var kategoriAkses    = "<?php echo $aksesEdit; ?>";
    var kategoriWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbarkategori = mainTab.cells("maintabbar").attachToolbar();
    toolbarkategori.setSkin("dhx_skyblue");
    toolbarkategori.setIconsPath(url + "assets/img/btn/");
    toolbarkategori.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarkategori.attachEvent("onClick", ckategori);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function ckategori(id){
        var module  = url + "master/kategori";
        var idRow   = gridkategori.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(kategoriWindows,"kategoriWindows",["kategori",500,350], module + "/loadFormToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(kategoriWindows,"kategoriWindows",["kategori",500,350], module + "/loadFormToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    };

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({
                                    type: "alert",
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridkategori, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridkategori,module + "/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridkategori = mainTab.cells("maintabbar").attachGrid();
    gridkategori.setImagePath(url + "assets/dhtmlx/imgs/");
    gridkategori.setHeader("Kode ,Nama Kategori, Keterangan, Tgl dibuat, Petugas");
    gridkategori.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
    gridkategori.setColAlign("left,left,left,center,center");
    gridkategori.setColTypes("ro,ro,ro,ro,ro");
    gridkategori.setInitWidths("200,250,*,150,150");
    gridkategori.init();
    gridkategori.enableDistributedParsing(true,10,300);
    gridkategori.load("<?php echo $data_source; ?>","json");

    if(kategoriAkses == "TRUE"){
        gridkategori.attachEvent("onRowDblClicked",gridkategoriDBLClick);
    }

    // [ACTION] GRID ACTION
    function gridkategoriDBLClick(){
        var module  = url + "master/kategori";
        form_modal(kategoriWindows,"kategoriWindows",["kategori",500,350], module + "/loadFormToolbar", module + "/loadform/" + gridkategori.getSelectedRowId());
    }
</script>