<script type="text/javascript">
    var minimum_stokAkses        = "<?php echo $aksesEdit; ?>";
    var minimum_stokWindows      = new dhtmlXWindows();
    var klinikCARIWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
	// [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
	toolbarminimum_stok = mainTab.cells("maintabbar").attachToolbar();
    toolbarminimum_stok.setSkin("dhx_skyblue");
 	toolbarminimum_stok.setIconsPath(url + "assets/img/btn/");
 	toolbarminimum_stok.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarminimum_stok.attachEvent("onClick", cminimum_stok);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function cminimum_stok(id){
        var module  = url + "master/minimum_stok";
        var idRow   = gridminimum_stok.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(minimum_stokWindows,"minimum_stokWindows",["Form : Minimum Stok",500,250], module + "/loadFormminimum_stokToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(minimum_stokWindows,"minimum_stokWindows",["Form : Minimum Stok",500,250], module + "/loadFormminimum_stokToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({                
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    }

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({ 
                                    type: "alert", 
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridminimum_stok, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            },
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridminimum_stok,url + "master/minimum_stok/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridminimum_stok = mainTab.cells("maintabbar").attachGrid();
    gridminimum_stok.setHeader("Item, Stok");
	gridminimum_stok.attachHeader("#text_filter,#text_filter"); 
	gridminimum_stok.setColAlign("left,center");
	gridminimum_stok.setColTypes("ro,ro");
	gridminimum_stok.setInitWidths("*,250");
	gridminimum_stok.init();
	gridminimum_stok.enableDistributedParsing(true,10,300);  
	//gridminimum_stok.load("<?php echo $data_source; ?>","json");
    refreshGrid(gridminimum_stok,url + "master/minimum_stok/listdata","json");

    if(minimum_stokAkses == "TRUE"){
    	gridminimum_stok.attachEvent("onRowDblClicked",gridminimum_stokDBLClick);
    }

    // [ACTION] GRID ACTION
	function gridminimum_stokDBLClick(){
        var module  = url + "master/minimum_stok";
        form_modal(minimum_stokWindows,"minimum_stokWindows",["Form : Minimum Stok",500,250], module + "/loadFormminimum_stokToolbar", module + "/loadform/" + gridminimum_stok.getSelectedRowId());
    }
</script>