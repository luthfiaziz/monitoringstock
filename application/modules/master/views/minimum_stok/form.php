<div id="minimum_stokloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:30%;"/>
</div>
<div class="form-wrapper">
    <?php
    $hidden_form = array('id' => !empty($id) ? $id : '');
    echo form_open_multipart($form_action, array('id' => 'fm_minimum_stok'), $hidden_form);
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span><b>Nama Item</b> <i>*</i></span>
                <?php echo form_dropdown('m_item_kode', $list_data, !empty($default->m_item_kode) ? $default->m_item_kode : '', 'id="m_item_kode" class="full"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Minimum Stok</b> <i>*</i></span>
                <?php echo form_input('m_stok_min', !empty($default->m_stok_min) ? $default->m_stok_min : '', 'id="m_stok_min" class="form_web uppercase onlynumber" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
    </table>
    <?php 
        echo form_close(); 
        echo arrman::getRequired();
    ?>
</div>
<script type="text/javascript">
    $("#m_item_kode", "#fm_minimum_stok").select2();

    function simpan(id){
        var toolbar = ["minimum_stokWindows","saveminimum_stok"];

        SimpanData('#fm_minimum_stok','#minimum_stokloading',toolbar);
    }

    function refreshminimum_stok(){
        close_form_modal(minimum_stokWindows,"minimum_stokWindows");
        refreshGrid(gridminimum_stok,url + "master/minimum_stok/listdata","json");
    }
    
    function bersihminimum_stok(){
        var data = [
                        ["unit_status","","combo"],
                    ];


        var focus = ["unit_name", "input"];

        refreshForm(data, "#fm_minimum_stok",focus);
    }   
</script>