<div id="rakloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <?php
    $hidden_form = array('id' => !empty($id) ? $id : '');
    echo form_open_multipart($form_action, array('id' => 'fm_rak'), $hidden_form);
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span><b>Kode Rak</b> <i>*</i></span><br/>
                <?php 
                    $readonly = ($readonly == true)?'readonly':'';
                    echo form_input('m_rak_kode', !empty($default->m_rak_kode) ? $default->m_rak_kode : '', 'id="m_rak_kode" class="form_web uppercase" placeholder="wajib diisi" style="width:40%"'.$readonly);
                ?>
               
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Nama Rak</b> <i>*</i></span>
                <?php echo form_input('m_rak_nama', !empty($default->m_rak_nama) ? $default->m_rak_nama : '', 'id="m_rak_nama" class="form_web uppercase" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Keterangan</b></span>
                 <?php echo form_textarea('m_rak_ket', !empty($default->m_rak_ket) ? $default->m_rak_ket : '', 'id="m_rak_ket" class="form_web" style="height:50px !important; padding:10px;"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <div id="gridRakForm" style="width: 100%; height: 240px;"></div>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo arrman::getRequired();
                ?>
            </td>
        </tr>
        
    </table>
    <?php
    echo form_close();
    ?>
</div>
<script type="text/javascript">

    gridRakForm  = new dhtmlXGridObject("gridRakForm");
    gridRakForm.setImagePath(url + "assets/dhtmlx/imgs/");
    gridRakForm.setHeader("Kode Item,Nama Item, Aktif Item");
    gridRakForm.setColAlign("left,left,center");
    gridRakForm.setColTypes("ro,ro,ch");
    gridRakForm.setInitWidths("100,*,70");
    gridRakForm.init();
    gridRakForm.load(url + "master/rak/loadDataToko/<?php echo $default->m_rak_kode;?>","json");
   
    function simpan(id){
        var toolbar = ["rakWindows","saverak","cancelrak"];

        var dataLainLain    = {
            dataGrid: $.csv.toArrays(gridRakForm.serializeToCSV())
        };

        SimpanDataTF('#fm_rak','#rakloading',toolbar,"",dataLainLain);
    }

    function refresh(){
        close_form_modal(rakWindows,"rakWindows");
        refreshGrid(gridrak,url + "master/rak/listdata","json");
    }
    
    function bersihrak(){
       

        var focus = ["m_rak_kode", "input"];

        refreshCombo(data, "#fm_rak", focus);
    }   
</script>