<script type="text/javascript">
    var rakAkses    = "<?php echo $aksesEdit; ?>";
    var rakWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
	// [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
	toolbarrak = mainTab.cells("maintabbar").attachToolbar();
        toolbarrak.setSkin("dhx_skyblue");
 	toolbarrak.setIconsPath(url + "assets/img/btn/");
 	toolbarrak.loadXML("<?php echo $main_toolbar_source; ?>");
        toolbarrak.attachEvent("onClick", crak);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function crak(id){
        var module  = url + "master/rak";
        var idRow   = gridrak.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(rakWindows,"rakWindows",["[Form] - Rak",500,550], module + "/loadFormrakToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(rakWindows,"rakWindows",["[Form] - Rak",500,550], module + "/loadFormrakToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({                
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    }

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({ 
                                    type: "alert", 
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridrak, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            },
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridrak,url + "master/rak/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
        gridrak = mainTab.cells("maintabbar").attachGrid();
        gridrak.setHeader("Kode Rak,Nama Rak,Keterangan");
	gridrak.attachHeader("#text_filter,#text_filter,#text_filter");
	gridrak.setColAlign("left,left,left");
	gridrak.setColTypes("ro,ro,ro");
	gridrak.setInitWidths("*,*,*");
	gridrak.init(); 
        gridrak.enableDistributedParsing(true,10,300);
	gridrak.load("<?php echo $data_source; ?>","json");

    if(rakAkses == "TRUE"){
    	gridrak.attachEvent("onRowDblClicked",gridrakDBLClick);
    }

    // [ACTION] GRID ACTION
	function gridrakDBLClick(){
        var module  = url + "master/rak";
        form_modal(rakWindows,"rakWindows",["[Form] - Rak",500,550], module + "/loadFormrakToolbar", module + "/loadform/" + gridrak.getSelectedRowId());
    }
</script>