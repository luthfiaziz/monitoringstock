<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 23/09/15
 * Time: 11:41
 */
?>
<div id="merekloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <?php
    echo form_open_multipart($form_action, array('id' => 'fm_merek'));
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span class="title_app"><b>Kode merek</b> <i>*</i></span>
                <?php
                $readonly = !empty($default->m_item_merk_kode)?'readonly':'';
                echo form_input('m_item_merk_kode', !empty($default->m_item_merk_kode) ? $default->m_item_merk_kode : '', 'id="m_item_merk_kode" maxlength="15" class="form_web" placeholder="wajib diisi" '.$readonly);
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Nama merek</b> <i>*</i></span>
                <?php echo form_input('data[m_item_merk_nama]', !empty($default->m_item_merk_nama) ? $default->m_item_merk_nama : '', 'id="m_item_merk_nama" class="form_web" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Kategori</b> <i>*</i></span>
                <?php echo form_dropdown('data[m_item_kategori_kode]', $kategori, !empty($default->m_item_kategori_kode) ? $default->m_item_kategori_kode : '', 'id="m_item_kategori_kode" class="full"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Jenis</b> <i>*</i></span>
                <?php echo form_dropdown('data[m_item_jenis_kode]', $jenis, !empty($default->m_item_jenis_kode) ? $default->m_item_jenis_kode : '', 'id="m_item_jenis_kode" class="full" data-source="' . $jenis_source . '"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Keterangan</b> <i></i></span>
                <?php echo form_textarea('data[m_item_merk_ket]', !empty($default->m_item_merk_ket) ? $default->m_item_merk_ket : '', 'id="m_item_merk_ket" class="form_web" style="height:85px;"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo arrman::getRequired();?>
            </td>
        </tr>

    </table>
    <?php
    echo form_close();
    ?>
</div>
<script type="text/javascript">
    $("#m_item_merk_kode").focus();
    $("#m_item_jenis_kode").select2();
    $("#m_item_kategori_kode").select2();

    $(function(){
        $("#m_item_kategori_kode").change(function(){
            get_options("#m_item_jenis_kode",{"kat_kode" : $(this).val()}, '');
        });
    });

    function simpan(id){
        var toolbar = ["merekWindows","savemerek"];

        SimpanDataTF('#fm_merek','#merekloading',toolbar);
    }

    function refresh(){
        close_form_modal(merekWindows,"merekWindows");
        refreshGrid(gridmerek,url + "master/merek/listdata","json");
    }

    function bersihmerek(){
    }
</script>