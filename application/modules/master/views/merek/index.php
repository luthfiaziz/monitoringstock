<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 23/09/15
 * Time: 11:41
 */
?>
<script type="text/javascript">
    var merekAkses    = "<?php echo $aksesEdit; ?>";
    var merekWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbarmerek = mainTab.cells("maintabbar").attachToolbar();
    toolbarmerek.setSkin("dhx_skyblue");
    toolbarmerek.setIconsPath(url + "assets/img/btn/");
    toolbarmerek.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarmerek.attachEvent("onClick", cmerek);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function cmerek(id){
        var module  = url + "master/merek";
        var idRow   = gridmerek.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(merekWindows,"merekWindows",["merek",500,450], module + "/loadFormToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(merekWindows,"merekWindows",["merek",500,450], module + "/loadFormToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    };

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({
                                    type: "alert",
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridmerek, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridmerek,module + "/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridmerek = mainTab.cells("maintabbar").attachGrid();
    gridmerek.setImagePath(url + "assets/dhtmlx/imgs/");
    gridmerek.setHeader("Kode ,Nama merek, Jenis, Kategori, Keterangan, Tgl dibuat, Petugas");
    gridmerek.attachHeader("#text_filter,#text_filter,#combo_filter,#combo_filter,#text_filter,#text_filter,#text_filter");
    gridmerek.setColAlign("left,left,left,left,left,center,center");
    gridmerek.setColTypes("ro,ro,ro,ro,ro,ro,ro");
    gridmerek.setInitWidths("120,180,180,180,*,100,100");
    gridmerek.init();
    gridmerek.enableDistributedParsing(true,10,300);
    gridmerek.load("<?php echo $data_source; ?>","json");

    if(merekAkses == "TRUE"){
        gridmerek.attachEvent("onRowDblClicked",gridmerekDBLClick);
    }

    // [ACTION] GRID ACTION
    function gridmerekDBLClick(){
        var module  = url + "master/merek";
        form_modal(merekWindows,"merekWindows",["merek",500,450], module + "/loadFormToolbar", module + "/loadform/" + gridmerek.getSelectedRowId());
    }
</script>