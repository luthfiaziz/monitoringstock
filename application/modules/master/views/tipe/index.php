<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 23/09/15
 * Time: 12:12
 */
?>
<script type="text/javascript">
    var tipeAkses    = "<?php echo $aksesEdit; ?>";
    var tipeWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbartipe = mainTab.cells("maintabbar").attachToolbar();
    toolbartipe.setSkin("dhx_skyblue");
    toolbartipe.setIconsPath(url + "assets/img/btn/");
    toolbartipe.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbartipe.attachEvent("onClick", ctipe);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function ctipe(id){
        var module  = url + "master/tipe";
        var idRow   = gridtipe.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(tipeWindows,"tipeWindows",["tipe",500,500], module + "/loadFormToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(tipeWindows,"tipeWindows",["tipe",500,500], module + "/loadFormToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    };

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({
                                    type: "alert",
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridtipe, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridtipe,module + "/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridtipe = mainTab.cells("maintabbar").attachGrid();
    gridtipe.setImagePath(url + "assets/dhtmlx/imgs/");
    gridtipe.setHeader("Kode ,Nama tipe, Merek, Jenis, Kategori, Keterangan, Tgl dibuat, Petugas");
    gridtipe.attachHeader("#text_filter,#text_filter,#combo_filter,#combo_filter,#combo_filter,#text_filter,#text_filter,#text_filter");
    gridtipe.setColAlign("left,left,left,left,left,left,center,center");
    gridtipe.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro");
    gridtipe.setInitWidths("100,150,150,150,150,*,100,100");
    gridtipe.init();
    gridtipe.enableDistributedParsing(true,10,300);
    gridtipe.load("<?php echo $data_source; ?>","json");

    if(tipeAkses == "TRUE"){
        gridtipe.attachEvent("onRowDblClicked",gridtipeDBLClick);
    }

    // [ACTION] GRID ACTION
    function gridtipeDBLClick(){
        var module  = url + "master/tipe";
        form_modal(tipeWindows,"tipeWindows",["tipe",500,500], module + "/loadFormToolbar", module + "/loadform/" + gridtipe.getSelectedRowId());
    }
</script>