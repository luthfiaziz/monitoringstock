<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 23/09/15
 * Time: 12:12
 */
?>
<div id="tipeloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <?php
    echo form_open_multipart($form_action, array('id' => 'fm_tipe'));
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span class="title_app"><b>Kode tipe</b> <i>*</i></span>
                <?php
                $readonly = !empty($default->m_item_tipe_kode)?'readonly':'';
                echo form_input('m_item_tipe_kode', !empty($default->m_item_tipe_kode) ? $default->m_item_tipe_kode : '', 'id="m_item_tipe_kode" maxlength="15" class="form_web onlynumber" placeholder="wajib diisi" '.$readonly);
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Nama tipe</b> <i>*</i></span>
                <?php echo form_input('data[m_item_tipe_nama]', !empty($default->m_item_tipe_nama) ? $default->m_item_tipe_nama : '', 'id="m_item_tipe_nama" class="form_web" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Kategori</b> <i>*</i></span>
                <?php echo form_dropdown('data[m_item_kategori_kode]', $kategori, !empty($default->m_item_kategori_kode) ? $default->m_item_kategori_kode : '', 'id="m_item_kategori_kode" class="full"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Jenis</b> <i>*</i></span>
                <?php echo form_dropdown('data[m_item_jenis_kode]', $jenis, !empty($default->m_item_jenis_kode) ? $default->m_item_jenis_kode : '', 'id="m_item_jenis_kode" class="full" data-source="' . $jenis_source . '"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Merk</b> <i>*</i></span>
                <?php echo form_dropdown('data[m_item_merk_kode]', $merk, !empty($default->m_item_merk_kode) ? $default->m_item_merk_kode : '', 'id="m_item_merk_kode" class="full" data-source="' . $merk_source . '"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Keterangan</b> <i></i></span>
                <?php echo form_textarea('data[m_item_tipe_ket]', !empty($default->m_item_tipe_ket) ? $default->m_item_tipe_ket : '', 'id="m_item_tipe_ket" class="form_web" style="height:85px;"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo arrman::getRequired();?>
            </td>
        </tr>

    </table>
    <?php
    echo form_close();
    ?>
</div>
<script type="text/javascript">
    $("#m_item_tipe_kode").focus();
    $("#m_item_jenis_kode").select2();
    $("#m_item_kategori_kode").select2();
    $("#m_item_merk_kode").select2();

    $(function(){
        $("#m_item_kategori_kode").change(function(){
            get_options("#m_item_jenis_kode",{"kat_kode" : $(this).val()}, '');
        });
    });
    
    $(function(){
        $("#m_item_jenis_kode").change(function(){
            get_options("#m_item_merk_kode",{"jenis_kode" : $(this).val()}, '');
        });
    });

    function simpan(id){
        var toolbar = ["tipeWindows","savetipe"];

        SimpanDataTF('#fm_tipe','#tipeloading',toolbar);
    }

    function refresh(){
        close_form_modal(tipeWindows,"tipeWindows");
        refreshGrid(gridtipe,url + "master/tipe/listdata","json");
    }

    function bersihtipe(){
    }
</script>