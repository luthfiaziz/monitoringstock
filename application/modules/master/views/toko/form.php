<div id="tokoloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper">
    <?php
    $hidden_form = array('id' => !empty($id) ? $id : '');
    echo form_open_multipart($form_action, array('id' => 'fm_toko'), $hidden_form);
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span><b>Kode Toko</b> <i>*</i></span><br/>
                <?php 
                    $readonly = ($readonly == true)?'readonly':'';
                    echo form_input('m_toko_kode', !empty($default->m_toko_kode) ? $default->m_toko_kode : '', 'id="m_toko_kode" class="form_web uppercase" placeholder="wajib diisi" style="width:40%"'.$readonly);
                ?>
               
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Nama Toko</b> <i>*</i></span>
                <?php echo form_input('m_toko_nama', !empty($default->m_toko_nama) ? $default->m_toko_nama : '', 'id="m_toko_nama" class="form_web uppercase" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Keterangan</b></span>
                 <?php echo form_textarea('m_toko_ket', !empty($default->m_toko_ket) ? $default->m_toko_ket : '', 'id="m_toko_ket" class="form_web" style="height:50px !important; padding:10px;"'); ?>
            </td>
        </tr>
        
    </table>
    <?php 
        echo form_close(); 
        echo arrman::getRequired();
    ?>
</div>
<script type="text/javascript">
   
    function simpan(id){
        var toolbar = ["tokoWindows","savetoko","canceltoko"];

        SimpanData('#fm_toko','#tokoloading',toolbar);
    }

    function refresh(){
        close_form_modal(tokoWindows,"tokoWindows");
        refreshGrid(gridtoko,url + "master/toko/listdata","json");
    }
    
    function bersihtoko(){
       

        var focus = ["m_toko_kode", "input"];

        refreshCombo(data, "#fm_toko", focus);
    }   
</script>