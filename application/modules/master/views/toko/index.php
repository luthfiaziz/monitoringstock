<script type="text/javascript">
    var tokoAkses    = "<?php echo $aksesEdit; ?>";
    var tokoWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
	// [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
	toolbartoko = mainTab.cells("maintabbar").attachToolbar();
        toolbartoko.setSkin("dhx_skyblue");
 	toolbartoko.setIconsPath(url + "assets/img/btn/");
 	toolbartoko.loadXML("<?php echo $main_toolbar_source; ?>");
        toolbartoko.attachEvent("onClick", ctoko);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function ctoko(id){
        var module  = url + "master/toko";
        var idRow   = gridtoko.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(tokoWindows,"tokoWindows",["[Form] - Toko",500,550], module + "/loadFormtokoToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(tokoWindows,"tokoWindows",["[Form] - Toko",500,550], module + "/loadFormtokoToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({                
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    }

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({ 
                                    type: "alert", 
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridtoko, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            },
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridtoko,url + "master/toko/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
        gridtoko = mainTab.cells("maintabbar").attachGrid();
        gridtoko.setHeader("Kode Toko,Nama Toko,Keterangan");
	gridtoko.attachHeader("#text_filter,#text_filter,#text_filter");
	gridtoko.setColAlign("left,left,left");
	gridtoko.setColTypes("ro,ro,ro");
	gridtoko.setInitWidths("*,*,*");
	gridtoko.init(); 
        gridtoko.enableDistributedParsing(true,10,300);
	gridtoko.load("<?php echo $data_source; ?>","json");

    if(tokoAkses == "TRUE"){
    	gridtoko.attachEvent("onRowDblClicked",gridtokoDBLClick);
    }

    // [ACTION] GRID ACTION
	function gridtokoDBLClick(){
        var module  = url + "master/toko";
        form_modal(tokoWindows,"tokoWindows",["[Form] - Toko",500,550], module + "/loadFormtokoToolbar", module + "/loadform/" + gridtoko.getSelectedRowId());
    }
</script>