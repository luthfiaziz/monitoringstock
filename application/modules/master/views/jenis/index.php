<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 23/09/15
 * Time: 10:57
 */
?>
<script type="text/javascript">
    var jenisAkses    = "<?php echo $aksesEdit; ?>";
    var jenisWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbarjenis = mainTab.cells("maintabbar").attachToolbar();
    toolbarjenis.setSkin("dhx_skyblue");
    toolbarjenis.setIconsPath(url + "assets/img/btn/");
    toolbarjenis.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarjenis.attachEvent("onClick", cjenis);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function cjenis(id){
        var module  = url + "master/jenis";
        var idRow   = gridjenis.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(jenisWindows,"jenisWindows",["jenis",500,400], module + "/loadFormToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(jenisWindows,"jenisWindows",["jenis",500,400], module + "/loadFormToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    };

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({
                                    type: "alert",
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridjenis, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridjenis,module + "/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridjenis = mainTab.cells("maintabbar").attachGrid();
    gridjenis.setImagePath(url + "assets/dhtmlx/imgs/");
    gridjenis.setHeader("Kode ,Nama jenis, Kategori, Keterangan, Tgl dibuat, Petugas");
    gridjenis.attachHeader("#text_filter,#text_filter,#combo_filter,#text_filter,#text_filter,#text_filter");
    gridjenis.setColAlign("left,left,left,left,center,center");
    gridjenis.setColTypes("ro,ro,ro,ro,ro,ro");
    gridjenis.setInitWidths("180,200,200,*,120,120");
    gridjenis.init();
    gridjenis.enableDistributedParsing(true,10,300);
    gridjenis.load("<?php echo $data_source; ?>","json");

    if(jenisAkses == "TRUE"){
        gridjenis.attachEvent("onRowDblClicked",gridjenisDBLClick);
    }

    // [ACTION] GRID ACTION
    function gridjenisDBLClick(){
        var module  = url + "master/jenis";
        form_modal(jenisWindows,"jenisWindows",["jenis",500,400], module + "/loadFormToolbar", module + "/loadform/" + gridjenis.getSelectedRowId());
    }
</script>