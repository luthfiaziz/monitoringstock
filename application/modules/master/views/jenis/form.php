<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 23/09/15
 * Time: 10:57
 */
?>
<div id="jenisloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <?php
    echo form_open_multipart($form_action, array('id' => 'fm_jenis'));
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span class="title_app"><b>Kode Jenis</b> <i>*</i></span>
                <?php
                $readonly = !empty($default->m_item_jenis_kode)?'readonly':'';
                echo form_input('m_item_jenis_kode', !empty($default->m_item_jenis_kode) ? $default->m_item_jenis_kode : '', 'id="m_item_jenis_kode" maxlength="15" class="form_web" placeholder="wajib diisi" '.$readonly);
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Nama Jenis</b> <i>*</i></span>
                <?php echo form_input('data[m_item_jenis_nama]', !empty($default->m_item_jenis_nama) ? $default->m_item_jenis_nama : '', 'id="m_item_jenis_nama" class="form_web" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Kategori</b> <i>*</i></span>
                <?php echo form_dropdown('data[m_item_kategori_kode]', $kategori, !empty($default->m_katerogi_kode) ? $default->m_katerogi_kode : '', 'id="m_katerogi_kode" class="full"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Keterangan</b> <i></i></span>
                <?php echo form_textarea('data[m_item_jenis_ket]', !empty($default->m_item_jenis_ket) ? $default->m_item_jenis_ket : '', 'id="m_item_jenis_ket" class="form_web" style="height:85px;"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo arrman::getRequired();?>
            </td>
        </tr>

    </table>
    <?php
    echo form_close();
    ?>
</div>
<script type="text/javascript">
    $("#m_item_jenis_kode").focus();
    $("#m_katerogi_kode").select2();

    function simpan(id){
        var toolbar = ["jenisWindows","savejenis"];

        SimpanDataTF('#fm_jenis','#jenisloading',toolbar);
    }

    function refresh(){
        close_form_modal(jenisWindows,"jenisWindows");
        refreshGrid(gridjenis,url + "master/jenis/listdata","json");
    }

    function bersihjenis(){
    }
</script>