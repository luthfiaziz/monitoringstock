<script type="text/javascript">
    var provinsiAkses    = "<?php echo $aksesEdit; ?>";
    var provinsiWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
	// [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
	toolbarprovinsi = mainTab.cells("maintabbar").attachToolbar();
    toolbarprovinsi.setSkin("dhx_skyblue");
 	toolbarprovinsi.setIconsPath(url + "assets/img/btn/");
 	toolbarprovinsi.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarprovinsi.attachEvent("onClick", cprovinsi);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function cprovinsi(id){
        var module  = url + "master/provinsi";
        var idRow   = gridprovinsi.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(provinsiWindows,"provinsiWindows",["provinsi",500,280], module + "/loadFormprovinsiToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(provinsiWindows,"provinsiWindows",["provinsi",500,280], module + "/loadFormprovinsiToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({                
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    }

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({ 
                                    type: "alert", 
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridprovinsi, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            },
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridprovinsi,url + "master/provinsi/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridprovinsi = mainTab.cells("maintabbar").attachGrid();
    gridprovinsi.setImagePath(url + "assets/dhtmlx/imgs/");
    gridprovinsi.setHeader("Kode Provinsi,Nama Provinsi,Tanggal Dibuat, Tanggal Diupdate, Dibuat Oleh, Status");
	gridprovinsi.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter");
	gridprovinsi.setColAlign("left,left,left,left,left,center");
	gridprovinsi.setColTypes("ro,ro,ro,ro,ro,ro");
	gridprovinsi.setInitWidths("*,*,*,*,*,*");
	gridprovinsi.init(); 
    gridprovinsi.enableDistributedParsing(true,10,300);
	gridprovinsi.load("<?php echo $data_source; ?>","json");

    if(provinsiAkses == "TRUE"){
    	gridprovinsi.attachEvent("onRowDblClicked",gridprovinsiDBLClick);
    }

    // [ACTION] GRID ACTION
	function gridprovinsiDBLClick(){
        var module  = url + "master/provinsi";
        form_modal(provinsiWindows,"provinsiWindows",["provinsi",500,280], module + "/loadFormprovinsiToolbar", module + "/loadform/" + gridprovinsi.getSelectedRowId());
    }
</script>