<div id="provinsiloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper">
    <?php
    $hidden_form = array('id' => !empty($id) ? $id : '');
    echo form_open_multipart($form_action, array('id' => 'fm_provinsi'), $hidden_form);
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span><b>Kode Provinsi</b> <i>*</i></span>
                <?php 
                    $readonly = ($readonly == true)?'readonly':'';
                    echo form_input('provinsi_kode', !empty($default->provinsi_kode) ? $default->provinsi_kode : '', 'id="provinsi_kode" class="form_web uppercase onlynumber" placeholder="wajib diisi" '.$readonly);
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Nama Provinsi</b> <i>*</i></span>
                <?php echo form_input('provinsi_nama', !empty($default->provinsi_nama) ? $default->provinsi_nama : '', 'id="provinsi_nama" class="form_web uppercase" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Status</b> <i>*</i></span>
                <?php echo form_dropdown('provinsi_status', array(""=>"--Pilih Data--","1"=>"Aktif", "0"=>"Tidak Aktif"), !empty($default->provinsi_status) ? $default->provinsi_status : '', 'id="provinsi_status" class="full"'); ?>
            </td>
        </tr>

    </table>
    <?php 
        echo form_close(); 
        echo arrman::getRequired();
    ?>
</div>
<script type="text/javascript">
    $("#provinsi_status").select2();
    $("#provinsi_kode").focus();

    function simpan(id){
        var toolbar = ["provinsiWindows","saveprovinsi","cancelprovinsi"];

        SimpanData('#fm_provinsi','#provinsiloading',toolbar);
    }

    function refresh(){
        close_form_modal(provinsiWindows,"provinsiWindows");
        refreshGrid(gridprovinsi,url + "master/provinsi/listdata","json");
    }
    
    function bersihprovinsi(){
        var data = [
                        ["provinsi_status","","combo"],
                    ];

        var focus = ["provinsi_kode", "input"];

        refreshCombo(data, "#fm_provinsi", focus);
    }   
</script>