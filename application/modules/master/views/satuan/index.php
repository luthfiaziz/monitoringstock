<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 23/09/15
 * Time: 16:26
 */
?>
<script type="text/javascript">
    var satuanAkses    = "<?php echo $aksesEdit; ?>";
    var satuanWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbarsatuan = mainTab.cells("maintabbar").attachToolbar();
    toolbarsatuan.setSkin("dhx_skyblue");
    toolbarsatuan.setIconsPath(url + "assets/img/btn/");
    toolbarsatuan.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarsatuan.attachEvent("onClick", csatuan);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function csatuan(id){
        var module  = url + "master/satuan";
        var idRow   = gridsatuan.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(satuanWindows,"satuanWindows",["satuan",500,350], module + "/loadFormToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(satuanWindows,"satuanWindows",["satuan",500,350], module + "/loadFormToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    };

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({
                                    type: "alert",
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridsatuan, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridsatuan,module + "/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridsatuan = mainTab.cells("maintabbar").attachGrid();
    gridsatuan.setImagePath(url + "assets/dhtmlx/imgs/");
    gridsatuan.setHeader("Kode ,Nama satuan, Tgl dibuat, Petugas, Status");
    gridsatuan.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#combo_filter");
    gridsatuan.setColAlign("left,left,center,center,center");
    gridsatuan.setColTypes("ro,ro,ro,ro,ro");
    gridsatuan.setInitWidths("200,*,150,150,150");
    gridsatuan.init();
    gridsatuan.enableDistributedParsing(true,10,300);
    gridsatuan.load("<?php echo $data_source; ?>","json");

    if(satuanAkses == "TRUE"){
        gridsatuan.attachEvent("onRowDblClicked",gridsatuanDBLClick);
    }

    // [ACTION] GRID ACTION
    function gridsatuanDBLClick(){
        var module  = url + "master/satuan";
        form_modal(satuanWindows,"satuanWindows",["satuan",500,350], module + "/loadFormToolbar", module + "/loadform/" + gridsatuan.getSelectedRowId());
    }
</script>