<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 23/09/15
 * Time: 16:26
 */
?>
<div id="satuanloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <?php
    echo form_open_multipart($form_action, array('id' => 'fm_satuan'));
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span class="title_app"><b>Kode satuan</b> <i>*</i></span>
                <?php
                $readonly = !empty($default->kode_satuan)?'readonly':'';
                echo form_input('kode_satuan', !empty($default->kode_satuan) ? $default->kode_satuan : '', 'id="kode_satuan" class="form_web" placeholder="wajib diisi" maxlength="4" '.$readonly);
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Nama satuan</b> <i>*</i></span>
                <?php echo form_input('data[nama_satuan]', !empty($default->nama_satuan) ? $default->nama_satuan : '', 'id="nama_satuan" class="form_web" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Status</b> <i>*</i></span>
                <?php echo form_dropdown('data[aktif]',array(''=>'--Pillih Status--','1'=>'Aktif','0'=>'Tidak aktif'), !empty($default->aktif) ? $default->aktif : '', 'id="aktif" class="full"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo arrman::getRequired();?>
            </td>
        </tr>

    </table>
    <?php
    echo form_close();
    ?>
</div>
<script type="text/javascript">
    $("#kode_satuan").focus();
    $("#aktif").select2();

    function simpan(id){
        var toolbar = ["satuanWindows","savesatuan"];

        SimpanDataTF('#fm_satuan','#satuanloading',toolbar);
    }

    function refresh(){
        close_form_modal(satuanWindows,"satuanWindows");
        refreshGrid(gridsatuan,url + "master/satuan/listdata","json");
    }

    function bersihsatuan(){
    }
</script>