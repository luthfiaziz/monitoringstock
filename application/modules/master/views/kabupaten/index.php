<script type="text/javascript">
    var kabupatenAkses    = "<?php echo $aksesEdit; ?>";
    var kabupatenWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
	// [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
	toolbarkabupaten = mainTab.cells("maintabbar").attachToolbar();
    toolbarkabupaten.setSkin("dhx_skyblue");
 	toolbarkabupaten.setIconsPath(url + "assets/img/btn/");
 	toolbarkabupaten.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarkabupaten.attachEvent("onClick", ckabupaten);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function ckabupaten(id){
        var module  = url + "master/kabupaten";
        var idRow   = gridkabupaten.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(kabupatenWindows,"kabupatenWindows",["kabupaten",500,330], module + "/loadFormkabupatenToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(kabupatenWindows,"kabupatenWindows",["kabupaten",500,330], module + "/loadFormkabupatenToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({                
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    }

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({ 
                                    type: "alert", 
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridkabupaten, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            },
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridkabupaten,url + "master/kabupaten/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridkabupaten = mainTab.cells("maintabbar").attachGrid();
    gridkabupaten.setImagePath(url + "assets/dhtmlx/imgs/");
    gridkabupaten.setHeader("Kode kabupaten,Nama kabupaten,Provinsi,Tanggal Dibuat, Tanggal Diupdate, Dibuat Oleh, Status");
	gridkabupaten.attachHeader("#text_filter,#text_filter,#combo_filter,#text_filter,#text_filter,#text_filter,#combo_filter");
	gridkabupaten.setColAlign("left,left,left,left,left,left,center");
	gridkabupaten.setColTypes("ro,ro,ro,ro,ro,ro,ro");
	gridkabupaten.setInitWidths("*,*,*,*,*,*,*");
	gridkabupaten.init();  
    gridkabupaten.enableDistributedParsing(true,10,300);
	gridkabupaten.load("<?php echo $data_source; ?>","json");

    if(kabupatenAkses == "TRUE"){
    	gridkabupaten.attachEvent("onRowDblClicked",gridkabupatenDBLClick);
    }

    // [ACTION] GRID ACTION
	function gridkabupatenDBLClick(){
        var module  = url + "master/kabupaten";
        form_modal(kabupatenWindows,"kabupatenWindows",["kabupaten",500,330], module + "/loadFormkabupatenToolbar", module + "/loadform/" + gridkabupaten.getSelectedRowId());
    }
</script>