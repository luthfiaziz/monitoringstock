<div id="kabupatenloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:30%;"/>
</div>
<div class="form-wrapper">
    <?php
    $hidden_form = array('id' => !empty($id) ? $id : '');
    echo form_open_multipart($form_action, array('id' => 'fm_kabupaten'), $hidden_form);
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span><b>Kode kabupaten</b> <i>*</i></span>
                <?php 
                    $kabupaten_id = !empty($default->kabupaten_kode) ? $default->kabupaten_kode : '';
                    $kabupaten_id = explode(".", $kabupaten_id);
                    $readonly     = ($readonly == true) ? "readonly" : '';
                    echo form_input('kabupaten_kode', !empty($default->kabupaten_kode) ? $default->kabupaten_kode : '', 'id="kabupaten_kode" class="form_web uppercase onlynumber" placeholder="wajib diisi" ' . $readonly);
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Nama kabupaten</b> <i>*</i></span>
                <?php echo form_input('kabupaten_ket', !empty($default->kabupaten_ket) ? $default->kabupaten_ket : '', 'id="kabupaten_ket" class="form_web uppercase" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Provinsi</b> <i>*</i></span>
                <?php echo form_dropdown('provinsi_kode', $provinsi_options, !empty($default->provinsi_kode) ? $default->provinsi_kode : '', 'id="provinsi_kode" class="full"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Status</b> <i>*</i></span>
                <?php echo form_dropdown('kabupaten_status', array(""=>"--Pilih Data--","1"=>"Aktif", "0"=>"Tidak Aktif"), !empty($default->kabupaten_status) ? $default->kabupaten_status : '', 'id="kabupaten_status" class="full"'); ?>
            </td>
        </tr>

    </table>
    <?php 
        echo form_close(); 
        echo arrman::getRequired();
    ?>
</div>
<script type="text/javascript">
    $("#provinsi_kode").select2();
    $("#kabupaten_status").select2();
    $("#kabupaten_kode").focus();

    function simpan(id){
        var toolbar = ["kabupatenWindows","savekabupaten","cancelkabupaten"];

        SimpanData('#fm_kabupaten','#kabupatenloading',toolbar);
    }

    function refresh(){
        close_form_modal(kabupatenWindows,"kabupatenWindows");
        refreshGrid(gridkabupaten,url + "master/kabupaten/listdata","json");
    }
    
    function bersihkabupaten(){
        var data = [
                        ["provinsi_kode","","combo"],
                        ["kabupaten_status","","combo"],
                    ];

        var focus = ["kabupaten_kode","input"];

        refreshForm(data, "#fm_kabupaten", focus);
    }   
</script>