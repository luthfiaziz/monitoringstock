<script type="text/javascript">
    var listAkses    = "<?php echo $aksesEdit; ?>";
    var listWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // ADD LAYOUT FIRST
    //-------------------------------------------------------------------------------------------------------------------------------
    layoutlist = mainTab.cells("maintabbar").attachLayout("2E");
    layoutlist.cells("a").setHeight(250);
    layoutlist.cells("a").fixSize(true,true);
    layoutlist.cells("a").hideHeader(true);

    layoutlist.cells("b").setHeight(130);
    layoutlist.cells("b").fixSize(true,true);
    layoutlist.cells("b").hideHeader(true);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbarlist = layoutlist.cells("a").attachToolbar();
    toolbarlist.setSkin("dhx_skyblue");
    toolbarlist.setIconsPath(url + "assets/img/btn/");
    toolbarlist.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarlist.attachEvent("onClick", clist);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function clist(id){
        var module  = url + "master/list_item";
        var idRow   = gridlist.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(listWindows,"listWindows",["list",800,400], module + "/loadFormToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(listWindows,"listWindows",["list",800,400], module + "/loadFormToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    };

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({
                                    type: "alert",
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridlist, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridlist,module + "/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridlistfilter = layoutlist.cells("b").attachToolbar();
    gridlistfilter.setSkin("dhx_skyblue");
    gridlistfilter.setIconsPath(url + "assets/img/btn/");

    gridlist = layoutlist.cells("b").attachGrid();
    gridlist.setImagePath(url + "assets/dhtmlx/imgs/");
    gridlist.setHeader("Kode ,Nama Item, Merek, Jenis, Kategori, Keterangan, Tgl dibuat, Petugas");
    gridlist.setColAlign("left,left,left,left,left,left,center,center");
    gridlist.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro");
    gridlist.setInitWidths("90,200,150,150,150,*,90,90");
    gridlist.init();
    gridlist.enableDistributedParsing(true,10,300);
    gridlist.load("<?php echo $data_source; ?>","json");

    if(listAkses == "TRUE"){
        gridlist.attachEvent("onRowDblClicked",gridlistDBLClick);
    }

    // [ACTION] GRID ACTION
    function gridlistDBLClick(){
        var module  = url + "master/list_item";
        form_modal(listWindows,"listWindows",["list",800,400], module + "/loadFormToolbar", module + "/loadform/" + gridlist.getSelectedRowId());
    }


    layoutlist.cells("b").setFooter("tes");
</script>