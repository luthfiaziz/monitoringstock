<?php
/**
 * Created by PhpStorm.
 * User: INTEX-N
 * Date: 28/09/15
 * Time: 11:04
 */
?>
<div id="listloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <?php
    echo form_open_multipart($form_action, array('id' => 'fm_list'));
    ?>
    <table class="form" width="100%">
        <tr>
            <td width="50%">
                <span class="title_app"><b>Kode Item</b> <i>*</i></span>
                <?php
                $readonly = !empty($default->m_item_kode)?'readonly':'';
                echo form_input('m_item_kode', !empty($default->m_item_kode) ? $default->m_item_kode : '', 'id="m_item_kode" maxlength="15" class="form_web" placeholder="wajib diisi" '.$readonly);
                ?>
            </td>
            <td>
                <span class="title_app"><b>Nama Item</b> <i>*</i></span>
                <?php echo form_input('data[m_item_nama]', !empty($default->m_item_nama) ? $default->m_item_nama : '', 'id="m_item_nama" class="form_web" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <span class="title_app"><b>Barcode</b> <i></i></span>
                <?php echo form_input('data[m_item_barcode]', !empty($default->m_item_barcode) ? $default->m_item_barcode : '', 'id="m_item_barcode" class="form_web" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Kategori</b> <i>*</i></span>
                <?php echo form_dropdown('data[m_item_kategori_kode]', $kategori, !empty($default->m_item_kategori_kode) ? $default->m_item_kategori_kode : '', 'id="m_item_kategori_kode" class="full"'); ?>
            </td>
            <td>
                <span class="title_app"><b>Satuan</b> <i>*</i></span>
                <?php echo form_dropdown('data[m_satuan_id]', $satuan, !empty($default->m_satuan_id) ? $default->m_satuan_id : '', 'id="m_satuan_id" class="full"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Jenis</b> <i>*</i></span>
                <?php echo form_dropdown('data[m_item_jenis_kode]', $jenis, !empty($default->m_item_jenis_kode) ? $default->m_item_jenis_kode : '', 'id="m_item_jenis_kode" class="full" data-source="' . $jenis_source . '"'); ?>
            </td>
            <td rowspan="2">
                <span class="title_app"><b>Keterangan</b> <i></i></span>
                <?php echo form_textarea('data[m_item_ket]', !empty($default->m_item_ket) ? $default->m_item_ket : '', 'id="m_item_ket" class="form_web" style="height:75px;"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Merk</b> <i>*</i></span>
                <?php echo form_dropdown('data[m_item_merk_kode]', $merk, !empty($default->m_item_merk_kode) ? $default->m_item_merk_kode : '', 'id="m_item_merk_kode" class="full" data-source="' . $merk_source . '"'); ?>
            </td>
        </tr>
<!--        <tr>-->
<!--            <td>-->
<!--                <span><b>Tipe</b> <i>*</i></span>-->
<!--                --><?php //echo form_dropdown('data[m_item_tipe_kode]', $tipe, !empty($default->m_item_tipe_kode) ? $default->m_item_tipe_kode : '', 'id="m_item_tipe_kode" class="full" data-source="' . $tipe_source . '"'); ?>
<!--            </td>-->
<!--            <td>&nbsp;</td>-->
<!--        </tr>-->
        <tr>
            <td colspan="2">
                <?php echo arrman::getRequired();?>
            </td>
        </tr>

    </table>
    <?php
    echo form_close();
    ?>
</div>
<script type="text/javascript">
    $("#m_item_kode").focus();
    $("#m_item_jenis_kode").select2();
    $("#m_item_kategori_kode").select2();
    $("#m_item_merk_kode").select2();
//    $("#m_item_tipe_kode").select2();
    $("#m_toko_kode").select2();
    $("#m_rak_kode").select2();
    $("#m_satuan_id").select2();

    $(function(){
        $("#m_item_kategori_kode").change(function(){
            get_options("#m_item_jenis_kode",{"kat_kode" : $(this).val()}, '');
        });
    });

    $(function(){
        $("#m_item_jenis_kode").change(function(){
            get_options("#m_item_merk_kode",{"jenis_kode" : $(this).val()}, '');
        });
    });

//    $(function(){
//        $("#m_item_merk_kode").change(function(){
//            get_options("#m_item_tipe_kode",{"merk_kode" : $(this).val()}, '');
//        });
//    });

    function simpan(id){
        var toolbar = ["listWindows","savelist"];

        SimpanDataTF('#fm_list','#listloading',toolbar);
    }

    function refresh(){
        close_form_modal(listWindows,"listWindows");
        refreshGrid(gridlist,url + "master/list_item/listdata","json");
    }

    function bersihlist(){
    }
</script>