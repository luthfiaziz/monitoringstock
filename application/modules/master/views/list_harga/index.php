<?php
/**
 * User: Bayu Nugraha (bayu.nugraha@intex.co.id)
 * Date: 08/10/15
 * Time: 14:29
 */
?>
<script type="text/javascript">
    var list_hargaAkses    = "<?php echo $aksesEdit; ?>";
    var list_hargaWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbarlist_harga = mainTab.cells("maintabbar").attachToolbar();
    toolbarlist_harga.setSkin("dhx_skyblue");
    toolbarlist_harga.setIconsPath(url + "assets/img/btn/");
    toolbarlist_harga.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarlist_harga.attachEvent("onClick", clist_harga);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function clist_harga(id){
        var module  = url + "master/list_harga";
        var idRow   = gridlist_harga.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(list_hargaWindows,"list_hargaWindows",["Harga",500,350], module + "/loadFormToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(list_hargaWindows,"list_hargaWindows",["Harga",500,350], module + "/loadFormToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    };

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({
                                    type: "alert",
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridlist_harga, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridlist_harga,module + "/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridlist_harga = mainTab.cells("maintabbar").attachGrid();
    gridlist_harga.setImagePath(url + "assets/dhtmlx/imgs/");
    gridlist_harga.setHeader("Kode Item,Nama Item, Harga jual, Harga Beli");
    gridlist_harga.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter");
    gridlist_harga.setColAlign("left,left,right,right");
    gridlist_harga.setColTypes("ro,ro,ron,ron");
    gridlist_harga.setInitWidths("200,*,250,250");
    gridlist_harga.setNumberFormat("0,000.00", 2, ".", ",");
    gridlist_harga.setNumberFormat("0,000.00", 3, ".", ",");
    gridlist_harga.init();
    gridlist_harga.enableDistributedParsing(true,10,300);
    gridlist_harga.load("<?php echo $data_source; ?>","json");

    if(list_hargaAkses == "TRUE"){
        gridlist_harga.attachEvent("onRowDblClicked",gridlist_hargaDBLClick);
    }

    // [ACTION] GRID ACTION
    function gridlist_hargaDBLClick(){
        var module  = url + "master/list_harga";
        form_modal(list_hargaWindows,"list_hargaWindows",["Harga",500,350], module + "/loadFormToolbar", module + "/loadform/" + gridlist_harga.getSelectedRowId());
    }
</script>