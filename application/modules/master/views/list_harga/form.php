<?php
/**
 * User: Bayu Nugraha (bayu.nugraha@intex.co.id)
 * Date: 08/10/15
 * Time: 14:29
 */
?>
<div id="list_hargaloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <?php
    echo form_open_multipart($form_action, array('id' => 'fm_list_harga'));
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span class="title_app"><b>Kode Item</b> <i></i></span>
                <?php
                $readonly = !empty($default->m_item_kode)?'readonly':'';
                echo form_input('m_item_kode', !empty($default->m_item_kode) ? $default->m_item_kode : '', 'id="m_item_kode" maxlength="15" class="form_web" placeholder="wajib diisi" ' . $readonly);
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Nama Item</b> <i></i></span>
                <?php echo form_input('m_item_nama', !empty($default->m_item_nama) ? $default->m_item_nama : '', 'id="m_item_nama" class="form_web" placeholder="wajib diisi" ' . $readonly); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Harga Jual</b> <i></i></span>
                <?php echo form_input('data[m_harga_jual]', !empty($default->m_harga_jual) ? $default->m_harga_jual : '', 'id="m_harga_jual" class="form_web onlynumber" placeholder=""'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Harga Beli</b> <i></i></span>
                <?php echo form_input('data[m_harga_beli]', !empty($default->m_harga_beli) ? $default->m_harga_beli : '', 'id="m_harga_beli" class="form_web onlynumber" placeholder=""'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo arrman::getRequired();?>
            </td>
        </tr>

    </table>
    <?php
    echo form_close();
    ?>
</div>
<script type="text/javascript">
    $("#m_harga_jual").focus();

    function simpan(id){
        var toolbar = ["list_hargaWindows","saveForm"];

        SimpanDataTF('#fm_list_harga','#list_hargaloading',toolbar);
    }

    function refresh(){
        close_form_modal(list_hargaWindows,"list_hargaWindows");
        refreshGrid(gridlist_harga,url + "master/list_harga/listdata","json");
    }

    function bersihkategori(){
    }
</script>