<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 20/10/15
 * Time: 9:39
 */
?>
<div id="supplierloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <?php
    echo form_open_multipart($form_action, array('id' => 'fm_supplier'));
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span class="title_app"><b>Kode Suplier</b> <i></i></span>
                <?php echo form_input('m_suplier_kode', !empty($default->m_suplier_kode) ? $default->m_suplier_kode : '', 'id="m_suplier_kode" maxlength="15" class="form_web" placeholder="Auto" readonly');?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Nama Supplier</b> <i>*</i></span>
                <?php echo form_input('data[m_suplier_nama]', !empty($default->m_suplier_nama) ? $default->m_suplier_nama : '', 'id="m_suplier_nama" class="form_web" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>No Telpon</b> <i>*</i></span>
                <?php echo form_input('data[m_suplier_no_tlp]', !empty($default->m_suplier_no_tlp) ? $default->m_suplier_no_tlp : '', 'id="m_suplier_no_tlp" class="form_web onlynumber" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Alamat</b> <i>*</i></span>
                <?php echo form_textarea('data[m_suplier_alamat]', !empty($default->m_suplier_alamat) ? $default->m_suplier_alamat : '', 'id="m_suplier_alamat" class="form_web" placeholder="wajib diisi" style="height:85px;"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo arrman::getRequired();?>
            </td>
        </tr>

    </table>
    <?php
    echo form_close();
    ?>
</div>
<script type="text/javascript">
    $("#m_suplier_nama").focus();

    function simpan(id){
        var toolbar = ["supplierWindows","savesupplier"];

        SimpanDataTF('#fm_supplier','#supplierloading',toolbar);
    }

    function refresh(){
        close_form_modal(supplierWindows,"supplierWindows");
        refreshGrid(gridsupplier,url + "master/supplier/listdata","json");
    }

    function bersihsupplier(){
    }
</script>