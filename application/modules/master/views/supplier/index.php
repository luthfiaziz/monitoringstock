<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 20/10/15
 * Time: 9:39
 */
?>
<script type="text/javascript">
    var supplierAkses    = "<?php echo $aksesEdit; ?>";
    var supplierWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbarsupplier = mainTab.cells("maintabbar").attachToolbar();
    toolbarsupplier.setSkin("dhx_skyblue");
    toolbarsupplier.setIconsPath(url + "assets/img/btn/");
    toolbarsupplier.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarsupplier.attachEvent("onClick", csupplier);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function csupplier(id){
        var module  = url + "master/supplier";
        var idRow   = gridsupplier.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(supplierWindows,"supplierWindows",["supplier",500,400], module + "/loadFormToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(supplierWindows,"supplierWindows",["supplier",500,400], module + "/loadFormToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    };

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({
                                    type: "alert",
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridsupplier, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridsupplier,module + "/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridsupplier = mainTab.cells("maintabbar").attachGrid();
    gridsupplier.setImagePath(url + "assets/dhtmlx/imgs/");
    gridsupplier.setHeader("Kode ,Nama Supplier, Alamat, No Tlp, Tgl Dibuat, Petugas");
    gridsupplier.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
    gridsupplier.setColAlign("left,left,left,left,center,center");
    gridsupplier.setColTypes("ro,ro,ro,ro,ro,ro");
    gridsupplier.setInitWidths("150,250,*,150,100,100");
    gridsupplier.init();
    gridsupplier.enableDistributedParsing(true,10,300);
    gridsupplier.load("<?php echo $data_source; ?>","json");

    if(supplierAkses == "TRUE"){
        gridsupplier.attachEvent("onRowDblClicked",gridsupplierDBLClick);
    }

    // [ACTION] GRID ACTION
    function gridsupplierDBLClick(){
        var module  = url + "master/supplier";
        form_modal(supplierWindows,"supplierWindows",["supplier",500,400], module + "/loadFormToolbar", module + "/loadform/" + gridsupplier.getSelectedRowId());
    }
</script>