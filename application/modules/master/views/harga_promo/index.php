<?php
/**
 * User: Bayu Nugraha (bayu.nugraha@intex.co.id)
 * Date: 08/10/15
 * Time: 15:54
 */
?>
<script type="text/javascript">
    var harga_promoAkses    = "<?php echo $aksesEdit; ?>";
    var harga_promoWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbarharga_promo = mainTab.cells("maintabbar").attachToolbar();
    toolbarharga_promo.setSkin("dhx_skyblue");
    toolbarharga_promo.setIconsPath(url + "assets/img/btn/");
    toolbarharga_promo.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarharga_promo.attachEvent("onClick", charga_promo);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function charga_promo(id){
        var module  = url + "master/harga_promo";
        var idRow   = gridharga_promo.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(harga_promoWindows,"harga_promoWindows",["Harga",750,500], module + "/loadFormToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(harga_promoWindows,"harga_promoWindows",["Harga",750,500], module + "/loadFormToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    };

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({
                                    type: "alert",
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridharga_promo, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridharga_promo,module + "/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridharga_promo = mainTab.cells("maintabbar").attachGrid();
    gridharga_promo.setImagePath(url + "assets/dhtmlx/imgs/");
    gridharga_promo.setHeader("Kode Item,Nama Item, Muali Promo, Akhir Promo, Harga Promo, Qty Promo");
    gridharga_promo.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
    gridharga_promo.setColAlign("left,left,center,center,right,center");
    gridharga_promo.setColTypes("ro,ro,ro,ro,ron,ro");
    gridharga_promo.setInitWidths("200,*,150,150,200,100");
    gridharga_promo.setNumberFormat("0,000.00", 4, ".", ",");
    gridharga_promo.init();
    gridharga_promo.enableDistributedParsing(true,10,300);
    gridharga_promo.load("<?php echo $data_source; ?>","json");

    if(harga_promoAkses == "TRUE"){
        gridharga_promo.attachEvent("onRowDblClicked",gridharga_promoDBLClick);
    }

    // [ACTION] GRID ACTION
    function gridharga_promoDBLClick(){
        var module  = url + "master/harga_promo";
        form_modal(harga_promoWindows,"harga_promoWindows",["Harga",750,500], module + "/loadFormToolbar", module + "/loadform/" + gridharga_promo.getSelectedRowId());
    }
</script>