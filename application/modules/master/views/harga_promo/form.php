<?php
/**
 * User: Bayu Nugraha (bayu.nugraha@intex.co.id)
 * Date: 08/10/15
 * Time: 15:54
 */
?>
<div id="harga_promoloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <?php
    echo form_open_multipart($form_action, array('id' => 'fm_harga_promo'));
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span class="title_app"><b>Kode Item</b> <i></i></span>
                <?php
                $readonly = !empty($default->m_item_kode)?'readonly':'';
                echo form_input('m_item_kode', !empty($default->m_item_kode) ? $default->m_item_kode : '', 'id="m_item_kode" maxlength="15" class="form_web" placeholder="wajib diisi" ' . $readonly);
                ?>
            </td>
            <td>
                <span class="title_app"><b>Nama Item</b> <i></i></span>
                <?php echo form_input('m_item_nama', !empty($default->m_item_nama) ? $default->m_item_nama : '', 'id="m_item_nama" class="form_web" placeholder="wajib diisi" ' . $readonly); ?>
            </td>
            <td>
                <span class="title_app"><b>Potongan Promo</b> <i></i></span>
                <?php echo form_input('data[m_harga_promo_harga]', !empty($default->m_harga_promo_harga) ? $default->m_harga_promo_harga : '', 'id="m_harga_promo_harga" class="form_web onlynumber"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Mulai Promo</b> <i></i></span>
                <?php echo form_input('data[m_harga_promo_periode_mulai]', !empty($default->m_harga_promo_periode_mulai) ? $default->m_harga_promo_periode_mulai : date("Y-m-d"), 'id="m_harga_promo_periode_mulai" class="form_web" readonly'); ?>
            </td>
            <td>
                <span class="title_app"><b>Akhir Promo</b> <i></i></span>
                <?php echo form_input('data[m_harga_promo_periode_akhir]', !empty($default->m_harga_promo_periode_akhir) ? $default->m_harga_promo_periode_akhir : date("Y-m-d"), 'id="m_harga_promo_periode_akhir" class="form_web" readonly'); ?>
            </td>
            <td>
                <span class="title_app"><b>Syarat Promo</b> <i></i></span>
                <?php echo form_input('data[m_harga_promo_qty_promo]', !empty($default->m_harga_promo_qty_promo) ? $default->m_harga_promo_qty_promo : '', 'id="m_harga_promo_qty_promo" class="form_web onlynumber"'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <span class="title_app"><b>Harga Jual Item</b> <i></i></span>
                <?php echo form_input('m_harga_jual', !empty($default->m_harga_jual) ? $default->m_harga_jual : '', 'id="m_harga_jual" class="form_web onlynumber" readonly'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div style="width: 100%; height: 220px;" id="gridHargaPromoForm"></div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <?php echo arrman::getRequired();?>
            </td>
        </tr>

    </table>
    <?php
    echo form_close();
    ?>
</div>
<script type="text/javascript">
    $("#m_harga_promo_harga").focus();

    cal1    = new dhtmlxCalendarObject("m_harga_promo_periode_mulai");
    cal2    = new dhtmlxCalendarObject("m_harga_promo_periode_akhir");

    gridHargaPromoForm  = new dhtmlXGridObject("gridHargaPromoForm");
    gridHargaPromoForm.setImagePath(url + "assets/dhtmlx/imgs/");
    gridHargaPromoForm.setHeader("Kode Toko,Nama Toko, Aktif Promo");
    gridHargaPromoForm.setColAlign("left,left,center");
    gridHargaPromoForm.setColTypes("ro,ro,ch");
    gridHargaPromoForm.setInitWidths("150,*,50");
    gridHargaPromoForm.init();
    gridHargaPromoForm.load(url + "master/harga_promo/loadDataToko/<?php echo $default->m_item_kode;?>","json");

    function simpan(id){
        var toolbar = ["harga_promoWindows","saveForm"];

        var dataLainLain    = {
            dataGrid: $.csv.toArrays(gridHargaPromoForm.serializeToCSV())
        };

        SimpanDataTF('#fm_harga_promo','#harga_promoloading',toolbar,"",dataLainLain);
    }

    function refresh(){
        close_form_modal(harga_promoWindows,"harga_promoWindows");
        refreshGrid(gridharga_promo,url + "master/harga_promo/listdata","json");
    }

    function bersihkategori(){
    }
</script>