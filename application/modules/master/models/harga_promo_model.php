<?php
/**
 * User: Bayu Nugraha (bayu.nugraha@intex.co.id)
 * Date: 08/10/15
 * Time: 15:54
 */

class Harga_promo_model extends CI_Model {
    private $_table1 = "m_harga_promo";
    private $_table2 = "m_item";
    private $_table3 = "m_toko";
    private $_table4 = "m_harga";

    public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("b.m_item_kode" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->select("a.*,b.*,c.m_harga_jual,c.m_harga_beli");
        $this->db->from($this->_table1 . " a");
        $this->db->join($this->_table2 . " b","a.m_item_kode=b.m_item_kode","right");
        $this->db->join($this->_table4 . " c","c.m_item_kode=b.m_item_kode","left");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData(){
        $result = $this->data()->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->m_item_kode,
                "data"=>array(
                    $value->m_item_kode,
                    $value->m_item_nama,
                    $value->m_harga_promo_periode_mulai,
                    $value->m_harga_promo_periode_akhir,
                    $value->m_harga_promo_harga,
                    $value->m_harga_promo_qty_promo
                )
            );
        }

        return $data;
    }

    public function loadDataToko($kode_item = ""){
        $this->db->from($this->_table3 . " a");
        $result = $this->db->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $cek    = $this->db->query("SELECT COUNT(a.m_toko_kode) as hit
                                        FROM 1nt3x_m_harga_promo_detail a
                                        LEFT JOIN 1nt3x_m_harga_promo b ON a.m_harga_promo_id = b.m_harga_promo_id
                                        WHERE a.m_toko_kode='" . $value->m_toko_kode . "'
                                        AND b.m_item_kode='" . $kode_item . "'")->row();
            $data[] = array(
                "id" => $value->m_toko_kode,
                "data"=>array(
                    $value->m_toko_kode,
                    $value->m_toko_nama,
                    ($cek->hit=="0")?"0":"1"
                )
            );
        }

        return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where("m_item_kode",$value);

        $total=$this->db->count_all_results();

        return $total;
    }

    public function getMaxData(){
        $sql    = $this->db->query("SELECT MAX(m_harga_promo_id) as noMax FROM 1nt3x_m_harga_promo")->row();

        return $sql;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->m_item_kategori_kode] = $row->m_item_kategori_nama;
        }

        return $option;
    }
}
