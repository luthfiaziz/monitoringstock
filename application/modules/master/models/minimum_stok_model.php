<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class minimum_stok_model extends CI_Model {
	private $_table1 = "1nt3x_vminstok";

     public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("m_item_kode" => $key);

        return $key;
    }

    public function data($key = '', $key2 =''){
        $this->db->from($this->_table1 . " a");

        if(!empty($key2)){
            $this->db->like($key2);
        }

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData($key =''){
    	$result = $this->data('', $key)->get();	
    	$data   = array();
        $no     = 1;
    	foreach ($result->result() as $value) {
			$data[] = array(
							"id" => $value->m_item_kode,
							"data"=>array(
                                           $value->m_item_nama,
                                           $value->m_stok_min,
								         )
						    );
        $no++;
    	}
    	return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where("m_item_kode",$value);

        $total=$this->db->count_all_results();

        return $total;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);
        $this->db->where("unit_status", 1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();
        
        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->unit_id] = $row->unit_name;
        }

        return $option;
    }
}
