<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 23/09/15
 * Time: 10:36
 */

class Kategori_model extends CI_Model {
    private $_table1 = "m_item_kategori";

    public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("m_item_kategori_kode" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->from($this->_table1 . " a");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData(){
        $result = $this->data()->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->m_item_kategori_kode,
                "data"=>array(
                    $value->m_item_kategori_kode,
                    $value->m_item_kategori_nama,
                    $value->m_item_kategori_ket,
                    $value->m_item_kategori_tgl_buat,
                    $value->m_item_kategori_petugas,
                )
            );
        }

        return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where("m_item_kategori_kode",$value);

        $total=$this->db->count_all_results();

        return $total;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->m_item_kategori_kode] = $row->m_item_kategori_nama;
        }

        return $option;
    }
}
