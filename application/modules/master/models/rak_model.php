<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Vredy Wijaya 
 * Create Date : 23/09/2015 - 14:13:00
 * Module Name : Rak Model ( For Template )
 */

class rak_model extends CI_Model {
    private $_table1 = "m_rak";
    private $_table2 = "m_item";

     public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("m_rak_kode" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->from($this->_table1 . " a");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData($key = ""){
    	$result = $this->data($key)->get();
    	$data   = array();

    	foreach ($result->result() as $value) {
			$data[] = array(
							"id" => $value->m_rak_kode,
							"data"=>array(
                                           $value->m_rak_kode,
                                           $value->m_rak_nama,
                                           $value->m_rak_ket
								         )
						    );
    	}

    	return $data;
    }

    public function loadDataToko($kode_rak = ""){
        $this->db->from($this->_table2 . " a");
        $result = $this->db->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $cek    = $this->db->query("SELECT COUNT(a.m_item_kode) as hit
                                        FROM 1nt3x_m_rak_detail a
                                        LEFT JOIN 1nt3x_m_rak b ON a.m_rak_kode = b.m_rak_kode
                                        WHERE a.m_item_kode='" . $value->m_item_kode . "'
                                        AND b.m_rak_kode='" . $kode_rak . "'")->row();
            $data[] = array(
                "id" => $value->m_item_kode,
                "data"=>array(
                    $value->m_item_kode,
                    $value->m_item_nama,
                    ($cek->hit=="0")?"0":"1"
                )
            );
        }

        return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where("m_rak_kode",$value);

        $total=$this->db->count_all_results();

        return $total;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();
        
        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->m_rak_kode] = $row->m_rak_nama;
        }

        return $option;
    }
}
