<?php
/**
 * User: Bayu Nugraha (bayu.nugraha@intex.co.id)
 * Date: 08/10/15
 * Time: 14:20
 */

class List_harga_model extends CI_Model {
    private $_table1 = "m_harga";
    private $_table2 = "m_item";

    public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("b.m_item_kode" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->from($this->_table1 . " a");
        $this->db->join($this->_table2 . " b","a.m_item_kode=b.m_item_kode","right");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData(){
        $result = $this->data()->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->m_item_kode,
                "data"=>array(
                    $value->m_item_kode,
                    $value->m_item_nama,
                    $value->m_harga_jual,
                    $value->m_harga_beli
                )
            );
        }

        return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where("m_item_kode",$value);

        $total=$this->db->count_all_results();

        return $total;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->m_item_kategori_kode] = $row->m_item_kategori_nama;
        }

        return $option;
    }
}
