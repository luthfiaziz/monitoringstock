<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Luthfi Aziz Nugraha 
 * Create Date : 26/07/2014 - 00:43:05
 * Module Name : Menu Model ( For Template )
 */

class provinsi_model extends CI_Model {
	private $_table1 = "provinsi";

     public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("provinsi_kode" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->from($this->_table1 . " a");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData(){
    	$result = $this->data()->get();	
    	$data   = array();

    	foreach ($result->result() as $value) {
			$data[] = array(
							"id" => $value->provinsi_kode,
							"data"=>array(
                                           $value->provinsi_kode,
										   $value->provinsi_nama,
                                           (($value->provinsi_tgl_buat == '0000-00-00')? '-' :$value->provinsi_tgl_buat),
										   (($value->provinsi_tgl_ubah == '0000-00-00')? '-' :$value->provinsi_tgl_ubah),
										   $value->provinsi_petugas,
										   (($value->provinsi_status == 1)? 'Aktif' : 'Tidak Aktif'),
								         )
						    );
    	}

    	return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where("provinsi_kode",$value);

        $total=$this->db->count_all_results();

        return $total;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();
        
        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->provinsi_kode] = $row->provinsi_nama;
        }

        return $option;
    }
}
