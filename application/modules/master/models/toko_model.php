<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Vredy Wijaya 
 * Create Date : 23/09/2015 - 10:58:00
 * Module Name : Toko Model ( For Template )
 */

class toko_model extends CI_Model {
	private $_table1 = "m_toko";

     public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("m_toko_kode" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->from($this->_table1 . " a");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData(){
    	$result = $this->data()->get();	
    	$data   = array();

    	foreach ($result->result() as $value) {
			$data[] = array(
							"id" => $value->m_toko_kode,
							"data"=>array(
                                           $value->m_toko_kode,
                                           $value->m_toko_nama,
                                           $value->m_toko_ket
								         )
						    );
    	}

    	return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where("m_toko_kode",$value);

        $total=$this->db->count_all_results();

        return $total;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();
        
        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->m_toko_kode] = $row->m_toko_nama;
        }

        return $option;
    }
}
