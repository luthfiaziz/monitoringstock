<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 20/10/15
 * Time: 9:39
 */

class supplier_model extends CI_Model {
    private $_table1 = "m_suplier";

    public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("m_suplier_kode" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->from($this->_table1 . " a");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData(){
        $result = $this->data()->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->m_suplier_kode,
                "data"=>array(
                    $value->m_suplier_kode,
                    $value->m_suplier_nama,
                    $value->m_suplier_alamat,
                    $value->m_suplier_no_tlp,
                    $value->m_suplier_tgl_buat,
                    $value->m_suplier_petugas
                )
            );
        }

        return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where("m_suplier_kode",$value);

        $total=$this->db->count_all_results();

        return $total;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->m_suplier_kode] = $row->m_suplier_nama;
        }

        return $option;
    }

    public function getMaxData(){
        $sql    = $this->db->query("SELECT MAX(RIGHT(m_suplier_kode,3)) as noMax FROM 1nt3x_m_suplier")->row();

        return $sql;
    }
}
