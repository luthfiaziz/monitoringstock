<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Luthfi Aziz Nugraha 
 * Create Date : 26/07/2014 - 00:43:05
 * Module Name : Menu Model ( For Template )
 */

class kabupaten_model extends CI_Model {
	private $_table1 = "kabupaten";
    private $_table2 = "provinsi";

     public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("kabupaten_kode" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->from($this->_table1 . " a");
        $this->db->join($this->_table2 . " b","b.provinsi_kode = a.provinsi_kode");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData(){
    	$result = $this->data()->get();	
    	$data   = array();

    	foreach ($result->result() as $value) {
			$data[] = array(
							"id" => $value->kabupaten_kode,
							"data"=>array(
                                           $value->kabupaten_kode,
										   $value->kabupaten_ket,
                                           $value->provinsi_nama,
                                           (($value->kabupaten_tgl_buat == '0000-00-00')? '-' : $value->kabupaten_tgl_buat),
										   (($value->kabupaten_tgl_ubah == '0000-00-00')? '-' : $value->kabupaten_tgl_ubah),
										   $value->kabupaten_petugas,
										   (($value->kabupaten_status == 1)? 'Aktif' : 'Tidak Aktif'),
								         )
						    );
    	}

    	return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where("kabupaten_kode",$value);

        $total=$this->db->count_all_results();

        return $total;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->kabupaten_kode] = $row->kabupaten_ket;
        }

        return $option;
    }
}
