<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_management
 *
 * @author Luthfi Aziz Nugraha
 */

class Kabupaten extends MX_Controller {
    // VARIABLE MODUL
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT kabupaten
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();
        
        // Set Module Location
        $this->_module = 'master/kabupaten';
        // Load Module Core
        $this->load->module('config/app_core');
        // Load Model
        $this->load->model('kabupaten_model','kabupaten');
        $this->load->model('provinsi_model','provinsi');

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX kabupaten
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];

        $data['load_form']              = base_url() . $this->_module . '/loadform';
        $data['main_toolbar_source']    = base_url() . $this->_module . '/loadMainToolbar/' . $menu_id;
        $data['data_source']            = base_url() . $this->_module . '/listdata';
        $data['aksesEdit']              = ($this->SESSION["otoritas_menu"][$menu_id]["role_access_edit"] == 1)? "TRUE" : "FALSE";
    	$this->load->view($this->_module . '/index',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR MAIN kabupaten
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadMainToolbar($id = ''){
        xml::xml_header();
        $xml  = '<toolbar>';

        if($this->SESSION["otoritas_menu"][$id]["role_access_add"] == 1){
            $xml .= '<item id="add" text="Tambah" type="button" img="add.png" imgdis="add.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_edit"] == 1){
            $xml .= '<item id="edit" text="Ubah" type="button" img="edit.png" imgdis="edit.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_delete"] == 1){
            $xml .= '<item id="delete" text="Hapus" type="button" img="delete.png" imgdis="delete.png"/>';
        }

        $xml .= '<item id="sep" type="separator"/>';
        $xml .= '<item id="refresh" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD DATA kabupaten
    //-------------------------------------------------------------------------------------------------------------------------------
    public function listdata(){
       $data   = $this->kabupaten->loadData();
       
       $result = array("rows" => $data);

       echo json_encode($result);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR FORM kabupaten
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadFormkabupatenToolbar(){
        xml::xml_header();

        $xml  = '<toolbar>';
        $xml .= '<item id="savekabupaten" text="Simpan" type="button" img="save.png" imgdis="save.png" action="simpan"/>';
        $xml .= '<item id="cancelkabupaten" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png" action="bersihkabupaten"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD FORM kabupaten
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadform($id = ''){
        $data["readonly"] = false;

        if (!empty($id)) {
            $data["id"]      = $id;
            $data["default"] = $this->kabupaten->data($id)->get()->row();
            $data["readonly"] = true; 
        }

        $data['form_action']        = base_url() . $this->_module . '/proses';
        $data['provinsi_options']   = $this->provinsi->options("--Pilih Provinsi--");

        $this->load->view($this->_module . '/form',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - SAVE DATA kabupaten
    //-------------------------------------------------------------------------------------------------------------------------------
    public function proses(){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;
        $id   = $this->input->post('id');
        
        if(empty($id))
            $this->form_validation->set_rules('kabupaten_kode', 'Kode kabupaten', 'trim|required|callback_ketersediaan_check');

        $this->form_validation->set_rules('kabupaten_ket', 'Nama kabupaten', 'trim|required');
        $this->form_validation->set_rules('provinsi_kode', 'Provinsi', 'trim|required');

        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', 'refresh()');

            $mkabupaten = array();
            $mkabupaten['kabupaten_ket']        = $this->input->post('kabupaten_ket');
            $mkabupaten['provinsi_kode']        = $this->input->post('provinsi_kode');
            $mkabupaten['kabupaten_petugas']    = $this->SESSION["user_username"];
            $mkabupaten['kabupaten_status']     = $this->input->post('kabupaten_status');

            if ($id == '') {
                $mkabupaten['kabupaten_kode']       = $this->input->post('kabupaten_kode');
                $mkabupaten['kabupaten_tgl_buat']   = date("Y-m-d");

                if ($crud->save_as_new('kabupaten', arrman::StripTagFilter($mkabupaten))) {
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh()');

                    $dataLog    = array(
                        'log_user'      => $this->SESSION['user_username'],
                        'log_kegiatan'  => "Simpan data kabupaten " . $this->input->post('kabupaten_kode'),
                        'log_waktu'     => date('Y-m-d H:i:s')
                    );
                    $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
                }
            } else {
                $mkabupaten['kabupaten_tgl_ubah'] = date("Y-m-d");

                $key = array("kabupaten_kode"=> $id);
                if ($crud->save('kabupaten', arrman::StripTagFilter($mkabupaten), $key)) {
                    $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh()');

                    $dataLog    = array(
                        'log_user'      => $this->SESSION['user_username'],
                        'log_kegiatan'  => "Rubah data kabupaten " . $id,
                        'log_waktu'     => date('Y-m-d H:i:s')
                    );
                    $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
                }
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors("<span class='error_mess'>","</span>"), "refresh()");
        }

        echo json_encode($message,true);
    }

    /*CUSTOM VALIDATION - cek id jika ada*/
    public function ketersediaan_check()
    {   
        $kabupaten_id = $this->input->post("kabupaten_kode");
        
        $ada = $this->kabupaten->hitungJikaAda($kabupaten_id);

        if ($ada > 0)
        {   
            $message = 'Kode <b><u>' . $kabupaten_id . '</u></b> untuk kabupaten <b>' . $this->input->post("kabupaten_ket") . '</b> sudah ada';
            $this->form_validation->set_message('ketersediaan_check', $message);
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - DELETE DATA kabupaten
    //-------------------------------------------------------------------------------------------------------------------------------
    public function delete() {
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        $key = array("kabupaten_kode"=> $this->input->post("id"));
        if ($crud->delete("kabupaten",$key)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '');

            $dataLog    = array(
                'log_user'      => $this->SESSION['user_username'],
                'log_kegiatan'  => "Hapus data kabupaten " . $this->input->post('id'),
                'log_waktu'     => date('Y-m-d H:i:s')
            );
            $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
        }

        echo json_encode($message);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - GET DATA kabupaten options
    //-------------------------------------------------------------------------------------------------------------------------------
    public function getKabupatenOptions($key = ''){
        $key = array("m_provinsi_id" => $this->input->post("m_provinsi_id"));
        $data = $this->kabupaten->options("--Pilih Kabupaten--",$key);

        echo json_encode($data);
    }
}