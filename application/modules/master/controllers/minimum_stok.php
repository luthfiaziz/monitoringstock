<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class minimum_stok extends MX_Controller {
    // VARIABLE MODUL
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT minimum_stok
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();
        
        // Set Module Location
        $this->_module = 'master/minimum_stok';
        // Load Module Core
        $this->load->module('config/app_core');
        // Load Model
        $this->load->model('minimum_stok_model','minimum_stok');
        $this->load->model('list_model','item');

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX minimum_stok
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];

        $data['load_form']              = base_url() . $this->_module . '/loadform';
        $data['main_toolbar_source']    = base_url() . $this->_module . '/loadMainToolbar/' . $menu_id;
        $data['data_source']            = base_url() . $this->_module . '/listdata';
        $data['aksesEdit']              = ($this->SESSION["otoritas_menu"][$menu_id]["role_access_edit"] == 1)? "TRUE" : "FALSE";
    	$this->load->view($this->_module . '/index',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR MAIN minimum_stok
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadMainToolbar($id = ''){
        xml::xml_header();
        $xml  = '<toolbar>';

        if($this->SESSION["otoritas_menu"][$id]["role_access_add"] == 1){
            $xml .= '<item id="add" text="Tambah" type="button" img="add.png" imgdis="add.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_edit"] == 1){
            $xml .= '<item id="edit" text="Ubah" type="button" img="edit.png" imgdis="edit.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_delete"] == 1){
            $xml .= '<item id="delete" text="Hapus" type="button" img="delete.png" imgdis="delete.png"/>';
        }

        $xml .= '<item id="sep" type="separator"/>';
        $xml .= '<item id="refresh" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD DATA minimum_stok
    //-------------------------------------------------------------------------------------------------------------------------------
    public function listdata(){
       $data   = $this->minimum_stok->loadData();
       
       $result = array("rows" => $data);

       echo json_encode($result);
    }

    public function loadformCari($kode ='', $nama =''){
        $nama_rep = str_replace("%20", " ", $nama);
        $key2 = "";
        if(!empty($kode) && $nama == "-"){
            $key2 = array("unit_id" => $kode);

        }else if($kode == "-" && !empty($nama)){
            $key2 = array("unit_name" => $nama_rep);

        }else if(!empty($kode) && !empty($nama)){
            $key2 = array("unit_id" => $kode, "unit_name" => $nama_rep);
        }
        

        $data   = $this->minimum_stok->loadData($key2);
        /*echo $this->db->last_query(); exit();*/
        $result = array("rows" => $data);

        echo json_encode($result);
    }

    public function FormCari(){

        $this->load->view($this->_module . '/FormCariminimum_stok');
    }

     public function ToolbarLoadFormCari(){
        xml::xml_header();

        $xml  = '<toolbar>';
        $xml .= "</toolbar>";

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR FORM minimum_stok
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadFormminimum_stokToolbar(){
        xml::xml_header();

        $xml  = '<toolbar>';
        $xml .= '<item id="saveminimum_stok" text="Simpan" type="button" img="save.png" imgdis="save.png" action="simpan"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD FORM minimum_stok
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadform($id = ''){
        $data["readonly"] = false;
        
        if (!empty($id)) {
            $data["id"]       = $id;
            $data["default"]  = $this->minimum_stok->data($id)->get()->row();
            $data["readonly"] = true;
        }
        
        $data['list_data']   = $this->item->options("-- Pilih Item --");
        $data['form_action'] = base_url() . $this->_module . '/proses';
        $this->load->view($this->_module . '/form',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - SAVE DATA minimum_stok
    //-------------------------------------------------------------------------------------------------------------------------------
    public function proses(){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;
        $id   = $this->input->post('id');

        if(empty($id)){
            $this->form_validation->set_rules('m_item_kode', 'Nama Item', 'trim|required|callback_ketersediaan_check');
        }else{
            $this->form_validation->set_rules('m_item_kode', 'Nama Item', 'trim|required');
        }
        
        $this->form_validation->set_rules('m_stok_min', 'Nama Item', 'trim|required');

        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', 'refreshminimum_stok()');

            $mminimum_stok = array();
            $mminimum_stok['m_item_kode']           = $this->input->post('m_item_kode');
            $mminimum_stok['m_stok_min']            = $this->input->post('m_stok_min');


            if ($id == '') {
                $this->db->trans_begin();
                $crud->save_as_new('m_stok_min', arrman::StripTagFilter($mminimum_stok));

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message = array(false, 'Proses Gagal', 'Proses penyimpanan data gagal.', 'refreshminimum_stok()');
                } else {
                    $this->db->trans_commit();
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refreshminimum_stok()');
                }

            } else {
                $key = array("m_item_kode"=> $id);
                $this->db->trans_begin();
                $crud->save('m_stok_min', arrman::StripTagFilter($mminimum_stok), $key);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message = array(false, 'Proses Gagal', 'Proses pengeditan data gagal.', 'refreshminimum_stok()');
                } else {
                    $this->db->trans_commit();
                    $message = array(true, 'Proses Berhasil', 'Proses pengeditan data berhasil.', 'refreshminimum_stok()');
                }
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors("<span class='error_mess'>","</span>"), "refreshminimum_stok()");
        }

        echo json_encode($message,true);
    }

    /*CUSTOM VALIDATION - cek id jika ada*/
    public function ketersediaan_check()
    {
        $unit_name = $this->input->post("m_item_kode");

        $ada = $this->minimum_stok->hitungJikaAda($unit_name);

        if ($ada > 0)
        {
            $this->form_validation->set_message('ketersediaan_check', 'Nama minimum_stok <b><u>' . $unit_name . '</u></b> yang anda inputkan sudah ada');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - DELETE DATA minimum_stok
    //-------------------------------------------------------------------------------------------------------------------------------
    public function delete() {
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        $key = array("m_item_kode"=> $this->input->post("id"));
        if ($crud->delete("m_stok_min",$key)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '');
        }

        echo json_encode($message);
    }

}

?>