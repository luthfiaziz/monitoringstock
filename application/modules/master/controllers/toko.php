<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_management
 *
 * @author Vredy Wijaya
 */

class toko extends MX_Controller {
    // VARIABLE MODUL
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT toko
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();
        
        // Set Module Location
        $this->_module = 'master/toko';
        // Load Module Core
        $this->load->module('config/app_core');
        // Load Model
        $this->load->model('toko_model','toko');

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX toko
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];

        $data['load_form']              = base_url() . $this->_module . '/loadform';
        $data['main_toolbar_source']    = base_url() . $this->_module . '/loadMainToolbar/' . $menu_id;
        $data['data_source']            = base_url() . $this->_module . '/listdata';
        $data['aksesEdit']              = ($this->SESSION["otoritas_menu"][$menu_id]["role_access_edit"] == 1)? "TRUE" : "FALSE";
    	$this->load->view($this->_module . '/index',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR MAIN toko
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadMainToolbar($id = ''){
        xml::xml_header();
        $xml  = '<toolbar>';

        if($this->SESSION["otoritas_menu"][$id]["role_access_add"] == 1){
            $xml .= '<item id="add" text="Tambah" type="button" img="add.png" imgdis="add.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_edit"] == 1){
            $xml .= '<item id="edit" text="Ubah" type="button" img="edit.png" imgdis="edit.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_delete"] == 1){
            $xml .= '<item id="delete" text="Hapus" type="button" img="delete.png" imgdis="delete.png"/>';
        }

        $xml .= '<item id="sep" type="separator"/>';
        $xml .= '<item id="refresh" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD DATA toko
    //-------------------------------------------------------------------------------------------------------------------------------
    public function listdata(){
       $data   = $this->toko->loadData();
       
       $result = array("rows" => $data);

       echo json_encode($result);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR FORM toko
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadFormtokoToolbar(){
        xml::xml_header();

        $xml  = '<toolbar>';
        $xml .= '<item id="savetoko" text="Simpan" type="button" img="save.png" imgdis="save.png" action="simpan"/>';
        $xml .= '<item id="canceltoko" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png" action="bersihtoko"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD FORM toko
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadform($id = ''){
        $data["readonly"] = false;
        
        if (!empty($id)) {
            $data["id"]       = $id;
            $data["default"]  = $this->toko->data($id)->get()->row();
            $data["readonly"] = true; 
        }

        $data['form_action'] = base_url() . $this->_module . '/proses';
        $this->load->view($this->_module . '/form',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - SAVE DATA toko
    //-------------------------------------------------------------------------------------------------------------------------------
    public function proses(){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;
        $id   = $this->input->post('id');

       // $this->form_validation->set_rules('m_toko_kode', 'Kode Toko', 'trim|required');
        if(empty($id)) 
            $this->form_validation->set_rules('m_toko_kode', 'Kode Toko', 'trim|required|callback_ketersediaan_check');
     
            $this->form_validation->set_rules('m_toko_nama', 'Nama Toko', 'trim|required');

        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', 'refresh()');

            $m_toko = array();
            $m_toko['m_toko_nama']     = $this->input->post('m_toko_nama');
            $m_toko['m_toko_petugas']  = $this->SESSION["user_username"];
            $m_toko['m_toko_ket']      = $this->input->post('m_toko_ket');

            if ($id == '') {
                $m_toko['m_toko_kode']     = $this->input->post('m_toko_kode');
                $m_toko['m_toko_tgl_buat'] = date("Y-m-d");

                if ($crud->save_as_new('m_toko', arrman::StripTagFilter($m_toko))) {
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh()');

                    $dataLog    = array(
                        'log_user'      => $this->SESSION['user_username'],
                        'log_kegiatan'  => "Simpan data toko " . $this->input->post('m_toko_kode'),
                        'log_waktu'     => date('Y-m-d H:i:s')
                    );
                    $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
                }
            } else {
                $m_toko['m_toko_tgl_ubah'] = date("Y-m-d");

                $key = array("m_toko_kode"=> $id);
                if ($crud->save('m_toko', arrman::StripTagFilter($m_toko), $key)) {
                    $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh()');

                    $dataLog    = array(
                        'log_user'      => $this->SESSION['user_username'],
                        'log_kegiatan'  => "Rubah data toko " . $id,
                        'log_waktu'     => date('Y-m-d H:i:s')
                    );
                    $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
                }
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors("<span class='error_mess'>","</span>"), "refresh()");
        }

        echo json_encode($message,true);
    }

    /*CUSTOM VALIDATION - cek id jika ada*/
    public function ketersediaan_check()
    {
        $m_toko_kode = $this->input->post("m_toko_kode");

        $ada = $this->toko->hitungJikaAda($m_toko_kode);

        if ($ada > 0)
        {
            $this->form_validation->set_message('ketersediaan_check', 'Kode Toko <b><u>' . $m_toko_kode . '</u></b> yang anda inputkan sudah ada');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - DELETE DATA toko
    //-------------------------------------------------------------------------------------------------------------------------------
    public function delete() {
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        $key = array("m_toko_kode"=> $this->input->post("id"));
        if ($crud->delete("m_toko",$key)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '');

            $dataLog    = array(
                'log_user'      => $this->SESSION['user_username'],
                'log_kegiatan'  => "Hapus data toko " . $this->input->post('id'),
                'log_waktu'     => date('Y-m-d H:i:s')
            );
            $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
        }

        echo json_encode($message);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - GET DATA toko options
    //-------------------------------------------------------------------------------------------------------------------------------
    public function gettokoOptions(){
        
    }
}

?>