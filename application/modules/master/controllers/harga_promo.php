<?php
/**
 * User: Bayu Nugraha (bayu.nugraha@intex.co.id)
 * Date: 08/10/15
 * Time: 15:53
 */
class Harga_promo extends MX_Controller{
    // VARIABLE MODUL
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT penjualan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();

        // Set Module Location
        $this->_module = 'master/harga_promo';
        // Load Module Core
        $this->load->module("auth/app_auth");
        $this->load->module("config/app_setting");
        $this->load->module("crud/app_crud");
        // Load Model
        $this->load->model('harga_promo_model','promo');

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }


    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];

        $data['load_form']              = base_url() . $this->_module . '/loadform';
        $data['main_toolbar_source']    = base_url() . $this->_module . '/loadMainToolbar/' . $menu_id;
        $data['data_source']            = base_url() . $this->_module . '/listdata';
        $data['aksesEdit']              = ($this->SESSION["otoritas_menu"][$menu_id]["role_access_edit"] == 1)? "TRUE" : "FALSE";
        $this->load->view($this->_module . '/index',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR MAIN provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadMainToolbar($id = ''){
        xml::xml_header();
        $xml  = '<toolbar>';

        if($this->SESSION["otoritas_menu"][$id]["role_access_edit"] == 1){
            $xml .= '<item id="edit" text="Ubah" type="button" img="edit.png" imgdis="edit.png"/>';
        }

        $xml .= '<item id="sep" type="separator"/>';
        $xml .= '<item id="refresh" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png"/>';
        $xml .= "</toolbar>";

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD DATA provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function listdata(){
        $data   = $this->promo->loadData();

        $result = array("rows" => $data);

        echo json_encode($result);
    }

    public function loadDataToko($kode_item = ""){
        $data   = $this->promo->loadDataToko($kode_item);

        $result = array("rows" => $data);

        echo json_encode($result);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR FORM provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadFormToolbar(){
        xml::xml_header();

        $xml  = '<toolbar>';
        $xml .= '<item id="saveForm" text="Simpan" type="button" img="save.png" imgdis="save.png" action="simpan"/>';
        $xml .= "</toolbar>";

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD FORM provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadform($id = ''){
        $data["readonly"]       = false;
        $data['form_action']    = base_url() . $this->_module . '/proses';

        if (!empty($id)) {
            $data["default"]    = $this->promo->data($id)->get()->row();
            $data["readonly"]   = true;
        }

        $this->load->view($this->_module . '/form',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - SAVE DATA provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function proses($id = ""){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;
        $dataIns                            = $this->input->post('data');
        $dataIns['m_harga_promo_petugas']   = $this->SESSION['user_username'];

        $this->db->trans_begin();
        $cek    = $this->ketersediaan_check();
        if ($cek == "1") {
            $noTrx  = $this->generateNo();
            $dataIns['m_harga_promo_id']        = $noTrx;
            $dataIns['m_item_kode']             = $this->input->post('m_item_kode');
            $dataIns['m_harga_promo_tgl_buat']  = date("Y-m-d");

            $crud->save_as_new_nrb('m_harga_promo', arrman::StripTagFilter($dataIns));

            $dataLog    = array(
                'log_user'      => $this->SESSION['user_username'],
                'log_kegiatan'  => "Simpan data harga promo " . $noTrx,
                'log_waktu'     => date('Y-m-d H:i:s')
            );
            $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
        } else {
            $noTrx  = $this->promo->data($this->input->post('m_item_kode'))->get()->row()->m_harga_promo_id;
            $dataIns['m_harga_promo_tgl_ubah']  = date("Y-m-d");

            $key = array("m_harga_promo_id"=> $noTrx);
            $crud->save_nrb('m_harga_promo', arrman::StripTagFilter($dataIns), $key);

            $dataLog    = array(
                'log_user'      => $this->SESSION['user_username'],
                'log_kegiatan'  => "Rubah data harga promo " . $noTrx,
                'log_waktu'     => date('Y-m-d H:i:s')
            );
            $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
        }

        $crud->delete_nrb("m_harga_promo_detail",array("m_harga_promo_id"=> $noTrx));
        $dataGrid                           = $this->input->post('dataGrid');
        foreach($dataGrid as $val){
            if($val[2] == '1'){
                $dataInsD   = array(
                    'm_harga_promo_id'  => $noTrx,
                    'm_toko_kode'       => $val[0]
                );
                $crud->save_as_new_nrb('m_harga_promo_detail', arrman::StripTagFilter($dataInsD));
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $message = array(true, 'Proses Gagal', 'Proses penyimpanan data gagal ( data di rollback ).', '');
            $this->db->trans_rollback();
        } else {
            $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh()');
            $this->db->trans_commit();
        }

        echo json_encode($message,true);
    }

    /*CUSTOM VALIDATION - cek id jika ada*/
    public function ketersediaan_check(){
        $provinsi_id = $this->input->post("m_item_kode");
        $ada = $this->promo->hitungJikaAda($provinsi_id);

        if ($ada > 0){
            return "0";
        }else{
            return "1";
        }
    }

    //GENERATE NO
    public function generateNo(){
        $dataMax    = $this->promo->getMaxData();
        $noNow      = $dataMax->noMax + 1;

        return $noNow;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - DELETE DATA provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function delete() {
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        $key = array("m_item_kode"=> $this->input->post("id"));
        if ($crud->delete("m_item",$key)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '');

            $dataLog    = array(
                'log_user'      => $this->SESSION['user_username'],
                'log_kegiatan'  => "Hapus data harga promo " . $this->input->post('id'),
                'log_waktu'     => date('Y-m-d H:i:s')
            );
            $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
        }

        echo json_encode($message);
    }
}