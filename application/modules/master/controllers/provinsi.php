<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_management
 *
 * @author Luthfi Aziz Nugraha
 */

class Provinsi extends MX_Controller {
    // VARIABLE MODUL
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();
        
        // Set Module Location
        $this->_module = 'master/provinsi';
        // Load Module Core
        $this->load->module('config/app_core');
        // Load Model
        $this->load->model('provinsi_model','provinsi');

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];

        $data['load_form']              = base_url() . $this->_module . '/loadform';
        $data['main_toolbar_source']    = base_url() . $this->_module . '/loadMainToolbar/' . $menu_id;
        $data['data_source']            = base_url() . $this->_module . '/listdata';
        $data['aksesEdit']              = ($this->SESSION["otoritas_menu"][$menu_id]["role_access_edit"] == 1)? "TRUE" : "FALSE";
    	$this->load->view($this->_module . '/index',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR MAIN provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadMainToolbar($id = ''){
        xml::xml_header();
        $xml  = '<toolbar>';

        if($this->SESSION["otoritas_menu"][$id]["role_access_add"] == 1){
            $xml .= '<item id="add" text="Tambah" type="button" img="add.png" imgdis="add.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_edit"] == 1){
            $xml .= '<item id="edit" text="Ubah" type="button" img="edit.png" imgdis="edit.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_delete"] == 1){
            $xml .= '<item id="delete" text="Hapus" type="button" img="delete.png" imgdis="delete.png"/>';
        }

        $xml .= '<item id="sep" type="separator"/>';
        $xml .= '<item id="refresh" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD DATA provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function listdata(){
       $data   = $this->provinsi->loadData();
       
       $result = array("rows" => $data);

       echo json_encode($result);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR FORM provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadFormprovinsiToolbar(){
        xml::xml_header();

        $xml  = '<toolbar>';
        $xml .= '<item id="saveprovinsi" text="Simpan" type="button" img="save.png" imgdis="save.png" action="simpan"/>';
        $xml .= '<item id="cancelprovinsi" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png" action="bersihprovinsi"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD FORM provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadform($id = ''){
        $data["readonly"] = false;
        
        if (!empty($id)) {
            $data["id"]       = $id;
            $data["default"]  = $this->provinsi->data($id)->get()->row();
            $data["readonly"] = true; 
        }

        $data['form_action'] = base_url() . $this->_module . '/proses';
        $this->load->view($this->_module . '/form',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - SAVE DATA provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function proses(){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;
        $id   = $this->input->post('id');

        $this->form_validation->set_rules('provinsi_kode', 'Kode Provinsi', 'trim|required');
        if(empty($id)) 
            $this->form_validation->set_rules('provinsi_kode', 'Kode Provinsi', 'trim|required|callback_ketersediaan_check');
     
        $this->form_validation->set_rules('provinsi_nama', 'Nama Provinsi', 'trim|required');

        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', 'refresh()');

            $mprovinsi = array();
            $mprovinsi['provinsi_nama']     = $this->input->post('provinsi_nama');
            $mprovinsi['provinsi_petugas']  = $this->SESSION["user_username"];
            $mprovinsi['provinsi_status']   = $this->input->post('provinsi_status');

            if ($id == '') {
                $mprovinsi['provinsi_kode']     = $this->input->post('provinsi_kode');
                $mprovinsi['provinsi_tgl_buat'] = date("Y-m-d");

                if ($crud->save_as_new('provinsi', arrman::StripTagFilter($mprovinsi))) {
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh()');

                    $dataLog    = array(
                        'log_user'      => $this->SESSION['user_username'],
                        'log_kegiatan'  => "Simpan data provinsi " . $this->input->post('provinsi_kode'),
                        'log_waktu'     => date('Y-m-d H:i:s')
                    );
                    $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
                }
            } else {
                $mprovinsi['provinsi_tgl_ubah'] = date("Y-m-d");

                $key = array("provinsi_kode"=> $id);
                if ($crud->save('provinsi', arrman::StripTagFilter($mprovinsi), $key)) {
                    $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh()');

                    $dataLog    = array(
                        'log_user'      => $this->SESSION['user_username'],
                        'log_kegiatan'  => "Rubah data provinsi " . $id,
                        'log_waktu'     => date('Y-m-d H:i:s')
                    );
                    $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
                }
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors("<span class='error_mess'>","</span>"), "refresh()");
        }

        echo json_encode($message,true);
    }

    /*CUSTOM VALIDATION - cek id jika ada*/
    public function ketersediaan_check()
    {
        $provinsi_id = $this->input->post("provinsi_kode");

        $ada = $this->provinsi->hitungJikaAda($provinsi_id);

        if ($ada > 0)
        {
            $this->form_validation->set_message('ketersediaan_check', 'Id <b><u>' . $provinsi_id . '</u></b> yang anda inputkan sudah ada');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - DELETE DATA provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function delete() {
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        $key = array("provinsi_kode"=> $this->input->post("id"));
        if ($crud->delete("provinsi",$key)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '');

            $dataLog    = array(
                'log_user'      => $this->SESSION['user_username'],
                'log_kegiatan'  => "Hapus data provinsi " . $this->input->post('id'),
                'log_waktu'     => date('Y-m-d H:i:s')
            );
            $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
        }

        echo json_encode($message);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - GET DATA provinsi options
    //-------------------------------------------------------------------------------------------------------------------------------
    public function getProvinsiOptions(){
        
    }
}

?>