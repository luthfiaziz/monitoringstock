<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 25/09/15
 * Time: 11:00
 */

class List_item extends MX_Controller {
    // VARIABLE MODUL
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();

        // Set Module Location
        $this->_module = 'master/list';
        // Load Module Core
        $this->load->module('config/app_core');
        // Load Model
        $this->load->model('list_model','list');
        $this->load->model('tipe_model','tipe');
        $this->load->model('merek_model','merk');
        $this->load->model('jenis_model','jenis');
        $this->load->model('kategori_model','kategori');
        $this->load->model('satuan_model','satuan');
        $this->load->model('rak_model','rak');
        $this->load->model('toko_model','toko');

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];

        $data['load_form']              = base_url() . $this->_module . '_item/loadform';
        $data['main_toolbar_source']    = base_url() . $this->_module . '_item/loadMainToolbar/' . $menu_id;
        $data['data_source']            = base_url() . $this->_module . '_item/listdata';
        $data['aksesEdit']              = ($this->SESSION["otoritas_menu"][$menu_id]["role_access_edit"] == 1)? "TRUE" : "FALSE";
        $this->load->view($this->_module . '/index',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR MAIN provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadMainToolbar($id = ''){
        xml::xml_header();
        $xml  = '<toolbar>';

        if($this->SESSION["otoritas_menu"][$id]["role_access_add"] == 1){
            $xml .= '<item id="add" text="Tambah" type="button" img="add.png" imgdis="add.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_edit"] == 1){
            $xml .= '<item id="edit" text="Ubah" type="button" img="edit.png" imgdis="edit.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_delete"] == 1){
            $xml .= '<item id="delete" text="Hapus" type="button" img="delete.png" imgdis="delete.png"/>';
        }

        $xml .= '<item id="sep" type="separator"/>';
        $xml .= '<item id="refresh" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png"/>';
        $xml .= "</toolbar>";

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD DATA provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function listdata($key   = ""){
        if($key != ""){
            $key    = array('a.m_item_kategori_kode' => $key);
        }
        $data   = $this->list->loadData($key);

        $result = array("rows" => $data);

        echo json_encode($result);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR FORM provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadFormToolbar(){
        xml::xml_header();

        $xml  = '<toolbar>';
        $xml .= '<item id="saveForm" text="Simpan" type="button" img="save.png" imgdis="save.png" action="simpan"/>';
        $xml .= "</toolbar>";

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD FORM provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadform($id = ''){
        $data["readonly"]       = false;
        $data['form_action']    = base_url() . $this->_module . '_item/proses/0';
        $data['kategori']       = $this->kategori->options("--Pillih Kategori--");
        $data['jenis']          = array("" => "--Pillih Jenis--");
        $data['merk']           = array("" => "--Pillih Merk--");
        $data['tipe']           = array("" => "--Pillih Tipe--");
        $data['satuan']         = $this->satuan->options("--Pillih Satuan--");
        $data['rak']            = $this->rak->options("--Pillih Rak--");
        $data['toko']           = $this->toko->options("--Pillih Toko--");
        $data['jenis_source']   = base_url() . $this->_module . "_item/optionJenis";
        $data['merk_source']    = base_url() . $this->_module . "_item/optionMerk";
        $data['tipe_source']    = base_url() . $this->_module . "_item/optionTipe";

        if (!empty($id)) {
            $data['form_action'] = base_url() . $this->_module . '_item/proses/1';
            $data["default"]    = $this->list->data($id)->get()->row();
            $data["readonly"]   = true;
            $data['jenis']      = $this->jenis->options("--Pillih Jenis--");
            $data['merk']       = $this->merk->options("--Pillih Merk--");
            $data['tipe']       = $this->tipe->options("--Pillih Merk--");
        }

        $this->load->view($this->_module . '/form',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - SAVE DATA provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function proses($id){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        if($id=='0')
            $this->form_validation->set_rules('m_item_kode', 'Kode Item', 'trim|required|callback_ketersediaan_check');

        $this->form_validation->set_rules('data[m_item_nama]', 'Nama tipe', 'trim|required');
        $this->form_validation->set_rules('data[m_item_kategori_kode]', 'Kategori', 'trim|required');
        $this->form_validation->set_rules('data[m_item_jenis_kode]', 'Jenis', 'trim|required');
        $this->form_validation->set_rules('data[m_item_merk_kode]', 'Merk', 'trim|required');
//        $this->form_validation->set_rules('data[m_item_tipe_kode]', 'Tipe', 'trim|required');
        $this->form_validation->set_rules('data[m_satuan_id]', 'Satuan', 'trim|required');
//        $this->form_validation->set_rules('data[m_rak_kode]', 'Rak', 'trim|required');
//        $this->form_validation->set_rules('data[m_toko_kode]', 'Toko', 'trim|required');
//        $this->form_validation->set_rules('data[m_item_harga]', 'Harga jual', 'trim|required|numeric');

        if ($this->form_validation->run($this)) {

            $dataIns                   = $this->input->post('data');
            $dataIns['m_item_petugas'] = $this->SESSION['user_username'];

            $this->db->trans_begin();
            if ($id == '0') {
                $dataIns['m_item_kode']     = $this->input->post('m_item_kode');
                $dataIns['m_item_tgl_buat'] = date("Y-m-d");

                $crud->save_as_new_nrb('m_item', arrman::StripTagFilter($dataIns));

                $dataLog    = array(
                    'log_user'      => $this->SESSION['user_username'],
                    'log_kegiatan'  => "Simpan data item " . $this->input->post('m_item_kode'),
                    'log_waktu'     => date('Y-m-d H:i:s')
                );
                $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
            } else {
                $dataIns['m_item_tgl_ubah'] = date("Y-m-d");

                $key = array("m_item_kode"=> $this->input->post('m_item_kode'));
                $crud->save_nrb('m_item', arrman::StripTagFilter($dataIns), $key);

                $dataLog    = array(
                    'log_user'      => $this->SESSION['user_username'],
                    'log_kegiatan'  => "Rubah data item " . $this->input->post('m_item_kode'),
                    'log_waktu'     => date('Y-m-d H:i:s')
                );
                $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
            }

            if ($this->db->trans_status() === FALSE) {
                $message = array(true, 'Proses Gagal', 'Proses penyimpanan data gagal ( data di rollback ).', '');
                $this->db->trans_rollback();
            } else {
                $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh()');
                $this->db->trans_commit();
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors("<span class='error_mess'>","</span>"), "refresh()");
        }

        echo json_encode($message,true);
    }

    /*CUSTOM VALIDATION - cek id jika ada*/
    public function ketersediaan_check()
    {
        $provinsi_id = $this->input->post("m_item_kode");

        $ada = $this->list->hitungJikaAda($provinsi_id);

        if ($ada > 0)
        {
            $this->form_validation->set_message('ketersediaan_check', 'Id <b><u>' . $provinsi_id . '</u></b> yang anda inputkan sudah ada');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - DELETE DATA provinsi
    //-------------------------------------------------------------------------------------------------------------------------------
    public function delete() {
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        $key = array("m_item_kode"=> $this->input->post("id"));
        if ($crud->delete("m_item",$key)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '');

            $dataLog    = array(
                'log_user'      => $this->SESSION['user_username'],
                'log_kegiatan'  => "Hapus data item" . $this->input->post('id'),
                'log_waktu'     => date('Y-m-d H:i:s')
            );
            $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
        }

        echo json_encode($message);
    }

    // OPTION jenis
    public function optionJenis(){
        $key    = array("m_item_kategori_kode"=> $this->input->post('kat_kode'));
        $data = $this->jenis->options("--Pillih Jenis--",$key);

        echo json_encode($data);
    }

    // OPTION merk
    public function optionMerk(){
        $key    = array("m_item_jenis_kode"=> $this->input->post('jenis_kode'));
        $data = $this->merk->options("--Pillih Merk--",$key);

        echo json_encode($data);
    }

    // OPTION merk
    public function optionTipe(){
        $key    = array("m_item_merk_kode"=> $this->input->post('merk_kode'));
        $data = $this->tipe->options("--Pillih Tipe--",$key);

        echo json_encode($data);
    }
}