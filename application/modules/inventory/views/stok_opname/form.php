<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 26/10/15
 * Time: 14:03
 */
?>
<div id="stok_opnameloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <?php
    echo form_open_multipart($form_action, array('id' => 'fm_stok_opname'));
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span class="title_app"><b>No Stok Opname</b> <i></i></span>
                <?php echo form_input('stok_opname_no', !empty($default->stok_opname_no) ? $default->stok_opname_no : '', 'id="stok_opname_no" maxlength="15" class="form_web" placeholder="Auto" readonly');?>
            </td>
            <td>
                <span class="title_app"><b>Tanggal Opname</b> <i>*</i></span>
                <?php echo form_input('data[stok_opname_tgl]',!empty($default->stok_opname_tgl) ? $default->stok_opname_tgl : date('Y-m-d'), 'id="stok_opname_tgl" class="form_web" readonly'); ?>
            </td>
            <td>
                <span class="title_app"><b>Bulan</b> <i>*</i></span>
                <?php
                $disabled   = !empty($default->stok_opname_no) ? "readonly":'';
                echo form_dropdown('data[stok_opname_bulan]',$bulan, !empty($default->stok_opname_bulan) ? $default->stok_opname_bulan : '', 'id="stok_opname_bulan" class="full" ' . $disabled); ?>
            </td>
            <td>
                <span class="title_app"><b>Tahun</b> <i>*</i></span>
                <?php echo form_dropdown('data[stok_opname_tahun]',$tahun, !empty($default->stok_opname_tahun) ? $default->stok_opname_tahun : '', 'id="stok_opname_tahun" class="full" ' . $disabled); ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <span class="title_app"><b>Keterangan</b> <i></i></span>
                <?php echo form_input('data[stok_opname_ket]',!empty($default->stok_opname_ket) ? $default->stok_opname_ket : '', 'id="stok_opname_ket" class="form_web"'); ?>
            </td>
            <td>
                <span class="title_app"><b>&nbsp;</b> <i></i></span>
                <input type="button" class="form_web" value="View Opname" onclick="viewOpname()">
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div style="width: 100%; height: 320px;" id="gridSOForm"></div>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <?php echo arrman::getRequired();?>
            </td>
        </tr>

    </table>
    <?php
    echo form_close();
    ?>
</div>
<script type="text/javascript">
    $("#stok_opname_bulan").select2();
    $("#stok_opname_tahun").select2();

    dhxCalPo    = new dhtmlxCalendarObject('stok_opname_tgl');

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridstok_opnameForm = new dhtmlXGridObject('gridSOForm');
    gridstok_opnameForm.setImagePath(url + "assets/dhtmlx/imgs/");
    gridstok_opnameForm.setHeader("Kode Item ,Nama Item, Stok Buku,Stok Fisik");
    gridstok_opnameForm.setColAlign("left,left,center,center");
    gridstok_opnameForm.setColTypes("ro,ro,ro,ed");
    gridstok_opnameForm.setInitWidths("90,*,70,70");
    gridstok_opnameForm.init();
    gridstok_opnameForm.enableDistributedParsing(true,10,300);
    gridstok_opnameForm.attachEvent("onEditCell", onCellEditSOForm);

    function onCellEditSOForm(stage, rowId, cellInd, nValue, oValue){
        if (stage == 2) {
            if(cellInd == 3) {
                if(isNaN(nValue)){
                    dhtmlx.alert("Isi dengan angka");
                    return false;
                }
            }
            return true;
        }
    }

    <?php
    if(!empty($default->stok_opname_no)){
        $id = explode('/',$default->stok_opname_no);
        $id = $id[0] . '-' . $id[1] . '-' . $id[2];
        echo 'gridstok_opnameForm.load("' . base_url() . 'inventory/stok_opname/listdataDetail/' . $id . '","json");';
    }
    ?>

    function simpan(id){
        var toolbar = ["stok_opnameWindows","savestok_opname"];
        var data    = {
            datagrid    : $.csv.toArrays(gridstok_opnameForm.serializeToCSV())
        };

        SimpanDataTF('#fm_stok_opname','#stok_opnameloading',toolbar,'',data);
    }

    function refresh(){
        close_form_modal(stok_opnameWindows,"stok_opnameWindows");
        refreshGrid(gridstok_opname,url + "inventory/stok_opname/listdata","json");
    }

    function listStockOpname(){
        var module  = url + "inventory/stok_opname";
        var gridProp    = {
            header       : "Stock Opname No,Tgl Opname, Keterangan,Petugas",
            headeralign  : ["text-align:left","text-align:left","text-align:left","text-align:left"],
            attachheader : "#text_filter,#text_filter,#text_filter,#text_filter",
            filteralign  : ["text-align:left","text-align:left","text-align:left","text-align:left"],
            colalign     : "left,left,left,center",
            coltypes     : "ro,ro,ro,ro",
            sizes        : "*,*,*,100",
            footer       : "Total Records : ,{#stat_count},#cspan,#cspan",
            urltarget    : module + "/listStokOpname",
            fungsi       : [
                ["onRowDblClicked", addStockOpname]
            ],
            hiddencolumn : []
        };
        form_modal_grid(stok_opnameWindows,"listStockOpname",["List Stok Opname",600,450], "tanpatoolbar", gridProp);
    }

    function addStockOpname(rId){
        var module  = url + "inventory/stok_opname";
        $("#stok_opname_no_trx").val(rId);
        var no_so   = rId.split('/');
        no_so       = no_so[0] + '-' + no_so[1] + '-' + no_so[2];
        gridstok_opnameForm.clearAll();
        gridstok_opnameForm.load(module + "/listDataDetailStokOpname/" + no_so,"json");
        close_form_modal(stok_opnameWindows,"listStockOpname");
    }

    function viewOpname(){
        var bulan   = $("#stok_opname_bulan").val();
        var tahun   = $("#stok_opname_tahun").val();
        if(bulan == "" || tahun == ""){
            dhtmlx.alert("Bulan atau tahun tidak boleh kosong.");
            return;
        }
        $("#stok_opname_bulan").attr("readonly","readonly");
        $("#stok_opname_tahun").attr("readonly","readonly");
        gridstok_opnameForm.clearAll();
        gridstok_opnameForm.load(url + "inventory/stok_opname/listDataOpname/" + bulan + "/" + tahun,"json");
    }
</script>