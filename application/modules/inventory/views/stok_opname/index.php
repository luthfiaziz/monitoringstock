<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 26/10/15
 * Time: 14:03
 */
?>
<script type="text/javascript">
    var stok_opnameAkses    = "<?php echo $aksesEdit; ?>";
    var stok_opnameWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbarstok_opname = mainTab.cells("maintabbar").attachToolbar();
    toolbarstok_opname.setSkin("dhx_skyblue");
    toolbarstok_opname.setIconsPath(url + "assets/img/btn/");
    toolbarstok_opname.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarstok_opname.attachEvent("onClick", cstok_opname);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function cstok_opname(id){
        var module  = url + "inventory/stok_opname";
        var idRow   = gridstok_opname.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(stok_opnameWindows,"stok_opnameWindows",["Form Stok Opname",650,550], module + "/loadFormToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }
            var no_pa   = idRow.split('/');
            no_pa       = no_pa[0] + '-' + no_pa[1] + '-' + no_pa[2];
            form_modal(stok_opnameWindows,"stok_opnameWindows",["Form Stok Opname",650,550], module + "/loadFormToolbar", module + "/loadform/" + no_pa);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    };

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({
                                    type: "alert",
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridstok_opname, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridstok_opname,module + "/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridstok_opname = mainTab.cells("maintabbar").attachGrid();
    gridstok_opname.setImagePath(url + "assets/dhtmlx/imgs/");
    gridstok_opname.setHeader("No Stok Opname ,Tgl Transaksi,Keterangan, Tgl Buat, Petugas");
    gridstok_opname.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#combo_filter");
    gridstok_opname.setColAlign("left,center,left,center,center");
    gridstok_opname.setColTypes("ro,ro,ro,ro,ro");
    gridstok_opname.setInitWidths("200,150,*,250,150");
    gridstok_opname.init();
    gridstok_opname.enableDistributedParsing(true,10,300);
    gridstok_opname.load("<?php echo $data_source; ?>","json");

    if(stok_opnameAkses == "TRUE"){
        gridstok_opname.attachEvent("onRowDblClicked",gridstok_opnameDBLClick);
    }

    // [ACTION] GRID ACTION
    function gridstok_opnameDBLClick(){
        var module  = url + "inventory/stok_opname";
        var no_pa   = gridstok_opname.getSelectedRowId().split('/');
        no_pa       = no_pa[0] + '-' + no_pa[1] + '-' + no_pa[2];
        form_modal(stok_opnameWindows,"stok_opnameWindows",["Form Stok Opname",650,550], module + "/loadFormToolbar", module + "/loadform/" + no_pa);
    }
</script>