<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 23/10/15
 * Time: 10:30
 */
?>
<script type="text/javascript">
    var adjustment_stokAkses    = "<?php echo $aksesEdit; ?>";
    var adjustment_stokWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbaradjustment_stok = mainTab.cells("maintabbar").attachToolbar();
    toolbaradjustment_stok.setSkin("dhx_skyblue");
    toolbaradjustment_stok.setIconsPath(url + "assets/img/btn/");
    toolbaradjustment_stok.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbaradjustment_stok.attachEvent("onClick", cadjustment_stok);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function cadjustment_stok(id){
        var module  = url + "inventory/adjustment_stok";
        var idRow   = gridadjustment_stok.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(adjustment_stokWindows,"adjustment_stokWindows",["Form Adjustment Stok",800,550], module + "/loadFormToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }
            var no_pa   = idRow.split('/');
            no_pa       = no_pa[0] + '-' + no_pa[1] + '-' + no_pa[2];
            form_modal(adjustment_stokWindows,"adjustment_stokWindows",["Form Adjustment Stok",800,550], module + "/loadFormToolbar", module + "/loadform/" + no_pa);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    };

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({
                                    type: "alert",
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridadjustment_stok, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridadjustment_stok,module + "/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridadjustment_stok = mainTab.cells("maintabbar").attachGrid();
    gridadjustment_stok.setImagePath(url + "assets/dhtmlx/imgs/");
    gridadjustment_stok.setHeader("No Adjustment ,Tgl Transaksi,Keterangan, No Stok Opname, Tgl Buat, Petugas");
    gridadjustment_stok.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#combo_filter");
    gridadjustment_stok.setColAlign("left,center,left,left,center,center");
    gridadjustment_stok.setColTypes("ro,ro,ro,ro,ro,ro");
    gridadjustment_stok.setInitWidths("200,150,*,200,250,150");
    gridadjustment_stok.init();
    gridadjustment_stok.enableDistributedParsing(true,10,300);
    gridadjustment_stok.load("<?php echo $data_source; ?>","json");

    if(adjustment_stokAkses == "TRUE"){
        gridadjustment_stok.attachEvent("onRowDblClicked",gridadjustment_stokDBLClick);
    }

    // [ACTION] GRID ACTION
    function gridadjustment_stokDBLClick(){
        var module  = url + "inventory/adjustment_stok";
        var no_pa   = gridadjustment_stok.getSelectedRowId().split('/');
        no_pa       = no_pa[0] + '-' + no_pa[1] + '-' + no_pa[2];
        form_modal(adjustment_stokWindows,"adjustment_stokWindows",["Form Adjustment Stok",800,550], module + "/loadFormToolbar", module + "/loadform/" + no_pa);
    }
</script>