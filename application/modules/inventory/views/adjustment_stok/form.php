<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 23/10/15
 * Time: 10:31
 */
?>
<div id="adjustment_stokloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <?php
    echo form_open_multipart($form_action, array('id' => 'fm_adjustment_stok'));
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span class="title_app"><b>No Adjustment</b> <i></i></span>
                <?php echo form_input('adj_no_trx', !empty($default->adj_no_trx) ? $default->adj_no_trx : '', 'id="adj_no_trx" maxlength="15" class="form_web" placeholder="Auto" readonly');?>
            </td>
            <td>
                <span class="title_app"><b>Tanggal transaksi</b> <i>*</i></span>
                <?php echo form_input('data[adj_tgl]',!empty($default->adj_tgl) ? $default->adj_tgl : date('Y-m-d'), 'id="adj_tgl" class="form_web" readonly'); ?>
            </td>
            <td>
                <span class="title_app"><b>No Stok Opname</b> <i>*</i></span>
                <?php echo form_input('data[stok_opname_no_trx]',!empty($default->stok_opname_no_trx) ? $default->stok_opname_no_trx : '', 'id="stok_opname_no_trx" class="form_web" readonly'); ?>
            </td>
            <td>
                <span class="title_app"><b>&nbsp;</b> <i></i></span>
                <input type="button" value="..." class="form_web" onclick="listStockOpname()">
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <span class="title_app"><b>Keterangan</b> <i></i></span>
                <?php echo form_input('data[adj_ket]',!empty($default->adj_ket) ? $default->adj_ket : '', 'id="adj_ket" class="form_web"'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div style="width: 100%; height: 320px;" id="gridPOForm"></div>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <?php echo arrman::getRequired();?>
            </td>
        </tr>

    </table>
    <?php
    echo form_close();
    ?>
</div>
<script type="text/javascript">
    dhxCalPo    = new dhtmlxCalendarObject('adj_tgl');

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridadjustment_stokForm = new dhtmlXGridObject('gridPOForm');
    gridadjustment_stokForm.setImagePath(url + "assets/dhtmlx/imgs/");
    gridadjustment_stokForm.setHeader("Kode Item ,Nama Item, Stok Buku,Stok Fisik,Selisih, Adjustment ,Keterangan");
    gridadjustment_stokForm.setColAlign("left,left,center,center,center,center,left");
    gridadjustment_stokForm.setColTypes("ro,ro,ro,ro,ro,ed,ed");
    gridadjustment_stokForm.setInitWidths("90,170,70,70,70,70,*");
    gridadjustment_stokForm.init();
    gridadjustment_stokForm.enableDistributedParsing(true,10,300);
    gridadjustment_stokForm.attachEvent("onEditCell", onCellEditSOForm);

    function onCellEditSOForm(stage, rowId, cellInd, nValue, oValue){
        if (stage == 2) {
            if(cellInd == 5) {
                if(isNaN(nValue)){
                    dhtmlx.alert("Isi dengan angka");
                    return false;
                }
            }
            return true;
        }
    }

    <?php
    if(!empty($default->adj_no_trx)){
        $id = explode('/',$default->adj_no_trx);
        $id = $id[0] . '-' . $id[1] . '-' . $id[2];
        echo 'gridadjustment_stokForm.load("' . base_url() . 'inventory/adjustment_stok/listdataDetail/' . $id . '","json");';
    }
    ?>

    function simpan(id){
        var toolbar = ["adjustment_stokWindows","saveadjustment_stok"];
        var data    = {
            datagrid    : $.csv.toArrays(gridadjustment_stokForm.serializeToCSV())
        };

        SimpanDataTF('#fm_adjustment_stok','#adjustment_stokloading',toolbar,'',data);
    }

    function refresh(){
        close_form_modal(adjustment_stokWindows,"adjustment_stokWindows");
        refreshGrid(gridadjustment_stok,url + "inventory/adjustment_stok/listdata","json");
    }

    function listStockOpname(){
        var module  = url + "inventory/adjustment_stok";
        var gridProp    = {
            header       : "Stock Opname No,Tgl Opname, Keterangan,Petugas",
            headeralign  : ["text-align:left","text-align:left","text-align:left","text-align:left"],
            attachheader : "#text_filter,#text_filter,#text_filter,#text_filter",
            filteralign  : ["text-align:left","text-align:left","text-align:left","text-align:left"],
            colalign     : "left,left,left,center",
            coltypes     : "ro,ro,ro,ro",
            sizes        : "*,*,*,100",
            footer       : "Total Records : ,{#stat_count},#cspan,#cspan",
            urltarget    : module + "/listStokOpname",
            fungsi       : [
                ["onRowDblClicked", addStockOpname]
            ],
            hiddencolumn : []
        };
        form_modal_grid(adjustment_stokWindows,"listStockOpname",["List Stok Opname",600,450], "tanpatoolbar", gridProp);
    }

    function addStockOpname(rId){
        var module  = url + "inventory/adjustment_stok";
        $("#stok_opname_no_trx").val(rId);
        var no_so   = rId.split('/');
        no_so       = no_so[0] + '-' + no_so[1] + '-' + no_so[2];
        gridadjustment_stokForm.clearAll();
        gridadjustment_stokForm.load(module + "/listDataDetailStokOpname/" + no_so,"json");
        close_form_modal(adjustment_stokWindows,"listStockOpname");
    }
</script>