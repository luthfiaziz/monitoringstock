<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 03/11/15
 * Time: 11:30
 */
?>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <table width="100%">
        <tr>
            <td>
                <fieldset>
                    <legend>Periode</legend>
                    <table>
                        <tr>
                            <td>
                                <span class="title_app"><b>Bulan</b> <i></i></span>
                                <?php echo form_dropdown('bulan',$bulan,date('m'),'id="bulan" class="full"'); ?>
                            </td>
                            <td>
                                <span class="title_app"><b>Tahun</b> <i></i></span>
                                <?php echo form_dropdown('tahun',$tahun,date('Y'), 'id="tahun" class="full"'); ?>
                            </td>
                            <td>
                                <span class="title_app"><b> &nbsp; &nbsp; &nbsp; </b> <i></i></span>
                                <input type="button" class="form_web" value="Lihat" onclick="showMutasi()">
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <div style="width: 100%;height: 350px;" id="gridStokMutasi"></div>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    var stok_mutasiAkses    = "<?php echo $aksesEdit; ?>";
    var stok_mutasiWindows  = new dhtmlXWindows();

    $("#bulan").select2();
    $("#tahun").select2();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridstok_mutasi = new dhtmlXGridObject("gridStokMutasi");
    gridstok_mutasi.setImagePath(url + "assets/dhtmlx/imgs/");
    gridstok_mutasi.setHeader("Kode Barang ,Nama Barang, Stok");
    gridstok_mutasi.attachHeader("#text_filter,#text_filter,#text_filter");
    gridstok_mutasi.setColAlign("left,left,center");
    gridstok_mutasi.setColTypes("ro,ro,ron");
    gridstok_mutasi.setInitWidths("200,*,150");
    gridstok_mutasi.init();
    gridstok_mutasi.enableDistributedParsing(true,10,300);

    function showMutasi(){
        var bulan   = $("#bulan").val();
        var tahun   = $("#tahun").val();
        if(bulan == "" || tahun == ""){
            dhtmlx.alert("Bulan atau tahun tidak boleh kosong");
            return;
        }
        gridstok_mutasi.clearAll();
        gridstok_mutasi.load(url + "inventory/stok_mutasi/listdata/" + bulan + "/" + tahun,"json");
    }
</script>