<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 13/11/15
 * Time: 10:26
 */
?>
<div id="penerimaan_barangloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <?php
    echo form_open_multipart($form_action, array('id' => 'fm_penerimaan_barang'));
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span class="title_app"><b>No Penerimaan</b> <i></i></span>
                <?php echo form_input('inv_pen_brg_kode', !empty($default->inv_pen_brg_kode) ? $default->inv_pen_brg_kode : '', 'id="inv_pen_brg_kode" maxlength="15" class="form_web" placeholder="Auto" readonly');?>
            </td>
            <td>
                <span class="title_app"><b>Tanggal transaksi</b> <i>*</i></span>
                <?php echo form_input('data[inv_pen_brg_date]',!empty($default->inv_pen_brg_date) ? $default->inv_pen_brg_date : date('Y-m-d'), 'id="inv_pen_brg_date" class="form_web" readonly'); ?>
            </td>
            <td>
                <span class="title_app"><b>No Ref</b> <i>*</i></span>
                <?php echo form_input('data[inv_po_no]',!empty($default->inv_po_no) ? $default->inv_po_no : '', 'id="inv_po_no" class="form_web" readonly'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>No Surat Jalan</b> <i></i></span>
                <?php echo form_input('data[inv_pen_brg_no_srt_jln]',!empty($default->inv_pen_brg_no_srt_jln) ? $default->inv_pen_brg_no_srt_jln : '', 'id="inv_pen_brg_no_srt_jln" class="form_web"'); ?>
            </td>
            <td colspan="2">
                <span class="title_app"><b>Keterangan</b> <i></i></span>
                <?php echo form_input('data[inv_pen_brg_ket]',!empty($default->inv_pen_brg_ket) ? $default->inv_pen_brg_ket : '', 'id="inv_pen_brg_ket" class="form_web"'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div style="width: 100%; height: 300px;" id="gridPOForm"></div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <?php echo arrman::getRequired();?>
            </td>
        </tr>

    </table>
    <?php
    echo form_close();
    ?>
</div>
<script type="text/javascript">
    $("#inv_po_tgl").focus();
    $("#m_suplier_kode").select2();

    dhxCalPo    = new dhtmlxCalendarObject('inv_po_tgl');

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridpenerimaan_barangForm = new dhtmlXGridObject('gridPOForm');
    gridpenerimaan_barangForm.setImagePath(url + "assets/dhtmlx/imgs/");
    gridpenerimaan_barangForm.setHeader("Kode Item ,Nama Item ,Qty Kirim ,Qty Terima");
    gridpenerimaan_barangForm.setColAlign("left,left,center,center");
    gridpenerimaan_barangForm.setColTypes("ro,ro,ro,edn");
    gridpenerimaan_barangForm.setInitWidths("120,*,80,80");
    gridpenerimaan_barangForm.init();
    gridpenerimaan_barangForm.enableDistributedParsing(true,10,300);
    gridpenerimaan_barangForm.attachEvent("onEditCell", onCellEditPOForm);

    function onCellEditPOForm(stage, rowId, cellInd, nValue, oValue){
        if (stage == 2) {
            if(cellInd == 3) {
                var qtyKirim    = parseInt(gridpenerimaan_barangForm.cells(rowId,2).getValue());
                if(isNaN(nValue)){
                    dhtmlx.alert("Isi dengan angka");
                    return false;
                }
                if(nValue < 0){
                    dhtmlx.alert("Qty terima tidak boleh kurang dari nol");
                    return false;
                }
                if(nValue > qtyKirim){
                    dhtmlx.alert("Qty Terima melebihi Qty Kirim");
                    return false;
                }
            }
            return true;
        }
    }

    <?php
    if(!empty($default->inv_pen_brg_kode)){
        $id = explode('/',$default->inv_pen_brg_kode);
        $id = $id[0] . '-' . $id[1] . '-' . $id[2];
        echo 'gridpenerimaan_barangForm.load("' . base_url() . 'inventory/penerimaan_barang/listdataDetail/' . $id . '","json");';
    }
    ?>

    function simpan(id){
        var total   = $("#inv_po_total").val();
        if(total == "0"){
            dhtmlx.alert("Tidak ada barang");
            return;
        }
        var toolbar = ["penerimaan_barangWindows","savepenerimaan_barang"];
        var data    = {
            datagrid    : $.csv.toArrays(gridpenerimaan_barangForm.serializeToCSV())
        };

        SimpanDataTF('#fm_penerimaan_barang','#penerimaan_barangloading',toolbar,'',data);
    }

    function refresh(){
        close_form_modal(penerimaan_barangWindows,"penerimaan_barangWindows");
        refreshGrid(gridpenerimaan_barang,url + "inventory/penerimaan_barang/listdata","json");
    }

    function openPo(){
        var module  = url + "inventory/penerimaan_barang";
        var gridProp    = {
            header       : "PO No,Tgl PO, Keterangan, Vending,Petugas",
            headeralign  : ["text-align:left","text-align:left","text-align:left","text-align:left","text-align:left"],
            attachheader : "#text_filter,#text_filter,#text_filter,#text_filter,#text_filter",
            filteralign  : ["text-align:left","text-align:left","text-align:left","text-align:left","text-align:left"],
            colalign     : "left,left,left,center,center",
            coltypes     : "ro,ro,ro,ro,ro",
            sizes        : "*,*,*,70,100",
            footer       : "Total Records : ,{#stat_count},#cspan,#cspan,#cspan",
            urltarget    : module + "/listPo",
            fungsi       : [
                ["onRowDblClicked", addPo]
            ],
            hiddencolumn : []
        };
        form_modal_grid(penerimaan_barangWindows,"listPo",["List Purcase Order",600,450], "tanpatoolbar", gridProp);
    }

    function addPo(rId){
        var module  = url + "inventory/penerimaan_barang";
        $("#inv_po_no").val(rId);
        var no_trx   = rId.split('/');
        no_trx       = no_trx[0] + '-' + no_trx[1] + '-' + no_trx[2];
        gridpenerimaan_barangForm.clearAll();
        gridpenerimaan_barangForm.load(module + "/listDataDetailPo/" + no_trx,"json");
        close_form_modal(penerimaan_barangWindows,"listPo");
    }

    function openTb(){
        var module  = url + "inventory/penerimaan_barang";
        var gridProp    = {
            header       : "Transfer No,Tgl Transfer, Keterangan, Vending,Petugas",
            headeralign  : ["text-align:left","text-align:left","text-align:left","text-align:left","text-align:left"],
            attachheader : "#text_filter,#text_filter,#text_filter,#text_filter,#text_filter",
            filteralign  : ["text-align:left","text-align:left","text-align:left","text-align:left","text-align:left"],
            colalign     : "left,left,left,center,center",
            coltypes     : "ro,ro,ro,ro,ro",
            sizes        : "*,*,*,70,100",
            footer       : "Total Records : ,{#stat_count},#cspan,#cspan,#cspan",
            urltarget    : module + "/listTb",
            fungsi       : [
                ["onRowDblClicked", addTb]
            ],
            hiddencolumn : []
        };
        form_modal_grid(penerimaan_barangWindows,"listTb",["List Transfer Barang",600,450], "tanpatoolbar", gridProp);
    }

    function addTb(rId){
        var module  = url + "inventory/penerimaan_barang";
        $("#inv_po_no").val(rId);
        var no_trx   = rId.split('/');
        no_trx       = no_trx[0] + '-' + no_trx[1] + '-' + no_trx[2] + '-' + no_trx[3] + '-' + no_trx[4];
        gridpenerimaan_barangForm.clearAll();
        gridpenerimaan_barangForm.load(module + "/listDataDetailTb/" + no_trx,"json");
        close_form_modal(penerimaan_barangWindows,"listTb");
    }
</script>