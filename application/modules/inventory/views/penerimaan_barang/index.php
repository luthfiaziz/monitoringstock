<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 13/11/15
 * Time: 10:26
 */
?>
<script type="text/javascript">
    var penerimaan_barangAkses    = "<?php echo $aksesEdit; ?>";
    var penerimaan_barangWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbarpenerimaan_barang = mainTab.cells("maintabbar").attachToolbar();
    toolbarpenerimaan_barang.setSkin("dhx_skyblue");
    toolbarpenerimaan_barang.setIconsPath(url + "assets/img/btn/");
    toolbarpenerimaan_barang.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarpenerimaan_barang.attachEvent("onClick", cpenerimaan_barang);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function cpenerimaan_barang(id){
        var module  = url + "inventory/penerimaan_barang";
        var idRow   = gridpenerimaan_barang.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(penerimaan_barangWindows,"penerimaan_barangWindows",["Form  Penerimaan Barang",800,550], module + "/loadFormToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }
            var no_pa   = idRow.split('/');
            no_pa       = no_pa[0] + '-' + no_pa[1] + '-' + no_pa[2];
            form_modal(penerimaan_barangWindows,"penerimaan_barangWindows",["Form  Penerimaan Barang",800,550], module + "/loadFormToolbar", module + "/loadform/" + no_pa);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    };

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({
                                    type: "alert",
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridpenerimaan_barang, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridpenerimaan_barang,module + "/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridpenerimaan_barang = mainTab.cells("maintabbar").attachGrid();
    gridpenerimaan_barang.setImagePath(url + "assets/dhtmlx/imgs/");
    gridpenerimaan_barang.setHeader("No Penerimaan ,Tgl Transaksi,No Surat Jalan, No Ref, Keterangan, Petugas");
    gridpenerimaan_barang.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#combo_filter");
    gridpenerimaan_barang.setColAlign("left,center,left,left,left,center");
    gridpenerimaan_barang.setColTypes("ro,ro,ro,ro,ro,ro");
    gridpenerimaan_barang.setInitWidths("200,150,200,200,*,150");
    gridpenerimaan_barang.init();
    gridpenerimaan_barang.enableDistributedParsing(true,10,300);
    gridpenerimaan_barang.load("<?php echo $data_source; ?>","json");

    if(penerimaan_barangAkses == "TRUE"){
        gridpenerimaan_barang.attachEvent("onRowDblClicked",gridpenerimaan_barangDBLClick);
    }

    // [ACTION] GRID ACTION
    function gridpenerimaan_barangDBLClick(){
        var module  = url + "inventory/penerimaan_barang";
        var no_pa   = gridpenerimaan_barang.getSelectedRowId().split('/');
        no_pa       = no_pa[0] + '-' + no_pa[1] + '-' + no_pa[2];
        form_modal(penerimaan_barangWindows,"penerimaan_barangWindows",["Form Penerimaan Barang",800,550], module + "/loadFormToolbar", module + "/loadform/" + no_pa);
    }
</script>