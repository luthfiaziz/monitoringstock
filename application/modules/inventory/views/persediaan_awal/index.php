<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 19/10/15
 * Time: 10:03
 */
?>
<script type="text/javascript">
    var persediaan_awalAkses    = "<?php echo $aksesEdit; ?>";
    var persediaan_awalWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbarpersediaan_awal = mainTab.cells("maintabbar").attachToolbar();
    toolbarpersediaan_awal.setSkin("dhx_skyblue");
    toolbarpersediaan_awal.setIconsPath(url + "assets/img/btn/");
    toolbarpersediaan_awal.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarpersediaan_awal.attachEvent("onClick", cpersediaan_awal);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function cpersediaan_awal(id){
        var module  = url + "inventory/persediaan_awal";
        var idRow   = gridpersediaan_awal.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(persediaan_awalWindows,"persediaan_awalWindows",["Form Persediaan Awal",650,550], module + "/loadFormToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }
            var no_pa   = idRow.split('/');
            no_pa       = no_pa[0] + '-' + no_pa[1] + '-' + no_pa[2];
            form_modal(persediaan_awalWindows,"persediaan_awalWindows",["Form Persediaan Awal",650,550], module + "/loadFormToolbar", module + "/loadform/" + no_pa);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    };

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({
                                    type: "alert",
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridpersediaan_awal, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridpersediaan_awal,module + "/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridpersediaan_awal = mainTab.cells("maintabbar").attachGrid();
    gridpersediaan_awal.setImagePath(url + "assets/dhtmlx/imgs/");
    gridpersediaan_awal.setHeader("No Persediaan ,Bulan,Tahun, Tgl dibuat, Petugas");
    gridpersediaan_awal.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
    gridpersediaan_awal.setColAlign("left,center,center,center,center");
    gridpersediaan_awal.setColTypes("ro,ro,ro,ro,ro");
    gridpersediaan_awal.setInitWidths("*,*,*,150,150");
    gridpersediaan_awal.init();
    gridpersediaan_awal.enableDistributedParsing(true,10,300);
    gridpersediaan_awal.load("<?php echo $data_source; ?>","json");

    if(persediaan_awalAkses == "TRUE"){
        gridpersediaan_awal.attachEvent("onRowDblClicked",gridpersediaan_awalDBLClick);
    }

    // [ACTION] GRID ACTION
    function gridpersediaan_awalDBLClick(){
        var module  = url + "inventory/persediaan_awal";
        var no_pa   = gridpersediaan_awal.getSelectedRowId().split('/');
        no_pa       = no_pa[0] + '-' + no_pa[1] + '-' + no_pa[2];
        form_modal(persediaan_awalWindows,"persediaan_awalWindows",["Form Persediaan Awal",650,550], module + "/loadFormToolbar", module + "/loadform/" + no_pa);
    }
</script>