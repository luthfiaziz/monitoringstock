<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 19/10/15
 * Time: 10:03
 */
?>
<div id="persediaan_awalloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <?php
    echo form_open_multipart($form_action, array('id' => 'fm_persediaan_awal'));
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span class="title_app"><b>No Persediaan</b> <i></i></span>
                <?php echo form_input('persediaan_awal_no', !empty($default->persediaan_awal_no) ? $default->persediaan_awal_no : '', 'id="persediaan_awal_no" maxlength="15" class="form_web" placeholder="Auto" readonly');?>
            </td>
            <td>
                <span class="title_app"><b>Bulan</b> <i>*</i></span>
                <?php echo form_dropdown('data[persediaan_awal_bulan]',$bulan, !empty($default->persediaan_awal_bulan) ? $default->persediaan_awal_bulan : '', 'id="persediaan_awal_bulan" class="full"'); ?>
            </td>
            <td>
                <span class="title_app"><b>Tahun</b> <i>*</i></span>
                <?php echo form_dropdown('data[persediaan_awal_tahun]',$tahun, !empty($default->persediaan_awal_tahun) ? $default->persediaan_awal_tahun : '', 'id="persediaan_awal_tahun" class="full"'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div style="width: 100%" id="toolbarPersediaanAwalForm"></div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div style="width: 100%; height: 340px;" id="gridPersediaanAwalForm"></div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <?php echo arrman::getRequired();?>
            </td>
        </tr>

    </table>
    <?php
    echo form_close();
    ?>
</div>
<script type="text/javascript">
    $("#m_item_persediaan_awal_kode").focus();
    $("#persediaan_awal_bulan").select2();
    $("#persediaan_awal_tahun").select2();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbarpersediaan_awalForm = new dhtmlXToolbarObject("toolbarPersediaanAwalForm");
    toolbarpersediaan_awalForm.setSkin("dhx_skyblue");
    toolbarpersediaan_awalForm.setIconsPath(url + "assets/img/btn/");
    toolbarpersediaan_awalForm.addButton('add',1,"Tambah Item","add.png","add.png");
    toolbarpersediaan_awalForm.addButton('delete',2,"Hapus Item","delete.png","delete.png");
    toolbarpersediaan_awalForm.attachEvent("onClick", cpersediaan_awalForm);

    function cpersediaan_awalForm(id){
        if(id == "add"){
            var module      = url + "inventory/persediaan_awal";
            form_modal(persediaan_awalWindows,"listBarang",["List Item",550,500],"tanpatoolbar", module + "/item");
        }

        if(id == "delete"){
            var rId = gridpersediaan_awalForm.getSelectedRowId();
            if(rId == null){
                dhtmlx.alert("Pillih satu data");
                return;
            }
            gridpersediaan_awalForm.deleteSelectedRows();
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridpersediaan_awalForm = new dhtmlXGridObject('gridPersediaanAwalForm');
    gridpersediaan_awalForm.setImagePath(url + "assets/dhtmlx/imgs/");
    gridpersediaan_awalForm.setHeader("Kode Item ,Nama Item,Qty,Batch No,tgl Produksi, Exp Date");
    gridpersediaan_awalForm.setColAlign("left,left,center,left,center,center");
    gridpersediaan_awalForm.setColTypes("ro,ro,ed,ed,dhxCalendar,dhxCalendar");
    gridpersediaan_awalForm.setInitWidths("90,*,70,100,80,80");
    gridpersediaan_awalForm.setDateFormat("%Y-%m-%d");
    gridpersediaan_awalForm.init();
    gridpersediaan_awalForm.enableDistributedParsing(true,10,300);

    <?php
    if(!empty($default->persediaan_awal_no)){
        $id = explode('/',$default->persediaan_awal_no);
        $id = $id[0] . '-' . $id[1] . '-' . $id[2];
        echo 'gridpersediaan_awalForm.load("' . base_url() . 'inventory/persediaan_awal/listdataDetail/' . $id . '","json");';
    }
    ?>

    function simpan(id){
        var toolbar = ["persediaan_awalWindows","savepersediaan_awal"];
        var data    = {
            datagrid    : $.csv.toArrays(gridpersediaan_awalForm.serializeToCSV())
        };

        SimpanDataTF('#fm_persediaan_awal','#persediaan_awalloading',toolbar,'',data);
    }

    function refresh(){
        close_form_modal(persediaan_awalWindows,"persediaan_awalWindows");
        refreshGrid(gridpersediaan_awal,url + "inventory/persediaan_awal/listdata","json");
    }

    function bersih(){
    }

    function pillihItemList(id){
        if(id == null){
            var idprodukAll = gridListItemSearch.getCheckedRows(0);
            var idproduk    = idprodukAll.split(',');
            for(i=0;i<idproduk.length;i++){
                var kodeItem    = gridListItemSearch.cells(idproduk[i],0).getValue();
                var namaItem    = gridListItemSearch.cells(idproduk[i],1).getValue();
                var uId         = gridpersediaan_awalForm.uid();
                var posisi      = gridpersediaan_awalForm.getRowsNum();
                gridpersediaan_awalForm.addRow(uId, [kodeItem,namaItem,'0','','',''], posisi);
            }
        }else{
            var kodeItem    = gridListItemSearch.cells(id,0).getValue();
            var namaItem    = gridListItemSearch.cells(id,1).getValue();
            var uId         = gridpersediaan_awalForm.uid();
            var posisi      = gridpersediaan_awalForm.getRowsNum();
            gridpersediaan_awalForm.addRow(uId, [kodeItem,namaItem,'0','','',''], posisi);
        }
        close_form_modal(persediaan_awalWindows,"listBarang");
    }
</script>