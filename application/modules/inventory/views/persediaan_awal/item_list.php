<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 19/10/15
 * Time: 10:58
 */
?>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <table class="form" width="100%">
        <tr>
            <td>
                <span class="title_app"><b>Kategori</b> <i></i></span>
                <?php echo form_dropdown('kategori_item',$kategori,'', 'id="kategori_item" class="full" onchange="loadGridItemList()"'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <div id="toolbarListItemSearch" style="width: 100%;"></div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="width: 100%; height: 330px;" id="gridListItemSearch"></div>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    $("#kategori_item").select2();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbarListItemSearch = new dhtmlXToolbarObject("toolbarListItemSearch");
    toolbarListItemSearch.setSkin("dhx_skyblue");
    toolbarListItemSearch.setIconsPath(url + "assets/img/btn/");
    toolbarListItemSearch.addButton('pillih',1,"Tambahkan yang di pillih","Button Check-01.png","Button Check-01.png");
    toolbarListItemSearch.attachEvent("onClick", cListItemSearch);

    function cListItemSearch(id){
        if(id == "pillih"){
            pillihItemList();
        }
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridListItemSearch = new dhtmlXGridObject('gridListItemSearch');
    gridListItemSearch.setImagePath(url + "assets/dhtmlx/imgs/");
    gridListItemSearch.setHeader("Kode Item ,Nama Item, Harga Beli, Harga Jual, <input type='checkbox' id='cekcek' onclick='cekListcek(this)' />");
    gridListItemSearch.setColAlign("left,left,right,right,center");
    gridListItemSearch.setColTypes("ro,ro,ron,ron,ch");
    gridListItemSearch.setInitWidths("90,*,80,80,50");
    gridListItemSearch.setNumberFormat("0,000.00", 2, ".", ",");
    gridListItemSearch.setNumberFormat("0,000.00", 3, ".", ",");
    gridListItemSearch.init();
    gridListItemSearch.enableDistributedParsing(true,10,300);

    function loadGridItemList(){
        var kategori    = $("#kategori_item").val();
        gridListItemSearch.clearAll();
        gridListItemSearch.load(url + "inventory/persediaan_awal/listDataItem/" + kategori,"json");
    }

    gridListItemSearch.attachEvent("onRowDblClicked",function(id){
        pillihItemList(id);
    });

    gridListItemSearch.attachEvent("onRowSelect", function(rId,cInd){
        var cek= gridListItemSearch.cells(rId,4).getValue();
        if(cek==0){
            gridListItemSearch.cells(rId,4).setValue(1);
        }else{
            gridListItemSearch.cells(rId,4).setValue(0);
        }
    });

    function cekListcek(isi) {
        var cekArr = gridListItemSearch.getAllItemIds().split(",");
        if(document.getElementById("cekcek").checked==true) {
            for(n=0;n<cekArr.length;n++) {
                gridListItemSearch.cells(cekArr[n],4).setValue(1);
            }
        } else {
            for(n=0;n<cekArr.length;n++) {
                gridListItemSearch.cells(cekArr[n],4).setValue(0);
            }
        }
    }
</script>