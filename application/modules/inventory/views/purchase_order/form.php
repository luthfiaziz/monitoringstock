<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 20/10/15
 * Time: 15:46
 */
?>
<div id="purchase_orderloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <?php
    echo form_open_multipart($form_action, array('id' => 'fm_purchase_order'));
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span class="title_app"><b>No PO</b> <i></i></span>
                <?php echo form_input('inv_po_no', !empty($default->inv_po_no) ? $default->inv_po_no : '', 'id="inv_po_no" maxlength="15" class="form_web" placeholder="Auto" readonly');?>
            </td>
            <td>
                <span class="title_app"><b>Tanggal transaksi</b> <i>*</i></span>
                <?php echo form_input('data[inv_po_tgl]',!empty($default->inv_po_tgl) ? $default->inv_po_tgl : date('Y-m-d'), 'id="inv_po_tgl" class="form_web" readonly'); ?>
            </td>
            <td>
                <span class="title_app"><b>Supplier</b> <i>*</i></span>
                <?php echo form_dropdown('data[m_suplier_kode]',$supplier, !empty($default->m_suplier_kode) ? $default->m_suplier_kode : '', 'id="m_suplier_kode" class="full"'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div style="width: 100%" id="toolbarPOForm"></div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div style="width: 100%; height: 230px;" id="gridPOForm"></div>
            </td>
        </tr>
        <tr>
            <td rowspan="2">
                <span class="title_app"><b>Keterangan</b> <i></i></span>
                <?php echo form_textarea('data[inv_po_ket]', !empty($default->inv_po_ket) ? $default->inv_po_ket : '', 'id="inv_po_ket" class="form_web"  style="height:85px;"');?>
            </td>
            <td>
                <span class="title_app"><b>Total</b> <i></i></span>
                <?php echo form_input('data[inv_po_total]',!empty($default->inv_po_total) ? $default->inv_po_total : '0', 'id="inv_po_total" class="form_web" style=" text-align:right;" readonly'); ?>
            </td>
            <td>
                <span class="title_app"><b>Diskon</b> <i></i></span>
                <?php echo form_input('data[inv_po_diskon]', !empty($default->inv_po_diskon) ? $default->inv_po_diskon : '0', 'id="inv_po_diskon" class="form_web" style=" text-align:right;" readonly');?>
            </td>
        </tr>
        <tr>
            <td>
                <span class="title_app"><b>Grandtotal</b> <i></i></span>
                <?php echo form_input('data[inv_po_grand_total]',!empty($default->inv_po_grand_total) ? $default->inv_po_grand_total : '0', 'id="inv_po_grand_total" class="form_web" style=" text-align:right;" readonly'); ?>
            </td>
            <td>
                <span class="title_app"><b>Pajak (%)</b> <i></i></span>
                <?php echo form_input('data[inv_po_pajak]', !empty($default->inv_po_pajak) ? $default->inv_po_pajak : '0', 'id="inv_po_pajak" maxlength="3" class="form_web" style=" text-align:right;"');?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <?php echo arrman::getRequired();?>
            </td>
        </tr>

    </table>
    <?php
    echo form_close();
    ?>
</div>
<script type="text/javascript">
    $("#inv_po_tgl").focus();
    $("#m_suplier_kode").select2();

    dhxCalPo    = new dhtmlxCalendarObject('inv_po_tgl');

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbarpurchase_orderForm = new dhtmlXToolbarObject("toolbarPOForm");
    toolbarpurchase_orderForm.setSkin("dhx_skyblue");
    toolbarpurchase_orderForm.setIconsPath(url + "assets/img/btn/");
    toolbarpurchase_orderForm.addButton('add',1,"Tambah Item","add.png","add.png");
    toolbarpurchase_orderForm.addButton('delete',2,"Hapus Item","delete.png","delete.png");
    toolbarpurchase_orderForm.attachEvent("onClick", cpurchase_orderForm);

    function cpurchase_orderForm(id){
        if(id == "add"){
            var module      = url + "inventory/persediaan_awal";
            form_modal(purchase_orderWindows,"listBarang",["List Item",550,500],"tanpatoolbar", module + "/item");
        }

        if(id == "delete"){
            var rId = gridpurchase_orderForm.getSelectedRowId();
            if(rId == null){
                dhtmlx.alert("Pillih satu data");
                return;
            }
            gridpurchase_orderForm.deleteSelectedRows();
            hitungTotal();
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridpurchase_orderForm = new dhtmlXGridObject('gridPOForm');
    gridpurchase_orderForm.setImagePath(url + "assets/dhtmlx/imgs/");
    gridpurchase_orderForm.setHeader("Kode Item ,Nama Item,Qty,Harga,Total,Diskon,Grandtotal, Keterangan");
    gridpurchase_orderForm.setColAlign("left,left,center,right,right,right,right,left");
    gridpurchase_orderForm.setColTypes("ro,ro,ed,ron,ron,edn,ron,ed");
    gridpurchase_orderForm.setInitWidths("90,*,50,70,90,80,90,120");
    gridpurchase_orderForm.setNumberFormat("0,000.00", 3, ".", ",");
    gridpurchase_orderForm.setNumberFormat("0,000.00", 4, ".", ",");
    gridpurchase_orderForm.setNumberFormat("0,000.00", 5, ".", ",");
    gridpurchase_orderForm.setNumberFormat("0,000.00", 6, ".", ",");
    gridpurchase_orderForm.init();
    gridpurchase_orderForm.enableDistributedParsing(true,10,300);
    gridpurchase_orderForm.attachEvent("onEditCell", onCellEditPOForm);

    function onCellEditPOForm(stage, rowId, cellInd, nValue, oValue){
        if (stage == 2) {
            if(cellInd == 2) {
                if(isNaN(nValue)){
                    dhtmlx.alert("Isi dengan angka");
                    return false;
                }
                if(nValue < 0){
                    dhtmlx.alert("quantity tidak boleh kurang dari nol");
                    return false;
                }else{
                    total       = parseInt(nValue) * parseInt(gridpurchase_orderForm.cells(rowId,3).getValue());
                    grandTotal  = total - parseInt(gridpurchase_orderForm.cells(rowId,5).getValue());
                    gridpurchase_orderForm.cells(rowId,4).setValue(total);
                    gridpurchase_orderForm.cells(rowId,6).setValue(grandTotal);
                }
            }else if(cellInd == 5){
                if(isNaN(nValue)){
                    dhtmlx.alert("Isi dengan angka");
                    return false;
                }
                total   = parseInt(gridpurchase_orderForm.cells(rowId,4).getValue());
                if(nValue > total){
                    dhtmlx.alert("Diskon tidak boleh lebih dari total");
                    return false;
                }else{
                    grandTotal  = total - nValue;
                    gridpurchase_orderForm.cells(rowId,6).setValue(grandTotal);
                }
            }
            hitungTotal();
            return true;
        }
    }

    <?php
    if(!empty($default->inv_po_no)){
        $id = explode('/',$default->inv_po_no);
        $id = $id[0] . '-' . $id[1] . '-' . $id[2];
        echo 'gridpurchase_orderForm.load("' . base_url() . 'inventory/purchase_order/listdataDetail/' . $id . '","json");';
    }
    ?>

    function simpan(id){
        var total   = $("#inv_po_total").val();
        if(total == "0"){
            dhtmlx.alert("Tidak ada barang");
            return;
        }
        var toolbar = ["purchase_orderWindows","savepurchase_order"];
        var data    = {
            datagrid    : $.csv.toArrays(gridpurchase_orderForm.serializeToCSV())
        };

        SimpanDataTF('#fm_purchase_order','#purchase_orderloading',toolbar,'',data);
    }

    function refresh(){
        close_form_modal(purchase_orderWindows,"purchase_orderWindows");
        refreshGrid(gridpurchase_order,url + "inventory/purchase_order/listdata","json");
    }

    function pillihItemList(id){
        if(id == null){
            var idprodukAll = gridListItemSearch.getCheckedRows(0);
            var idproduk    = idprodukAll.split(',');
            for(i=0;i<idproduk.length;i++){
                var kodeItem    = gridListItemSearch.cells(idproduk[i],0).getValue();
                if(checkingBarangChild(kodeItem) == false){
                    var namaItem    = gridListItemSearch.cells(idproduk[i],1).getValue();
                    var hargaBeli   = gridListItemSearch.cells(idproduk[i],2).getValue();
                    var uId         = gridpurchase_orderForm.uid();
                    var posisi      = gridpurchase_orderForm.getRowsNum();
                    gridpurchase_orderForm.addRow(uId, [kodeItem,namaItem,'0',hargaBeli,'0','0','0','-'], posisi);
                }
            }
        }else{
            var kodeItem    = gridListItemSearch.cells(id,0).getValue();
            if(checkingBarangChild(kodeItem) == false){
                var namaItem    = gridListItemSearch.cells(id,1).getValue();
                var hargaBeli   = gridListItemSearch.cells(id,2).getValue();
                var uId         = gridpurchase_orderForm.uid();
                var posisi      = gridpurchase_orderForm.getRowsNum();
                gridpurchase_orderForm.addRow(uId, [kodeItem,namaItem,'0',hargaBeli,'0','0','0','-'], posisi);
            }
        }
        hitungTotal();
        close_form_modal(purchase_orderWindows,"listBarang");
    }

    function checkingBarangChild(kode_barang){
        condition = false;
        gridpurchase_orderForm.forEachRow(function(ids){
            if(gridpurchase_orderForm.cells(ids,0).getValue() == kode_barang) {
                condition = true;
            }
        });

        return condition;
    }

    function hitungTotal(){
        var total       = 0;
        var diskon      = 0;
        var grandtotal  = 0;
        gridpurchase_orderForm.forEachRow(function(ids){
            total       += parseInt(gridpurchase_orderForm.cells(ids,4).getValue());
            diskon      += parseInt(gridpurchase_orderForm.cells(ids,5).getValue());
            grandtotal  += parseInt(gridpurchase_orderForm.cells(ids,6).getValue());
        });

        $("#inv_po_total").val(total);
        $("#inv_po_diskon").val(diskon);
        $("#inv_po_grand_total").val(grandtotal);
    }
</script>