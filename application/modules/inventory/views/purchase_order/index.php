<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 20/10/15
 * Time: 15:46
 */
?>
<script type="text/javascript">
    var purchase_orderAkses    = "<?php echo $aksesEdit; ?>";
    var purchase_orderWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbarpurchase_order = mainTab.cells("maintabbar").attachToolbar();
    toolbarpurchase_order.setSkin("dhx_skyblue");
    toolbarpurchase_order.setIconsPath(url + "assets/img/btn/");
    toolbarpurchase_order.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarpurchase_order.attachEvent("onClick", cpurchase_order);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function cpurchase_order(id){
        var module  = url + "inventory/purchase_order";
        var idRow   = gridpurchase_order.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(purchase_orderWindows,"purchase_orderWindows",["Form  Purcase Order",800,550], module + "/loadFormToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }
            var no_pa   = idRow.split('/');
            no_pa       = no_pa[0] + '-' + no_pa[1] + '-' + no_pa[2];
            form_modal(purchase_orderWindows,"purchase_orderWindows",["Form  Purcase Order",800,550], module + "/loadFormToolbar", module + "/loadform/" + no_pa);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    };

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({
                                    type: "alert",
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridpurchase_order, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridpurchase_order,module + "/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridpurchase_order = mainTab.cells("maintabbar").attachGrid();
    gridpurchase_order.setImagePath(url + "assets/dhtmlx/imgs/");
    gridpurchase_order.setHeader("No PO ,Tgl Transaksi,Keterangan, Grandtotal, Supplier, Status");
    gridpurchase_order.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#combo_filter,#combo_filter");
    gridpurchase_order.setColAlign("left,center,left,right,center,center");
    gridpurchase_order.setColTypes("ro,ro,ro,ron,ro,ro");
    gridpurchase_order.setNumberFormat("0,000.00", 3, ".", ",");
    gridpurchase_order.setInitWidths("200,150,*,200,250,150");
    gridpurchase_order.init();
    gridpurchase_order.enableDistributedParsing(true,10,300);
    gridpurchase_order.load("<?php echo $data_source; ?>","json");

    if(purchase_orderAkses == "TRUE"){
        gridpurchase_order.attachEvent("onRowDblClicked",gridpurchase_orderDBLClick);
    }

    // [ACTION] GRID ACTION
    function gridpurchase_orderDBLClick(){
        var module  = url + "inventory/purchase_order";
        var no_pa   = gridpurchase_order.getSelectedRowId().split('/');
        no_pa       = no_pa[0] + '-' + no_pa[1] + '-' + no_pa[2];
        form_modal(purchase_orderWindows,"purchase_orderWindows",["Form Purcase Order",800,550], module + "/loadFormToolbar", module + "/loadform/" + no_pa);
    }
</script>