<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 11/11/15
 * Time: 16:02
 */
?>
<div id="transfer_barangloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div class="form-wrapper" style="padding:20px; height: 100%; background:#cee3ff; display:block; overflow:hidden; padding-bottom:40px;">
    <?php
    echo form_open_multipart($form_action, array('id' => 'fm_transfer_barang'));
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span class="title_app"><b>No Transfer</b> <i></i></span>
                <?php echo form_input('transfer_barang_no', !empty($default->transfer_barang_no) ? $default->transfer_barang_no : '', 'id="transfer_barang_no" maxlength="15" class="form_web" placeholder="Auto" readonly');?>
            </td>
            <td>
                <span class="title_app"><b>Tanggal Transfer</b> <i>*</i></span>
                <?php echo form_input('data[transfer_barang_tgl]',!empty($default->transfer_barang_tgl) ? $default->transfer_barang_tgl : date('Y-m-d'), 'id="transfer_barang_tgl" class="form_web" readonly'); ?>
            </td>
            <td>
                <span class="title_app"><b>Tujuan</b> <i>*</i></span>
                <?php
                $disabled   = !empty($default->transfer_barang_no) ? "readonly":'';
                echo form_dropdown('data[transfer_barang_tujuan]',$tujuan, !empty($default->transfer_barang_tujuan) ? $default->transfer_barang_tujuan : '', 'id="transfer_barang_tujuan" class="full" ' . $disabled); ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <span class="title_app"><b>Keterangan</b> <i></i></span>
                <?php echo form_input('data[transfer_barang_ket]',!empty($default->transfer_barang_ket) ? $default->transfer_barang_ket : '', 'id="transfer_barang_ket" class="form_web"'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div style="width: 100%;" id="toolSOForm"></div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div style="width: 100%; height: 270px;" id="gridSOForm"></div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <?php echo arrman::getRequired();?>
            </td>
        </tr>

    </table>
    <?php
    echo form_close();
    ?>
</div>
<script type="text/javascript">
    $("#transfer_barang_tujuan").select2();

    dhxCalPo    = new dhtmlxCalendarObject('transfer_barang_tgl');

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbartransfer_barangForm = new dhtmlXToolbarObject("toolSOForm");
    toolbartransfer_barangForm.setSkin("dhx_skyblue");
    toolbartransfer_barangForm.setIconsPath(url + "assets/img/btn/");
    toolbartransfer_barangForm.addButton('add',1,"Tambah Item","add.png","add.png");
    toolbartransfer_barangForm.addButton('delete',2,"Hapus Item","delete.png","delete.png");
    toolbartransfer_barangForm.attachEvent("onClick", ctransfer_barangForm);

    function ctransfer_barangForm(id){
        if(id == "add"){
            var module      = url + "inventory/persediaan_awal";
            form_modal(transfer_barangWindows,"listBarang",["List Item",550,500],"tanpatoolbar", module + "/item");
        }

        if(id == "delete"){
            var rId = gridtransfer_barangForm.getSelectedRowId();
            if(rId == null){
                dhtmlx.alert("Pillih satu data");
                return;
            }
            gridtransfer_barangForm.deleteSelectedRows();
        }
    }

    function pillihItemList(id){
        if(id == null){
            var idprodukAll = gridListItemSearch.getCheckedRows(0);
            var idproduk    = idprodukAll.split(',');
            for(i=0;i<idproduk.length;i++){
                var kodeItem    = gridListItemSearch.cells(idproduk[i],0).getValue();
                if(checkingBarangChild(kodeItem) == false){
                    var namaItem    = gridListItemSearch.cells(idproduk[i],1).getValue();
                    var uId         = gridtransfer_barangForm.uid();
                    var posisi      = gridtransfer_barangForm.getRowsNum();
                    gridtransfer_barangForm.addRow(uId, [kodeItem,namaItem,'0'], posisi);
                }
            }
        }else{
            var kodeItem    = gridListItemSearch.cells(id,0).getValue();
            if(checkingBarangChild(kodeItem) == false){
                var namaItem    = gridListItemSearch.cells(id,1).getValue();
                var uId         = gridtransfer_barangForm.uid();
                var posisi      = gridtransfer_barangForm.getRowsNum();
                gridtransfer_barangForm.addRow(uId, [kodeItem,namaItem,'0'], posisi);
            }
        }
        close_form_modal(transfer_barangWindows,"listBarang");
    }

    function checkingBarangChild(kode_barang){
        condition = false;
        gridtransfer_barangForm.forEachRow(function(ids){
            if(gridtransfer_barangForm.cells(ids,0).getValue() == kode_barang) {
                condition = true;
            }
        });

        return condition;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridtransfer_barangForm = new dhtmlXGridObject('gridSOForm');
    gridtransfer_barangForm.setImagePath(url + "assets/dhtmlx/imgs/");
    gridtransfer_barangForm.setHeader("Kode Item ,Nama Item, Qty Transfer");
    gridtransfer_barangForm.setColAlign("left,left,center");
    gridtransfer_barangForm.setColTypes("ro,ro,ed");
    gridtransfer_barangForm.setInitWidths("90,*,80");
    gridtransfer_barangForm.init();
    gridtransfer_barangForm.enableDistributedParsing(true,10,300);
    gridtransfer_barangForm.attachEvent("onEditCell", onCellEditSOForm);

    function onCellEditSOForm(stage, rowId, cellInd, nValue, oValue){
        if (stage == 2) {
            if(cellInd == 2) {
                if(isNaN(nValue)){
                    dhtmlx.alert("Isi dengan angka");
                    return false;
                }
            }
            return true;
        }
    }

    <?php
    if(!empty($default->transfer_barang_no)){
        $id = explode('/',$default->transfer_barang_no);
        $id = $id[0] . '-' . $id[1] . '-' . $id[2] . '-' . $id[3] . '-' . $id[4];
        echo 'gridtransfer_barangForm.load("' . base_url() . 'inventory/transfer_barang/listdataDetail/' . $id . '","json");';
    }
    ?>

    function simpan(id){
        var toolbar = ["transfer_barangWindows","savetransfer_barang"];
        var data    = {
            datagrid    : $.csv.toArrays(gridtransfer_barangForm.serializeToCSV())
        };

        SimpanDataTF('#fm_transfer_barang','#transfer_barangloading',toolbar,'',data);
    }

    function refresh(){
        close_form_modal(transfer_barangWindows,"transfer_barangWindows");
        refreshGrid(gridtransfer_barang,url + "inventory/transfer_barang/listdata","json");
    }
</script>