<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 11/11/15
 * Time: 14:46
 */
?>
<script type="text/javascript">
    var transfer_barangAkses    = "<?php echo $aksesEdit; ?>";
    var transfer_barangWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbartransfer_barang = mainTab.cells("maintabbar").attachToolbar();
    toolbartransfer_barang.setSkin("dhx_skyblue");
    toolbartransfer_barang.setIconsPath(url + "assets/img/btn/");
    toolbartransfer_barang.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbartransfer_barang.attachEvent("onClick", ctransfer_barang);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function ctransfer_barang(id){
        var module  = url + "inventory/transfer_barang";
        var idRow   = gridtransfer_barang.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(transfer_barangWindows,"transfer_barangWindows",["Form Transfer Barang",650,550], module + "/loadFormToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }
            var no_pa   = idRow.split('/');
            no_pa       = no_pa[0] + '-' + no_pa[1] + '-' + no_pa[2] + '-' + no_pa[3] + '-' + no_pa[4];
            form_modal(transfer_barangWindows,"transfer_barangWindows",["Form Transfer Barang",650,550], module + "/loadFormToolbar", module + "/loadform/" + no_pa);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    };

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({
                                    type: "alert",
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridtransfer_barang, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridtransfer_barang,module + "/listdata","json");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridtransfer_barang = mainTab.cells("maintabbar").attachGrid();
    gridtransfer_barang.setImagePath(url + "assets/dhtmlx/imgs/");
    gridtransfer_barang.setHeader("No Transfer ,Tgl Transaksi,Keterangan, Tujuan, Petugas");
    gridtransfer_barang.attachHeader("#text_filter,#text_filter,#text_filter,#combo_filter,#combo_filter");
    gridtransfer_barang.setColAlign("left,center,left,center,center");
    gridtransfer_barang.setColTypes("ro,ro,ro,ro,ro");
    gridtransfer_barang.setInitWidths("200,150,*,250,150");
    gridtransfer_barang.init();
    gridtransfer_barang.enableDistributedParsing(true,10,300);
    gridtransfer_barang.load("<?php echo $data_source; ?>","json");

    if(transfer_barangAkses == "TRUE"){
        gridtransfer_barang.attachEvent("onRowDblClicked",gridtransfer_barangDBLClick);
    }

    // [ACTION] GRID ACTION
    function gridtransfer_barangDBLClick(){
        var module  = url + "inventory/transfer_barang";
        var no_pa   = gridtransfer_barang.getSelectedRowId().split('/');
        no_pa       = no_pa[0] + '-' + no_pa[1] + '-' + no_pa[2] + '-' + no_pa[3] + '-' + no_pa[4];
        form_modal(transfer_barangWindows,"transfer_barangWindows",["Form Transfer Barang",650,550], module + "/loadFormToolbar", module + "/loadform/" + no_pa);
    }
</script>