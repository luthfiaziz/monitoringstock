<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 26/10/15
 * Time: 14:03
 */

class stok_opname_model extends CI_Model {
    private $_table1 = "inv_stok_opname";
    private $_table2 = "inv_stok_opname_detail";
    private $_table3 = "m_item";
    private $_table4 = "inv_stok_berjalan";

    public function __construct() {
        parent::__construct();

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("stok_opname_no" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->select("a.*");
        $this->db->from($this->_table1 . " a");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function dataDetail($key = ""){
        $this->db->select("a.*,b.m_item_nama");
        $this->db->from($this->_table2 . " a");
        $this->db->join($this->_table3 . " b","a.m_item_kode=b.m_item_kode","left");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData($key = ''){
        $result = $this->data($key)->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->stok_opname_no,
                "data"=>array(
                    $value->stok_opname_no,
                    $value->stok_opname_tgl,
                    $value->stok_opname_ket,
                    $value->stok_opname_tgl_buat,
                    $value->stok_opname_petugas
                )
            );
        }

        return $data;
    }

    public function loadDataDetail($key = ""){
        $result = $this->dataDetail($key)->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->stok_opname_detail_id,
                "data"=>array(
                    $value->m_item_kode,
                    $value->m_item_nama,
                    $value->stok_opname_detail_qty_sys,
                    $value->stok_opname_detail_qty_fisik
                )
            );
        }

        return $data;
    }

    public function loadDataOpname($key){
        $this->db->select("a.m_item_kode,b.m_item_nama,SUM(a.stok_berjalan_qty_keluar) AS qty_out, SUM(a.stok_berjalan_qty_masuk) AS qty_in");
        $this->db->from($this->_table4 . " a");
        $this->db->join($this->_table3 . " b","a.m_item_kode=b.m_item_kode","left");
        $this->db->where($key);
        $this->db->group_by("a.m_item_kode");
        $result = $this->db->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $qty    = $value->qty_in - $value->qty_out;
            $data[] = array(
                "id" => $value->m_item_kode,
                "data"=>array(
                    $value->m_item_kode,
                    $value->m_item_nama,
                    $qty,
                    "0"
                )
            );
        }

        return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where($this->_kunci($value));

        $total=$this->db->count_all_results();

        return $total;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->stok_opname_no] = $row->stok_opname_no;
        }

        return $option;
    }

    public function getMaxData(){
        $sql    = $this->db->query("SELECT MAX(RIGHT(stok_opname_no,5)) as noMax FROM 1nt3x_inv_stok_opname")->row();

        return $sql;
    }
}