<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 19/10/15
 * Time: 10:03
 */

class persediaan_awal_model extends CI_Model {
    private $_table1 = "inv_persediaan_awal";
    private $_table2 = "inv_persediaan_awal_detail";
    private $_table3 = "m_item";

    public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("persediaan_awal_no" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->from($this->_table1 . " a");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function dataDetail($key = ""){
        $this->db->select("a.*,b.m_item_nama");
        $this->db->from($this->_table2 . " a");
        $this->db->join($this->_table3 . " b","a.m_item_kode=b.m_item_kode","left");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function dataItem($key = ""){
        $this->db->select("a.*,b.m_harga_beli,b.m_harga_jual");
        $this->db->from($this->_table3 . " a");
        $this->db->join("m_harga b","a.m_item_kode=b.m_item_kode","left");

        if(!empty($key))
            $this->db->where($key);

        return $this->db;
    }

    public function loadData($key = ''){
        $result = $this->data($key)->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->persediaan_awal_no,
                "data"=>array(
                    $value->persediaan_awal_no,
                    $value->persediaan_awal_bulan,
                    $value->persediaan_awal_tahun,
                    $value->persediaan_awal_tgl_buat,
                    $value->persediaan_awal_petugas,
                )
            );
        }

        return $data;
    }

    public function loadDataDetail($key = ""){
        $result = $this->dataDetail($key)->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->persediaan_awal_detail_id,
                "data"=>array(
                    $value->m_item_kode,
                    $value->m_item_nama,
                    $value->persediaan_awal_detail_qty,
                    $value->batch_no,
                    $value->tgl_produksi,
                    $value->exp_date
                )
            );
        }

        return $data;
    }

    public function loadDataItem($key = ""){
        $result = $this->dataItem($key)->get();
        $data   = array();
        foreach ($result->result() as $value) {
            $data[] = array(
                "id"        => $value->m_item_kode,
                "data"      => array(
                    $value->m_item_kode,
                    $value->m_item_nama,
                    ($value->m_harga_beli=="")?"0":$value->m_harga_beli,
                    ($value->m_harga_jual=="")?"0":$value->m_harga_jual,
                    '0'
                )
            );
        }

        return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where($this->_kunci($value));

        $total=$this->db->count_all_results();

        return $total;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->persediaan_awal_no] = $row->persediaan_awal_bulan . " - " . $row->persediaan_awal_tahun;
        }

        return $option;
    }

    public function getMaxData(){
        $sql    = $this->db->query("SELECT MAX(RIGHT(persediaan_awal_no,5)) as noMax FROM 1nt3x_inv_persediaan_awal")->row();

        return $sql;
    }
}