<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 03/11/15
 * Time: 11:30
 */

class stok_mutasi_model extends CI_Model {
    private $_table1 = "inv_stok_berjalan";
    private $_table2 = "m_item";

    public function __construct() {
        parent::__construct();

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("stok_berjalan_no_trx" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->select("a.m_item_kode,b.m_item_nama,(SUM(a.stok_berjalan_qty_masuk) - SUM(a.stok_berjalan_qty_keluar)) AS stok");
        $this->db->from($this->_table1 . " a");
        $this->db->join($this->_table2 . " b","a.m_item_kode=b.m_item_kode","left");
        $this->db->group_by("a.m_item_kode");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData($key = ''){
        $result = $this->data($key)->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->m_item_kode,
                "data"=>array(
                    $value->m_item_kode,
                    $value->m_item_nama,
                    $value->stok
                )
            );
        }

        return $data;
    }
}