<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 13/11/15
 * Time: 10:16
 */

class penerimaan_barang_model extends CI_Model {
    private $_table1 = "inv_penerimaan_barang";
    private $_table2 = "inv_penerimaan_barang_detail";
    private $_table3 = "m_item";

    public function __construct() {
        parent::__construct();

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("inv_pen_brg_kode" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->select("a.*");
        $this->db->from($this->_table1 . " a");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function dataDetail($key = ""){
        $this->db->select("a.*,b.m_item_nama");
        $this->db->from($this->_table2 . " a");
        $this->db->join($this->_table3 . " b","a.m_item_kode=b.m_item_kode","left");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData($key = ''){
        $result = $this->data($key)->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->inv_pen_brg_kode,
                "data"=>array(
                    $value->inv_pen_brg_kode,
                    $value->inv_pen_brg_date,
                    $value->inv_pen_brg_no_srt_jln,
                    $value->inv_po_no,
                    $value->inv_pen_brg_ket,
                    $value->inv_pen_brg_petugas
                )
            );
        }

        return $data;
    }

    public function loadDataDetail($key = ""){
        $result = $this->dataDetail($key)->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->inv_pen_brg_detail_kode,
                "data"=>array(
                    $value->m_item_kode,
                    $value->m_item_nama,
                    $value->qty_po_sisa,
                    $value->inv_pen_brg_detail_qty
                )
            );
        }

        return $data;
    }

    public function loadDataPo(){
        $result = $this->db->query("SELECT * FROM 1nt3x_inv_po
                                    WHERE m_toko_kode = '" . $this->SESSION['toko_kode'] . "'");
        $data   = array();
        foreach ($result->result() as $value) {
            $dataDetail = $this->db->query("SELECT
                                    SUM(a.inv_po_detail_qty) AS qty_po
                                FROM
                                    1nt3x_inv_po_detail a
                                WHERE
                                    a.inv_po_no = '" . $value->inv_po_no . "'")->row();
            $dataDetailTerima = $this->db->query("SELECT
                                                SUM(c.inv_pen_brg_detail_qty) AS qty_terima
                                            FROM
                                                1nt3x_inv_penerimaan_barang b
                                            LEFT JOIN 1nt3x_inv_penerimaan_barang_detail c ON c.inv_pen_brg_kode = b.inv_pen_brg_kode
                                            WHERE
                                                b.inv_po_no ='" . $value->inv_po_no . "'")->row();
            $vending    = $dataDetail->qty_po - $dataDetailTerima->qty_terima;
            if($vending > 0){
                $data[] = array(
                    "id" => $value->inv_po_no,
                    "data"=>array(
                        $value->inv_po_no,
                        $value->inv_po_tgl,
                        $value->inv_po_ket,
                        $vending,
                        $value->inv_po_petugas
                    )
                );
            }
        }

        return $data;
    }

    public function loadDataDetailPo($key = ""){
        $this->db->select("a.*,b.m_item_nama");
        $this->db->from("1nt3x_inv_po_detail a");
        $this->db->join($this->_table3 . " b","a.m_item_kode=b.m_item_kode","left");
        $this->db->where(array("inv_po_no" => $key));
        $result = $this->db->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $dataVending    = $this->db->query("SELECT
                                    SUM(a.inv_pen_brg_detail_qty) AS qty_terima
                                FROM
                                    1nt3x_inv_penerimaan_barang_detail a
                                LEFT JOIN
                                    1nt3x_inv_penerimaan_barang b ON a.inv_pen_brg_kode=b.inv_pen_brg_kode
                                WHERE
                                    b.inv_po_no = '" . $key ."' AND a.m_item_kode='" . $value->m_item_kode ."'
                                GROUP BY a.m_item_kode")->row();
            $vending    = $value->inv_po_detail_qty - $dataVending->qty_terima;
            $data[] = array(
                "id" => $value->inv_po_detail_id,
                "data"=>array(
                    $value->m_item_kode,
                    $value->m_item_nama,
                    $vending,
                    '0'
                )
            );
        }

        return $data;
    }

    public function loadDataTb(){
        $result = $this->db->query("SELECT * FROM 1nt3x_inv_transfer_barang
                                    WHERE transfer_barang_tujuan = '" . $this->SESSION['toko_kode'] . "'");
        $data   = array();
        foreach ($result->result() as $value) {
            $dataDetailTransfer = $this->db->query("SELECT
                                                SUM(
                                                    a.transfer_barang_detail_qty
                                                ) AS qty_kirim
                                            FROM
                                                1nt3x_inv_transfer_barang_detail a
                                            WHERE
                                                a.transfer_barang_no = '" . $value->transfer_barang_no . "'")->row();
            $dataDetailTerima = $this->db->query("SELECT
                                                SUM(c.inv_pen_brg_detail_qty) AS qty_terima
                                            FROM
                                                1nt3x_inv_penerimaan_barang b
                                            LEFT JOIN 1nt3x_inv_penerimaan_barang_detail c ON c.inv_pen_brg_kode = b.inv_pen_brg_kode
                                            WHERE
                                                b.inv_po_no ='" . $value->transfer_barang_no . "'")->row();
            $vending    = $dataDetailTransfer->qty_kirim - $dataDetailTerima->qty_terima;
            if($vending > 0){
                $data[] = array(
                    "id" => $value->transfer_barang_no,
                    "data"=>array(
                        $value->transfer_barang_no,
                        $value->transfer_barang_tgl,
                        $value->transfer_barang_ket,
                        $vending,
                        $value->transfer_barang_petugas
                    )
                );
            }
        }

        return $data;
    }

    public function loadDataDetailTb($key = ""){
        $this->db->select("a.*,b.m_item_nama");
        $this->db->from("1nt3x_inv_transfer_barang_detail a");
        $this->db->join($this->_table3 . " b","a.m_item_kode=b.m_item_kode","left");
        $this->db->where(array("transfer_barang_no" => $key));
        $result = $this->db->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $dataVending    = $this->db->query("SELECT
                                    SUM(a.inv_pen_brg_detail_qty) AS qty_terima
                                FROM
                                    1nt3x_inv_penerimaan_barang_detail a
                                LEFT JOIN
                                    1nt3x_inv_penerimaan_barang b ON a.inv_pen_brg_kode=b.inv_pen_brg_kode
                                WHERE
                                    b.inv_po_no = '" . $key ."' AND a.m_item_kode='" . $value->m_item_kode ."'
                                GROUP BY a.m_item_kode")->row();
            $vending    = $value->transfer_barang_detail_qty - $dataVending->qty_terima;
            $data[] = array(
                "id" => $value->transfer_barang_detail_no,
                "data"=>array(
                    $value->m_item_kode,
                    $value->m_item_nama,
                    $vending,
                    '0'
                )
            );
        }

        return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where($this->_kunci($value));

        $total=$this->db->count_all_results();

        return $total;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->inv_po_no] = $row->inv_po_no;
        }

        return $option;
    }

    public function getMaxData(){
        $sql    = $this->db->query("SELECT MAX(RIGHT(inv_pen_brg_kode,5)) as noMax FROM 1nt3x_inv_penerimaan_barang")->row();

        return $sql;
    }
}