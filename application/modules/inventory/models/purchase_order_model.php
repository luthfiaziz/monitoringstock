<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 20/10/15
 * Time: 15:46
 */

class purchase_order_model extends CI_Model {
    private $_table1 = "inv_po";
    private $_table2 = "inv_po_detail";
    private $_table3 = "m_item";
    private $_table4 = "m_suplier";

    public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("inv_po_no" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->select("a.*,b.m_suplier_nama");
        $this->db->from($this->_table1 . " a");
        $this->db->join($this->_table4 . " b","a.m_suplier_kode=b.m_suplier_kode","left");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function dataDetail($key = ""){
        $this->db->select("a.*,b.m_item_nama");
        $this->db->from($this->_table2 . " a");
        $this->db->join($this->_table3 . " b","a.m_item_kode=b.m_item_kode","left");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData($key = ''){
        $result = $this->data($key)->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->inv_po_no,
                "data"=>array(
                    $value->inv_po_no,
                    $value->inv_po_tgl,
                    $value->inv_po_ket,
                    $value->inv_po_grand_total,
                    $value->m_suplier_nama,
                    $value->inv_po_status
                )
            );
        }

        return $data;
    }

    public function loadDataDetail($key = ""){
        $result = $this->dataDetail($key)->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->inv_po_detail_id,
                "data"=>array(
                    $value->m_item_kode,
                    $value->m_item_nama,
                    $value->inv_po_detail_qty,
                    $value->inv_po_detail_harga,
                    $value->inv_po_detail_total,
                    $value->inv_po_detail_diskon,
                    $value->inv_po_detail_grandtotal,
                    $value->inv_po_detail_ket
                )
            );
        }

        return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where($this->_kunci($value));

        $total=$this->db->count_all_results();

        return $total;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->inv_po_no] = $row->inv_po_no;
        }

        return $option;
    }

    public function getMaxData(){
        $sql    = $this->db->query("SELECT MAX(RIGHT(inv_po_no,5)) as noMax FROM 1nt3x_inv_po")->row();

        return $sql;
    }
}