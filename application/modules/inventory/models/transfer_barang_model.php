<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 11/11/15
 * Time: 14:46
 */

class transfer_barang_model extends CI_Model {
    private $_table1 = "inv_transfer_barang";
    private $_table2 = "inv_transfer_barang_detail";
    private $_table3 = "m_item";
    private $_table4 = "inv_stok_berjalan";
    private $_table5 = "m_toko";

    public function __construct() {
        parent::__construct();

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("transfer_barang_no" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->select("a.*,(b.m_toko_nama) AS tujuan");
        $this->db->from($this->_table1 . " a");
        $this->db->join($this->_table5 . " b","a.transfer_barang_tujuan=b.m_toko_kode","left");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function dataDetail($key = ""){
        $this->db->select("a.*,b.m_item_nama");
        $this->db->from($this->_table2 . " a");
        $this->db->join($this->_table3 . " b","a.m_item_kode=b.m_item_kode","left");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData($key = ''){
        $result = $this->data($key)->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->transfer_barang_no,
                "data"=>array(
                    $value->transfer_barang_no,
                    $value->transfer_barang_tgl,
                    $value->transfer_barang_ket,
                    $value->tujuan,
                    $value->transfer_barang_petugas
                )
            );
        }

        return $data;
    }

    public function loadDataDetail($key = ""){
        $result = $this->dataDetail($key)->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->transfer_barang_detail_no,
                "data"=>array(
                    $value->m_item_kode,
                    $value->m_item_nama,
                    $value->transfer_barang_detail_qty
                )
            );
        }

        return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where($this->_kunci($value));

        $total=$this->db->count_all_results();

        return $total;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->stok_opname_no] = $row->stok_opname_no;
        }

        return $option;
    }

    public function getMaxData(){
        $sql    = $this->db->query("SELECT MAX(RIGHT(transfer_barang_no,5)) as noMax FROM 1nt3x_inv_transfer_barang")->row();

        return $sql;
    }
}