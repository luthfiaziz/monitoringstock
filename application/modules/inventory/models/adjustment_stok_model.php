<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 23/10/15
 * Time: 10:30
 */

class adjustment_stok_model extends CI_Model {
    private $_table1 = "inv_adj";
    private $_table2 = "inv_adj_detail";
    private $_table3 = "m_item";
    private $_table4 = "inv_stok_opname";

    public function __construct() {
        parent::__construct();

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("adj_no_trx" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->select("a.*");
        $this->db->from($this->_table1 . " a");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function dataDetail($key = ""){
        $this->db->select("a.*,b.m_item_nama");
        $this->db->from($this->_table2 . " a");
        $this->db->join($this->_table3 . " b","a.m_item_kode=b.m_item_kode","left");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData($key = ''){
        $result = $this->data($key)->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->adj_no_trx,
                "data"=>array(
                    $value->adj_no_trx,
                    $value->adj_tgl,
                    $value->adj_ket,
                    $value->stok_opname_no_trx,
                    $value->adj_tgl_buat,
                    $value->adj_petugas
                )
            );
        }

        return $data;
    }

    public function loadDataDetail($key = ""){
        $result = $this->dataDetail($key)->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->adj_detail_id,
                "data"=>array(
                    $value->m_item_kode,
                    $value->m_item_nama,
                    $value->adj_detail_qtyb,
                    $value->adj_detail_qtyf,
                    $value->adj_detail_qtys,
                    $value->adj_detail_adjust,
                    $value->adj_detail_ket
                )
            );
        }

        return $data;
    }

    public function loadDataOpname(){
        $result = $this->db->query("SELECT * FROM 1nt3x_inv_stok_opname
                                    WHERE m_toko_kode = '" . $this->SESSION['toko_kode'] . "'
                                    AND stok_opname_no NOT IN (SELECT stok_opname_no_trx FROM 1nt3x_inv_adj)"
        );
        $data   = array();

        foreach ($result->result() as $value) {
            $data[] = array(
                "id" => $value->stok_opname_no,
                "data"=>array(
                    $value->stok_opname_no,
                    $value->stok_opname_tgl,
                    $value->stok_opname_ket,
                    $value->stok_opname_petugas
                )
            );
        }

        return $data;
    }

    public function loadDataDetailStokOpname($key = ""){
        $this->db->select("a.*,b.m_item_nama");
        $this->db->from($this->_table4 . "_detail a");
        $this->db->join($this->_table3 . " b","a.m_item_kode=b.m_item_kode","left");
        $this->db->where($key);
        $result = $this->db->get();
        $data   = array();

        foreach ($result->result() as $value) {
            $selisih    = $value->stok_opname_detail_qty_fisik - $value->stok_opname_detail_qty_sys;
            $data[] = array(
                "id" => $value->stok_opname_detail_id,
                "data"=>array(
                    $value->m_item_kode,
                    $value->m_item_nama,
                    $value->stok_opname_detail_qty_sys,
                    $value->stok_opname_detail_qty_fisik,
                    $selisih,
                    $selisih,
                    "-"
                )
            );
        }

        return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where($this->_kunci($value));

        $total=$this->db->count_all_results();

        return $total;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->adj_no_trx] = $row->adj_no_trx;
        }

        return $option;
    }

    public function getMaxData(){
        $sql    = $this->db->query("SELECT MAX(RIGHT(adj_no_trx,5)) as noMax FROM 1nt3x_inv_adj")->row();

        return $sql;
    }
}