<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 03/11/15
 * Time: 11:29
 */

class stok_mutasi extends MX_Controller {
    // VARIABLE MODUL
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();

        // Set Module Location
        $this->_module = 'inventory/stok_mutasi';
        // Load Module Core
        $this->load->module('config/app_core');
        // Load Model
        $this->load->model('stok_mutasi_model','stok_mutasi');

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];

        $data['bulan']  = arrman::get_month();
        $data['tahun']  = arrman::get_year('2010');

        $this->load->view($this->_module . '/index',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD DATA
    //-------------------------------------------------------------------------------------------------------------------------------
    public function listdata($bulan = "",$tahun = ""){
        $periode    = $tahun . "-" . $bulan;
        $key    = array(
            'a.m_toko_kode'                                 => $this->SESSION['toko_kode'],
            'DATE_FORMAT(`a`.`stok_berjalan_tgl`,"%Y-%m")'  => $periode
        );
        $data   = $this->stok_mutasi->loadData($key);

        $result = array("rows" => $data);

        echo json_encode($result);
    }
}