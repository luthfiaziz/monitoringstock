<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 13/11/15
 * Time: 10:16
 */

class penerimaan_barang extends MX_Controller {
    // VARIABLE MODUL
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();

        // Set Module Location
        $this->_module = 'inventory/penerimaan_barang';
        // Load Module Core
        $this->load->module('config/app_core');
        // Load Model
        $this->load->model('penerimaan_barang_model','penerimaan_barang');

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];

        $data['load_form']              = base_url() . $this->_module . '/loadform';
        $data['main_toolbar_source']    = base_url() . $this->_module . '/loadMainToolbar/' . $menu_id;
        $data['data_source']            = base_url() . $this->_module . '/listdata';
        $data['aksesEdit']              = ($this->SESSION["otoritas_menu"][$menu_id]["role_access_edit"] == 1)? "TRUE" : "FALSE";
        $this->load->view($this->_module . '/index',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR MAIN
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadMainToolbar($id = ''){
        xml::xml_header();
        $xml  = '<toolbar>';

        if($this->SESSION["otoritas_menu"][$id]["role_access_add"] == 1){
            $xml .= '<item id="add" text="Tambah" type="button" img="add.png" imgdis="add.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_edit"] == 1){
            $xml .= '<item id="edit" text="Ubah" type="button" img="edit.png" imgdis="edit.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_delete"] == 1){
            $xml .= '<item id="delete" text="Hapus" type="button" img="delete.png" imgdis="delete.png"/>';
        }

        $xml .= '<item id="sep" type="separator"/>';
        $xml .= '<item id="refresh" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png"/>';
        $xml .= "</toolbar>";

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD DATA
    //-------------------------------------------------------------------------------------------------------------------------------
    public function listdata(){
        $key    = array('m_toko_kode' => $this->SESSION['toko_kode']);
        $data   = $this->penerimaan_barang->loadData($key);

        $result = array("rows" => $data);

        echo json_encode($result);
    }

    public function listdataDetail($id = ""){
        $id = explode('-',$id);
        $id = $id[0] . '/' . $id[1] . '/' . $id[2];
        $data   = $this->penerimaan_barang->loadDataDetail($id);

        $result = array("rows" => $data);

        echo json_encode($result);
    }

    public function listPo(){
        $data   = $this->penerimaan_barang->loadDataPo();

        $result = array("rows" => $data);

        echo json_encode($result);
    }

    public function listDataDetailPo($id = ""){
        $id = explode('-',$id);
        $id = $id[0] . '/' . $id[1] . '/' . $id[2];
        $data   = $this->penerimaan_barang->loadDataDetailPo($id);

        $result = array("rows" => $data);

        echo json_encode($result);
    }

    public function listTb(){
        $data   = $this->penerimaan_barang->loadDataTb();

        $result = array("rows" => $data);

        echo json_encode($result);
    }

    public function listDataDetailTb($id = ""){
        $id = explode('-',$id);
        $id = $id[0] . '/' . $id[1] . '/' . $id[2] . '/' . $id[3] . '/' . $id[4];
        $data   = $this->penerimaan_barang->loadDataDetailTb($id);

        $result = array("rows" => $data);

        echo json_encode($result);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR FORM
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadFormToolbar(){
        xml::xml_header();

        $xml  = '<toolbar>';
        $xml .= '<item id="saveForm" text="Simpan" type="button" img="save.png" imgdis="save.png" action="simpan"/>';
        $xml .= '<item id="page_setup" type="buttonSelect" img="import-files.png" imgdis="import-files.png" text="Import">
                    <item type="button" id="po" text="Purcase Order" action="openPo"/>
                    <item type="button" id="tb" text="Transfer Barang" action="openTb"/>
                 </item>';
        $xml .= "</toolbar>";

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD FORM
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadform($id = ''){
        $data["readonly"]   = false;
        $data['form_action']= base_url() . $this->_module . '/proses/0';

        if (!empty($id)) {
            $id = explode('-',$id);
            $id = $id[0] . '/' . $id[1] . '/' . $id[2];
            $data['form_action'] = base_url() . $this->_module . '/proses/1';
            $data["default"]  = $this->penerimaan_barang->data($id)->get()->row();
            $data["readonly"] = true;
        }

        $this->load->view($this->_module . '/form',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - SAVE DATA
    //-------------------------------------------------------------------------------------------------------------------------------
    public function proses($id){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $this->form_validation->set_rules('data[inv_po_no]', 'No Ref', 'trim|required');

        if ($this->form_validation->run($this)) {

            $dataIns                        = $this->input->post('data');
            $dataIns['m_toko_kode']         = $this->SESSION['toko_kode'];
            $dataIns['inv_pen_brg_petugas'] = $this->SESSION['user_username'];

            $this->db->trans_begin();
            if ($id == '0') {
                $no_trx                         = $this->generateNo($dataIns);
                $dataIns['inv_pen_brg_kode']    = $no_trx;
                $dataIns['inv_pen_brg_tgl_buat']= date("Y-m-d");

                $crud->save_as_new_nrb('inv_penerimaan_barang', arrman::StripTagFilter($dataIns));

                $dataLog    = array(
                    'log_user'      => $this->SESSION['user_username'],
                    'log_kegiatan'  => "Simpan data Transfer Barang " . $no_trx,
                    'log_waktu'     => date('Y-m-d H:i:s')
                );
                $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
            } else {
                $no_trx                         = $this->input->post('inv_pen_brg_kode');
                $dataIns['inv_pen_brg_tgl_ubah']= date("Y-m-d");

                $key = array("inv_pen_brg_kode"=> $no_trx);
                $crud->save_nrb('inv_penerimaan_barang', arrman::StripTagFilter($dataIns), $key);

                $dataLog    = array(
                    'log_user'      => $this->SESSION['user_username'],
                    'log_kegiatan'  => "Rubah data Transfer Barang " . $no_trx,
                    'log_waktu'     => date('Y-m-d H:i:s')
                );
                $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
            }

            $keyDet = array('inv_pen_brg_kode' => $no_trx);
            $crud->delete_nrb('inv_penerimaan_barang_detail',$keyDet);
            $keySB  = array('stok_berjalan_no_trx' => $no_trx);
            $crud->delete_nrb('inv_stok_berjalan',$keySB);
            $dataGrid   = $this->input->post("datagrid");
            if($dataGrid != ""){
                foreach($dataGrid as $val){
                    $dataInsDet = array(
                        'inv_pen_brg_kode'          => $no_trx,
                        'inv_pen_brg_detail_qty'    => $val[3],
                        'm_item_kode'               => $val[0],
                        'qty_po_sisa'               => $val[2]
                    );
                    $crud->save_as_new_nrb('inv_penerimaan_barang_detail', arrman::StripTagFilter($dataInsDet));

                    $dataInsSB  = array(
                        'stok_berjalan_tgl'     => $dataIns['inv_pen_brg_date'],
                        'stok_berjalan_ket'     => "Penerimaan Barang",
                        'stok_berjalan_no_trx'  => $no_trx,
                        'stok_berjalan_tgl_buat'=> date('Y-m-d'),
                        'stok_berjalan_petugas' => $this->SESSION['user_username'],
                        'm_toko_kode'           => $this->SESSION['toko_kode'],
                        'm_item_kode'           => $val[0],
                        'stok_berjalan_qty_masuk'=> $val[3],
                        'stok_berjalan_qty_keluar'=> 0
                    );
                    $crud->save_as_new_nrb('inv_stok_berjalan', arrman::StripTagFilter($dataInsSB));
                }
            }

            if ($this->db->trans_status() === FALSE) {
                $message = array(true, 'Proses Gagal', 'Proses penyimpanan data gagal ( data di rollback ).', '');
                $this->db->trans_rollback();
            } else {
                $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh()');
                $this->db->trans_commit();
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors("<span class='error_mess'>","</span>"), "");
        }

        echo json_encode($message,true);
    }

    /*CUSTOM VALIDATION - cek id jika ada*/
    public function ketersediaan_check(){
        $key                = $this->input->post('data');
        $key['m_toko_kode'] = $this->SESSION['toko_kode'];

        $ada = $this->purchase_order->hitungJikaAda($key);

        if ($ada > 0)
        {
            $this->form_validation->set_message('ketersediaan_check','Persediaan awal bulan <b>' . $key['persediaan_awal_bulan'] . '</b> di tahun <b>' . $key['persediaan_awal_tahun'] . '</b> sudah ada.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function generateNo(){
        $dataMax    = $this->penerimaan_barang->getMaxData();
        $noNow      = $dataMax->noMax + 1;
        $noNew      = "PB" . "/" . date('mY') . "/" . sprintf("%05s",$noNow);

        return $noNew;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - DELETE DATA
    //-------------------------------------------------------------------------------------------------------------------------------
    public function delete() {
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $this->db->trans_begin();

        $key = array("inv_pen_brg_kode"=> $this->input->post("id"));
        $crud->delete_nrb("inv_penerimaan_barang",$key);
        $keySB  = array('stok_berjalan_no_trx' => $this->input->post("id"));
        $crud->delete_nrb('inv_stok_berjalan',$keySB);

        $dataLog    = array(
            'log_user'      => $this->SESSION['user_username'],
            'log_kegiatan'  => "Hapus data Penerimaan Barang" . $this->input->post('id'),
            'log_waktu'     => date('Y-m-d H:i:s')
        );
        $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));

        if ($this->db->trans_status() === FALSE) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            $this->db->trans_rollback();
        } else {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '');
            $this->db->trans_commit();
        }

        echo json_encode($message);
    }
}