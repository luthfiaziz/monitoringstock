<?php
/**
 * User: Bayu Nugraha (bayu.nugraha.dm@gmail.com)
 * Date: 23/10/15
 * Time: 10:30
 */

class adjustment_stok extends MX_Controller {
    // VARIABLE MODUL
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();

        // Set Module Location
        $this->_module = 'inventory/adjustment_stok';
        // Load Module Core
        $this->load->module('config/app_core');
        // Load Model
        $this->load->model('adjustment_stok_model','adjustment_stok');

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];

        $data['load_form']              = base_url() . $this->_module . '/loadform';
        $data['main_toolbar_source']    = base_url() . $this->_module . '/loadMainToolbar/' . $menu_id;
        $data['data_source']            = base_url() . $this->_module . '/listdata';
        $data['aksesEdit']              = ($this->SESSION["otoritas_menu"][$menu_id]["role_access_edit"] == 1)? "TRUE" : "FALSE";
        $this->load->view($this->_module . '/index',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR MAIN
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadMainToolbar($id = ''){
        xml::xml_header();
        $xml  = '<toolbar>';

        if($this->SESSION["otoritas_menu"][$id]["role_access_add"] == 1){
            $xml .= '<item id="add" text="Tambah" type="button" img="add.png" imgdis="add.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_edit"] == 1){
            $xml .= '<item id="edit" text="Ubah" type="button" img="edit.png" imgdis="edit.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_delete"] == 1){
            $xml .= '<item id="delete" text="Hapus" type="button" img="delete.png" imgdis="delete.png"/>';
        }

        $xml .= '<item id="sep" type="separator"/>';
        $xml .= '<item id="refresh" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png"/>';
        $xml .= "</toolbar>";

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD DATA
    //-------------------------------------------------------------------------------------------------------------------------------
    public function listdata(){
        $key    = array('m_toko_kode' => $this->SESSION['toko_kode']);
        $data   = $this->adjustment_stok->loadData($key);

        $result = array("rows" => $data);

        echo json_encode($result);
    }

    public function listdataDetail($id = ""){
        $id = explode('-',$id);
        $id = $id[0] . '/' . $id[1] . '/' . $id[2];
        $data   = $this->adjustment_stok->loadDataDetail($id);

        $result = array("rows" => $data);

        echo json_encode($result);
    }

    public function listStokOpname(){
        $data   = $this->adjustment_stok->loadDataOpname();

        $result = array("rows" => $data);

        echo json_encode($result);
    }

    public function listDataDetailStokOpname($id = ""){
        $id = explode('-',$id);
        $id = $id[0] . '/' . $id[1] . '/' . $id[2];
        $id = array("stok_opname_no" => $id);
        $data   = $this->adjustment_stok->loadDataDetailStokOpname($id);

        $result = array("rows" => $data);

        echo json_encode($result);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR FORM
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadFormToolbar(){
        xml::xml_header();

        $xml  = '<toolbar>';
        $xml .= '<item id="saveForm" text="Simpan" type="button" img="save.png" imgdis="save.png" action="simpan"/>';
        $xml .= "</toolbar>";

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD FORM
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadform($id = ''){
        $data["readonly"]   = false;
        $data['form_action']= base_url() . $this->_module . '/proses/0';
        $data['bulan']      = arrman::get_month();
        $data['tahun']      = arrman::get_year(2010);

        if (!empty($id)) {
            $id = explode('-',$id);
            $id = $id[0] . '/' . $id[1] . '/' . $id[2];
            $data['form_action'] = base_url() . $this->_module . '/proses/1';
            $data["default"]  = $this->adjustment_stok->data($id)->get()->row();
            $data["readonly"] = true;
        }

        $this->load->view($this->_module . '/form',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - SAVE DATA
    //-------------------------------------------------------------------------------------------------------------------------------
    public function proses($id){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $this->form_validation->set_rules('data[stok_opname_no_trx]', 'No Stok Opname', 'trim|required');

        if ($this->form_validation->run($this)) {

            $dataIns                = $this->input->post('data');
            $dataIns['m_toko_kode'] = $this->SESSION['toko_kode'];
            $dataIns['adj_petugas'] = $this->SESSION['user_username'];

            $this->db->trans_begin();
            if ($id == '0') {
                $no_trx                     = $this->generateNo($dataIns);
                $dataIns['adj_no_trx']      = $no_trx;
                $dataIns['adj_tgl_buat']    = date("Y-m-d");

                $crud->save_as_new_nrb('inv_adj', arrman::StripTagFilter($dataIns));

                $dataLog    = array(
                    'log_user'      => $this->SESSION['user_username'],
                    'log_kegiatan'  => "Simpan data adjustment " . $no_trx,
                    'log_waktu'     => date('Y-m-d H:i:s')
                );
                $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
            } else {
                $no_trx                     = $this->input->post('adj_no_trx');
                $dataIns['adj_tgl_ubah']    = date("Y-m-d");

                $key = array("adj_no_trx"=> $no_trx);
                $crud->save_nrb('inv_adj', arrman::StripTagFilter($dataIns), $key);

                $dataLog    = array(
                    'log_user'      => $this->SESSION['user_username'],
                    'log_kegiatan'  => "Simpan data POS",
                    'log_waktu'     => date('Y-m-d H:i:s')
                );
                $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));

                $dataLog    = array(
                    'log_user'      => $this->SESSION['user_username'],
                    'log_kegiatan'  => "Rubah data adjustment " . $no_trx,
                    'log_waktu'     => date('Y-m-d H:i:s')
                );
                $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));
            }

            $keyDet = array('adj_no_trx' => $no_trx);
            $crud->delete_nrb('inv_adj_detail',$keyDet);
            $keySB  = array('stok_berjalan_no_trx' => $no_trx);
            $crud->delete_nrb('inv_stok_berjalan',$keySB);
            $dataGrid   = $this->input->post("datagrid");
            if($dataGrid != ""){
                foreach($dataGrid as $val){
                    $dataInsDet = array(
                        'adj_no_trx'        => $no_trx,
                        'adj_detail_ket'    => $val[6],
                        'm_item_kode'       => $val[0],
                        'adj_detail_qtyb'   => $val[2],
                        'adj_detail_qtyf'   => $val[3],
                        'adj_detail_qtys'   => $val[4],
                        'adj_detail_adjust' => $val[5]
                    );
                    $crud->save_as_new_nrb('inv_adj_detail', arrman::StripTagFilter($dataInsDet));

                    $dataInsSB  = array(
                        'stok_berjalan_tgl'     => $dataIns['adj_tgl'],
                        'stok_berjalan_ket'     => "Adjustment Stok",
                        'stok_berjalan_no_trx'  => $no_trx,
                        'stok_berjalan_tgl_buat'=> date('Y-m-d'),
                        'stok_berjalan_petugas' => $this->SESSION['user_username'],
                        'm_toko_kode'           => $this->SESSION['toko_kode'],
                        'm_item_kode'           => $val[0],
                        'stok_berjalan_qty_masuk'=> $val[5],
                        'stok_berjalan_qty_keluar'=> 0
                    );
                    $crud->save_as_new_nrb('inv_stok_berjalan', arrman::StripTagFilter($dataInsSB));
                }
            }

            if ($this->db->trans_status() === FALSE) {
                $message = array(true, 'Proses Gagal', 'Proses penyimpanan data gagal ( data di rollback ).', '');
                $this->db->trans_rollback();
            } else {
                $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh()');
                $this->db->trans_commit();
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors("<span class='error_mess'>","</span>"), "");
        }

        echo json_encode($message,true);
    }

    /*CUSTOM VALIDATION - cek id jika ada*/
    public function ketersediaan_check(){
        $key                = $this->input->post('data');
        $key['m_toko_kode'] = $this->SESSION['toko_kode'];

        $ada = $this->adjustment_stok->hitungJikaAda($key);

        if ($ada > 0)
        {
            $this->form_validation->set_message('ketersediaan_check','Persediaan awal bulan <b>' . $key['persediaan_awal_bulan'] . '</b> di tahun <b>' . $key['persediaan_awal_tahun'] . '</b> sudah ada.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function generateNo(){
        $dataMax    = $this->adjustment_stok->getMaxData();
        $noNow      = $dataMax->noMax + 1;
        $noNew      = "AS" . "/" . date('mY') . "/" . sprintf("%05s",$noNow);

        return $noNew;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - DELETE DATA
    //-------------------------------------------------------------------------------------------------------------------------------
    public function delete() {
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $this->db->trans_begin();

        $key = array("adj_no_trx"=> $this->input->post("id"));
        $crud->delete("inv_adj",$key);
        $keySB  = array('stok_berjalan_no_trx' => $this->input->post("id"));
        $crud->delete_nrb('inv_stok_berjalan',$keySB);

        $dataLog    = array(
            'log_user'      => $this->SESSION['user_username'],
            'log_kegiatan'  => "Hapus data adjustment " . $this->input->post('id'),
            'log_waktu'     => date('Y-m-d H:i:s')
        );
        $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));

        if ($this->db->trans_status() === FALSE) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            $this->db->trans_rollback();
        } else {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '');
            $this->db->trans_commit();
        }

        echo json_encode($message);
    }
}