<div id="penjualanloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:30%;"/>
</div>
<div class="form-wrapper">
    <?php
    $hidden_form = array('id' => !empty($id) ? $id : '');
    echo form_open_multipart($form_action, array('id' => 'fm_penjualan'), $hidden_form);
    ?>
    
    <table class="form" width="100%">
        <tr>
            <td>
                <table class="form" width="100%">
        <tr>
            <td>
                <span><b>No.Penjualan/Invoice</b> <i>*</i></span>
                <?php 
                    $penjualan_id = !empty($default->trx_pos_no) ? $default->trx_pos_no : '';
                    $penjualan_id = explode(".", $penjualan_id);
                    $readonly     = ($readonly == true) ? "readonly" : '';
                    echo form_input('trx_pos_no', !empty($default->trx_pos_no) ? $default->trx_pos_no : $no_invoice, 'id="trx_pos_no" class="form_web uppercase onlynumber" placeholder="wajib diisi" readonly');
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Tanggal</b></span>
                <?php echo form_input('trx_pos_tgl', !empty($default->trx_pos_tgl) ? $default->trx_pos_tgl : date('d-m-Y'), 'id="trx_pos_tgl" class="form_web uppercase" readonly'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <span><b>Keterangan</b></span>
                <?php echo form_input('trx_pos_ket', !empty($default->trx_pos_ket) ? $default->trx_pos_ket : '', 'id="trx_pos_ket" class="form_web uppercase"'); ?>
            </td>
        </tr>
       
        
</table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            Kode
                        </td>
                        <td>
                            Nama 
                        </td>
                        <td>
                            Qty
                        </td>
                        <td>
                            Harga
                        </td>
                         <td>
                            Disc(%)
                        </td>
                        <td>
                            Total
                        </td>
                        <td>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo form_dropdown('m_item_no_kode', $produk_options, !empty($default->m_item_no_kode) ? $default->m_item_no_kode : '', 'id="m_item_no_kode" class="full"'); ?>
                        </td>
                        <td>
                             <?php echo form_input('m_item_no_kode_tmp', !empty($default->m_item_no_kode_tmp) ? $default->m_item_no_kode_tmp : '', 'id="m_item_no_kode_tmp" class="form_web uppercase" hidden'); ?>
                            <?php echo form_input('m_item_nama', !empty($default->m_item_nama) ? $default->m_item_nama : '', 'id="m_item_nama" class="form_web uppercase" readonly'); ?>
                        </td>
                        <td>
                            <?php echo form_input('trx_pos_detail_qty', !empty($default->trx_pos_detail_qty) ? $default->trx_pos_detail_qty : '', 'id="trx_pos_detail_qty" class="form_web onlynumber disableEnable" onkeyup="hitungtotal()"'); ?>
                        </td>
                        <td>
                            <?php echo form_input('m_item_harga', !empty($default->m_item_harga) ? $default->m_item_harga : '', 'id="m_item_harga" class="form_web uppercase" readonly'); ?>
                        </td>
                        <td>
                            <?php echo form_input('trx_pos_detail_disc', !empty($default->trx_pos_detail_disc) ? $default->trx_pos_detail_disc : '', 'id="trx_pos_detail_disc" class="form_web onlynumber disableEnable" onkeyup="hitungtotal()"'); ?>
                        </td>
                        <td>
                            <?php echo form_input('trx_pos_detail_total', !empty($default->trx_pos_detail_total) ? $default->trx_pos_detail_total : '', 'id="trx_pos_detail_total" class="form_web uppercase" readonly'); ?>
                        </td>
                        <td>
                            <input type="button" name="btn_penjualan" id="btn_penjualan" value=" Add " onclick="tbh_barang()"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <div style="width: 100%; height: 200px;" id="gridpenjualan"></div>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td width="30%">
                            <input type='button' value='Delete' onclick='deleteRowPenjualan()' class='submitSum' style='width: 50px;'>
                            <input type='button' value='Bayar' onclick='addRowPenjualan()' class='submitSum' style='width: 50px;'>

                        </td>
                        <td width="30%">
                            
                        </td>
                        <td width="40%">
                            <table>
                                <tr>
                                    <td>
                                        Total
                                    </td>
                                    <td>
                                        <?php echo form_input('trx_pos_sub_total', !empty($default->trx_pos_sub_total) ? $default->trx_pos_sub_total : '', 'id="trx_pos_sub_total" class="form_web" readonly'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Diskon (%)
                                    </td>
                                    <td>
                                         <?php echo form_input('trx_pos_diskon', !empty($default->trx_pos_diskon) ? $default->trx_pos_diskon : '', 'id="trx_pos_diskon" class="form_web onlynumber disableEnable" onkeyup="hitunggrandtotal()"'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Pajak (%)
                                    </td>
                                    <td>
                                        <?php echo form_input('trx_pos_tax', !empty($default->trx_pos_tax) ? $default->trx_pos_tax : '', 'id="trx_pos_tax" class="form_web onlynumber disableEnable" onkeyup="hitunggrandtotal()"'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Grand Total
                                    </td>
                                    <td>
                                        <?php echo form_input('trx_pos_grand_total', !empty($default->trx_pos_grand_total) ? $default->trx_pos_grand_total : '', 'id="trx_pos_grand_total" class="form_web" readonly'); ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                            </td>
        </tr>
    </table>    

    <?php 
        echo form_close(); 
        echo arrman::getRequired();
    ?>
</div>
<script type="text/javascript">
    //$("#booking_id").select2();
    //$("#member_kode").select2();
    $("#m_item_no_kode").select2();
    var pembayaranWindows  = new dhtmlXWindows();
    
     $("#m_item_no_kode","#fm_penjualan").change(function(){
            $('#trx_pos_detail_qty').val("");
            $('#m_item_nama').val("");
            $('#m_item_harga').val("");
            $('#trx_pos_detail_disc').val("");
            $('#trx_pos_detail_total').val("");
            
            var arrCurrency = $(this).val().split('|');
            $('#m_item_no_kode_tmp').val(arrCurrency[0]);
            $('#m_item_nama').val(arrCurrency[1]);
            $('#m_item_harga').val(arrCurrency[2]);
        });

    function simpan(id){
        var toolbar = ["penjualanWindows","savepenjualan","cancelpenjualan"];

        SimpanData('#fm_penjualan','#penjualanloading',toolbar);
    }

    function refresh(){
        close_form_modal(penjualanWindows,"penjualanWindows");
        refreshGrid(gridpenjualan,url + "penjualan/penjualan/listdata","json");
    }
    
    function bersihpenjualan(){
        var data = [
                       
                        ["m_item_no_kode","","combo"],
                    ];

        var focus = ["trx_pos_no","input"];

        refreshForm(data, "#fm_penjualan", focus);
    }
    
    gridpenjualanForm = new dhtmlXGridObject("gridpenjualan");
    gridpenjualanForm.setHeader("Qty,Harga,Discont,total,kode barang,trx pos no, Kode, Nama, Qty, Harga, Discount,Total");
    gridpenjualanForm.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left");
    gridpenjualanForm.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
    gridpenjualanForm.setInitWidths("*,*,*,*,*,*,*,*,*,*,*,*");
    gridpenjualanForm.attachFooter("&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;");
    gridpenjualanForm.init();
    <?php
    if(!empty($data_source4)){
        echo "gridpenjualanForm.load('".$data_source4."','json');";
    }
    ?>
    

    function tbh_barang(){
        var produk_nama                 = $("#m_item_nama").val();
        var qty                         = $('#trx_pos_detail_qty').val();

        if(produk_nama == ''){
            alert("Produk harus dipilih");
        
        }else if(qty == ''){
            alert("qty tidak boleh kosong");
        
        }else if(qty == '0'){
            alert("qty tidak boleh kosong");
        
        }else{
            var no   = 1;
            gridpenjualanForm.forEachRow(function(id){
                no++;
            });
            var id = gridpenjualanForm.uid();
           
            var col1 = $('#trx_pos_detail_qty').val();
            var col2 = $('#m_item_harga').val();
            var col3 = $('#trx_pos_detail_disc').val();
            var col4 = $('#trx_pos_detail_total').val();
            var col5 = $('#m_item_no_kode_tmp').val();
            var col6 = $('#trx_pos_no').val();
          

            var posisi = gridpenjualanForm.getRowsNum();
            gridpenjualanForm.addRow(id, [col1,col2,col3,col4,col5,col6,col5,produk_nama,col1,col2,col3,col4], posisi);
            
            $('#trx_pos_detail_qty').val("");
            $('#m_item_nama').val("");
            $('#m_item_harga').val("");
            $('#trx_pos_detail_disc').val("");
            $('#trx_pos_detail_total').val("");
            
            var jml_grand_total = sumColumn(11);
            $("#trx_pos_sub_total").val(jml_grand_total);
            hitunggrandtotal();

            
        }
    }
    
     function deleteRowPenjualan(){
        var rId = gridpenjualanForm.getSelectedRowId();
        if(rId==null){
            dhtmlx.alert("Choose a data.");
            return;
        } else {
            gridpenjualanForm.deleteSelectedRows();
            
            var jml_grand_total = sumColumn(11);
            $("#trx_pos_sub_total").val(jml_grand_total);
            hitunggrandtotal();
            
        }
    }
    
    function hitungtotal(){
      
      var qty =  $("#trx_pos_detail_qty").val();
      var harga =  $("#m_item_harga").val();
      var diskon =  $("#trx_pos_detail_disc").val();
      
      
      
      var total = qty * harga ;
      var diskonnya = total * (diskon / 100);
      var total2 = total - diskonnya;
      
      
      
      $("#trx_pos_detail_total").val(total2);
      
  }
  
  function sumColumn(ind) {
     var out = 0;
     for (var i = 0; i < gridpenjualanForm.getRowsNum(); i++) {
         out += parseInt(gridpenjualanForm.cells2(i, ind).getValue());
         
         
         
     }
     return out;
  }
  
  function hitunggrandtotal(){
      
      var trx_pos_sub_total =  $("#trx_pos_sub_total").val();
      var trx_pos_diskon    =  $("#trx_pos_diskon").val();
      var trx_pos_tax       =  $("#trx_pos_tax").val();
      
      
      var diskon = (trx_pos_diskon / 100) * trx_pos_sub_total;
      
      
      
      var total = trx_pos_sub_total - diskon;
      var pajak = (trx_pos_tax / 100) * total;
      var grandtotal = total + pajak;
      $("#trx_pos_grand_total").val(grandtotal);
      
      
      
  }
  
  function addRowPenjualan(){
      
         var trx_pos_no     = $("#trx_pos_no").val();
         var trx_pos_tgl    = $("#trx_pos_tgl").val();
         var trx_pos_ket    = $("#trx_pos_ket").val();
         
         var trx_pos_sub_total      = $("#trx_pos_sub_total").val();
         var trx_pos_diskon         = $("#trx_pos_diskon").val();
         var trx_pos_tax            = $("#trx_pos_tax").val();
         var trx_pos_grand_total    = $("#trx_pos_grand_total").val();
        
    
      var jml = gridpenjualanForm.getRowsNum();
      //alert(jml);
      if(jml == 0){
          alert("Maaf, transaksi tidak boleh kosong.");
      }else{
          
          var data = {
							trx_pos_no:trx_pos_no,
                                                        trx_pos_tgl:trx_pos_tgl,
                                                        trx_pos_ket:trx_pos_ket,
                                                        
                                                        trx_pos_sub_total:trx_pos_sub_total,
                                                        trx_pos_diskon:trx_pos_diskon,
                                                        trx_pos_tax:trx_pos_tax,
                                                        trx_pos_grand_total:trx_pos_grand_total,
                                                        data_grid: getData(gridpenjualanForm,[])
                                                        
                                                    
                                                        
                                                       
						};
                                                                                $.ajax({
                                                                                type: "POST",
                                                                                url  : url + "penjualan/penjualan/proses",
                                                                                dataType: "json",
                                                                                data:data,
           
                                                                                    success: function(result){
                                                                                            
  
                                                                                             var module  = url + "penjualan/penjualan";
                                                                                              form_modal(pembayaranWindows,"pembayaranWindows",["Pembayaran",500,600], module + "/loadFormpembayaranToolbar", module + "/loadform2?trx_pos_no="+trx_pos_no+"&trx_pos_sub_total="+trx_pos_sub_total+"&trx_pos_diskon="+trx_pos_diskon+"&trx_pos_tax="+trx_pos_tax+"&trx_pos_grand_total="+trx_pos_grand_total);
                                                                                              close_form_modal(penjualanWindows,"penjualanWindows");
                   
               
                                                                                    }
                                                                                });	  																	

        
        
      }
  }
  
  function getData(grid,arrEx) {
	rowsnum = grid.getColumnCount();
	x = 1; data = "";
	grid.forEachRow(function(id){
		if(x==1) { spt = ""; } else { spt = "~"; }
		rows = "";
		n = 0;
		for(i=0;i<rowsnum;i++) {
			if(arrEx == "") {
				if(i==0) { sptc = ""; } else { sptc = "`"; }
				rows = rows+sptc+grid.cells(id,i).getValue(); 
			} else {
				if(include(arrEx,i)!=true) {
					if(n==0) { sptc = ""; } else { sptc = "`"; }
					rows = rows+sptc+grid.cells(id,i).getValue(); 
					n++;
				}	
			}
		}
		data = data+spt+rows;
		x++;
    });
	return data;
} 
</script>