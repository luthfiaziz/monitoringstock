

<div id="pembayaranloading" class="imageloading"></div>
<div class="form-wrapper">
    <?php
    
    
    $hidden_form = array('id' => !empty($id) ? $id : '');
    echo form_open_multipart($form_action, array('id' => 'fm_pembayaran'), $hidden_form);
    
    
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <table>
                    <tr>
                        <td width="25%">
                                <span><b>Total</b></span>
                               
                        </td>
                        <td>
                               <?php echo form_input('trx_pos_no', !empty($default->trx_pos_no) ? $default->trx_pos_no : $trx_pos_no, 'id="trx_pos_no" class="form_web" style="display:none" disabled'); ?>

                              <?php echo form_input('trx_pos_sub_total', !empty($default->trx_pos_sub_total) ? $default->trx_pos_sub_total : $trx_pos_sub_total, 'id="trx_pos_sub_total" class="form_web" disabled'); ?>

                        </td>
                       
                    </tr>
                     <tr>
                        <td>
                                 <span><b>Diskon (%)</b></span>
                        </td>
                        <td>
                                <?php echo form_input('trx_pos_diskon', !empty($default->trx_pos_diskon) ? $default->trx_pos_diskon : $trx_pos_diskon, 'id="trx_pos_diskon" class="form_web" disabled'); ?>

                        </td>
                        
                    </tr>
                     <tr>
                        <td>
                               <span><b>Pajak (%)</b></span>
                        </td>
                        <td>
                              <?php echo form_input('trx_pos_tax', !empty($default->trx_pos_tax) ? $default->trx_pos_tax : $trx_pos_tax, 'id="trx_pos_tax" class="form_web" disabled'); ?>

                        </td>
                     
                    </tr>
                     <tr>
                        <td>
                               <span><b>Grand Total</b></span>
                        </td>
                        <td>
                              <?php echo form_input('trx_pos_grand_total', !empty($default->trx_pos_grand_total) ? $default->trx_pos_grand_total : $trx_pos_grand_total, 'id="trx_pos_grand_total" class="form_web" disabled'); ?>

                        </td>
                     
                    </tr>
                    <tr>
                        <td>
                               <span><b>Pembayaran</b></span>
                        </td>
                        <td>
                               <?php echo form_dropdown('trx_pos_jenis_pembayaran', array(""=>"--Pilih Pembayaran--","Cash"=>"Cash", "Debit"=>"Debit", "Credit"=>"Credit"), !empty($default->trx_pos_jenis_pembayaran) ? $default->trx_pos_jenis_pembayaran : '', 'id="trx_pos_jenis_pembayaran" class="full"'); ?>

                        </td>
                     
                    </tr>
                    <tr>
                        <td>
                               <span><b>Jumlah Bayar</b></span>
                        </td>
                        <td>
                               <?php echo form_input('trx_pos_bayar', !empty($default->trx_pos_bayar) ? $default->trx_pos_bayar : '0', 'id="trx_pos_bayar" class="form_web onlynumber disableEnable" onkeyup="hitungbayar()" disabled'); ?>

                        </td>
                     
                    </tr>
                     <tr>
                        <td>
                               <span><b>Bank</b></span>
                        </td>
                        <td>
                               <?php echo form_dropdown('bank_id', $bank_options, !empty($default->bank_id) ? $default->bank_id : '', 'id="bank_id" class="full" disabled'); ?>

                        </td>
                     
                    </tr>
                     <tr>
                        <td>
                               <span><b>No.Kartu</b></span>
                        </td>
                        <td>
                               <?php echo form_input('trx_pos_no_kartu', !empty($default->trx_pos_no_kartu) ? $default->trx_pos_no_kartu : '', 'id="trx_pos_no_kartu" class="form_web" disabled'); ?>

                        </td>
                     
                    </tr>
                    </table>
                           </td>
        </tr>
        
      
        
        <tr>
            <td>
                <table>
                    <tr>
                        <td width="25%"></td>
                        <td width="50%">
                            <table>
                                <tr>
                                    <td><b><h3><center>KEMBALIAN</center></h3></b></td>
                                </tr>
                                 <tr>
                                     <td>
                                 <center>
                                     <?php echo form_input('kembalian', !empty($default->kembalian) ? $default->kembalian : '', 'id="kembalian" class="form_web" style="display:none"'); ?>
                                     <h1><span id="totalhasil"></span></h1>
                                 </center>
                                     </td>
                                </tr>
                                 <tr>
                                     <td>
                                <center>
                                     <input type="button" name="btnproses" id="btnproses" value=" CETAK INVOICE " onclick="invoice()"/> 
                                </center>
                                </td>
                                </tr>
                            </table>
                        </td>
                        <td width="25%"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <?php 
        echo form_close(); 
        //echo arrman::getRequired();
    ?>
</div>
<script type="text/javascript">
    
    $("#bank_id").select2();
    $("#trx_pos_jenis_pembayaran").select2();
    
     $("#trx_pos_jenis_pembayaran","#fm_pembayaran").change(function(){
            var jenis = $('#trx_pos_jenis_pembayaran').val();
            if(jenis == 'cash'){
                $("#trx_pos_bayar").removeAttr("disabled");
                
            }else{
                $("#trx_pos_bayar").removeAttr("disabled");
                $("#bank_id").removeAttr("disabled");
                $("#trx_pos_no_kartu").removeAttr("disabled");
                //$("#nama_pemilik_kartu").removeAttr("disabled");
            }
           
        });
        
     var invoiceWindows  = new dhtmlXWindows();
     
      function hitungbayar(){
          
        var trx_pos_grand_total      =$("#trx_pos_grand_total").val();
        var trx_pos_bayar            =$("#trx_pos_bayar").val();
    
        
        var kembali = trx_pos_bayar - trx_pos_grand_total;
        $("#kembalian").val(kembali);
       
        var rp = toRp(kembali);
        document.getElementById( 'totalhasil' ).innerHTML = rp;
      }
    
 function toRp(angka){
    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
    var rev2    = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += '.';
        }
    }
    return 'Rp. ' + rev2.split('').reverse().join('') + ',00';
}
  
  function invoice(){
      
         var trx_pos_no                         = $("#trx_pos_no").val();
         var trx_pos_jenis_pembayaran           = $("#trx_pos_jenis_pembayaran").val();
         var trx_pos_bayar                      = $("#trx_pos_bayar").val();
         var bank_id                            = $("#bank_idk").val();
         var trx_pos_no_kartu                   = $("#trx_pos_no_kartu").val();
         var kembalian                          = $("#kembalian").val();
        
         
         if(trx_pos_bayar == ''){
             
             alert("Pembayaran tidak boleh kosong");
             
         }else if(trx_pos_bayar == '0'){
             
             alert("Pembayaran tidak boleh 0");
             
         }else{
        
              var data = {
							trx_pos_no:trx_pos_no,
                                                        trx_pos_jenis_pembayaran:trx_pos_jenis_pembayaran,
                                                        trx_pos_bayar:trx_pos_bayar,
                                                        bank_id:bank_id,
                                                        trx_pos_no_kartu:trx_pos_no_kartu,
                                                        //nama_pemilik_kartu:nama_pemilik_kartu,
                                                        kembalian:kembalian
                                                      
                                                        
                                                       
						};
                                                                                $.ajax({
                                                                                type: "POST",
                                                                                url  : url + "penjualan/penjualan/proses_update?trx_pos_no="+trx_pos_no,
                                                                                dataType: "json",
                                                                                data:data,
           
                                                                                    success: function(result){
                                                                                            
                                                                                            window.open(url + "penjualan/penjualan/invoice?trx_pos_no="+trx_pos_no,'Invoice','width=500,height=300');
                                                                                            refreshGrid(gridpenjualan,url + "penjualan/penjualan/listdata","json");
                                                                                            close_form_modal(pembayaranWindows,"pembayaranWindows");
               
                                                                                    }
                                                                                });	  																	

        
        
                                                            }
  }
</script>

