<script type="text/javascript">
    var penjualanAkses    = "<?php echo $aksesEdit; ?>";
    var penjualanWindows  = new dhtmlXWindows();

    //-------------------------------------------------------------------------------------------------------------------------------
	// [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
	toolbarpenjualan = mainTab.cells("maintabbar").attachToolbar();
        toolbarpenjualan.setSkin("dhx_skyblue");
 	toolbarpenjualan.setIconsPath(url + "assets/img/btn/");
 	toolbarpenjualan.loadXML("<?php echo $main_toolbar_source; ?>");
        toolbarpenjualan.attachEvent("onClick", cpenjualan);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function cpenjualan(id){
        var module  = url + "penjualan/penjualan";
        var idRow   = gridpenjualan.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(penjualanWindows,"penjualanWindows",["Penjualan",800,800], module + "/loadFormpenjualanToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(penjualanWindows,"penjualanWindows",["Penjualan",800,800], module + "/loadFormpenjualanToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            dhtmlx.confirm({                
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id": idRow
                    }

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({ 
                                    type: "alert", 
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridpenjualan, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            },
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridpenjualan,url + "penjualan/penjualan/listdata","json");
        }

        if(id == "open_pos"){
            window.open(url + "penjualan/pos","POS kasir");
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
        gridpenjualan = mainTab.cells("maintabbar").attachGrid();
        gridpenjualan.setHeader("Cetak,No Penjualan, Tanggal, Keterangan, Sub Total, Diskon, Tax, Grand Total");
	gridpenjualan.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
	gridpenjualan.setColAlign("left,left,left,left,left,left,left,left");
	gridpenjualan.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro");
	gridpenjualan.setInitWidths("*,*,*,*,*,*,*,*");
	gridpenjualan.init();  
        gridpenjualan.enableDistributedParsing(true,10,300);
	gridpenjualan.load("<?php echo $data_source; ?>","json");

/*
    if(penjualanAkses == "TRUE"){
    	gridpenjualan.attachEvent("onRowDblClicked",gridpenjualanDBLClick);
    }

    // [ACTION] GRID ACTION
	function gridpenjualanDBLClick(){
        var module  = url + "sales/penjualan";
        form_modal(penjualanWindows,"penjualanWindows",["penjualan",800,800], module + "/loadFormpenjualanToolbar", module + "/loadform/" + gridpenjualan.getSelectedRowId());
    }
    */
     var printquotationWindows  = new dhtmlXWindows();
      function printInvoice(id){

          var arrFak  = id.split("/");
          id   = arrFak[0] + "-" + arrFak[1] + "-" + arrFak[2] + "-" + arrFak[3];
                         
            dhtmlx.confirm({
            text    : "Do you want invoice printing?",
            ok      : "YES",
            cancel  : "NO",
            callback:function(result){
                if(result){ 
                     
                    var module  = url + "penjualan/pos/print_struk/" + id;
                    window.open(module);
                }
            }
        });
    }
</script>