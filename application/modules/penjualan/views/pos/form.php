<?php
/**
 * User: Bayu Nugraha (bayu.nugraha@intex.co.id)
 * Date: 30/09/15
 * Time: 12:15
 */
?>
<style type="text/css">
    .button{
        height: 40px;
        width: 60px;
        font-size: 25px;
        text-align: center;
    }

    .jenisBayar{
        height: 40px;
        width: 120px;
        font-size: 18px;
        text-align: center;
    }

    #grandTotal{
        height: 50px !important;
        font-size: 50px !important;
        text-align: right !important;
    }
</style>
<div id="posloading" class="imageloading">
    <img src="<?php echo base_url(); ?>assets/img/logo/loading.gif" alt="" style="height:80px !important; position:relative !important; margin:0 auto !important; left:-10%; top:25%;"/>
</div>
<div style="width: 100%; height: 100%; padding: 10px 10px 10px 10px; background-color: #eeeeee;">
    <form id="barcodeform" name="barcodeform" action="javascript:void(0)">
        <table class="form" width="100%">
            <tr>
                <td colspan="2">
                    <span class="title_app"><b>Barcode</b> <i></i></span>
                    <?php echo form_input('barcodePos', '', 'id="barcodePos" class="form_web"');?>
                    <input type="submit" style="display: none;">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <fieldset style="width:100%;">
                        <table width="100%" cellspacing="0" cellpadding="3">
                            <tr>
                                <td width="41">
                                    <input type="button" value=" " id="tmp_qtyBarang" class="form_web disableItemBarang" onclick='ubahInput("qtyBarang")' style="width: 38px; height: 38px;" disabled="disabled">
                                </td>
                                <td>
                                    <span class="title_app"><b>Qty barang</b> <i></i></span>
                                    <input type="text" class="form_web disableItemBarang onlynumber" value="0" id="qtyBarang" style="text-align: right !important;" disabled="disabled">
                                </td>
                                <td width="45" align="right">
                                    <input type="button" class="disableItemBarang" value="OK" onclick="submitEditBarang()" style="width: 40px; height: 40px; font-size: 15px;" disabled="disabled">
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <fieldset style="width: 100%;">
                        <table width="100%">
                            <tr>
                                <td>
                                    <span class="title_app"><b>Subtotal</b> <i></i></span>
                                    <input type="text" value="0" class="form_web onlynumber" name="data[trx_pos_sub_total]" id="subTotal" style="text-align: right !important;" readonly>
                                </td>
                                <td>&nbsp; &nbsp;</td>
                                <td>
                                    <span class="title_app"><b>Total Qty</b> <i></i></span>
                                    <input type="text" value="0" class="form_web onlynumber" name="qtyTotal" id="qtyTotal" style="text-align: right !important;" readonly>
                                </td>
                                <td>&nbsp; &nbsp;</td>
                                <td>
                                    <span class="title_app"><b>Total Discount</b> <i></i></span>
                                    <input type="text" value="0" class="form_web onlynumber" name="data[trx_pos_diskon]" id="discountTotal" style="text-align: right !important;" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <span class="title_app"><b>Grandtotal</b> <i></i></span>
                                    <input type="text" value="0" class="form_web onlynumber" name="data[trx_pos_grand_total]" id="grandTotal" id="grandTotal" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <table width="100%">
                                        <tr>
                                            <td width="40">
                                                <input type="button" value=" " id="tmp_bayar" class="form_web" onclick='ubahInput("bayar")' style="width: 38px; height: 38px; background-color: #008000;">
                                            </td>
                                            <td>
                                                <span class="title_app"><b>Jumlah Bayar</b> <i></i></span>
                                                <input type="text" value="0" class="form_web onlynumber" name="data[trx_pos_bayar]" id="bayar" onkeyup="hitungKembalian()" style="text-align: right !important;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <span class="title_app"><b>Jumlah Kembalian</b> <i></i></span>
                                    <input type="text" value="0" class="form_web onlynumber" name="data[trx_pos_kembalian]" id="kembalian" style="text-align: right !important;" readonly>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td width="40%" valign="top">
                    <table width="100%">
                        <tr>
                            <td colspan="4">
                                <input type="button" value="MULTYPAYMENT" onclick="multyPayment()" style="height: 40px; width: 245px;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" value="7" class="button" onclick="keyAngka(7)">
                            </td>
                            <td>
                                <input type="button" value="8" class="button" onclick="keyAngka(8)">
                            </td>
                            <td>
                                <input type="button" value="9" class="button" onclick="keyAngka(9)">
                            </td>
                            <td>
                                <input type="button" value="HAPUS" onclick="hapusAngka()" style="height: 40px; width: 60px;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" value="4" class="button" onclick="keyAngka(4)">
                            </td>
                            <td>
                                <input type="button" value="5" class="button" onclick="keyAngka(5)">
                            </td>
                            <td>
                                <input type="button" value="6" class="button" onclick="keyAngka(6)">
                            </td>
                            <td>
                                <input type="button" value="CLEAR" onclick="resetForm()" style="height: 40px; width: 60px;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" value="1" class="button" onclick="keyAngka(1)">
                            </td>
                            <td>
                                <input type="button" value="2" class="button" onclick="keyAngka(2)">
                            </td>
                            <td>
                                <input type="button" value="3" class="button" onclick="keyAngka(3)">
                            </td>
                            <td rowspan="2">
                                <input type="hidden" value="bayar" id="inputKe">
                                <input type="button" value="CASH" onclick="prosesSimpan()" style="height: 85px; width: 60px;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" value="0" class="button" onclick="keyAngka(0)">
                            </td>
                            <td>
                                <input type="button" value="00" class="button" onclick='keyAngka("00")'>
                            </td>
                            <td>
                                <input type="button" value="." class="button" onclick='keyAngka(".")'>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table width="100%">
                        <tr>
                            <td align="right">
                                <a href="#" onclick="bayarUang(10000)">
                                    <img src="<?php echo base_url()?>/assets/img/icon/10rebu.jpg" width="100%" height="50">
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <a href="#" onclick="bayarUang(20000)">
                                    <img src="<?php echo base_url()?>/assets/img/icon/20rebu.jpg" width="100%" height="50">
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <a href="#" onclick="bayarUang(50000)">
                                    <img src="<?php echo base_url()?>/assets/img/icon/50rebu.jpg" width="100%" height="50">
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <a href="#" onclick="bayarUang(100000)">
                                    <img src="<?php echo base_url()?>/assets/img/icon/100rebu.jpeg" width="100%" height="50">
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
    $("#barcodePos").focus();
    $("#namaBank").select2();
    var posWindows  = new dhtmlXWindows();

    function ubahInput(id){
        var oId = $("#inputKe").val();
        $("#tmp_" + oId).attr("style","width: 38px; height: 38px;");
        $("#tmp_" + id).attr("style","width: 38px; height: 38px; background-color: #008000;");
        $("#inputKe").val(id);
        $("#" + id).focus();
    }

    ;(function($){
        $.fn.extend({
            donetyping: function(callback,timeout){
                timeout = timeout || 30; // 1 second default timeout
                var timeoutReference,
                    doneTyping = function(el){
                        if (!timeoutReference) return;
                        timeoutReference = null;
                        callback.call(el);
                    };
                return this.each(function(i,el){
                    var $el = $(el);
                    // Chrome Fix (Use keyup over keypress to detect backspace)
                    // thank you @palerdot
                    $el.is(':input') && $el.on('keyup keypress',function(e){
                        // This catches the backspace button in chrome, but also prevents
                        // the event from triggering too premptively. Without this line,
                        // using tab/shift+tab will make the focused element fire the callback.
                        if (e.type=='keyup' && e.keyCode!=8) return;

                        // Check if timeout has been set. If it has, "reset" the clock and
                        // start over again.
                        if (timeoutReference) clearTimeout(timeoutReference);
                        timeoutReference = setTimeout(function(){
                            // if we made it here, our timeout has elapsed. Fire the
                            // callback
                            doneTyping(el);
                        }, timeout);
                    }).on('blur',function(){
                            // If we can, fire the event since we're leaving the field
                            doneTyping(el);
                        });
                });
            }
        });
    })(jQuery);

    $('#barcodePos').donetyping(function(){
        var data = {
            barcode : $('#barcodePos').val()
        };
        $.ajax({
            url     : url + 'penjualan/pos/scanBarcode',
            data    : data,
            type    : 'POST',
            dataType: 'JSON',
            success :function(hasil){
                if(hasil[0]==0){
                    $('#barcodePos').attr('style','border: red 1px solid;');
                }else{
                    $('#barcodePos').attr('style','border: green 1px solid;');
                    var id          = gridPos.uid();
                    var posisi      = gridPos.getRowsNum();
                    if(checkingBarangChild(hasil[1]) == false){
                        gridPos.addRow(hasil[1], [hasil[1],hasil[2],'0',hasil[3],'1','0',hasil[3],'0'], posisi);
                    }else{
                        qty_before      = gridPos.cells(hasil[1],4).getValue();
                        qty_after       = parseInt(qty_before) + 1;
                        prod_discount   = gridPos.cells(hasil[1],5).getValue();

                        grandTotal      = (parseInt(hasil[3]) * parseInt(qty_after));

                        gridPos.cells(hasil[1],4).setValue(qty_after);
                        gridPos.cells(hasil[1],6).setValue(grandTotal);
                    }
                    cekPromo(hasil[1]);
                    hitungSubtotal();
                }
                $('#barcodePos').val('');
                $('#barcodePos').focus();
            }
        });
    });

    function cekPromo(kode_item){
        var qty     = gridPos.cells(kode_item,4).getValue();
        var harga   = gridPos.cells(kode_item,3).getValue();
        var total   = 0;
        var data    = {
            kode_item   : kode_item,
            qty         : qty
        };
        $.ajax({
            url     : url + 'penjualan/pos/cekPromo',
            data    : data,
            type    : 'POST',
            dataType: 'JSON',
            success :function(hasil){
                if(hasil[0] == "1"){
                    gridPos.cells(kode_item,5).setValue(hasil[1]);
                    total   = (parseInt(harga) * parseInt(qty));
                    gridPos.cells(kode_item,6).setValue(total);
                }else{
                    if(parseInt(hasil[1]) > parseInt(qty)){
                        gridPos.cells(kode_item,5).setValue(0);
                        total   = (parseInt(harga) * parseInt(qty));
                        gridPos.cells(kode_item,6).setValue(total);
                    }
                }
                hitungSubtotal();
            }
        });
    }

    function hitungSubtotal(){
        var qty     = 0;
        var discount= 0;
        var total   = 0;
        gridPos.forEachRow(function(id){
            qty     += parseInt(gridPos.cells(id,4).getValue());
            discount+= parseInt(gridPos.cells(id,5).getValue());
            total   += parseInt(gridPos.cells(id,6).getValue());
        });
        $("#subTotal").val(total);
        $("#qtyTotal").val(qty);
        $("#discountTotal").val(discount);
        var grandtotal  = total - discount;
        $("#grandTotal").val(grandtotal);
        hitungKembalian();
    }

    function hitungKembalian(){
        var grandtotal  = parseInt($("#grandTotal").val());
        var bayar       = parseInt($("#bayar").val());
        var kembalian   = bayar - grandtotal;
        $("#kembalian").val(kembalian);
    }

    function hitungDiskon(){
        var rId         = gridPos.getSelectedRowId();
        var qty         = parseInt($("#qtyBarang").val());
        var disPersen   = parseFloat($("#diskonPersen").val());
        var harga       = parseInt(gridPos.cells(rId,3).getValue());
        var diskon      = (qty * harga) * (disPersen / 100);
        $("#diskonBarang").val(diskon);
    }

    function hitungDiskonPersen(){
        var rId         = gridPos.getSelectedRowId();
        var qty         = parseInt($("#qtyBarang").val());
        var disPersen   = parseInt($("#diskonBarang").val());
        var harga       = parseInt(gridPos.cells(rId,3).getValue());
        var diskon      = (disPersen / (qty * harga)) * 100;
        $("#diskonPersen").val(diskon);
    }

    function submitEditBarang(){
        var id      = gridPos.getSelectedRowId();
        var harga   = parseInt(gridPos.cells(id,3).getValue());
        var total   = harga * parseInt($("#qtyBarang").val());
        gridPos.cells(id,4).setValue($("#qtyBarang").val());
        gridPos.cells(id,6).setValue(total);
        cekPromo(id);
        ubahInput("bayar");
        $("#qtyBarang").val('0');
        $(".disableItemBarang").attr("disabled","disabled");
        $("#barcodePos").focus();
        hitungSubtotal();
    }

    function bayarUang(pecahan){
        var bayar   = parseInt($("#bayar").val());
        var total   = bayar + parseInt(pecahan);

        $("#bayar").val(total);
        hitungKembalian();
    }

    function keyAngka(input){
        var id      = $("#inputKe").val();
        var bayar   = $("#" + id).val();
        var hasil   = bayar + input;
        if(bayar == "0"){
            if(input == "." && bayar != "0."){
                $("#" + id).val(hasil);
            }else if(input == "0" || input == "00"){
                $("#" + id).val("0");
            }else{
                $("#" + id).val(input);
            }
        }else{
            $("#" + id).val(hasil);
        }
        hitungKembalian();
    }

    function resetForm(){
//        $(".disableItemBarang").attr("disabled","disabled");
//        $(".form_web").val('0');
//        $("#namaBank").select2('val','');
//        $("#barcodePos").focus();
//        gridPos.clearAll();
        var id      = $("#inputKe").val();
        $("#" + id).val('0');
        if(id == "bayar"){
            hitungKembalian();
        }
    }

    function hapusAngka(){
        var id      = $("#inputKe").val();
        var bayar   = $("#" + id).val();
        var panjang = parseInt(bayar.length) - 1;
        var newBayar= bayar.slice(0,panjang);
        if(panjang == 0 && bayar != "0"){
            $("#" + id).val('0');
        }else if(panjang == 0 && bayar == "0"){
            $("#" + id).val('0');
        }else{
            $("#" + id).val(newBayar);
        }
        if(id == "bayar"){
            hitungKembalian();
        }
    }

    function print_klinik(no_faktur){
        var arrFak  = no_faktur.split("/");
        no_faktur   = arrFak[0] + "-" + arrFak[1] + "-" + arrFak[2] + "-" + arrFak[3];
        window.open(url + "penjualan/pos/print_struk/" + no_faktur,"Struk Apotek","width=300,height=400");
    }

    function refreshPos(no_fak){
        $(".disableItemBarang").attr("disabled","disabled");
        $(".form_web").val('0');
        $("#namaBank").select2('val','');
        $("#barcodePos").val('');
        $("#tmp_bayar").val(' ');
        $("#tmp_qtyBarang").val(' ');
        gridPos.clearAll();
        ubahInput("bayar");
        print_klinik(no_fak);
        $("#barcodePos").focus();
    }

    function prosesSimpan(){
        if($("#bayar").val() == "0"){
            var grandTotal  = $("#grandTotal").val();
            $("#bayar").val(grandTotal);
            hitungKembalian();
        }
        gridPos.setSerializableColumns("true,true,true,true,true,true,true,false");
        var data    = {
            dataIns : {
                trx_pos_sub_total   : $("#subTotal").val(),
                trx_pos_diskon      : $("#discountTotal").val(),
                trx_pos_grand_total : $("#grandTotal").val()
            },
            trx_pos_bayar       : $("#bayar").val(),
            trx_pos_kembalian   : $("#kembalian").val(),
            dataGrid            : $.csv.toArrays(gridPos.serializeToCSV())
        };
        if($("#qtyTotal").val() == "0"){
            dhtmlx.alert("Tidak ada barang yang di jual");
            return false;
        }
        if(parseInt(data['trx_pos_kembalian']) < 0){
            dhtmlx.alert("Jumlah bayar masih kurang");
            return false;
        }
        dhtmlx.confirm({
            text:"Anda yakin ingin melakukan proses ini?",
            ok:"Ya",
            cancel:"Tidak",
            callback:function(result){
                if(result){
//                    $("#posloading").show();
                    $.ajax({
                        url     : url + 'penjualan/pos/prosesCash',
                        data    : data,
                        type    : 'POST',
                        dataType: 'JSON',
                        success :function(res){
//                            $("#posloading").hide();
                            message_type ="alert";
                            judul = "Proses Sukses";

                            if(res[0] == false){
                                message_type = "alert-error";
                                judul = "Terdapat Kesalahan";
                            }

                            dhtmlx.message({
                                title: judul,
                                text: res[2],
                                type: message_type,
                                callback:function(){
                                    if(res[0] == true)
                                        return eval(res[3]);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    function multyPayment(){
        if($("#qtyTotal").val() == "0"){
            dhtmlx.alert("Tidak ada barang yang di jual");
            return false;
        }
        var module  = url + "penjualan/pos";
        form_modal(posWindows,"posWindows",["Form Multypayment",850,520], "tanpatoolbar", module + "/loadFormMultyPayment");
    }
</script>