<?php
/**
 * User: Bayu Nugraha (bayu.nugraha@intex.co.id)
 * Date: 13/10/15
 * Time: 11:02
 */
?>
<style>
    body{
        font-size:12px;
        font-family:arial;
    }

    .font{
        font-size:12px;
    }

</style>

<body onLoad="print()">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font">
    <tr>
        <td colspan="5" style="font-size:25px" align="center"><?php echo ucwords(strtolower($toko->m_toko_nama)); ?></td>
    </tr>
    <tr>
        <td colspan="5" style="font-size:15px" align="center"><?php echo ucwords(strtolower($toko->m_toko_ket)); ?></td>
    </tr>
    <tr>
        <td colspan="5" cellpadding="0" align="center">
            ===================================
        </td>
    </tr>
    <tr>
        <td>No.Fak</td>
        <td>&nbsp;</td>
        <td>:</td>
        <td>&nbsp;</td>
        <td><?php echo $faktur; ?></td>
    </tr>
    <tr>
        <td>Kasir</td>
        <td>&nbsp;</td>
        <td>:</td>
        <td>&nbsp;</td>
        <td><?php echo ucwords(strtolower($user->user_fullname)); ?></td>
    </tr>
    <tr>
        <td width="23%">Tanggal</td>
        <td>&nbsp;</td>
        <td width="2%">:</td>
        <td>&nbsp;</td>
        <td><?php  echo $pos->trx_pos_tgl; ?></td>
    </tr>
    <tr>
        <td colspan="5" cellpadding="0" align="center">
            ===================================
        </td>
    </tr>
</table>



<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font">
    <?php
    $item   = 0;
    $tagihan= 0;
    foreach($posDetail as $row){
        $total  = $row->trx_pos_detail_qty * $row->trx_pos_detail_harga;
        $item   += $row->trx_pos_detail_qty;
    ?>
            <tr>
                <td width="100"><?php echo ucwords(strtolower($row->m_item_nama)) ?></td>
                <td width="5" align="center" valign="top"><?php echo $row->trx_pos_detail_qty; ?></td>
                <td width="45" align="right" valign="top"><?php echo number_format($row->trx_pos_detail_harga,0,",","."); ?></td>
                <td align="right" valign="top"><?php echo number_format($total,0,",","."); ?></td>
            </tr>
    <?php
        if($row->trx_pos_detail_disc != 0){
    ?>
            <tr>
                <td>Diskon item</td>
                <td align="center"></td>
                <td align="center"></td>
                <td align="right">(<?php echo number_format($row->trx_pos_detail_disc,0,",","."); ?>)</td>
            </tr>
    <?php
        }
    }
    ?>
    <tr>
        <td colspan="4" cellpadding="0" align="center">
            -----------------------------------------------------------(*)
        </td>
    </tr>
    <tr>
        <td>Total Item</td>
        <td align="center"><?php echo $item;?></td>
        <td></td>
        <td align="right"><?php echo number_format($pos->trx_pos_grand_total,0,",",".");?></td>
    </tr>
    <?php
    foreach($posJenis as $row){
        ?>
        <tr>
            <td width="100"><?php echo ucwords(strtolower($row->trx_pos_jns_pem_status)) ?></td>
            <td width="5" align="center" valign="top"></td>
            <td width="45" align="right" valign="top"></td>
            <td align="right" valign="top"><?php echo number_format($row->trx_pos_jns_pem_bayar,0,",","."); ?></td>
        </tr>
    <?php
    }
    ?>
    <tr>
    <tr>
        <td colspan="4" cellpadding="0" align="center">
            -----------------------------------------------------------(-)
        </td>
    </tr>
    <tr>
        <td>Kembalian</td>
        <td align="center"></td>
        <td></td>
        <td align="right"><?php echo number_format($pos->trx_pos_kembalian,0,",",".");?></td>
    </tr>
    <tr>
        <td>PPN</td>
        <td align="right" colspan="2">(<?php echo number_format($pos->trx_pos_tax,0,",",".");?>)</td>
        <td align="right"></td>
    </tr>
</table>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font">
    <tr>
        <td cellpadding="0" align="center">
            ===================================
        </td>
    </tr>
    <tr>
        <td cellpadding="0" align="center">
            Terimakasi telah belanja di tempat kami
        </td>
    </tr>
    <tr>
        <td cellpadding="0" align="center">
            ===================================
        </td>
    </tr>
</table>
</body>
<script>
    clearTimeout();
    setTimeout(function(){
        window.close();
    },10000);
</script>