<?php
/**
 * Created by PhpStorm.
 * User: INTEX-N
 * Date: 12/10/15
 * Time: 14:17
 */
$jenisPembayaran    = array(
    'cash'  => "Cash",
    'debit' => "Debit",
    'credit' => "Credit"
);
?>
<style type="text/css">
    .button2{
        height: 30px;
        width: 55px;
        font-size: 20px;
        text-align: center;
    }

    .buttonSamping{
        height: 30px;
        width: 100px;
    }

    .jenisBayar{
        height: 40px;
        width: 120px;
        font-size: 18px;
        text-align: center;
    }

    #tmp_Grandtotal{
        height: 50px !important;
        font-size: 50px !important;
        text-align: right !important;
    }
</style>
<div style="padding: 5px 5px 5px 5px; width: 100%; height: 100%; background-color: #cee3ff;">
    <table class="form">
        <tr>
            <td valign="top">
                <span class="title_app"><b>Grandtotal</b> <i></i></span>
                <input type="text" value="0" class="form_web onlynumber" name="tmp_Grandtotal" id="tmp_Grandtotal" readonly>
            </td>
            <td>&nbsp; &nbsp;</td>
            <td rowspan="2" width="30%" valign="top">
                <table width="100%">
                    <tr>
                        <td colspan="2">
                            <input type="button" value="HAPUS" onclick="hapusAngka()" style="height: 30px; width: 115px;">
                        </td>
                        <td colspan="2">
                            <input type="button" value="CLEAR" onclick="resetForm()" style="height: 30px; width: 130px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="button" value="7" class="button2" onclick="keyAngka(7)">
                        </td>
                        <td>
                            <input type="button" value="8" class="button2" onclick="keyAngka(8)">
                        </td>
                        <td>
                            <input type="button" value="9" class="button2" onclick="keyAngka(9)">
                        </td>
                        <td>
                            <input type="button" value="OK" onclick="addToGrid()" style="height: 30px; width: 70px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="button" value="4" class="button2" onclick="keyAngka(4)">
                        </td>
                        <td>
                            <input type="button" value="5" class="button2" onclick="keyAngka(5)">
                        </td>
                        <td>
                            <input type="button" value="6" class="button2" onclick="keyAngka(6)">
                        </td>
                        <td rowspan="3">
                            <input type="button" value="PROSES" style="width: 70px; height: 105px;" onclick="simpanMultiPayment()">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="button" value="1" class="button2" onclick="keyAngka(1)">
                        </td>
                        <td>
                            <input type="button" value="2" class="button2" onclick="keyAngka(2)">
                        </td>
                        <td>
                            <input type="button" value="3" class="button2" onclick="keyAngka(3)">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="button" value="0" class="button2" onclick="keyAngka(0)">
                        </td>
                        <td>
                            <input type="button" value="00" class="button2" onclick='keyAngka("00")'>
                        </td>
                        <td>
                            <input type="button" value="." class="button2" onclick='keyAngka(".")'>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table width="100%">
                    <tr>
                        <td valign="top">
                            <span class="title_app"><b>Jenis Pembayaran</b> <i></i></span>
                            <?php echo form_dropdown('jenisBayar', $jenisPembayaran,'', 'id="jenisBayar" class="full" onChange="setJenisTransaksi()"'); ?>
                        </td>
                        <td>&nbsp; &nbsp;</td>
                        <td>
                            <input type="button" value=" " id="tmp_bayarMP" class="form_web" onclick='ubahInput("bayarMP")' style="width: 38px; height: 38px;">
                        </td>
                        <td>&nbsp; &nbsp;</td>
                        <td valign="top" colspan="5">
                            <span class="title_app"><b>Jumlah Bayar</b> <i></i></span>
                            <input type="text" class="form_web onlynumber" name="bayarMP" id="bayarMP" style="height: 25px !important; text-align: right;">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <span class="title_app"><b>Nama Bank</b> <i></i></span>
                            <?php echo form_dropdown('data[bank_id]', $bank,'', 'id="namaBank" class="full disableDebetCredit" disabled="disabled"'); ?>
                        </td>
                        <td>&nbsp; &nbsp;</td>
                        <td>
                            <input type="button" value=" " id="tmp_noKartu" class="form_web disableDebetCredit" onclick='ubahInput("noKartu")' style="width: 38px; height: 38px;" disabled="disabled">
                        </td>
                        <td>&nbsp; &nbsp;</td>
                        <td valign="top">
                            <span class="title_app"><b>No Kartu</b> <i></i></span>
                            <input type="text" class="form_web disableDebetCredit" name="no_kartu" id="noKartu" style="height: 25px !important;" disabled="disabled">
                        </td>
                        <td>&nbsp; &nbsp;</td>
                        <td>
                            <input type="button" value=" " id="tmp_noTransaksi" class="form_web disableDebetCredit" onclick='ubahInput("noTransaksi")' style="width: 38px; height: 38px;" disabled="disabled">
                        </td>
                        <td>&nbsp; &nbsp;</td>
                        <td valign="top">
                            <span class="title_app"><b>No Transaksi Debet / Credit</b> <i></i></span>
                            <input type="text" class="form_web disableDebetCredit" name="no_trx" id="noTransaksi" style="height: 25px !important;" disabled="disabled">
                        </td>
                    </tr>
                </table>
            </td>
            <td>&nbsp; &nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                <div id="toolbarMultipayment" style="width: 99%"></div>
            </td>
        </tr>
        <tr>
            <td colspan="3" valign="top">
                <div id="gridMultipayment" style="height: 200px; width: 100%;"></div>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="3">
                <span class="title_app"><b>Kembalian</b> <i></i></span>
                <input type="text" value="0" class="form_web onlynumber" name="tmp_kembalian" id="tmp_kembalian" readonly>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    $("#namaBank").select2();
    $("#jenisBayar").select2();
    ubahInput("bayarMP");

    var grandTotal  = $("#grandTotal").val();
    $("#tmp_Grandtotal").val(grandTotal);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
    toolbarPembayaran = new dhtmlXToolbarObject("toolbarMultipayment");
    toolbarPembayaran.setSkin("dhx_skyblue");
    toolbarPembayaran.setIconsPath(url + "assets/img/btn/");
    toolbarPembayaran.addButton("delete",0,"Hapus pembayaran","delete.png","delete.png");
    toolbarPembayaran.attachEvent("onClick", cToolMulti);

    function cToolMulti(id){
        var rId  = gridPosPembayaran.getSelectedRowId();
        if(rId == null){
            dhtmlx.alert("Pillih pembayaran terlebih dahulu");
            return;
        }
        gridPosPembayaran.deleteSelectedRows();
        hitungKembalianPembayaran();
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridPosPembayaran = new dhtmlXGridObject('gridMultipayment');
    gridPosPembayaran.setImagePath(url + "assets/dhtmlx/imgs/");
    gridPosPembayaran.setHeader("Jenis Pembayaran,Jumlah bayar, No Kartu,No transaksi,Bank, Kode Bank");
    gridPosPembayaran.setColAlign("left,right,left,left,left,left");
    gridPosPembayaran.setColTypes("ro,ron,ro,ro,ro,ro");
    gridPosPembayaran.setNumberFormat("0,000.00", 1, ".", ",");
    gridPosPembayaran.setInitWidths("150,130,180,200,150,0");
    gridPosPembayaran.attachFooter("Total,#stat_total,&nbsp;,&nbsp;,&nbsp;,&nbsp;");
    gridPosPembayaran.init();
    gridPosPembayaran.enableDistributedParsing(true,10,300);

    function setJenisTransaksi(){
        var jt  = $("#jenisBayar").val();
        if(jt == "cash"){
            $(".disableDebetCredit").attr("disabled","disabled");
            $("#noKartu").val("");
            $("#noTransaksi").val("");
            $("#namaBank").select2("val","");
        }else{
            $(".disableDebetCredit").removeAttr("disabled");
        }
        ubahInput("bayarMP");
    }

    function addToGrid(){
        var id          = gridPosPembayaran.uid();
        var posisi      = gridPosPembayaran.getRowsNum();
        var jenisBayar  = $("#jenisBayar").val();
        var bank        = $("#namaBank").val().split('|');
        if(checkingPembayaran(jenisBayar) == true){
            dhtmlx.alert("Jenis pembayaran " + jenisBayar + " sudah ada.");
        }else{
            gridPosPembayaran.addRow(id, [jenisBayar,$("#bayarMP").val(),$("#noKartu").val(),$("#noTransaksi").val(),bank[1],bank[0]], posisi);
        }
        hitungKembalianPembayaran();
    }

    function checkingPembayaran(kode_barang){
        condition = false;
        gridPosPembayaran.forEachRow(function(ids){
            if(gridPosPembayaran.cells(ids,0).getValue() == kode_barang) {
                condition = true;
            }
        });

        return condition;
    }

    function simpanMultiPayment(){
        gridPos.setSerializableColumns("true,true,true,true,true,true,true,false");
        var data    = {
            dataIns : {
                trx_pos_sub_total   : $("#subTotal").val(),
                trx_pos_diskon      : $("#discountTotal").val(),
                trx_pos_grand_total : $("#grandTotal").val()
            },
            trx_pos_kembalian   : $("#tmp_kembalian").val(),
            dataGridPembayaran  : $.csv.toArrays(gridPosPembayaran.serializeToCSV()),
            dataGrid            : $.csv.toArrays(gridPos.serializeToCSV())
        };
        if(parseInt(data['trx_pos_kembalian']) < 0){
            dhtmlx.alert("Jumlah bayar masih kurang");
            return false;
        }
        dhtmlx.confirm({
            text:"Anda yakin ingin melakukan proses ini?",
            ok:"Ya",
            cancel:"Tidak",
            callback:function(result){
                if(result){
//                    $("#posloading").show();
                    $.ajax({
                        url     : url + 'penjualan/pos/proses',
                        data    : data,
                        type    : 'POST',
                        dataType: 'JSON',
                        success :function(res){
//                            $("#posloading").hide();
                            message_type ="alert";
                            judul = "Proses Sukses";

                            if(res[0] == false){
                                message_type = "alert-error";
                                judul = "Terdapat Kesalahan";
                            }

                            dhtmlx.message({
                                title: judul,
                                text: res[2],
                                type: message_type,
                                callback:function(){
                                    close_form_modal(posWindows,"posWindows");
                                    if(res[0] == true)
                                        return eval(res[3]);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    function hitungKembalianPembayaran(){
        var totalBayar  = 0;
        gridPosPembayaran.forEachRow(function(rId){
            totalBayar  +=  parseInt(gridPosPembayaran.cells(rId,1).getValue());
        });
        var grandTotal  = parseInt($("#tmp_Grandtotal").val());
        var kembalian   = totalBayar - grandTotal;
        $("#tmp_kembalian").val(kembalian);
    }
</script>