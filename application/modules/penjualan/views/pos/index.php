<?php
/**
 * User: Bayu Nugraha (bayu.nugraha@intex.co.id)
 * Date: 30/09/15
 * Time: 12:15
 */
?>
<html>
<link rel="icon" href="<?php echo $icon; ?>" type="image/logo">
<head>
    <title>POS Kasir</title>
    <?php
    echo $js_header;
    echo $css_header;
    ?>
</head>
<body></body>
<script type="text/javascript">
    var url         = "<?php echo base_url(); ?>";
    var posWindows  = new dhtmlXWindows();
    var dhxLayoutData, layout_core, layout_body, statusbar;

    dhxLayoutData = {
        parent: document.body,
        pattern: "3J",
        cells: [
            {id: "a",header: false},
            {id: "b",header: false},
            {id: "c",header: false}
        ]
    };
    myLayout = new dhtmlXLayoutObject(dhxLayoutData);
    var statusbar_html = "<center>Copyright &copy; PT. Marui Intex 2015. All rights reserved.</center>";
    statusbar = myLayout.attachStatusBar();
    statusbar.setText(statusbar_html);

    myLayout.cells('b').setWidth(550);
//    myLayout.cells('c').setHeight(350);
    myLayout.cells("a").fixSize(true,true);
    myLayout.cells("b").fixSize(true,true);
    myLayout.cells("c").fixSize(true,true);
    myLayout.cells("b").attachURL(url+"penjualan/pos/form",true);

    toolBarPos = myLayout.cells("b").attachToolbar();
    toolBarPos.setIconsPath(url + "assets/img/btn/");
//    toolBarPos.addButton("hapus", 0, "Hapus Barang", "delete.png", "delete.png");
    toolBarPos.addText("info", 10, "<b>System POS v.1.0</b>");
//    toolBarPos.attachEvent("onclick", tbClickPos);
//    toolBarPos.addSpacer("hapus");

    function tbClickPos(id){
//        if(id == 'hapus'){
//            var rId     = gridPos.getSelectedId();
//            if(rId == null){
//                dhtmlx.alert("Pillih salah satu barang");
//            }else{
                gridPos.deleteRow(id);
                $("#qtyBarang").val('0');
                $("#diskonBarang").val('0');
                $(".disableItemBarang").attr("disabled","disabled");
                hitungSubtotal();
                ubahInput("bayar");
                $("#barcodePos").focus();
//            }
//        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridPos = myLayout.cells("c").attachGrid();
    gridPos.setImagePath(url + "assets/dhtmlx/imgs/");
    gridPos.setHeader("Kode Barang, Nama Barang, Stock, Harga, Qty, Diskon (Rp), Total, Hapus");
    gridPos.setColAlign("left,left,center,right,center,right,right,center");
    gridPos.setColTypes("ro,ro,ro,ron,ro,ron,ron,ro");
    gridPos.setNumberFormat("0,000.00", 3, ".", ",");
    gridPos.setNumberFormat("0,000.00", 5, ".", ",");
    gridPos.setNumberFormat("0,000.00", 6, ".", ",");
    gridPos.setInitWidths("100,*,0,100,50,100,100,75");
    gridPos.init();
    gridPos.enableDistributedParsing(true,10,300);
    gridPos.attachEvent("onRowSelect", function(id,ind){
        $("#qtyBarang").val(gridPos.cells(id,4).getValue());
        $(".disableItemBarang").removeAttr("disabled");
        ubahInput("qtyBarang");
        $("#qtyBarang").focus();
    });

    // [OBJECT] Layout cell b
    layoutCellB = myLayout.cells('a').attachLayout("2U");
    layoutCellB.cells('a').setWidth(220);
    layoutCellB.cells("a").fixSize(true,true);
    layoutCellB.cells("a").setText("Kategori Barang");
    layoutCellB.cells("b").setText("List Barang");

    //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] DATA VIEW
    //-------------------------------------------------------------------------------------------------------------------------------
    viewPosKeterangan = layoutCellB.cells("a").attachDataView({
        type:{
            template    :"<span class='dhx_strong'>#Nama#</span><span class='dhx_light'>#Ket#</span>",
            height      :70,
            margin      :0,
            padding     :5
        },
        tooltip:{
            template:"<b>#Ket#<b>"
        }
    });
    viewPosKeterangan.load(url + "penjualan/pos/list_kategori","json");
    viewPosKeterangan.attachEvent("onItemClick", function (id, ev, html){
        viewPosBarang.clearAll();
        viewPosBarang.load(url + "penjualan/pos/loadListBarang/" + id,"json");
        $("#barcodePos").focus();
    });

    viewPosBarang = layoutCellB.cells("b").attachDataView({
        type:{
            template    : "<span class='dhx_strong'>#Nama#</span><span class='dhx_light'>#Harga#</span><br>#Barcode#",
            height      : 70,
            width       : 185,
            margin      : 0,
            padding     : 5
        },
        tooltip:{
            template:"<b>#Ket#<b>"
        }
    });
    viewPosBarang.attachEvent("onItemClick", function (id, ev, html){
        var data = {
            kode_item : id
        };
        $.ajax({
            url     : url + 'penjualan/pos/getDataBarang',
            data    : data,
            type    : 'POST',
            dataType: 'JSON',
            success :function(hasil){
                var id          = gridPos.uid();
                var posisi      = gridPos.getRowsNum();
                if(checkingBarangChild(hasil[1]) == false){
                    var img = '<img src="<?php echo base_url()."assets/img/btn/Button Close-01.png";?>" onClick=\'tbClickPos("'+hasil[1]+'")\' width="15" height="15">';
                    gridPos.addRow(hasil[1], [hasil[1],hasil[2],'0',hasil[3],'1','0',hasil[3],img], posisi);
                }else{
                    qty_before      = gridPos.cells(hasil[1],4).getValue();
                    qty_after       = parseInt(qty_before) + 1;
                    prod_discount   = gridPos.cells(hasil[1],5).getValue();

                    grandTotal      = (parseInt(hasil[3]) * parseInt(qty_after));

                    gridPos.cells(hasil[1],4).setValue(qty_after);
                    gridPos.cells(hasil[1],6).setValue(grandTotal);
                }
                cekPromo(hasil[1]);
                hitungSubtotal();
                $("#barcodePos").focus();
            }
        });
    });

    function checkingBarangChild(kode_barang){
        condition = false;
        gridPos.forEachRow(function(ids){
            if(gridPos.cells(ids,0).getValue() == kode_barang) {
                condition = true;
            }
        });

        return condition;
    }

    $(document).delegate(".onlynumber","keydown",function(event){
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9
            || event.keyCode == 27 || event.keyCode == 13
            || (event.keyCode == 65 && event.ctrlKey === true)
            || (event.keyCode >= 35 && event.keyCode <= 39)){
            return;
        }else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    });
</script>
</html>