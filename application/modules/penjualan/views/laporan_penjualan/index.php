<div id="laporan_penjualanloading" class="imageloading"></div>
<div class="form-wrapper">
    <?php
    
    
    $hidden_form = array('id' => !empty($id) ? $id : '');
    echo form_open_multipart($form_action, array('id' => 'laporan_penjualan'), $hidden_form);
    
    
    ?>
    <table class="form" width="45%">
        <tr>
             <td>
                  Periode Penjualan : 
            </td>
            <td>
                  <?php echo form_input('periode_awal', !empty($default->periode_awal) ? $default->periode_awal : date("Y-m-01"), 'id="periode_awal" class="form_web"'); ?>
                 
                       
            </td>
             <td>
                  s/d
            </td>
            <td>
                  <?php echo form_input('periode_akhir', !empty($default->periode_akhir) ? $default->periode_akhir : date("Y-m-d"), 'id="periode_akhir" class="form_web"'); ?>
                       
            </td>
           
            <td>
                  <input type="button" name="cetak_penjualan" id="cetak_penjualan" value=" EXPORT PDF " onclick="ctk_penjualan()"/>
                                                   
            </td>
        </tr>
        
    </table>
    <?php 
        echo form_close(); 
      
    ?>
</div>
<script type="text/javascript">
     var laporan_penjualanWindows  = new dhtmlXWindows();

    
    function ctk_penjualan(){
          var periode_awal     = $("#periode_awal").val();
          var periode_akhir    = $("#periode_akhir").val();
            
            if(periode_awal == ''){
                
                alert("Periode Tanggal harus diisi");
            }else if(periode_akhir == ''){
                
                alert("Periode Tanggal harus diisi");
           }else if(periode_awal == '' & periode_akhir == ''){
                
                alert("Periode Tanggal harus diisi");
           }else{
                
                window.open(url + "penjualan/laporan_penjualan/lap_penjualan?periode_awal="+periode_awal+"&periode_akhir="+periode_akhir,'Report Penjualan','width=800,height=500');
           }
                                 
    }
   
    cal1 = new dhtmlxCalendarObject('periode_awal');
    cal1.setDateFormat('%Y-%m-%d');
    
    cal2 = new dhtmlxCalendarObject('periode_akhir');
    cal2.setDateFormat('%Y-%m-%d');
</script>