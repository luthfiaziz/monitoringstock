<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_management
 *
 * @author Vredy Wijaya
 */

class Laporan_penjualan extends MX_Controller {
    // VARIABLE MODUL
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;
    
   
    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT laporan penjualan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();
        
        // Set Module Location
        $this->_module = 'penjualan/laporan_penjualan';
      
        // Load Module Core
        $this->load->module('config/app_core');
        // Load Model
        //$this->load->model('penjualan_model','penjualan');
      

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX cek penjualan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];
        $data['form_action']            = base_url() . $this->_module . '/proses';
        $data['main_toolbar_source']    = base_url() . $this->_module . '/loadMainToolbar/' . $menu_id;
    	$this->load->view($this->_module . '/index',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR MAIN penjualan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadMainToolbar($id = ''){
        xml::xml_header();
        $xml  = '<toolbar>';
       
        $xml .= "</toolbar>";  

        echo $xml;
    }
    
    function lap_penjualan(){ 
       
        $periode1              =$this->input->get('periode_awal');
        $periode2              =$this->input->get('periode_akhir');
        
        $periode_awal  = $periode1." 00:00:00";
        $periode_akhir = $periode2." 23:00:00";
        
        $username                  =$this->SESSION["user_username"];
                
        $tgl_cetak = date("Y-m-d H:i:s");
                $this->load->library('header/report_penjualan');
                //$this->load->library('myfunctions');
                $pdf = new report_penjualan('L', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Vredy Wijaya');
		$pdf->SetTitle('Report Penjualan');
		$pdf->SetSubject('F');
		$pdf->setPrintHeader(true);
		$pdf->setPrintFooter(true);
              
                $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));		
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);		
		$pdf->SetMargins(5,30, 5);
		$pdf->SetHeaderMargin(10);
		$pdf->SetFooterMargin(23);
		
		$pdf->SetFont('times', '', 10);
		$pdf->AddPage();
              
                  $html = '<table width="100%" border="0">';
               
                $html .= '
                        <tr>
                        <td><span align="center" style="font-size:14px;font-weight:bold;">
                    <b>LAPORAN DATA PENJUALAN</b><br/>
                    PERIODE '.$periode1.' s/d '.$periode2.'
                    </span><br /></td>
                       
                        </tr>                                      
                    ';
                
                $html .= '</table>';
                 
                
               
                
            
                         
                $html .= '<table width="100%" border="1" align="center">';
                $html .= '
                        <tr>
                        <td width="20" align="center"><b> No.</b></td>
                        <td width="100" align="center"><b> No.Invoice</b></td>
                        <td width="70" align="center"><b> Tgl.Transaksi</b></td>
                        <td width="160" align="center"><b> Keterangan</b></td>
                        <td width="60" align="center"><b> Total</b></td>
                        <td width="60" align="center"><b> Diskon</b></td>
                        <td width="60" align="center"><b> Pajak</b></td>
                        <td width="70" align="center"><b> Grand Total</b></td>
                        <td width="60" align="center"><b> Jenis Pembayaran</b></td>
                        <td width="60" align="center"><b> Total Bayar</b></td>
                        <td width="60" align="center"><b> Kembalian</b></td>
                       
                        </tr>
                    ';
                
                $no = 1;
               
                 $sql="SELECT * FROM 1nt3x_trx_pos where trx_pos_tgl BETWEEN '$periode1' and '$periode2'";
                 $result= $this->db->query($sql);
                 $r	=$result->row();
                 
                foreach($result->result() as $r){
                    
                   
                    $trx_pos_no             = $r->trx_pos_no;
                    $trx_pos_tgl            = $r->trx_pos_tgl;
                    $trx_pos_ket            = $r->trx_pos_ket;
                    $trx_pos_sub_total      = $r->trx_pos_sub_total;
                    $trx_pos_diskon         = $r->trx_pos_diskon;
                    $trx_pos_tax            = $r->trx_pos_tax;
                    $trx_pos_grand_total    = $r->trx_pos_grand_total;
                    $trx_pos_jenis_pembayaran       = $r->trx_pos_jenis_pembayaran;
                    $trx_pos_bayar          = $r->trx_pos_bayar;
                    //$nama_pemilik_kartu     = $r->nama_pemilik_kartu;
                    $trx_pos_no_kartu               = $r->trx_pos_no_kartu;
                   // $nama_bank              = $r->nama_bank;
                   //$booking_id             = $r->booking_id;
                   // $member_kode            = $r->member_kode;
                    $kembalian              = $r->kembalian;
                    
                    
                
                $html .= '
                        <tr>
                        <td align="center"> '.$no.'</td>
                        <td> '.$trx_pos_no.'</td>
                        <td> '.$trx_pos_tgl.'</td>
                        <td> '.$trx_pos_ket . ' </td>
                        <td align="right"> '.number_format( $trx_pos_sub_total , 0 , "" , '.' ) .'</td>
                        <td> '.$trx_pos_diskon . ' </td>
                        <td> '.$trx_pos_tax . ' </td>
                        <td align="right"> '.number_format( $trx_pos_grand_total , 0 , "" , '.' ) . ' </td>
                        <td> '.$trx_pos_jenis_pembayaran . ' </td>
                        <td align="right"> '.number_format( $trx_pos_bayar , 0 , "" , '.' ) .' </td>
                        <td align="right"> '.number_format( $kembalian , 0 , "" , '.' ) .' </td>
                        
                      
                      </tr>
                    ';
                $no++;
                        $total1 += $trx_pos_sub_total;
                        $total2 += $trx_pos_grand_total;
                        $total3 += $trx_pos_bayar;
                        $total4 += $kembalian;
                        }
                        
                $html .= '
                        <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Total :</td>
                        <td align="right"> '.number_format( $total1 , 0 , "" , '.' ) .'</td>
                        <td></td>
                        <td></td>
                        <td align="right"> '.number_format( $total2 , 0 , "" , '.' ) .' </td>
                        <td></td>
                        <td align="right"> '.number_format( $total3 , 0 , "" , '.' ) .' </td>
                        <td align="right"> '.number_format( $total4 , 0 , "" , '.' ) .' </td>
                        
                      
                      </tr>
                    ';
              
                     
                $html .= '</table>';
                
                $html .= '<br /><table width="90%" border="0">';
               
                $html .= '
                        <tr>
                        <td><span align="right" style="font-size:8px;font-weight:bold;">
                        Created by : '.$username.', Tgl.Cetak : '.$tgl_cetak.'
                    </span><br /></td>
                       
                        </tr>                                      
                    ';
                
                $html .= '</table>';
                
                $pdf->writeHTML($html, true, false, true, false, '');
                //$pdf->AddPage();
                $pdf->Output('report_penjualan.pdf', 'I');
    }
}

?>