<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_management
 *
 * @author Vredy Wijaya
 */

class Penjualan extends MX_Controller {
    // VARIABLE MODUL
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT penjualan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();
        
        // Set Module Location
        $this->_module = 'penjualan/penjualan';
        // Load Module Core
        $this->load->module('config/app_core');
        // Load Model
        $this->load->model('penjualan_model','penjualan');
        //$this->load->model('master/booking_model','booking');
        //$this->load->model('master/member_model','member');
        

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX penjualan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];

        $data['load_form']              = base_url() . $this->_module . '/loadform';
        $data['main_toolbar_source']    = base_url() . $this->_module . '/loadMainToolbar/' . $menu_id;
        $data['data_source']            = base_url() . $this->_module . '/listdata';
        $data['aksesEdit']              = ($this->SESSION["otoritas_menu"][$menu_id]["role_access_edit"] == 1)? "TRUE" : "FALSE";
    	$this->load->view($this->_module . '/index',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR MAIN penjualan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadMainToolbar($id = ''){
        xml::xml_header();
        $xml  = '<toolbar>';

//        if($this->SESSION["otoritas_menu"][$id]["role_access_add"] == 1){
//            $xml .= '<item id="add" text="Tambah" type="button" img="add.png" imgdis="add.png"/>';
//        }
        if($this->SESSION["otoritas_menu"][$id]["role_access_add"] == 1){
            $xml .= '<item id="open_pos" text="Open Pos" type="button" img="add.png" imgdis="add.png"/>';
        }
        /*
        if($this->SESSION["otoritas_menu"][$id]["role_access_edit"] == 1){
            $xml .= '<item id="edit" text="Ubah" type="button" img="edit.png" imgdis="edit.png"/>';
        }

        if($this->SESSION["otoritas_menu"][$id]["role_access_delete"] == 1){
            $xml .= '<item id="delete" text="Hapus" type="button" img="delete.png" imgdis="delete.png"/>';
        }
        */
        $xml .= '<item id="sep" type="separator"/>';
        $xml .= '<item id="refresh" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }
    
    public function loadFormpembayaranToolbar(){
        xml::xml_header();
        
        $xml  = '<toolbar>';
        //$xml .= '<item id="savepenjualan" text="Simpan" type="button" img="save.png" imgdis="save.png" action="simpan"/>';
        //$xml .= '<item id="cancelpenjualan" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png" action="bersihpenjualan"/>';
        $xml .= "</toolbar>";  
        
        echo $xml;
    }
    

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD DATA penjualan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function listdata(){
        $key    = array("toko_kode" => $this->SESSION['toko_kode']);
       $data   = $this->penjualan->loadData($key);
       
       $result = array("rows" => $data);

       echo json_encode($result);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR FORM penjualan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadFormpenjualanToolbar(){
        xml::xml_header();

        $xml  = '<toolbar>';
        //$xml .= '<item id="savepenjualan" text="Simpan" type="button" img="save.png" imgdis="save.png" action="simpan"/>';
        //$xml .= '<item id="cancelpenjualan" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png" action="bersihpenjualan"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD FORM penjualan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadform($id = ''){
        $data["readonly"] = false;

        if (!empty($id)) {
            $data["id"]      = $id;
            $data["default"] = $this->penjualan->data($id)->get()->row();
            $data["readonly"] = true; 
        }else{
            //jika blm ada idnya
            $tahun = date("Y");
            $bulan = date("m");
           
         
                $this->db->from("trx_pos");
                //$this->db->where();
                
                $total=$this->db->count_all_results();
                $no_urut = $total + 1;
                
                if($no_urut >= 1 and $no_urut < 10){
                    
                    $no_invoice = "00000".$no_urut."/".$bulan."/".$tahun;
                    
                }else if($no_urut >= 10 and $no_urut < 100){
                    
                    $no_invoice = "0000".$no_urut."/".$bulan."/".$tahun;
                    
                }else if($no_urut >= 100 and $no_urut < 1000){
                    
                    $no_invoice = "000".$no_urut."/".$bulan."/".$tahun;
                 
                    
                }else if($no_urut >= 1000 and $no_urut < 10000){
                    
                    $no_invoice = "00".$no_urut."/".$bulan."/".$tahun;
                    
                }else if($no_urut >= 10000 and $no_urut < 100000){
                    
                    $no_invoice = "0".$no_urut."/".$bulan."/".$tahun;
                
                }else if($no_urut >= 100000 and $no_urut < 1000000){
                    
                    $no_invoice = $no_urut."/".$bulan."/".$tahun;
                    
                }else{
                    
                    $no_invoice = "000001/".$bulan."/".$tahun;
                    
                }
        
        }
        
        $data['no_invoice']         = $no_invoice;
        $data['form_action']        = base_url() . $this->_module . '/proses';
        //$data['booking_options']   = $this->booking->options("--Pilih Booking--");
        //$data['member_options']   = $this->member->options("--Pilih Member--");
        $data['produk_options']   = $this->penjualan->options1("--Pilih Produk--");

        $this->load->view($this->_module . '/form',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - SAVE DATA perusahaan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function proses(){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;
        $id   = $this->input->post('id');
        
        if(empty($id))
            $this->form_validation->set_rules('trx_pos_no', 'No Invoice', 'trim|required|callback_ketersediaan_check');


        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', 'refresh()');

            $mpenjualan = array();
            $mpenjualan['trx_pos_no']                           = $this->input->post('trx_pos_no');
           //$mpenjualan['penjualan_tgl']                       = $this->input->post('penjualan_tgl');
            //tanggal convert
            $date_sql                                          = $this->input->post('trx_pos_tgl');
            
            $mpenjualan['trx_pos_tgl']                       = $this->konversi_tanggal($date_sql);
            //$mpenjualan['trx_pos_tgl']                          = $this->input->post('trx_pos_tgl');
            
            $mpenjualan['trx_pos_ket']                          = $this->input->post('trx_pos_ket');
            $mpenjualan['trx_pos_no_kartu']                     = $this->input->post('trx_pos_no_kartu');
            
            $mpenjualan['trx_pos_sub_total']                    = $this->input->post('trx_pos_sub_total');
            $mpenjualan['trx_pos_diskon']                       = $this->input->post('trx_pos_diskon');
            $mpenjualan['trx_pos_tax']                          = $this->input->post('trx_pos_tax');
            $mpenjualan['trx_pos_grand_total']                  = $this->input->post('trx_pos_grand_total');
            $mpenjualan['trx_pos_jenis_pembayaran']             = $this->input->post('trx_pos_jenis_pembayaran');
            $mpenjualan['bank_id']                              = $this->input->post('bank_id');
            $mpenjualan['toko_kode']                            = $this->input->post('toko_kode');
                     

            if ($id == '') {
                 $mpenjualan['trx_pos_tgl_buat']               = date("Y-m-d");
                 $mpenjualan['trx_pos_petugas']                = $this->SESSION["user_username"];

                if ($crud->save_as_new('trx_pos', arrman::StripTagFilter($mpenjualan))) {
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh()');
                }
                
                                $tmp_detail = $this->input->post("data_grid");
                                if($this->scan_array($tmp_detail) == FALSE){
                                    $data_detail = ($tis->setDhataDetail($tmp_detail));
                                 }
                           
                                foreach($data_detail as $value){
                                
                                $transaksi_detail = array();
                                $transaksi_detail["trx_pos_detail_qty"]                 = $value["trx_pos_detail_qty"];
                                $transaksi_detail["trx_pos_detail_harga"]               = $value["trx_pos_detail_harga"];
                                $transaksi_detail["trx_pos_detail_disc"]                = $value["trx_pos_detail_disc"];
                                $transaksi_detail["trx_pos_detail_total"]               = $value["trx_pos_detail_total"];
                                $transaksi_detail["barang_id"]                          = $value["barang_id"];
                                $transaksi_detail["trx_pos_no"]                         = $value["trx_pos_no"];
                                
                              
                                     
                             
                                 if ($crud->save_as_new('trx_pos_detail', $transaksi_detail)) {
                                         $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh()');
                                }
                            }  
                            
            } else {
                
                $mpenjualan['trx_pos_tgl_ubah']               = date("Y-m-d");
                $mpenjualan['trx_pos_petugas']                = $this->SESSION["user_username"];

                $key = array("trx_pos_no"=> $id);
                if ($crud->save('trx_pos', arrman::StripTagFilter($mpenjualan), $key)) {
                    $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh()');
                }
                
                $key2    = array("trx_pos_no"=> $this->input->post("trx_pos_no"));
                $crud->delete("trx_pos_detail",$key2);
                
                  $tmp_detail = $this->input->post("data_grid");
                                if($this->scan_array($tmp_detail) == FALSE){
                                    $data_detail = ($this->setDataDetail($tmp_detail));
                                 }
                           
                                foreach($data_detail as $value){
                                
                                $transaksi_detail = array();
                                $transaksi_detail["trx_pos_detail_qty"]                 = $value["trx_pos_detail_qty"];
                                $transaksi_detail["trx_pos_detail_harga"]               = $value["trx_pos_detail_harga"];
                                $transaksi_detail["trx_pos_detail_disc"]                = $value["trx_pos_detail_disc"];
                                $transaksi_detail["trx_pos_detail_total"]               = $value["trx_pos_detail_total"];
                                $transaksi_detail["barang_id"]                          = $value["barang_id"];
                                $transaksi_detail["trx_pos_no"]                         = $value["trx_pos_no"];
                              
                                     
                             
                                 if ($crud->save_as_new('trx_pos_detail', $transaksi_detail)) {
                                         $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh()');
                                }
                            }  
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors("<span class='error_mess'>","</span>"), "refresh()");
        }

        echo json_encode($message,true);
    }

    function konversi_tanggal($date)    
    {    
        $exp = explode('-',$date);    
        if(count($exp) == 3)    
        {    
            $date = $exp[2].'-'.$exp[1].'-'.$exp[0];    
        }    
            return $date;    
    }   
    
    function scan_array($array = array()){
        if(empty($array)) return TRUE;

        return FALSE;
    }
    
    function  setDataDetail($data = array()){
            $arrBaris = array();
            $arrBaris = preg_split("[~]",$data);

                foreach($arrBaris as $baris){
                    $arrData[] = preg_split("[`]",$baris);
                }

            $arrKolom = array(
            "kol1"=>"trx_pos_detail_qty",
            "kol2"=>"trx_pos_detail_harga",
            "kol3"=>"trx_pos_detail_disc",
            "kol4"=>"trx_pos_detail_total",
            "kol5"=>"barang_id",
            "kol6"=>"trx_pos_no",
            
           
                
            );

        $dataGrid = array();
        foreach($arrData as $key => $value){
            $i = 0;
            foreach($arrKolom as $keyKol => $valKol){
                $dataGrid[$key][$valKol] = $arrData[$key][$i];

                $i++;
            }
        }

        return $dataGrid;
    }
    
    /*CUSTOM VALIDATION - cek id jika ada*/
    public function ketersediaan_check()
    {   
        $trx_pos_no = $this->input->post("trx_pos_no");
        
        $ada = $this->penjualan->hitungJikaAda($trx_pos_no);

        if ($ada > 0)
        {   
            $message = 'No.Penjualan <b><u>' . $trx_pos_no . '</u></b> sudah terdaftar';
            $this->form_validation->set_message('ketersediaan_check', $message);
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - DELETE DATA penjualan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function delete() {
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        $key = array("trx_pos_no"=> $this->input->post("id"));
        if ($crud->delete("perusahaan",$key)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '');
        }

        echo json_encode($message);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - GET DATA perusahaan options
    //-------------------------------------------------------------------------------------------------------------------------------
    public function getPenjualanOptions($key = ''){
        $key = array("trx_pos_no" => $this->input->post("trx_pos_no"));
        $data = $this->penjualan->options("--Pilih Penjualan--",$key);

        echo json_encode($data);
    }
    
     public function loadform2(){
        
        
      $trx_pos_no                       = $this->input->get('trx_pos_no');
      $trx_pos_sub_total                = $this->input->get('trx_pos_sub_total');
      $trx_pos_diskon                   = $this->input->get('trx_pos_diskon');
      $trx_pos_tax                      = $this->input->get('trx_pos_tax');
      $trx_pos_grand_total              = $this->input->get('trx_pos_grand_total');
    
            $data["trx_pos_no"]             = $trx_pos_no;
            $data["trx_pos_sub_total"]      = $trx_pos_sub_total;
            $data["trx_pos_diskon"]         = $trx_pos_diskon;
            $data["trx_pos_tax"]            = $trx_pos_tax;
            $data["trx_pos_grand_total"]    = $trx_pos_grand_total;
          
            $data["readonly"] = true; 
            $data['bank_options']           = $this->penjualan->options2("--Pilih Bank--");
            $data['form_action']           = base_url() . $this->_module . '/proses_update';

        
        $this->load->view($this->_module . '/form2',$data);
        

    }
    
     public function proses_update(){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;
     
            $mpenjualan = array();
            
            
            $mpenjualan['trx_pos_no']                            = $this->input->post('trx_pos_no');
            $mpenjualan['trx_pos_jenis_pembayaran']              = $this->input->post('trx_pos_jenis_pembayaran');
            $mpenjualan['trx_pos_bayar']                         = $this->input->post('trx_pos_bayar');
            $mpenjualan['bank_id']                               = $this->input->post('bank_id');
            $mpenjualan['trx_pos_no_kartu']                      = $this->input->post('trx_pos_no_kartu');
            $mpenjualan['kembalian']                             = $this->input->post('kembalian');
                        
            $no_faktur                                               = $this->input->get('trx_pos_no');
            
                $key = array("trx_pos_no"=> $no_faktur);
                if ($crud->save('trx_pos', $mpenjualan, $key)) {
                    $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh()');
                }
                
               
          
         echo json_encode($no_faktur,true);
    }
    
    function invoice(){ 
       
        $trx_pos_no              =$this->input->get('trx_pos_no');
        $username                    =$this->SESSION["m_user_username"];
                
              
                $this->load->library('header/header_invoice');
                //$this->load->library('myfunctions');
                $pdf = new header_invoice('L', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Vredy Wijaya');
		$pdf->SetTitle('Invoice');
		$pdf->SetSubject('F');
		$pdf->setPrintHeader(true);
		$pdf->setPrintFooter(true);
              
                $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));		
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);		
		$pdf->SetMargins(5,30, 5);
		$pdf->SetHeaderMargin(10);
		$pdf->SetFooterMargin(23);
		
		$pdf->SetFont('times', '', 9);
		$pdf->AddPage();
               /*
                $html = '<table width="791" border="0">';
               
                $html .= '
                        <tr>
                        <td></td>
                        <td></td>
                        </tr>                                      
                    ';
                
                $html .= '</table>';
                */
                  $html = '<table width="50%" border="0">';
               
                $html .= '
                        <tr>
                        <td></td>
                       
                        </tr>                                      
                    ';
                
                $html .= '</table>';
                  //$no=1;
                    /*
                $html .= '<span align="center" style="font-size:14px;font-weight:bold;">
                    <b>INVOICE</b>
                    </span>
                    <br />
                  ';
                */
                
                 $sql="SELECT * FROM 1nt3x_trx_pos WHERE trx_pos_no = '$trx_pos_no'";
                 $result= $this->db->query($sql);
                 $r	=$result->row();
                 
                foreach($result->result() as $r){
                    
                   
                    $trx_pos_tgl          = $r->trx_pos_tgl;
                    $trx_pos_sub_total    = $r->trx_pos_sub_total;
                    $trx_pos_diskon       = $r->trx_pos_diskon;
                    $trx_pos_tax          = $r->trx_pos_tax;
                    $trx_pos_grand_total  = $r->trx_pos_grand_total;
                    $jenis_pembayaran     = $r->jenis_pembayaran;
                    $trx_pos_bayar        = $r->trx_pos_bayar;
                    // $nama_pemilik_kartu     = $r->nama_pemilik_kartu;
                    $trx_pos_no_kartu     = $r->trx_pos_no_kartu;
                    $bank_id              = $r->bank_id;
                    // $booking_kode           = $r->booking_kode;
                    //  $booking_id             = $r->booking_id;
                    //  $member_kode            = $r->member_kode;
                    $kembalian            = $r->kembalian;
                  
               
                
                 $html .= '<table width="0%" border="0">';
               
                $html .= '
                        <tr>
                        <td>';
                
                        $html .= '<table width="50%">';
               
                        $html .= '
                            <tr>
                            <td width="20%">No.Invoice</td>
                            <td>: '.$trx_pos_no.'</td>
                            </tr>
                            <tr>
                            <td>Tgl.Transaksi</td>
                            <td>: '.$trx_pos_tgl.'</td>
                            </tr>
                            
                            
                        ';
                
                        $html .= '</table>';
                 
                 
           // }

               
                         $html .= '
                             <br />
                             </td>
                        </tr> 
                        <tr>
                        <td>
                    ';
                
               
                         
                $html .= '<table width="50%" border="0">';
                $html .= '
                        <tr>
                        <td width="20" align="center"><b> No.</b></td>
                        <td width="60" align="center"><b> ID.Item</b></td>
                        <td width="60" align="center"><b> Item</b></td>
                        <td width="60" align="center"></td>
                        
                        </tr>
                    ';
                
                $no = 1;
               
                 $sql2="SELECT * FROM 1nt3x_trx_pos_detail where trx_pos_no = '$trx_pos_no'";
                 $result2 = $this->db->query($sql2);
                 $r2	=$result2->row();
                   
                        foreach($result2->result() as $r2){	
                            
                 $barang_id = $r2->barang_id;
                 $trx_pos_detail_qty = $r2->trx_pos_detail_qty;
                 $trx_pos_detail_disc = $r2->trx_pos_detail_disc;
                 $trx_pos_detail_total = $r2->trx_pos_detail_total;
                 
                 $sql3="SELECT * FROM 1nt3x_m_item where m_item_no_kode = '$barang_id'";
                 $result3 = $this->db->query($sql3);
                 $r3	=$result3->row();
                 $total3     =$result3->num_rows();
                 
                // if($total3 > 0){
                     
                     $m_item_nama = $r3->m_item_nama;
                     $trx_pos_detail_harga = $r3->trx_pos_detail_harga;
                   /*  
                 }else{
                     
                        $sql4="SELECT * FROM 1nt3x_produk_paket where produk_paket_kode = '$produk_kode'";
                        $result4 = $this->db->query($sql4);
                        $r4	=$result4->row();
                        
                        $produk_nama = $r4->produk_paket_nama;
                        $produk_harga = $r4->produk_paket_harga;
                 }
                */
                $total = $trx_pos_detail_harga * $trx_pos_detail_qty;
                $html .= '
                        <tr>
                        <td align="center"> '.$no.'</td>
                        <td> '.$bank_id.'</td>
                        <td> '.$m_item_nama.'</td>
                        <td align="right"></td>
                        </tr>
                        
                    ';
                 $html .= '
                        <tr>
                        <td></td>
                        
                        <td> '.number_format( $trx_pos_detail_harga , 0 , "" , '.' ) .' qty : '.$trx_pos_detail_qty.'</td>
                         <td> '.$trx_pos_detail_disc.'</td>
                        <td align="right"> '.number_format( $trx_pos_detail_total , 0 , "" , '.' ) .'</td>
                        </tr>
                        
                    ';
             
                $no++;
                 
                        }
              
                     
                $html .= '</table>';
                
                 $html .= '
                             </td>
                        </tr> 
                        <tr>
                        <td>
                    ';
                 
                $html .= '<table width="50%" border="0">';
                
                 $html .= '

                  <tr>
                    <td width="20"> 
                     </td>
                    <td width="60"> 
                    </td>
                   
                    <td width="120">
                    <br/>
                    <br/>
                        ';
                            
                            $html .= '<table>';
                            $html .= '
                                <tr>
                                <td>
                                Total
                                </td>
                                <td align = "right">
                                '.number_format( $trx_pos_sub_total , 0 , "" , '.' ) .'
                                </td>
                                </tr>
                                
                                 <tr>
                                <td>
                                Diskon(%)
                                </td>
                                <td align = "right">
                                '.$trx_pos_diskon.'
                                </td>
                                </tr>
                                
                                 <tr>
                                <td>
                                Pajak(%)
                                </td>
                                <td align = "right">
                                '.$trx_pos_tax.'
                                </td>
                                </tr>
                                
                                 <tr>
                                <td>
                                Grand Total
                                </td>
                                <td align = "right">
                                '.number_format( $trx_pos_grand_total , 0 , "" , '.' ) .'
                                </td>
                                </tr>
                                 
                                ';
                            
                            if($jenis_pembayaran == 'Cash'){
                                $html .= '
                                 <tr>
                                <td>
                                Cash
                                </td>
                                <td align = "right">
                                '.number_format( $trx_pos_bayar , 0 , "" , '.' ) .'
                                </td>
                                </tr>

                                ';
                            }
                            
                         
                            if($jenis_pembayaran == 'Debit'){
                                $html .= '
                                 <tr>
                                <td>
                                Debit
                                </td>
                                <td align = "right">
                                '.number_format( $trx_pos_bayar , 0 , "" , '.' ) .'
                                </td>
                                </tr>

                                ';
                            }
                            
                            if($jenis_pembayaran == 'Credit'){
                                $html .= '
                                 <tr>
                                <td>
                                Credit
                                </td>
                                <td align = "right">
                                '.number_format( $trx_pos_bayar , 0 , "" , '.' ) .'
                                </td>
                                </tr>

                                ';
                            }
                            
                             $html .= '
                                 <tr>
                                <td>
                                Kembali
                                </td>
                                <td align = "right">
                                '.number_format( $kembalian , 0 , "" , '.' ) .'
                                </td>
                                </tr>

                                ';
                             
                             $html .= '</table>';
                             
                             
                 $html .= '
                    </td>
                    
                  </tr>

                    ';
                
                $html .= '</table>';
                
                 $html .= '
                             </td>
                        </tr> 
                        
                    ';
                 
                  $html .= '</table>';
                
               }
                
                $pdf->writeHTML($html, true, false, true, false, '');
                //$pdf->AddPage();
                $pdf->Output('invoice.pdf', 'I');
    }
}

?>