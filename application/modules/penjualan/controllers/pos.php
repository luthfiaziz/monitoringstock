<?php
/**
 * User: Bayu Nugraha (bayu.nugraha@intex.co.id)
 * Date: 30/09/15
 * Time: 12:09
 */

class Pos extends MX_Controller{
    // VARIABLE MODUL
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT penjualan
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();

        // Set Module Location
        $this->_module = 'penjualan/pos';
        // Load Module Core
        $this->load->module("auth/app_auth");
        $this->load->module("config/app_setting");
        $this->load->module("crud/app_crud");
        // Load Model
        $this->load->model('pos_model','pos');
        $this->load->model('master/list_model','list');
        $this->load->model('master/toko_model','toko');
        $this->load->model('pengaturan/pengguna_model','pengguna');

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    public function index(){
        $setting = $this->app_setting;

        $setting->set_plugin(array(
            'jquery','jquery-form','bgrins-spectrum-9fe9e8c',
            'custom', 'fullscreen-jquery','select2-jquery',
            'jquery-scrollbar','jquery-csv-toarray','bootstrap','dhtmlx',
            'css-form','custom-scrollbar','mouseover','anchorjquery','numeric','autonumeric','alphanumeric','jquery_print'
        ));

        $dataPerusahaan     = $this->cpanel->getPerusahaanProfile();
        $icon               = empty($dataPerusahaan->profile_app_icon)?"icon_intex.png":$dataPerusahaan->profile_app_icon;
        $data['icon']       = base_url().'assets/img/logo/'.$icon;
        $data["js_header" ] = $setting->get_js();
        $data["css_header"] = $setting->get_css();

        if(empty($this->SESSION['toko_kode'])){
            echo "Please login first";
        }else{
            $this->load->view($this->_module . "/index",$data);
        }
    }

    public function form(){
        $data['no_invoice'] = $this->generateNo();
        $data['bank']       = $this->pos->optionsBank("--Pillih Bank--");

        $this->load->view($this->_module . "/form",$data);
    }

    public function loadFormMultyPayment(){
        $data['bank']       = $this->pos->optionsBank("--Pillih Bank--");

        $this->load->view($this->_module . "/form_pembayaran",$data);
    }

    public function loadListToolbar(){
        xml::xml_header();

        $xml  = '<toolbar>';
        $xml .= '<item id="pillih" text="Pillih" type="button" img="Button Check-01.png" imgdis="Button Check-01.png" action="pillihItem"/>';
        $xml .= "</toolbar>";

        echo $xml;
    }

    public function loadListBarang($kode_kat = ""){
        $key    = array('m_item_kategori_kode' => $kode_kat);
        $result = $this->pos->loadListDataBarang($key);

        echo json_encode($result);
    }

    public function list_kategori(){
        $data  	  = $this->pos->loadDataKategori();

        echo json_encode($data);
//        echo $data;
    }

    public function generateNo(){
        $dataMax    = $this->pos->getMaxData();
        $noNow      = $dataMax->noMax + 1;
//        $noNew      = sprintf("%07s",$noNow)."-".$this->SESSION['toko_kode']."-".date('m')."-".date('y');
        $noNew      = $this->SESSION['toko_kode']."/".date('m')."/".date('Y')."/".sprintf("%05s",$noNow);

        return $noNew;
    }

    public function scanBarcode(){
        $barcode= $this->input->post('barcode');
        $key    = array('a.m_item_barcode' => $barcode);
        $result = $this->pos->data($key)->get();

        if($result->num_rows()==1){
            $hasil  = array(
                1,
                $result->row()->m_item_kode,
                $result->row()->m_item_nama,
                ($result->row()->m_harga_jual=="")?"0":$result->row()->m_harga_jual
            );
        }else{
            $hasil  = array(0,'Barcode tidak terdaftar');
        }

        echo json_encode($hasil);
    }

    public function getDataBarang(){
        $kode_item  = $this->input->post('kode_item');
        $key        = array('a.m_item_kode' => $kode_item);
        $result     = $this->pos->data($key)->get();

            $hasil  = array(
                1,
                $result->row()->m_item_kode,
                $result->row()->m_item_nama,
                ($result->row()->m_harga_jual=="")?"0":$result->row()->m_harga_jual
            );

        echo json_encode($hasil);
    }

    public function cekPromo(){
        $kode_item  = $this->input->post('kode_item');
        $qty        = $this->input->post('qty');

        $dataPromo  = $this->pos->getDataPromo($kode_item);
        if($dataPromo == 0){
            $hasil  = array(0,0);
        }else{
            if($qty >= $dataPromo->m_harga_promo_qty_promo){
                $kelipatan  = floor($qty / $dataPromo->m_harga_promo_qty_promo);
                $hargaPromo = $dataPromo->m_harga_promo_harga;
                if($kelipatan != 0){
                    $hargaPromo = $dataPromo->m_harga_promo_harga * $kelipatan;
                }
                $hasil  = array(1,$hargaPromo);
            }else{
                $hasil  = array(0,$dataPromo->m_harga_promo_qty_promo);
            }
        }

        echo json_encode($hasil);
    }

    public function proses(){
        $crud = $this->app_crud;

        $no_trx                         = $this->generateNo();
        $dataIns                        = $this->input->post('dataIns');
        $dataIns['trx_pos_no']          = $no_trx;
        $dataIns['trx_pos_tgl']         = date('Y-m-d H:i:s');
        $dataIns['trx_pos_ket']         = "POS toko oleh " . $this->SESSION['user_username'];
        $dataIns['trx_pos_tgl_buat']    = date('Y-m-d');
        $dataIns['trx_pos_petugas']     = $this->SESSION['user_username'];
        $dataIns['toko_kode']           = $this->SESSION['toko_kode'];
        $dataIns['trx_pos_tax']         = $dataIns['trx_pos_sub_total'] * 0.1;
        $dataIns['trx_pos_kembalian']   = $this->input->post('trx_pos_kembalian');

        $this->db->trans_begin();

        $crud->save_as_new_nrb('trx_pos', arrman::StripTagFilter($dataIns));

        $dataGrid   = $this->input->post('dataGrid');
        foreach($dataGrid as $val){
            $dataInsDet = array(
                'trx_pos_detail_qty'    => $val[4],
                'trx_pos_detail_harga'  => $val[3],
                'trx_pos_detail_disc'   => $val[5],
                'trx_pos_detail_total'  => $val[6],
                'item_kode'             => $val[0],
                'trx_pos_no'            => $no_trx
            );
            $crud->save_as_new_nrb('trx_pos_detail', arrman::StripTagFilter($dataInsDet));

            $dataInsSB  = array(
                'stok_berjalan_tgl'     => date('Y-m-d'),
                'stok_berjalan_ket'     => "POS toko",
                'stok_berjalan_no_trx'  => $no_trx,
                'stok_berjalan_tgl_buat'=> date('Y-m-d'),
                'stok_berjalan_petugas' => $this->SESSION['user_username'],
                'm_toko_kode'           => $this->SESSION['toko_kode'],
                'm_item_kode'           => $val[0],
                'stok_berjalan_qty_masuk'=> 0,
                'stok_berjalan_qty_keluar'=> $val[4]
            );
            $crud->save_as_new_nrb('inv_stok_berjalan', arrman::StripTagFilter($dataInsSB));
        }

        $dataGridPembayaran = $this->input->post('dataGridPembayaran');
        foreach($dataGridPembayaran as $val){
            $dataInsJns = array(
                'trx_pos_jns_pem_status'    => $val[0],
                'trx_pos_jns_pem_bayar'     => $val[1],
                'trx_pos_jns_pem_no_kartu'  => $val[2],
                'trx_pos_jns_pem_no_ref'    => $val[3],
                'trx_pos_no'                => $no_trx
            );
            if($val[0] != "cash"){
                $dataInsJns['m_bank_id']    = $val[5];
            }
            $crud->save_as_new_nrb('trx_pos_jns_pem', arrman::StripTagFilter($dataInsJns));
        }

        $dataLog    = array(
            'log_user'      => $this->SESSION['user_username'],
            'log_kegiatan'  => "Simpan data POS toko " . $no_trx,
            'log_waktu'     => date('Y-m-d H:i:s')
        );
        $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));

        if ($this->db->trans_status() === FALSE) {
            $message = array(false, 'Proses Gagal', 'Proses penyimpanan data gagal ( data di rollback ).', '');
            $this->db->trans_rollback();
        } else {
            $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refreshPos("' . $no_trx . '")');
            $this->db->trans_commit();
        }

        echo json_encode($message,true);
    }

    public function prosesCash(){
        $crud = $this->app_crud;

        $no_trx                         = $this->generateNo();
        $dataIns                        = $this->input->post('dataIns');
        $dataIns['trx_pos_no']          = $no_trx;
        $dataIns['trx_pos_tgl']         = date('Y-m-d H:i:s');
        $dataIns['trx_pos_ket']         = "POS toko oleh " . $this->SESSION['user_username'];
        $dataIns['trx_pos_tgl_buat']    = date('Y-m-d');
        $dataIns['trx_pos_petugas']     = $this->SESSION['user_username'];
        $dataIns['toko_kode']           = $this->SESSION['toko_kode'];
        $dataIns['trx_pos_tax']         = $dataIns['trx_pos_sub_total'] * 0.1;
        $dataIns['trx_pos_kembalian']   = $this->input->post('trx_pos_kembalian');

        $this->db->trans_begin();

        $crud->save_as_new_nrb('trx_pos', arrman::StripTagFilter($dataIns));

        $dataGrid   = $this->input->post('dataGrid');
        foreach($dataGrid as $val){
            $dataInsDet = array(
                'trx_pos_detail_qty'    => $val[4],
                'trx_pos_detail_harga'  => $val[3],
                'trx_pos_detail_disc'   => $val[5],
                'trx_pos_detail_total'  => $val[6],
                'item_kode'             => $val[0],
                'trx_pos_no'            => $no_trx
            );
            $crud->save_as_new_nrb('trx_pos_detail', arrman::StripTagFilter($dataInsDet));

            $dataInsSB  = array(
                'stok_berjalan_tgl'     => date('Y-m-d'),
                'stok_berjalan_ket'     => "POS toko",
                'stok_berjalan_no_trx'  => $no_trx,
                'stok_berjalan_tgl_buat'=> date('Y-m-d'),
                'stok_berjalan_petugas' => $this->SESSION['user_username'],
                'm_toko_kode'           => $this->SESSION['toko_kode'],
                'm_item_kode'           => $val[0],
                'stok_berjalan_qty_masuk'=> 0,
                'stok_berjalan_qty_keluar'=> $val[4]
            );
            $crud->save_as_new_nrb('inv_stok_berjalan', arrman::StripTagFilter($dataInsSB));
        }

        $dataInsJns = array(
            'trx_pos_jns_pem_status'  => 'cash',
            'trx_pos_jns_pem_bayar'   => $this->input->post('trx_pos_bayar'),
            'trx_pos_no'              => $no_trx
        );
        $crud->save_as_new_nrb('trx_pos_jns_pem', arrman::StripTagFilter($dataInsJns));

        $dataLog    = array(
            'log_user'      => $this->SESSION['user_username'],
            'log_kegiatan'  => "Simpan data POS toko " . $no_trx,
            'log_waktu'     => date('Y-m-d H:i:s')
        );
        $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));

        if ($this->db->trans_status() === FALSE) {
            $message = array(false, 'Proses Gagal', 'Proses penyimpanan data gagal ( data di rollback ).', '');
            $this->db->trans_rollback();
        } else {
            $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refreshPos("' . $no_trx . '")');
            $this->db->trans_commit();
        }

        echo json_encode($message,true);
    }

    public function print_struk($no_faktur=''){
        $arrFak             = explode("-",$no_faktur);
        $no_faktur          = $arrFak[0] . "/" . $arrFak[1] . "/" . $arrFak[2] . "/" . $arrFak[3];
        $data['faktur']     = $no_faktur;
        $data['pos']        = $this->pos->dataPos($no_faktur)->get()->row();
        $data["toko"]       = $this->toko->data($this->SESSION['toko_kode'])->get()->row();
        $data['posDetail']  = $this->pos->dataPosDetail($no_faktur)->get()->result();
        $data['posJenis']   = $this->pos->dataPosJenis(array("a.trx_pos_no" => $no_faktur))->get()->result();
        $data['user']       = $this->pengguna->data($this->SESSION['user_id'])->get()->row();

        $this->load->view($this->_module."/struk",$data);
    }
}