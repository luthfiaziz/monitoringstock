<?php
/**
 * User: Bayu Nugraha (bayu.nugraha@intex.co.id)
 * Date: 30/09/15
 * Time: 17:34
 */

class Pos_model extends CI_Model {
    private $_table1 = "trx_pos";
    private $_table2 = "trx_pos_detail";
    private $_table3 = "m_item";
    private $_table4 = "m_item_kategori";
    private $_table5 = "trx_pos_jns_pem";
    private $_table6 = "m_bank";
    private $SESSION;

    public function __construct() {
        parent::__construct();
        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    public function getMaxData(){
        $sql    = $this->db->query("SELECT MAX(RIGHT(trx_pos_no,5)) as noMax FROM 1nt3x_trx_pos WHERE toko_kode = '" . $this->SESSION['toko_kode'] . "'")->row();

        return $sql;
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("trx_pos_no" => $key);

        return $key;
    }

    public function dataPos($key = ""){
        $this->db->from($this->_table1);
        $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function dataPosDetail($key = ""){
        $this->db->select("a.*,b.m_item_nama");
        $this->db->from($this->_table2 . " a");
        $this->db->join($this->_table3 . " b","a.item_kode=b.m_item_kode","left");
        $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function dataPosJenis($key = ""){
        $this->db->select("a.*,b.m_bank_nama");
        $this->db->from($this->_table5 . " a");
        $this->db->join($this->_table6 . " b","a.m_bank_id=b.m_bank_id","left");
        $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadDataKategori(){
        $this->db->from($this->_table4);
        $result = $this->db->get();
        $data   = array();
//        $data   = '<data>';
        foreach ($result->result() as $value) {
//            $data   .= '
//                <item id="' . $value->m_item_kategori_kode . '">
//                    <Nama><![CDATA[' . $value->m_item_kategori_nama . ']]></Nama>
//                    <Ket><![CDATA[' . $value->m_item_kategori_ket . ']]></Ket>
//                </item>';
            $data[] = array(
                "id"=>$value->m_item_kategori_kode,
                "Nama"=>$value->m_item_kategori_nama,
                "Ket"=>$value->m_item_kategori_ket
            );
        }
//        $data   .= '</data>';

        return $data;
    }

    public function data($key = ""){
        $this->db->select("a.*,b.m_harga_beli,b.m_harga_jual");
        $this->db->from($this->_table3 . " a");
        $this->db->join("m_harga b","a.m_item_kode=b.m_item_kode","left");

        if(!empty($key))
            $this->db->where($key);

        return $this->db;
    }

    public function loadListDataBarang($key = ""){

        $result = $this->data($key)->get();
        $data   = array();
        foreach ($result->result() as $value) {
            $data[] = array(
                "id"        => $value->m_item_kode,
                "Nama"      => $value->m_item_nama,
                "Harga"     => ($value->m_harga_jual=="")?"0":$value->m_harga_jual,
                "Ket"       => $value->m_item_ket,
                "Barcode"   => $value->m_item_barcode
            );
        }

        return $data;
    }

    public function getDataPromo($key = ""){
        $this->db->select("a.*,b.m_item_nama");
        $this->db->from('m_harga_promo a');
        $this->db->join($this->_table3 . " b","a.m_item_kode=b.m_item_kode","right");
        $this->db->where(array(
            'a.m_item_kode'                     => $key,
            "m_harga_promo_periode_mulai <="    => date('Y-m-d'),
            "m_harga_promo_periode_akhir >="    => date('Y-m-d')
        ));

        $result     = $this->db->get()->row();
        $promo_id   = $result->m_harga_promo_id;

        $hit    = $this->cekDetailPromo($promo_id);
        if($hit>0){
            return $result;
        }else{
            return 0;
        }
    }

    public function cekDetailPromo($kode_promo = ""){
        $this->db->select("COUNT(m_toko_kode) as hit");
        $this->db->from('m_harga_promo_detail');
        $this->db->where(array('m_harga_promo_id' => $kode_promo,'m_toko_kode' => $this->SESSION['toko_kode']));

        return $this->db->get()->row()->hit;
    }

    public function optionsBank($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from("m_bank");

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->m_bank_id . "|" . $row->m_bank_nama] = $row->m_bank_nama;
        }

        return $option;
    }
}