<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Vredy Wijaya 
 * Create Date : 29/09/2015 - 10:13:00
 * Module Name : penjualan Model ( For Template )
 */

class penjualan_model extends CI_Model {
    private $_table1 = "trx_pos";
    private $_table2 = "m_item";
    private $_table3 = "m_bank";
    
   
     public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("trx_pos_no" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->from($this->_table1);
       

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData($key){
    	$result = $this->data($key)->get();
    	$data   = array();

    	foreach ($result->result() as $value) {
            $trx_pos_no = $value->trx_pos_no;
            $printbutton = "<input class='buttongrid' type='button' value='Print Invoice' onclick=\"printInvoice('" . $trx_pos_no. "')\"></input>";
			$data[] = array(
							"id" => $value->trx_pos_no,
							"data"=>array(
                                                                                    $printbutton,
                                                                                    $value->trx_pos_no,
                                                                                    $value->trx_pos_tgl,
                                                                                    $value->trx_pos_ket,
                                                                                    $value->trx_pos_sub_total,
                                                                                    $value->trx_pos_diskon,
                                                                                    $value->trx_pos_tax,
                                                                                    $value->trx_pos_grand_total
                                                                                    
								         )
						    );
    	}

    	return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where("trx_pos_no",$value);

        $total=$this->db->count_all_results();

        return $total;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->trx_pos_no] = $row->trx_pos_no;
        }

        return $option;
    }
    
     public function options1($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table2);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            
             
                $option[$row->m_item_no_kode.'|'.$row->m_item_nama.'|'.$row->m_item_harga] = $row->m_item_no_kode;
        }

        return $option;
    }
    
         public function options2($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table3);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->m_bank_id] = $row->m_bank_nama;
        }

        return $option;
    }
}
