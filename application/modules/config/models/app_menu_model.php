<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Luthfi Aziz Nugraha 
 * Create Date : 26/07/2014 - 00:43:05
 * Module Name : Menu Model ( For Template )
 */

class app_menu_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }

    private $_tmenu1 = "1nt3x_menus";
    private $_tmenu2 = "1nt3x_menus_role";
    private $_tmenu3 = "m_grup_user";

    private function _kunci($data = array()){
        $result = $data;

        if(!is_array($data))
            $result = array("1nt3x_menus_id" => $data);

        return $result;
    }

    public function data_menu($id = '', $role_id = ''){
        $sql = "SELECT
                    *
                FROM
                    (
                        SELECT
                            `a`.*, `child`.`have_child`
                        FROM
                            (`1nt3x_menus` a)
                        LEFT OUTER JOIN(
                            SELECT
                                menu_parent,
                                COUNT(*)AS have_child
                            FROM
                                `1nt3x_menus`
                            GROUP BY
                                menu_parent
                        )child ON `a`.menu_id = `child`.menu_parent
                    )my_menu
                JOIN `1nt3x_roles_access` b ON `b`.`menu_id` = `my_menu`.`menu_id`
                JOIN `1nt3x_roles` c ON `c`.`role_id` = `b`.`role_id` 
                WHERE 
                    `my_menu`.`menu_parent` = '{$id}' 
                AND
                    `b`.`role_id`   = '{$role_id}'
                ORDER BY `my_menu`.`menu_position` ASC
                ";

        return $this->db->query($sql);
    }

    public function clickcount($menuid = "",$roleid = "", $userid = ""){
        return $this->db->query("UPDATE 1nt3x_menus_click_count SET m_click_count = m_click_count + 1 WHERE 1nt3x_menus_id = '{$menuid}' AND role_id ='{$roleid}' AND m_user_id = '{$userid}'");
    }

    

}

/* End of file tm_gelar.php */
/* Location: ./application/modules/crud/models/crud.php */