$(document).ready(function(){
   $(document).delegate(".keypress","keyup",function(event){
        fungsi = $(this).attr("fungsi");
        if (event.keyCode == '13') {
            return eval(fungsi);
        }

        return false;
    });

   $(document).delegate(".onlynumber","keydown",function(event){
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 
            || event.keyCode == 27 || event.keyCode == 13 
            || (event.keyCode == 65 && event.ctrlKey === true) 
            || (event.keyCode >= 35 && event.keyCode <= 39)){
                return;
        }else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
   });

   $(document).delegate(".onlychar","keyup blur",function(){
        var node = $(this);
        node.val(node.val().replace(/[^a-z-A-Z]/g,'') );
   });


    $(document).bind("contextmenu",function(e) {
     e.preventDefault();
    });
});