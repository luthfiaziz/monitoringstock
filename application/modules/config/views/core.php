<html>
<link rel="icon" href="<?php echo $icon; ?>" type="image/logo">
<head>
	<title><?php echo $title; ?></title>
	<?php 
		echo $js_header;
		echo $css_header;
	?>
    <script type="text/javascript">
        // $(document).ready(function(){$(document).delegate(".keypress","keyup",function(event){return fungsi=$(this).attr("fungsi"),"13"==event.keyCode?eval(fungsi):!1}),$(document).delegate(".onlynumber","keydown",function(e){46==e.keyCode||8==e.keyCode||9==e.keyCode||27==e.keyCode||13==e.keyCode||65==e.keyCode&&e.ctrlKey===!0||e.keyCode>=35&&e.keyCode<=39||(e.shiftKey||(e.keyCode<48||e.keyCode>57)&&(e.keyCode<96||e.keyCode>105))&&e.preventDefault()}),$(document).delegate(".onlychar","keyup blur",function(){var e=$(this);e.val(e.val().replace(/[^a-z-A-Z]/g,""))}),$(document).bind("contextmenu",function(e){e.preventDefault()})});
		$(document).ready(function(){
		   $(document).delegate(".keypress","keyup",function(event){
		        fungsi = $(this).attr("fungsi");
		        if (event.keyCode == '13') {
		            return eval(fungsi);
		        }

		        return false;
		    });

		   $(document).delegate(".onlynumber","keydown",function(event){
		        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 
		            || event.keyCode == 27 || event.keyCode == 13 
		            || (event.keyCode == 65 && event.ctrlKey === true) 
		            || (event.keyCode >= 35 && event.keyCode <= 39)){
		                return;
		        }else {
		            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
		                event.preventDefault(); 
		            }   
		        }
		   });

		   $(document).delegate(".onlychar","keyup blur",function(){
		        var node = $(this);
		        node.val(node.val().replace(/[^a-z-A-Z]/g,'') );
		   });

		    $(document).delegate(".disabledspace","keydown",function(event){
		        if (event.keyCode == 32){
		        	event.preventDefault();
		        }
		   });



		    $(document).bind("contextmenu",function(e) {
		     e.preventDefault();
		    });
		});
		
    </script>
</head>
<body>
	<?php (!empty($page_content))?$this->load->view($page_content)  : 'Setting aplikasi anda dengan benar'; ?>
</body>
</html>