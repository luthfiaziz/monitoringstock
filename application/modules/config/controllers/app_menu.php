<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Developed By : Luthfi Aziz Nugraha
 * Module Name  : App Menu ( For Template )
 */

class App_menu extends MX_Controller {
    // VARIABEL SESSION TEMP
    private $SESSION;

    public function __construct() {
        parent::__construct();
        $this->load->model("app_menu_model");

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    public function get_tree($grup_id = ''){
        xml::xml_header();

        echo '<tree id="0">';      

        $this->get_menu_tree(0,$grup_id);

        echo '</tree>'; 
    }

    public function get_top($grup_id = ''){
        xml::xml_header();
        
        echo '<menu>';      

        $this->get_menu_top(0,$grup_id);

        echo '</menu>'; 
    }

	public function get_menu_top($id = '', $grup_id = '') {
            $m_module=$this->app_menu_model->data_menu($id, $grup_id);

            if($m_module->num_rows()>0){
                foreach($m_module->result() as $rs) {
                    if($rs->menu_type == "separator"){
                            echo "<item id=\"" . $rs->menu_url . "\" type=\"" . $rs->menu_type  . "\"/>";
                    }else{

                        if($rs->role_access_view == 1){
                        if($rs->have_child > 0){
                            echo "<item id=\"" . $rs->menu_url . "\" text=\"" . ucwords($rs->menu_name) . "\"  img=\"" . $rs->menu_img . "\"  imgdis=\"" . $rs->menu_img . "\">";          

                            $this->get_menu_top($rs->menu_id,$rs->role_id);                       

                            echo "</item>";
                        }else if($rs->have_child == null || $rs->have_child == "" ){
                            echo "<item id=\"" . $rs->menu_url . "\" text=\"" . ucwords($rs->menu_name) . "\"  img=\"" . $rs->menu_img . "\"  imgdis=\"" . $rs->menu_img . "\"/>";
                        }else{

                        }
			}
                    }
                }		    
            }
	}

    public function get_menu_tree($id = '', $grup_id = '') {
            $m_module=$this->app_menu_model->data_menu($id, $grup_id);

            if($m_module->num_rows()>0){
                foreach($m_module->result() as $rs) {
                    if($rs->have_child > 0){
                        if($this->SESSION["m_otoritas_menu"][$rs->menu_id]["m_role_view"] == 1){
                            echo "<item id=\"" . $rs->menu_url . "\" text=\"" . ucwords($rs->menu_name) . "\">";            

                            $this->get_menu_tree($rs->menu_id,$rs->m_role_id);                       

                            echo "</item>";
                        }
                    }else if($rs->have_child == null){
                        if($this->SESSION["m_otoritas_menu"][$rs->menu_id]["m_role_view"] == 1){
                            echo "<item id=\"" . $rs->menu_url . "\" text=\"" . ucwords($rs->menu_name) . "\" />";
                        }
                    }else{

                    }
                }           
            }
    }

    public function clickcount(){
        $menu_url = trim($this->input->post("menu_url"));
        $role_id  = $this->SESSION["m_role_id"];
        $user_id  = $this->SESSION["m_user_id"];

        $message = array(true, "failed");

        if($this->app_menu_model->clickcount($menu_url,$role_id,$user_id)){
            $message = array(true, "succes");
        }

        echo json_encode($message);
    }
}