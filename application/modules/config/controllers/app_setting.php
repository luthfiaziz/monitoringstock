<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Developed By : Luthfi Aziz Nugraha
 * Module Name  : Setting Environtment ( For Template )
 */

class App_setting extends MX_Controller {
    private $favicon;
    private $plugin_list = array();
    private $temp_plugin = array();
    private $list_plugin_js = array();
    private $list_plugin_css = array();
    private $list_js = array();
    private $list_css = array();
    private $list_layout = array();

    public function __construct() {
        parent::__construct();
        $this->config();
    }

    private function config() {
        $this->path_asset = base_url() . "assets/";

        /*
         * Daftar Plugin
         * Format untuk mendaftarkan plugin
         * array(
         * 	  'nama_plugin' => array(
         * 			'js' => array(
         * 				'nama_file_js1',
         * 				'nama_file_js2'
         * 			),
         * 			'css' => array(
         * 				'nama_file_css1',
         * 				'nama_file_css2'
         * 			)
         * 	   )
         * )
         */
        $this->plugin_list = array(
            'jquery' =>array(
                'js'  => array(
                    'js/jquery-1.11.1.min',
                ),
            ),
            'jquery-ui' =>array(
                'js'  => array(
                    'js/jquery-ui/jquery-ui.min',
                ),
                'css'=>array(
                    'js/jquery-ui/jquery-ui.min'
                )
            ),
            'bgrins-spectrum-9fe9e8c' =>array(
                'js'  => array(
                    'js/bgrins-spectrum-9fe9e8c/spectrum',
                ),
                'css'=>array(
                    'js/bgrins-spectrum-9fe9e8c/spectrum'
                )
            ),
            'jquery-easy-ui' =>array(
                'js'  => array(
                    'js/jquery-easyui/jquery-easyui.min',
                ),
                'css'=>array(
                    'js/jquery-easyui/themes/default/easyui',
                    'js/jquery-easyui/themes/icon'
                )
            ),
            'tooltip' => array(
                'js' => array(
                        'js/poshytip-1.2/src/jquery.poshytip.min'
                    ),
                'css'=>array(
                        'js/poshytip-1.2/src/tip-violet/tip-violet',
                )         
            ),
            'jquery-mobile' => array(
                'js' => array(
                        'jquery.mobile/jquery.mobile-1.4.5'
                    ),
                'css'=>array(
                        'jquery.mobile/jquery.mobile-1.4.5.min',
                )         
            ),
            'foundation' =>array(
                'js'  => array(
                    'css/foundation-5.4.6/js/foundation.min',
                    'css/foundation-5.4.6/js/foundation/foundation.alert',
                    'css/foundation-5.4.6/js/foundation/foundation.abide',
                    'css/foundation-5.4.6/js/foundation/foundation.accordion',
                ),
                'css'=>array(
                    'css/foundation-5.4.6/css/foundation.min'
                )
            ),
            'custom' => array(
                'js' => array(
                    'js/global_function'
                ),
                'css' => array(
                    'css/stylelogin',
                    'css/stylesheet',
                )
            ),
            'custom-tab' => array(
                'js' => array(
                    'js/global_function'
                ),
            ),
            'fullscreen-jquery'=> array(
                'js' => array(
                    'js/fullscreen-jquery/jquery.fullscreen-min'
                    ),
            ),
            'select2-jquery'=> array(
                'js' => array(
                    'js/select2-3.5.2/select2.min'
                    ),
                'css' => array(
                    'js/select2-3.5.2/select2'
                )
            ),
            'jquery-form'=>array(
                'js' => array(
                    'js/jquery-form.min'
                )
            ),
            'popup-gallery'=> array(
                'js' => array(
                    'js/popup_gallery/dist/jquery.magnific-popup.min'
                    ),
                'css' => array(
                    'js/popup_gallery/dist/magnific-popup'
                )
            ),
            'jquery-scrollbar'=> array(
                'js' => array(
                    'js/jquery_custom_sb/jquery.mCustomScrollbar.concat.min'
                    ),
                'css' => array(
                    'js/jquery_custom_sb/jquery.mCustomScrollbar.min'
                )
            ),
            'jquery-csv-toarray'=> array(
                'js' => array(
                    'js/jquery.csv-0.71.min'
                    ),
            ),
            'highchart'=> array(
                'js' => array(
                    'js/highcharts-4.0.4/js/highcharts'
                    ),
            ),
            'dhtmlx' => array(
                'js' => array(
                    'dhtmlx/dhtmlx',
                    'dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_json'
                    ),
                'css' => array(
                    'dhtmlx/dhtmlx',
                    'dhtmlx/skin/dhtmlx_custom'
                )
            ),
            'dhtmlx-skinweb'=> array(
                'js' => array(
                    'dhtmlx/dhtmlx',
                    'dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_json'
                    ),
                'css' => array(
                    'dhtmlx/dhtmlx',
                    'dhtmlx/skin_web/dhtmlx_custom'
                )
            ),
            'bootstrap' => array(
                'js' => array(
                    'css/bootstrap-3.3.5-dist/js/bootstrap.min',
                    ),
                'css' => array(
                    'css/bootstrap-3.3.5-dist/css/bootstrap.min',
                )
            ),
            'css-form' => array(
                'css'=> array(
                        'css/css_form',
                        'css/login',
                        'css/button',
			   '�ss/style'
                    )
                ),
            'bootbox-js' => array(
                'js' => array(
                        'js/bootbox.min'
                    )
                ),
            'custom-scrollbar'=> array(
                'js' => array(
                    'js/custom_scrollbar/source/customscroll'
                    ),
                'css' => array(
                    'js/custom_scrollbar/source/customscroll'
                )
            ),
            'mouseover'=> array(
                'js' => array(
                    'js/mouseover'
                    ),
                'css' => array(
                    'css/mouseover'
                )
            ),
            'anchorjquery'=> array(
                'js' => array(
                    'js/jquery.smoothscroll',
                    ),
            ),
            'numeric'=> array(
                'js' => array(
                    'js/jquery.numeric.min',
                    ),
            ),
            'autonumeric'=> array(
                'js' => array(
                    'js/autoNumeric',
                    ),
            ),
            'alphanumeric'=> array(
                'js' => array(
                    'js/jquery.alphanum',
                    ),
            ),
			'jquery_print'=> array(
                'js' => array(
  				'jquery_print/jquery.printPage',
               ),
            ),
            'mobile_tab'=> array(
                'css' => array(
                'css/style_tab',
               ),
            ),


        );

        //Daftarkan Layout Yang Akan Dibuat
        $this->layout = array(
                "layout-style-1" => "layout/footin_layout/index",
                "layout-style-2" => "layout/samsat/index",
            );
    }

    private function reg_plugin($plugin_name) {
        /*
         * Me'looping daftar plugin untuk mencocokan nama plugin yang dipanggil
         */
        $alpha = 0;
        foreach ($this->plugin_list as $key => $value) {
            if (!in_array($key, $this->temp_plugin)) {
                if ($key == $plugin_name) {
                    /*
                     * Mendaftarkan file javascript jika ada yang disertakan kedalam plugin
                     */
                    if (isset($value['js'])) {
                        $idx = 0;
                        foreach ($value['js'] as $js) {
                            if (!empty($js)) {
                                $index = str_pad($alpha, 4, "0", STR_PAD_LEFT) . str_pad($idx, 4, "0", STR_PAD_LEFT);
                                $this->list_plugin_js[$index] = $js . '.js';
                                $idx++;
                            }
                        }
                    }

                    /*
                     * Mendaftarkan file css jika ada yang disertakan kedalam plugin
                     */
                    if (isset($value['css'])) {
                        $idx = 0;
                        foreach ($value['css'] as $css) {
                            if (!empty($css)) {
                                $index = str_pad($alpha, 4, "0", STR_PAD_LEFT) . str_pad($idx, 4, "0", STR_PAD_LEFT);
                                $this->list_plugin_css[$index] = $css . '.css';
                                $idx++;
                            }
                        }
                    }

                    /*
                     * Jika nama plugin yang dipanggil sama dengan index name daftar plugin proses looping dihentikan
                     */
                    break;
                }
                $alpha++;
            } else {
                /*
                 * Jika nama plugin sudah terdaftar
                 */
                break;
            }
        }
    }

    public function set_plugin($plugin = array()) {
        if (is_array($plugin)) {
            foreach ($plugin as $value) {
                $this->reg_plugin($value);
            }
        } else {
            if ($plugin == 'all') {
                foreach ($this->plugin_list as $key => $value) {
                    $this->reg_plugin($key);
                }
            } else {
                $this->reg_plugin($plugin);
            }
        }
    }

    public function get_js() {
        $js_path = $this->path_asset;
        $list = "";

        $list_plugin = $this->list_plugin_js;
        ksort($list_plugin);
        foreach ($list_plugin as $value) {
            $list .= "<script src='" . $js_path . $value . "'></script>";
        }

        $list_js = $this->list_js;
        ksort($list_js);
        foreach ($list_js as $value) {
            $list .= "<script src='" . $js_path . $value . "'></script>";
        }

        return "<!-- Begin : Javacript -->" . $list . "<!-- End : Javacript -->";
    }

    public function get_css() {
        $css_path = $this->path_asset;
        $list = "";

        $list_plugin = $this->list_plugin_css;
        ksort($list_plugin);
        foreach ($list_plugin as $value) {
            $list .= "<link href='" . $css_path . $value . "' rel='stylesheet'>";
        }

        $list_css = $this->list_css;
        ksort($list_css);
        foreach ($list_css as $value) {
            $list .= "<link href='" . $css_path . $value . "' rel='stylesheet'>";
        }

        return "<!-- Begin : CSS -->" . $list . "<!-- End : CSS -->";
    }

    public function set_css($css = array()) {
        if (is_array($css)) {
            foreach ($css as $value) {
                $this->list_css[] = $value . '.css';
            }
        } else {
            $this->list_css[] = $css . '.css';
        }
    }

    public function set_js($js = array()) {
        if (is_array($js)) {
            foreach ($js as $value) {
                $this->list_js[] = $value . '.js';
            }
        } else {
            $this->list_js[] = $js . '.js';
        }
    }

    public function get_layout($index = ''){
        return $this->layout[$index];
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
