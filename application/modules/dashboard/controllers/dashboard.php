<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_management
 *
 * @author Syarief Hidayatullah
 */

class dashboard extends MX_Controller {
    // VARIABLE MODUL
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT ubah_status
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();
        
        // Set Module Location
        $this->_module = 'dashboard/dashboard';
        // Load Module Core
        $this->load->module('config/app_core');
		
		$this->load->model('dashboard_model','dm');
        $this->load->model("cpanel/cpanel_model","cpanel");
       

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX ubah_status
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
    	$this->load->view($this->_module.'/index');
    }
	
	
	
	public function form_atas(){
		/*$data['pendaftaran_kendaraan'] = $this->dm->count_kendaraan("pen_history_1");
		$data['mutasi_masuk'] 		   = $this->dm->count_kendaraan("pen_history_3");
		$data['mutasi_ubah_bentuk']    = $this->dm->count_kendaraan("pen_history_9");
		$data['pendataan_tahunan']     = $this->dm->count_kendaraan("pen_history_5");*/
		
		$this->load->view($this->_module.'/form_atas');
	}
	
	
	public function grafik($nm_grafik = ''){
		$nm_grafik = (empty($nm_grafik)) ? "grafik" : $nm_grafik;
		$this->load->view($this->_module.'/grafik/'.$nm_grafik);	
	}
	
	
	public function shortcut(){
        $dataPerusahaan         = $this->cpanel->getPerusahaanProfile();
        $icon                   = empty($dataPerusahaan->profile_logo)?"retail.png":$dataPerusahaan->profile_logo;
        $data['logo']           = base_url().'assets/img/logo/'.$icon;
		$this->load->view($this->_module.'/shortcut',$data);
	}
  
}