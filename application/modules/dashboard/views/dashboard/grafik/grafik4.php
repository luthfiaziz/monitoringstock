<script>
	$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container3',
				type: 'area'
            },
            title: {
                text: '<h6>Penjualan Produk Tahun 2014</h6>'
            },
            xAxis: {
				rotation: -45,
                categories: ["2010","2011","2012","2013","2014","2015","2016"]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah Pendaftaran'
                },
                stackLabels: {
					enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
			
		tooltip: {
             headerFormat : '<span style="font-size:10px">{point.key}</span><table style="width:150px">',
             pointFormat  : '<tr>' +
                   '<td style="color:{series.color};padding:0; font-size:10px">{series.name}: </td>' +
                       '<td style="padding:0; font-size:10px"><b>{point.y}</b></td>'+
                       '</tr>',
             footerFormat : '</table>',
             shared: true,
             useHTML: true
         },
		 
            legend: {
                align: 'right',
                x: -15,
                verticalAlign: 'top',
                y: 25,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            
            plotOptions: {
				line: {
                    dataLabels: {
                        enabled: false
                    },
                    enableMouseTracking: true
                },
				series: {
				  enableMouseTracking: true,
				}
            },
            series: [{
                name: 'Sepatu',
                data: [400,120,600,100,780,80,350,330,310,730]
            }, {
                name: 'Tas',
                data: [240,100,320,410,390,140,590,210,250,230]
            }, {
                name: 'Baju',
                data: [100,200,300,200,100,400,100,300,120,230]
			}]
        });
    });
    
});
</script>



<body>
	<div id="container3" align="center" style="width:100%; height:100%">
    	
    </div>
</body>
