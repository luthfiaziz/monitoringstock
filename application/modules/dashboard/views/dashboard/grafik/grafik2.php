<script>
	$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container3',
				type: 'column'
            },
            title: {
                text: '<h6>Pendaftaran Kendaraan Tahun 2010</h6>'
            },
            xAxis: {
				rotation: -45,
                categories: ["2010","2011","2012","2013","2014","2015","2016"]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah Pendaftaran'
                },
                stackLabels: {
					enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
			
		tooltip: {
             headerFormat : '<span style="font-size:10px">{point.key}</span><table style="width:150px">',
             pointFormat  : '<tr>' +
                   '<td style="color:{series.color};padding:0; font-size:10px">{series.name}: </td>' +
                       '<td style="padding:0; font-size:10px"><b>{point.y}</b></td>'+
                       '</tr>',
             footerFormat : '</table>',
             shared: true,
             useHTML: true
         },
		 
            legend: {
                align: 'right',
                x: -15,
                verticalAlign: 'top',
                y: 25,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            
            plotOptions: {
				line: {
                    dataLabels: {
                        enabled: false
                    },
                    enableMouseTracking: true
                },
				series: {
				  enableMouseTracking: true,
				}
            },
            series: [{
                name: 'Motor',
                data: [400,900,120,230,290,980,120,100,310,930]
            }, {
                name: 'Mobil',
                data: [110,510,380,450,3450,120,570,230,120,730]
            }, {
                name: 'Mobil Bus',
                data: [100,200,120,800,110,240,870,450,230,120]
			}]
        });
    });
    
});
</script>



<body>
	<div id="container3" align="center" style="width:100%; height:100%">
    	
    </div>
</body>
