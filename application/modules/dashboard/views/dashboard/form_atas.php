<style>
	.container_chart{
		margin-top:2px;
		cursor:pointer;
	}
	
	.title{
		padding-left:10px;
		font-size:12px;
		text-shadow:none;
		color:#FFF;
		background-color:#74BABA;
	}
	
	.title:hover{
		color:#CF3;
		background-color:#274E4E;
		border-right:#CF0 10px solid;
	}
	
	.span{
		color:#FFF;
		font-weight:300;
		font-size:22px;
	}
	
</style>

<body>
<div class="container_chart" onClick="lihat_grafik('grafik4')">
<table width="100%" height="0px" cellspacing="0" class="container_chart">
  <tr>
    <td width="20%" height="65" align="center" bgcolor="#60B0B0">&nbsp;</td>
    <td width="50%" class="title">
    	Penjualan<br>
        <span class="span">2.300</span>
    </td>
  </tr>
</table>
</div>

<div class="container_chart" onClick="lihat_grafik('grafik4')">
<table width="100%" height="65" cellspacing="0" class="container_chart">
  <tr>
    <td width="20%" align="center" bgcolor="#60B0B0">&nbsp;</td>
    <td width="50%" class="title">
    	Pembelian<br>
        <span class="span">9.190</span>
    </td>
  </tr>
</table>
</div>

<div class="container_chart" onClick="lihat_grafik('grafik3')">
<table width="100%" height="65" cellspacing="0" class="container_chart">
  <tr>
    <td width="20%" align="center" bgcolor="#60B0B0">&nbsp;</td>
    <td width="50%" class="title">Current Stok<br>
        <span class="span">5.700</span>
    </td>
  </tr>
</table>
</div>

<div class="container_chart" onClick="lihat_grafik('grafik2')">
<table width="100%" height="65" cellspacing="0" class="container_chart">
  <tr>
    <td width="20%" align="center" bgcolor="#60B0B0">&nbsp;</td>
    <td width="50%" class="title">
    	Brand<br>
        <span class="span">1.498</span>
    </td>
  </tr>
</table>
</div>

<div class="container_chart" onClick="lihat_grafik('grafik5')">
<table width="100%" height="65" cellspacing="0" class="container_chart">
  <tr>
    <td width="20%" align="center" bgcolor="#60B0B0"><img src="<?php echo base_url(); ?>assets/img/icon_dashboard/short5.png" height="30px" style="display:none"></td>
    <td width="50%" class="title">
    	User<br>
        <span class="span">17</span>
    </td>
  </tr>
</table>
</div>


</body>