<html>
<link rel="icon" href="<?php echo $icon; ?>" type="image/logo">
<head>
    <title>POS Mobile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <?php
   		echo $js_header;
		echo $css_header;
    ?>
</head>
<body>

	<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
	<!-- WRAPPER MAIN MENU -->
	<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
	<div data-role="page" id="mainmenu">
		<!-- HEADER -->
		<div class="ui-header ui-bar-inherit custom-bg" data-role="header" role="banner">
			<h1 class="ui-title" role="heading" aria-level="1">MINI STOP</h1>
		</div>
		<!-- CONTENT -->
		<div class="ui-content" role="main">
			<div class="ui-grid-b">
				<div class="ui-block-a">
		        	<a href="#transaction" class="ui-btn ui-custom" data-transition="fade">
		        		<img src="<?php echo base_url(); ?>assets/img/icon/calculator.png" height="70%;"/>
		        		<div style="margin-top:10px">Transaksi</div>
		        	</a>
				</div>
				<div class="ui-block-b">
    				<a href="#minimumstok" class="ui-btn ui-custom" onclick="clearMinimStokPage()">
		        		<img src="<?php echo base_url(); ?>assets/img/icon/kencring.png" height="70%;"/>
    					<div style="margin-top:10px">Minimum Stok</div>
    				</a>
				</div>
				<div class="ui-block-c">
    				<a href="#laporanpenjualan" class="ui-btn ui-custom">
		        		<img src="<?php echo base_url(); ?>assets/img/icon/grap.png" height="70%;"/>
    					<div style="margin-top:10px">Laporan Penjualan</div>
    				</a>
				</div>


				<div class="ui-block-a">
    				<a href="#" class="ui-btn ui-custom">
		        		<img src="<?php echo base_url(); ?>assets/img/icon/compi.png" height="70%;"/>
    					<div style="margin-top:10px">Pengaturan</div>
    				</a>
				</div>
				<div class="ui-block-b">
    				<a href="#" class="ui-btn ui-custom" id="logout">
		        		<img src="<?php echo base_url(); ?>assets/img/icon/teimer.png" height="70%;"/>
    					<div style="margin-top:10px">Keluar Aplikasi</div>
    				</a>
				</div>
			</div><!-- /grid-b -->
		</div>
		<!-- FOOTER -->
		<div class="ui-footer ui-bar-a custom-bg" data-theme="a" data-role="footer" role="contentinfo" data-position="fixed">
			<div data-role="controlgroup" data-type="horizontal" align="center">
	        	<a href="#mainmenu" class="ui-btn ui-corner-all ui-icon-home ui-btn-icon-left" data-transition="fade">Home</a>
		    </div>
		</div>
	</div>

	<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
	<!-- WRAPPER TRANSACTION -->
	<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
	<div data-role="page" id="transaction">
		<!-- HEADER -->
		<div class="ui-header ui-bar-inherit custom-bg" data-role="header" role="banner">
			<h1 class="ui-title" role="heading" aria-level="1">MINI STOP</h1>
		</div>

		<!-- CONTENT -->
		<div class="ui-content" role="main" style="padding:0px">
			<div class="ui-header ui-bar-inherit" data-role="header" role="banner">
				<input type="text" name="search-mini" id="search-mini" data-mini="true" placeholder="Kode Barcode" style="text-align:center"/>
			</div>
			<div class="ui-grid-a" style="height:100%;">
				<!-- GRID BLOCK -->
				<div class="ui-block-a" style="width:70%;">
					<div id="gridcashier" class="maingrid"></div>
				</div>

				<!-- CASH BLOCK -->
				<div class="ui-block-b" style="width:30%;">
					<div class="ui-grid-a" style="height:100%;">
					    <div class="ui-block-a" style="width:100%">
					    	<div class="ui-bar ui-bar-a blueredup">
					    		<span class="left">Sales Name</span>
					    		<span class="right"><?php echo $sales; ?></span>
					    	</div>
					    </div>
					    <div class="ui-block-a" style="width:100%">
					    	<div class="ui-bar ui-bar-a blueavenue">
					    		<span class="left">Subtotal</span>
					    		<span class="right" id="subtotal">0</span>
					    	</div>
					    </div>
					    <div class="ui-block-a" style="width:100%">
					    	<div class="ui-bar ui-bar-a blueavenue">
					    		<span class="left">Discount</span>
					    		<span class="right" id="diskon">0</span>
					    	</div>
					    </div>
					    <div class="ui-block-a" style="width:100%">
					    	<div class="ui-bar ui-bar-a blueavenue">
					    		<span class="left">Tax</span>
					    		<span class="right" id="pajak">0</span>
					    	</div>
					    </div>
					    <div class="ui-block-a" style="width:100%">
					    	<div class="ui-bar ui-bar-a rightres blueavenue">
					    		<div id="grandtotal">
					    			0
					    		</div>
					    	</div>
					    </div>
					    <div class="ui-block-a" style="width:100%">
								<div class="ui-grid-b" style="height:100%;">
								    <div class="ui-block-a" style="width:50%" id="listbarang">
								    	<div class="ui-bar ui-bar-a rightres cursor payment bright">
								    		<div class="blockbutton">
								    			<img style="" src="<?php echo base_url(); ?>assets/img/icon/goods.png" height="25px">
								    			<div style="display:inline; position:relative; top:-5px"></div>
								    		</div>
								    	</div>
								    </div>
								    <div class="ui-block-b" style="width:50%" id="tobarcode">
									    <div class="ui-bar ui-bar-a rightres cursor payment bleft">
								    		<div class="blockbutton">
								    			<img style="" src="<?php echo base_url(); ?>assets/img/icon/barcode.png" height="25px">
								    			<div style="display:inline; position:relative; top:-5px"></div>
								    		</div>
									    </div>
								    </div>
								    <div class="ui-block-a" style="width:50%" id="calculator">
								    	<div  id="calculator" class="ui-bar ui-bar-a rightres cursor payment bright">
								    		<div class="blockbutton">
								    			<img style="" src="<?php echo base_url(); ?>assets/img/icon/calculator1.png" height="25px">
								    			<div style="display:inline; position:relative; top:-5px"></div>
								    		</div>
								    	</div>
								    </div>
								    <div class="ui-block-b" style="width:50%" id="deletebarang">
									    <div class="ui-bar ui-bar-a rightres cursor payment bleft">
								    		<div class="blockbutton">
								    			<img style="" src="<?php echo base_url(); ?>assets/img/icon/trash.png" height="25px">
								    			<div style="display:inline; position:relative; top:-5px"></div>
								    		</div>
									    </div>
								    </div>
								    <div class="ui-block-a" style="width:50%" id="payment">
									    <div class="ui-bar ui-bar-a rightres cursor payment bright">
								    		<div class="blockbutton">
								    			<img style="" src="<?php echo base_url(); ?>assets/img/icon/cash.png" height="25px">
								    			<div style="display:inline; position:relative; top:-5px"></div>
								    		</div>
									    </div>
								    </div>
								    <div class="ui-block-b" style="width:50%">
								    	<div class="ui-bar ui-bar-a rightres cursor payment bleft">
								    		<div class="blockbutton">
								    			<img style="" src="<?php echo base_url(); ?>assets/img/icon/credit.png" height="25px">
								    			<div style="display:inline; position:relative; top:-5px"></div>
								    		</div>
								    	</div>
								    </div>
					            </div>
					    </div>
		            </div>
				</div>
            </div>
		</div>

		<!-- FOOTER -->
		<div class="ui-footer ui-bar-a custom-bg" data-theme="a" data-role="footer" role="contentinfo" data-position="fixed">
			<div data-role="controlgroup" data-type="horizontal" align="center">
	        	<a href="#mainmenu" class="ui-btn ui-corner-all ui-icon-home ui-btn-icon-left" data-transition="fade">Home</a>
		    </div>
		</div>
	</div>


	<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
	<!-- WRAPPER LIST BARANG -->
	<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
	<div id="listbarangpage" style="display:none" class="coverlay">
		<div class="wrapperbarang-in">
			<div class="listbarangwrapper">
				<table width="100%" height="100%" border="0">
					<tr>
						<td height="1%" class="headtable" align="center" width="30%">KATEGORI</td>
						<td height="1%" class="headtable" align="center">DAFTAR BARANG</td>
					</tr>
					<tr>
						<td valign="top">
							<div class="loading_ktg" style="display:none">
								<img src="<?php echo base_url(); ?>assets/img/logo/circle-loading-animation.gif" style="margin-left:25%;">
							</div>
							<div class="listcontent" id="kategori_target">

							</div>
						</td>
						<td valign="top">
							<div class="loading_brg" style="display:none">
								<img src="<?php echo base_url(); ?>assets/img/logo/circle-loading-animation.gif" style="margin-left:25%">
							</div>
							<div class="listcontent" id="list_target" style="height:300px !important">
								
							</div>
							<br/>
							<div id="decinc" style="text-align:center;display:none">
								<div id="decrement" class="buttonqty">-</div>
								<div id="qtyvalue" class="buttonqty">1</div>
								<div id="increment" class="buttonqty">+</div>
								<div id="submitQty"> OK </div>
								<div id="tempGoods" class="hide"></div>
							</div>
						</td>
					</tr>
					<tr>
						<td height="1%" colspan="2" class="bc">
	        				<a href="#" class="ui-btn ui-corner-all ui-icon-delete ui-btn-icon-left" data-theme="b" style="width:15%; float:right; background:#eeeeee; color:#000000; border:0px solid #dddddd" id="closelistbarang">Close</a>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
	<!-- WRAPPER LIST CALCULATOR -->
	<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
	<div id="wincalculator" style="display:none" class="coverlay">
		<div class="calculatorwrapper-in">
			<div class="listbarangwrapper">
				<table id="calculators">
					<tr>
						<td class="btninput" colspan="4"><input class='rinput' type="text" name='input' value='0' disabled/></td>
					</tr>
					<tr>
						<td class="btnnumber color" nb="1">1</td>
						<td class="btnnumber color" nb="2">2</td>
						<td class="btnnumber color" nb="3">3</td>
						<td class="btnfunction color" indexedit="4">Qty</td>
					</tr>
					<tr>
						<td class="btnnumber color" nb="4">4</td>
						<td class="btnnumber color" nb="5">5</td>
						<td class="btnnumber color" nb="6">6</td>
						<td class="btnfunction color" indexedit="5">Disc</td>
					</tr>
					<tr>
						<td class="btnnumber color" nb="7">7</td>
						<td class="btnnumber color" nb="8">8</td>
						<td class="btnnumber color" nb="9">9</td>
						<td class="btnfunction color" indexedit="3">Price</td>
					</tr>
					<tr>
						<td class="btnnumber color" nb="C">C</td>
						<td class="btnnumber color" nb="0">0</td>
						<td class="btnnumber color" nb="del">X</td>
						<td class="btnok color">OK</td>
					</tr>
				</table>
	        	<!-- <a href="#" class="ui-btn ui-corner-all ui-icon-delete ui-btn-icon-left" data-theme="b" style="width:15%; float:right; background:#eeeeee; color:#000000; border:0px solid #dddddd" id="closecalculator">Close</a> -->
			</div>
		</div>
	</div>


	<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
	<!-- WRAPPER LIST PAYMENT -->
	<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
	<div id="winpayment" style="display:none" class="coverlay">
		<div class="paymentwrapper-in">
			<div class="listbarangwrapper">
				<table width="100%">
					<tr>
						<td valign="top">
							<table width="100%" style="border-collapse:collapse">
								<tr>
									<td>
										<p class='pfont' class='ktrans'>Subtotal</p>
										<input type="text" name="tsubtotal" id="tsubtotal" value="0" class="pinput" style="width:96% !important" readonly>
									</td>
								</tr>
								<tr>
									<td>
										<!-- <p class='pfont' style="position:relative; top:5px;">Discount</p> -->
										<!-- <div id='bdiscount' class='ktrans'></div> -->
										<p class='pfont'>Discount</p>
										<input type="text" name="pdiscount" id="tdiscount" value="0" class="pinput" style="width:96% !important" readonly>
									</td>
								</tr>
								<tr>
									<td>	
										<!-- <p class='pfont' style="position:relative; top:5px;">Tax</p> -->
										<!-- <div id='btax' class='ktrans'></div> -->
										<p class='pfont'>Tax</p>
										<input type="text" name="ptax" id="ttax" value="0" class="pinput" style="width:96% !important" readonly>
									</td>
								</tr>
								<tr>
									<td>
										<p class='pfont'>Grandtotal</p>
										<input type="text" name="tgrandtotal" id="tgrandtotal" value="0" class="pinput" style="width:96% !important" readonly>
									</td>
								</tr>
							</table>
						</td>
						<td align="center">
							<table>
								<tr>
									<td class="pbtnnb color" pv="1">1</td>
									<td class="pbtnnb color" pv="2">2</td>
									<td class="pbtnnb color" pv="3">3</td>
								</tr>
								<tr>
									<td class="pbtnnb color" pv="4">4</td>
									<td class="pbtnnb color" pv="5">5</td>
									<td class="pbtnnb color" pv="6">6</td>
								</tr>
								<tr>
									<td class="pbtnnb color" pv="7">7</td>
									<td class="pbtnnb color" pv="8">8</td>
									<td class="pbtnnb color" pv="9">9</td>
								</tr>
								<tr>
									<td class="pbtnnb color" pv="C">C</td>
									<td class="pbtnnb color" pv="0">0</td>
									<td class="pbtnnb color" pv="del">X</td>
								</tr>
								<tr>
									<td class="pbtnfc color false" pv="CS" id='cash'>CASH</td>
									<td class="pbtnfc color" pv="CR" id='credit'>CREDIT</td>
									<td class="pbtnfc color" pv="DB" id='debit'>DEBIT</td>
								</tr>
							</table>
						</td>
						<td valign="top">
							<table>
								<tr>
									<td>
										<p class='pfont'>Nama Bank</p>
										<select id='jenisbank' style="width:97% !important; text-align:left !important" disabled>
											<?php 
												foreach ($bank as $key => $value) {
											?>
												<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
											<?php
												}
											?>
										</select>
									</td>
								</tr>
								<tr>
									<td>	
										<p class='pfont' style="position:relative; top:5px;">No Kartu</p>
										<div id='bnocard' class='ktrans' dbcr='disabled'></div>
										<input type="text" name="pnocard" id="tnocard" value="0" class="pinput" disabled>
									</td>
								</tr>
								<tr>
									<td>	
										<p class='pfont' style="position:relative; top:5px;">No Transaksi Kartu</p>
										<div id='bnotrans' class='ktrans' dbcr='disabled'></div>
										<input type="text" name="pnotrans" id="tnotrans" value="0" class="pinput" disabled>
									</td>
								</tr>
								<tr>
									<td>	
										<p class='pfont' style="position:relative; top:5px;">Bayar</p>
										<div id='bbay' class='ktrans'></div>
										<input type="text" name="ppayment" id="tpayment" value="0" class="pinput" readonly>
									</td>
								</tr>
								<tr>
									<td>
										<p class='pfont'>Kembalian</p>
										<input type="text" name="tkembali" id="tkembali" value="0" class="pinput" style="width:97% !important" readonly>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<br/>
				<table width="100%">
					<tr>
						<td>
	        				<a href="#" class="ui-btn ui-corner-all ui-icon-delete ui-btn-icon-top" data-theme="b" style="background:#eeeeee; color:#000000; border:0px solid #dddddd; font-size:12px !important;" id="closepy">BATAL</a>
						</td>
						<td>
							<a href="#" class="ui-btn ui-corner-all ui-icon-refresh ui-btn-icon-top" data-theme="b" style="background:#eeeeee; color:#000000; border:0px solid #dddddd; font-size:12px !important;" paymenttype="cs" id="pbtnproses">PROSES</a>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>	


	<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
	<!-- WRAPPER MINIMUM STOK-->
	<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
	<div data-role="page" id="minimumstok">
		<!-- HEADER -->
		<div class="ui-header ui-bar-inherit custom-bg" data-role="header" role="banner">
			<h1 class="ui-title" role="heading" aria-level="1">MINI STOP</h1>
		</div>
		<!-- CONTENT -->
		<div class="ui-content" role="main">
			<div id="minimstokpage">
				<ul data-role="listview">
					<?php 
						foreach ($kategori as $key => $value) {
					?>	
		  					<li><a class='ministoklink' href"#" data-source="<?php echo base_url() ?>mb.pos/tab/minstok-detail/<?php echo !empty($value["jumlah_item_stokminim"][1]) ? $value["jumlah_item_stokminim"][1] : "-"; ?>"><span style="float:left"><?php echo $value["m_item_kategori_nama"]; ?></span><span style="float:right" class="box box_minimstok"><?php echo $value["jumlah_item_stokminim"][0]; ?></span></a></li>
					<?php
						}
					?>	
				</ul>
			</div>
			<div id="minimstokpage_detail" style="display:none">

			</div>
		</div>
		<!-- FOOTER -->
		<div class="ui-footer ui-bar-a custom-bg" data-theme="a" data-role="footer" role="contentinfo" data-position="fixed">
			<div data-role="controlgroup" data-type="horizontal" align="center">
	        	<a href="#mainmenu" class="ui-btn ui-corner-all ui-icon-home ui-btn-icon-left" data-transition="fade">Home</a>
		    </div>
		</div>
	</div>

	<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
	<!-- WRAPPER LAPORAN PENJUALAN -->
	<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
	<div data-role="page" id="laporanpenjualan">
		<!-- HEADER -->
		<div class="ui-header ui-bar-inherit custom-bg" data-role="header" role="banner">
			<h1 class="ui-title" role="heading" aria-level="1">MINI STOP</h1>
		</div>

		<!-- CONTENT -->
		<div class="ui-content" role="main" style="margin:0px; padding:0px;">
			<div class="ui-header ui-bar-inherit" data-role="header" role="banner">
				<div class="ui-grid-e">
					<div class="ui-block-a">
						<select id="jenis_laporan">
							<option value="">Pilih Jenis Laporan</option>
						</select>  
					</div>
					<div class="ui-block-b">
						<select id="tgl_input_awal">
							<option value="">Pilih Tgl Awal</option>
						</select>  
					</div>
					<div class="ui-block-c">
						<select id="tgl_input_akhir">
							<option value="">Pilih Tgl Akhir</option>
						</select>  
					</div>


					<div class="ui-block-d">
						<select id="bulan">
							<option value="">Pilih Bulan</option>
						</select>  
					</div>
					<div class="ui-block-e">
						<select id="tahun">
							<option value="">Pilih Tahun</option>
						</select>  
					</div>
					<div class="ui-block-f">
						<button style="background-color:#00af8e !important; border:none" class="button_phone" onClick="search_bulanan()">lihat</button>
					</div>
				</div><!-- /grid-b -->
				<br/>
				<div id="gridlaporan" class="gridlaporan"></div>	 
			</div>
		</div>
		<!-- FOOTER -->
		<div class="ui-footer ui-bar-a custom-bg" data-theme="a" data-role="footer" role="contentinfo" data-position="fixed">
			<div data-role="controlgroup" data-type="horizontal" align="center">
	        	<a href="#mainmenu" class="ui-btn ui-corner-all ui-icon-home ui-btn-icon-left" data-transition="fade">Home</a>
		    </div>
		</div>
	</div>

	<div id="base_url" style="display:none"><?php echo base_url(); ?></div>
</body>
<script type="text/javascript">	
	$("#jenisbank").select2({ width: '97%'});
	// BASE URL
	var base_url 	= $("#base_url").html().trim();

	// VAR CALCULATOR QTY
    var num 		= $(".rinput").val();

    // ----------------------------------------------------------------------------------------------
    // GRID LAPORAN INIT
    // ----------------------------------------------------------------------------------------------
	var alignLaporan = [
			["text-align : center"],
			["text-align : center"],
			["text-align : center"],
			["text-align : center"],
			["text-align : center"],
			["text-align : center"],
			["text-align : center"],
			];

	gridLaporan = new dhtmlXGridObject("gridlaporan");
	gridLaporan.setHeader("No. Transaksi,Tgl Awal,Tgl Akhir, Qty Jual, Total",null,alignLaporan);
	gridLaporan.setColAlign("center,center,center,center,center");	
	gridLaporan.setColTypes("ro,ro,ro,ro,ro");
	gridLaporan.setSkin("dhx_web");
	gridLaporan.setInitWidths("100,*,*,*,*");
	gridLaporan.init();  

    // ----------------------------------------------------------------------------------------------
    // GRID KASIR INIT
    // ----------------------------------------------------------------------------------------------
	var alignKasir 	= [
						["text-align : center"],
						["text-align : center"],
						["text-align : center"],
						["text-align : center"],
						["text-align : center"],
						["text-align : center"],
						["text-align : center"],
						];

    gridKasir = new dhtmlXGridObject("gridcashier");
    gridKasir.setHeader("Kode Barang,Nama Barang,Stock,Harga,Qty,Diskon (Rp),Total",null,alignKasir);
    gridKasir.setColAlign("center,center,center,center,center,center,center");
    gridKasir.setColTypes("ro,ro,ro,ron,ro,ron,ron");
    gridKasir.setSkin("dhx_web");
    gridKasir.setNumberFormat("0,000.00", 3, ".", ",");
    gridKasir.setNumberFormat("0,000.00", 5, ".", ",");
    gridKasir.setNumberFormat("0,000.00", 6, ".", ",");
    gridKasir.setInitWidths("100,*,50,100,50,100,100");
    gridKasir.init();  
    gridKasir.setColumnHidden(2,true);


	$(function(){
		$(".listcontent").mCustomScrollbar({theme:"3d-thick-dark"});
	})

	// CALL LIST BARANG
	$("#listbarang").click(function(){
		KategoriBarang();
		ListBarang();
		$("#listbarangpage").fadeIn();
		$("#decinc").hide();
	});

	$("#closelistbarang").click(function(){
		$("#listbarangpage").fadeOut();
	});

	// CALL CACULATOR
	$("#calculator").click(function(){
        var rId     = gridKasir.getSelectedId();
        if(rId == null){
            dhtmlx.alert("Pillih salah satu barang");
        }else{
			$("#wincalculator").fadeIn();
			$(".rinput").val("0");
			num = '0';
			$(".btnfunction").removeClass("clicked");
        }
	});

	$("#closecalculator").click(function(){
		$("#wincalculator").fadeOut();
	});

	// CALL WIN PAYMENT
	$("#payment").click(function(){
		if(gridKasir.getRowsNum() <= 0){
			dhtmlx.alert("List barang masih kosong!");

			return false;
		}

		$("#bnotrans").attr("dbcr","disabled");
		$("#bnocard").attr("dbcr","disabled");

		$("#bnotrans").removeClass("checkedgreen");
		$("#bnocard").removeClass("checkedgreen");

		$("#tnocard").attr('disabled','disabled');
		$("#tnotrans").attr('disabled','disabled');

		$(".pbtnfc").removeClass('false');
		$("#cash").addClass('false');

		$('.ktrans').removeClass('checkedgreen');
		$("#bbay").addClass('checkedgreen');

		$("#winpayment").show();
		$("#tdiscount").val(0);
		$("#ttax").val(0);
		$("#tnocard").val(0);
		$("#tnotrans").val(0);
		$("#tpayment").val(0);
		$("#tkembali").val(0);
		$("#tgrandtotal").val(0);

		pnum = 0;
		vactive = 'tpayment';

		var st  		= $("#subtotal").html().trim();
		var dk 			= $("#diskon").html().trim();
		var pj			= $("#pajak").html().trim();
		var gt			= $("#grandtotal").html().trim();

		$("#tsubtotal").val(st);
		$("#tdiscount").val(dk);
		$("#ttax").val(pj);
		$("#tgrandtotal").val(gt);

		$("#jenisbank").select2("val", "");
	});

	$("#closepy").click(function(){
		$("#winpayment").fadeOut();
	});


	// DELETE BARANG
	$("#deletebarang").click(function(){
        var rId     = gridKasir.getSelectedId();
        if(rId == null){
            dhtmlx.alert("Pillih salah satu barang");
        }else{
            gridKasir.deleteSelectedRows();
            gridKasir.selectRow(0);

      		hitungSubtotal();
        }
	});

	// ----------------------------------------------------------------
	// LOAD KATEGORI
	// ----------------------------------------------------------------
	function KategoriBarang(data){
		$.ajax({
			type: "POST",
			url: base_url + "mb.pos/tab/search_ktg",
			data: data,
			dataType: "json",
			beforeSend: function() {
		        $(".loading_ktg").show();
				$("#kategori_target").hide();
		    },
			success: function(data){
        		$(".loading_ktg").hide();
				$("#kategori_target").show();
				$("#kategori_target").html(data);
			}
		});
	}

	// ----------------------------------------------------------------
	// LOAD LIST
	// ----------------------------------------------------------------
	function ListBarang(data){
		$.ajax({
			type: "POST",
			url: base_url + "mb.pos/tab/search_brg",
			data: data,
			dataType: "json",
			beforeSend: function() {
		        $(".loading_brg").show();
				$("#list_target").hide();
		    },
			success: function(data){
        		$(".loading_brg").hide();
				$("#list_target").show();
				$("#list_target").html(data);
			}
		});
	}

	// ----------------------------------------------------------------
	// SEARCH BARANG BY KATEGORI
	// ----------------------------------------------------------------
	function SearchBarangByKategori(id){
		add_mark(id);

		var data = {
			kd_kategori : $(id).attr("id")
		}

		ListBarang(data);
		$("#decinc").hide();
	}

	function add_mark(id){
		$('ul.kategoribarang li').each(function(i) { $(this).attr("class","false"); });
		$(id).attr("class","true");
	}

	// ----------------------------------------------------------------
	// ADD BARANG TO GRID
	// ----------------------------------------------------------------
	function AddBarang(id){
		var m_item_kode = "#" + id;

		$("#decinc").show();
		$("#qtyvalue").html("1");

		// ADD BARANG TO TEMP
		var item_id = $(m_item_kode).attr("id").trim();
		var item_nm = $(m_item_kode).attr("item_nm").trim();

		$("#tempGoods").attr("item_id",item_id);
		$("#tempGoods").attr("item_nm",item_nm);
		$("#tempGoods").attr("item_qt","1");
	}

	$("#decrement").click(function(){
		var qtybefore = parseInt($("#qtyvalue").html().trim());
		var qtyafter  = qtybefore - 1;

		if(qtyafter > 0){
			$("#qtyvalue").html(qtyafter);
			$("#tempGoods").attr("item_qt",qtyafter);
		}
	});

	$("#increment").click(function(){
		var qtybefore = parseInt($("#qtyvalue").html().trim());
		var qtyafter  = qtybefore + 1;

		$("#qtyvalue").html(qtyafter);
		$("#tempGoods").attr("item_qt",qtyafter);
	});
	
	$("#submitQty").click(function(){
		var item_id = $("#tempGoods").attr("item_id");
		var item_nm = $("#tempGoods").attr("item_nm");
		var item_qt = $("#tempGoods").attr("item_qt");

        var data = {
            kode_item : item_id
        };

		$.ajax({
		    url     : base_url + 'penjualan/pos/getDataBarang',
		    data    : data,
		    type    : 'POST',
		    dataType: 'JSON',
		    success :function(hasil){
		        var id          = gridKasir.uid();
		        var posisi      = gridKasir.getRowsNum();
		        if(checkingBarang(hasil[1]) == false){
		        	qty 			= item_qt;
		        	total           = parseInt(qty) * parseInt(hasil[3]);

		            gridKasir.addRow(hasil[1], [hasil[1],hasil[2],'0',hasil[3],qty,'0',total], posisi);
		        }else{
		            qty_before      = gridKasir.cells(hasil[1],4).getValue();
		            qty_after       = parseInt(qty_before) + parseInt(item_qt);
		            prod_discount   = gridKasir.cells(hasil[1],5).getValue();

		            grandTotal      = (parseInt(hasil[3]) * parseInt(qty_after));

		            gridKasir.cells(hasil[1],4).setValue(qty_after);
		            gridKasir.cells(hasil[1],6).setValue(grandTotal);
		        }
		        cekPromo(hasil[1]);
		        // hitungSubtotal();
		        // $("#search-mini").focus();
		    }
		});

		$("#listbarangpage").fadeOut();
	});

    function checkingBarang(kode_barang){
        condition = false;
        gridKasir.forEachRow(function(ids){
            if(gridKasir.cells(ids,0).getValue() == kode_barang) {
                condition = true;
            }
        });

        return condition;
    }

    function cekPromo(kode_item){
        var qty     = gridKasir.cells(kode_item,4).getValue();
        var harga   = gridKasir.cells(kode_item,3).getValue();
        var total   = 0;
        var data    = {
            kode_item   : kode_item,
            qty         : qty
        };
        $.ajax({
            url     : base_url + 'penjualan/pos/cekPromo',
            data    : data,
            type    : 'POST',
            dataType: 'JSON',
            success :function(hasil){
                if(hasil[0] == "1"){
                    gridKasir.cells(kode_item,5).setValue(hasil[1]);
                    total   = (parseInt(harga) * parseInt(qty));
                    gridKasir.cells(kode_item,6).setValue(total);
                }else{
                    if(parseInt(hasil[1]) > parseInt(qty)){
                        gridKasir.cells(kode_item,5).setValue(0);
                        total   = (parseInt(harga) * parseInt(qty));
                        gridKasir.cells(kode_item,6).setValue(total);
                    }
                }
                hitungSubtotal();
            }
        });
    }  

    function hitungSubtotal(){
        var total   = 0;
        var diskon  = 0;
        var gtotal  = 0;
        gridKasir.forEachRow(function(id){
            total    += parseInt(gridKasir.cells(id,6).getValue());
            diskon   += parseInt(gridKasir.cells(id,5).getValue());
        });

        gtotal = total - diskon;	
        pajak  = (10/100) * gtotal;

        $("#diskon").html(toRp(diskon));
        $("#pajak").html(toRp(pajak));
        $("#subtotal").html(toRp(total));
        $("#grandtotal").html(toRp(gtotal + pajak));
    }

	// ----------------------------------------------------------------
    // CALCULATOR
	// ----------------------------------------------------------------
    $(".btnnumber").each(function(index) {
	    $(this).on("click", function(){
	    	if($(this).attr('nb') != 'del' && $(this).attr('nb') != 'C'){
	    		console.log($(this).attr('nb'));
		    	if(num == '0'){
		    		num = $(this).attr('nb');
		    	}else{
		    		if(num.length <= 16){
			    		num = num + $(this).attr('nb');
			    	}
		    	}
	    	}

	    	// CLEAR
	    	if($(this).attr('nb') == 'C'){
	    		num = '0';
	    		$(".rinput").val(num);
	    	}

	    	// COMA
	    	if($(this).attr('nb') == 'del'){
	    		num = num.slice(0, (-1));

	    		if(num == ''){
	    			num = '0';
	    		}
	    	}

	    	$(".rinput").val(num);
	    });
	});

	$(".btnfunction").each(function(index){
		$(this).on("click", function(){
			$(".btnfunction").removeClass("clicked");
			$(this).addClass('clicked');

			indexedit = $(this).attr("indexedit");

			$(".btnok").attr("indexedit",indexedit);
		});
	});

	$(".btnok").click(function(){
		// PREPARE VARIABLE
        var rId    		 = gridKasir.getSelectedId();
		var indexedit 	 = $(this).attr('indexedit');
		var valueedit 	 = $('.rinput').val();
		var total        = 0;

		// INIT VALUE BEFORE
		var pricebefore  = gridKasir.cells(rId,3).getValue();
		var qtybefore    = gridKasir.cells(rId,4).getValue();
		var discbefore   = gridKasir.cells(rId,5).getValue();

		if(typeof(indexedit) === 'undefined'){
            alert("Tekan fungsi Qty/Disc/Price terlebih dahulu!");

            return false;
		}

		if(valueedit == '0'){
			alert('inputan tidak boleh 0');
			return false;
		}

		// SET VALUE GRID

		if(indexedit == '3'){
			total = (parseInt(valueedit) * parseInt(qtybefore)) - (parseInt(qtybefore) * parseInt(discbefore));
		}

		if(indexedit == '4'){
			total = (parseInt(pricebefore) * parseInt(valueedit)) - (parseInt(valueedit) * parseInt(discbefore));
		}

		if(indexedit == '5'){
			total = (parseInt(pricebefore) * parseInt(qtybefore)) - (parseInt(qtybefore) * parseInt(valueedit));
		}

      	gridKasir.cells(rId,indexedit).setValue(valueedit);
      	gridKasir.cells(rId,6).setValue(total);

      	// AFTER CLOSE WINDOE
      	$("#wincalculator").fadeOut();
      	$(this).removeAttr('indexedit');

      	hitungSubtotal();
	});
	

	// ----------------------------------------------------------------
	// BARCODE FUNCTION
	// ----------------------------------------------------------------
	$("#tobarcode").click(function(){
		$('#search-mini').focus();
	});
	
	// CREATE FUNCTION
	;(function($){
	    $.fn.extend({
	        donetyping: function(callback,timeout){
	            timeout = timeout || 30; // 1 second default timeout
	            var timeoutReference,
	                doneTyping = function(el){
	                    if (!timeoutReference) return;
	                    timeoutReference = null;
	                    callback.call(el);
	                };
	            return this.each(function(i,el){
	                var $el = $(el);
	                // Chrome Fix (Use keyup over keypress to detect backspace)
	                // thank you @palerdot
	                $el.is(':input') && $el.on('keyup keypress',function(e){
	                    // This catches the backspace button in chrome, but also prevents
	                    // the event from triggering too premptively. Without this line,
	                    // using tab/shift+tab will make the focused element fire the callback.
	                    if (e.type=='keyup' && e.keyCode!=8) return;

	                    // Check if timeout has been set. If it has, "reset" the clock and
	                    // start over again.
	                    if (timeoutReference) clearTimeout(timeoutReference);
	                    timeoutReference = setTimeout(function(){
	                        // if we made it here, our timeout has elapsed. Fire the
	                        // callback
	                        doneTyping(el);
	                    }, timeout);
	                }).on('blur',function(){
	                        // If we can, fire the event since we're leaving the field
	                        doneTyping(el);
	                    });
	            });
	        }
	    });
	})(jQuery);

	// CALL FUNCTION
	$('#search-mini').donetyping(function(){
        var data = {
            barcode : $('#search-mini').val()
        };

        $.ajax({
            url     : base_url + 'penjualan/pos/scanBarcode',
            data    : data,
            type    : 'POST',
            dataType: 'JSON',
            success :function(hasil){
                if(hasil[0]==0){
                    $('#search-mini').attr('style','border: red 1px solid; text-align:center');
                }else{
                    $('#search-mini').attr('style','border: green 1px solid; text-align:center');
                    var id          = gridKasir.uid();
                    var posisi      = gridKasir.getRowsNum();
                    if(checkingBarang(hasil[1]) == false){
                        gridKasir.addRow(hasil[1], [hasil[1],hasil[2],'0',hasil[3],'1','0',hasil[3],'0'], posisi);
                    }else{
                        qty_before      = gridKasir.cells(hasil[1],4).getValue();
                        qty_after       = parseInt(qty_before) + 1;
                        prod_discount   = gridKasir.cells(hasil[1],5).getValue();

                        grandTotal      = (parseInt(hasil[3]) * parseInt(qty_after));

                        gridKasir.cells(hasil[1],4).setValue(qty_after);
                        gridKasir.cells(hasil[1],6).setValue(grandTotal);
                    }
                    cekPromo(hasil[1]);
                    $('#search-mini').val('');
                    $('#search-mini').focus();
                    hitungSubtotal();
                }
            }
        });
    });


	// ----------------------------------------------------------------
    // PAYMENT
	// ----------------------------------------------------------------
	var pnum    = 0;
	var vactive = 'tpayment';

	$(".ktrans").each(function(index){
		$(this).on("click", function(){

			$(".ktrans").removeClass('checkedgreen');
			$(this).addClass('checkedgreen');

			var id = $(this).attr("id");
			// if(id == 'bdiscount'){
			// 	pnum = replaceAllDot($("#tdiscount").val());
			// 	vactive = 'tdiscount';
			// }

			// if(id == 'btax'){
			// 	pnum = replaceAllDot($("#ttax").val());
			// 	vactive = 'ttax';
			// }

			if(id == 'bnocard'){
				if($(this).attr('dbcr') == 'disabled'){
					$(".ktrans").removeClass('checkedgreen');
					alert('button credit atau debit belum aktif');
					return false;
				}

				pnum = replaceAllDot($("#tnocard").val());
				vactive = 'tnocard';
			}

			if(id == 'bnotrans'){
				if($(this).attr('dbcr') == 'disabled'){
					$(".ktrans").removeClass('checkedgreen');
					alert('button credit atau debit belum aktif');

					return false;
				}

				pnum = replaceAllDot($("#tnotrans").val());
				vactive = 'tnotrans';
			}

			if(id == 'bbay'){
				pnum = replaceAllDot($("#tpayment").val());
				vactive = 'tpayment';
			}
		});
	});


    $(".pbtnnb").each(function(index) {
	    $(this).on("click", function(){
	    	if($(this).attr('pv') != 'del' && $(this).attr('pv') != 'C'){
		    	if(pnum == '0'){
		    		pnum = $(this).attr('pv');
		    		// console.log(pnum.length + " - 1");
		    	}else{
		    		// console.log(pnum.length + " - 2");
		    		if(vactive == 'ttax'){
			    		if(pnum.length < 3){
				    		pnum = pnum + $(this).attr('pv');
				    	}
		    		}else{
			    		if(pnum.length <= 15){
				    		pnum = pnum + $(this).attr('pv');
				    	}
		    		}
		    	}
	    	}

	    	// CLEAR
	    	if($(this).attr('pv') == 'C'){
	    		pnum = '0';
	    		$("#" + vactive).val(pnum);
	    	}

	    	// COMA
	    	if($(this).attr('pv') == 'del'){
	    		if(pnum != '0'){
		    		pnum = pnum.slice(0, (-1));

		    		if(pnum == ''){
		    			pnum = '0';
		    		}
		    	}
	    	}

	    	if(vactive == 'tnocard'){
	    		$("#" + vactive).val(pnum);

	    		return false;
	    	} 

	    	if(vactive == 'tnotrans'){
	    		$("#" + vactive).val(pnum);

	    		return false;
	    	}

	    	if(vactive == 'ttax'){
	    		$("#" + vactive).val(pnum);

	    		hitungPayment();

	    		return false;
	    	}

	    	$("#" + vactive).val(toRp(pnum));

	    	hitungPayment();
	    });
	});

	$(".btnfunctions").each(function(index){
		$(this).on("click", function(){
			$(".btnfunction").removeClass("clicked");
			$(this).addClass('clicked');

			indexedit = $(this).attr("indexedit");

			$(".btnok").attr("indexedit",indexedit);
		});
	});

	$(".pbtnfc").click(function(){
		// PREPARE VARIABLE

		$('.pbtnfc').removeClass('false');

		$(this).addClass('false');

		pvValue = $(this).attr('pv');

		if(pvValue == 'CR' || pvValue == 'DB'){
			$("#jenisbank").removeAttr('disabled');
			$("#tnotrans").removeAttr('disabled');
			$("#tnocard").removeAttr('disabled');

			$("#tnotrans").attr('readonly','readonly');
			$("#tnocard").attr('readonly','readonly');

			$("#bnotrans").attr("dbcr","enabled");
			$("#bnocard").attr("dbcr","enabled");

			if(pvValue == 'CR'){
				$("#pbtnproses").attr("paymenttype","cr");
			}

			if(pvValue == 'DB'){
				$("#pbtnproses").attr("paymenttype","db");
			}
		}else{
			$("#jenisbank").attr('disabled','disabled');
			$("#tnotrans").attr('disabled','disabled');
			$("#tnocard").attr('disabled','disabled');

			$("#bnotrans").attr("dbcr","disabled");
			$("#bnocard").attr("dbcr","disabled");

			$("#bnotrans").removeClass("checkedgreen");
			$("#bnocard").removeClass("checkedgreen");

			$("#tnotrans").val(0);
			$("#tnocard").val(0);
			$("#jenisbank").select2("val","");

			$("#pbtnproses").attr("paymenttype","cs");

			vactive = 'tpayment';
			pnum    = 0;
		}
	});

	function hitungPayment(){		
		var subtotal = replaceAllDot($("#tsubtotal").val());
		var discount = replaceAllDot($("#tdiscount").val());
		var tax      = replaceAllDot($("#ttax").val());
		var bayar    = replaceAllDot($("#tpayment").val());

		var grandtotal = (parseInt(subtotal) - parseInt(discount)) + parseInt(tax);
		var kembalian  = parseInt(bayar) - parseInt(grandtotal);

		$("#tkembali").val(toRp(kembalian));


		$("#tgrandtotal").val(toRp(grandtotal));

	}
	
	function replaceAllDot(mystring){
		return mystring.replace(/\./g,'');
	}

	$("#pbtnproses").click(prosesSimpan);

	function prosesSimpan(){
        if($("#tpayment").val() == "0"){
            var grandTotal  = $("#tgrandtotal").val();
            $("#tpayment").val(grandTotal);
            hitungPayment();
        }
        var data    = {
            dataIns : {
                trx_pos_sub_total   : replaceAllDot($("#tsubtotal").val()),
                trx_pos_diskon      : replaceAllDot($("#tdiscount").val()),
                trx_pos_grand_total : replaceAllDot($("#tgrandtotal").val())
            },
            trx_pos_bayar       : replaceAllDot($("#tpayment").val()),
            trx_pos_kembalian   : replaceAllDot($("#tkembali").val()),
            paymenttype         : $(this).attr("paymenttype"),
            tnocard			    : replaceAllDot($("#tnocard").val()),
            tnotrans			: replaceAllDot($("#tnotrans").val()),
            dataGrid            : $.csv.toArrays(gridKasir.serializeToCSV()),
            jenisbank           : $("#jenisbank").val()
        };

		if(gridKasir.getRowsNum() <= 0){
			dhtmlx.alert("List barang masih kosong!");

			return false;
		}

        if(parseInt(data['trx_pos_kembalian']) < 0){
            dhtmlx.alert("Jumlah bayar masih kurang");
            return false;
        }

        dhtmlx.confirm({
            text:"Anda yakin ingin melakukan proses ini?",
            ok:"Ya",
            cancel:"Tidak",
            callback:function(result){
                if(result){
//                    $("#posloading").show();
                    $.ajax({
                        url     : base_url + 'mb.pos/tab/simpan-data',
                        data    : data,
                        type    : 'POST',
                        dataType: 'JSON',
                        success :function(res){
//                            $("#posloading").hide();
                            message_type ="alert";
                            judul = "Proses Sukses";

                            if(res[0] == false){
                                message_type = "alert-error";
                                judul = "Terdapat Kesalahan";
                            }

                            dhtmlx.message({
                                title: judul,
                                text: res[2],
                                type: message_type,
                                callback:function(){
                                    if(res[0] == true)
                                        return eval(res[3]);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    function print_klinik(no_faktur){
        var arrFak  = no_faktur.split("/");
        no_faktur   = arrFak[0] + "-" + arrFak[1] + "-" + arrFak[2] + "-" + arrFak[3];
        console.log(no_faktur);
        window.open(base_url + "mobile_pos/tab_pos/print_struk/" + no_faktur,"Struk","width=300,height=400");
    }

    function refreshPos(no_faktur){
    	gridKasir.clearAll();
    	$("#subtotal").html('0');
    	$("#grandtotal").html('0');
    	$("#winpayment").hide();

    	print_klinik(no_faktur);
    }


    $("#logout").click(function(){
		dhtmlx.confirm({
		    text:"Anda yakin ingin keluar dari aplikasi ini?",
		    callback:function(result){
		    	if(result){
					$.post(base_url + "cpanel/logout", function(data) {
						window.location = base_url + "mb.pos/tab";
					});
		    	}
		    }
		});
    });

    // -----------------------------------------------------------------------------------------------------------------------
    // MINIMUM STOK
    // -----------------------------------------------------------------------------------------------------------------------

    function clearMinimStokPage(){
    	$("#minimstokpage").show();
        $("#minimstokpage_detail").html("");
        $("#minimstokpage_detail").hide();
    }

	$(".ministoklink").each(function(index){
		$(this).on("click", function(){
			var datasource = $(this).attr('data-source');

            $.ajax({
                url     : datasource,
                type    : 'POST',
                dataType: 'JSON',
                success :function(res){
                	$("#minimstokpage").hide();
                	$("#minimstokpage_detail").html(res.minstok);
                	$("#minimstokpage_detail").show();
                }
            });
		});
	});



</script>