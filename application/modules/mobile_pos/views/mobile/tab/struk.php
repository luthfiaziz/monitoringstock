<?php
/**
 * User: Bayu Nugraha (bayu.nugraha@intex.co.id)
 * Date: 13/10/15
 * Time: 11:02
 */
?>
<style>
    body{
        font-family:arial;
    }

    .font{
        font-size:25px;
    }

</style>

<body>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font">
    <tr>
        <td colspan="5" style="font-size:45px" align="center"><?php echo ucwords(strtolower($toko->m_toko_nama)); ?></td>
    </tr>
    <tr>
        <td colspan="5" style="font-size:30px" align="center"><?php echo ucwords(strtolower($toko->m_toko_ket)); ?></td>
    </tr>
    <tr>
        <td colspan="5" cellpadding="0" align="center">
            =====================
        </td>
    </tr>
    <tr>
        <td class="font">No.Fak</td>
        <td>&nbsp;</td>
        <td class="font">:</td>
        <td>&nbsp;</td>
        <td class="font"><?php echo $faktur; ?></td>
    </tr>
    <tr>
        <td class="font">Kasir</td>
        <td>&nbsp;</td>
        <td class="font">:</td>
        <td>&nbsp;</td>
        <td class="font"><?php echo ucwords(strtolower($user->user_fullname)); ?></td>
    </tr>
    <tr>
        <td class="font" width="25%">Tanggal</td>
        <td>&nbsp;</td>
        <td class="font" width="2%">:</td>
        <td>&nbsp;</td>
        <td class="font"><?php  echo $pos->trx_pos_tgl; ?></td>
    </tr>
    <tr>
        <td colspan="5" cellpadding="0" align="center">
            =====================
        </td>
    </tr>
</table>



<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font">
    <?php
    $item   = 0;
    $tagihan= 0;
    foreach($posDetail as $row){
        $total  = $row->trx_pos_detail_qty * $row->trx_pos_detail_harga;
        $item   += $row->trx_pos_detail_qty;
    ?>
            <tr>
                <td width="140"><?php echo ucwords(strtolower($row->m_item_nama)) ?></td>
                <td width="5" align="center" valign="top"><?php echo $row->trx_pos_detail_qty; ?></td>
                <td width="130" align="right" style="text-align: right !important;" valign="top"><?php echo number_format($row->trx_pos_detail_harga,0,",","."); ?></td>
                <td align="right" valign="top"><?php echo number_format($total,0,",","."); ?></td>
            </tr>
    <?php
        if($row->trx_pos_detail_disc != 0){
    ?>
            <tr>
                <td>Diskon item</td>
                <td align="center"></td>
                <td align="center"></td>
                <td align="right">(<?php echo number_format($row->trx_pos_detail_disc,0,",","."); ?>)</td>
            </tr>
    <?php
        }
    }
    ?>
    <tr>
        <td colspan="4" cellpadding="0" align="center">
            ---------------------------------------------(*)
        </td>
    </tr>
    <tr>
        <td>Total Item</td>
        <td align="center"><?php echo $item;?></td>
        <td></td>
        <td align="right"><?php echo number_format($pos->trx_pos_grand_total,0,",",".");?></td>
    </tr>
    <?php
    foreach($posJenis as $row){
        ?>
        <tr>
            <td width="100"><?php echo ucwords(strtolower($row->trx_pos_jns_pem_status)) ?></td>
            <td width="5" align="center" valign="top"></td>
            <td width="45" align="right" valign="top"></td>
            <td align="right" valign="top"><?php echo number_format($row->trx_pos_jns_pem_bayar,0,",","."); ?></td>
        </tr>
    <?php
    }
    ?>
    <tr>
    <tr>
        <td colspan="4" cellpadding="0" align="center">
            ---------------------------------------------(-)
        </td>
    </tr>
    <tr>
        <td>Kembalian</td>
        <td align="center"></td>
        <td></td>
        <td align="right"><?php echo number_format($pos->trx_pos_kembalian,0,",",".");?></td>
    </tr>
    <tr>
        <td>PPN</td>
        <td align="right" colspan="2">(<?php echo number_format($pos->trx_pos_tax,0,",",".");?>)</td>
        <td align="right"></td>
    </tr>
</table>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font">
    <tr>
        <td cellpadding="0" align="center">
            ===================================
        </td>
    </tr>
    <tr>
        <td class="font" cellpadding="0" align="center" style="font-size: 35px !important">
            Terimakasih telah belanja di tempat kami
        </td>
    </tr>
    <tr>
        <td cellpadding="0" align="center">
            ===================================
        </td>
    </tr>
</table>
</body>