<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
<!-- WRAPPER MINIMUM STOK-->
<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
<div data-role="page" id="minimumstok">
	<!-- HEADER -->
	<div class="ui-header ui-bar-inherit custom-bg" data-role="header" role="banner">
		<h1 class="ui-title" role="heading" aria-level="1">MINI STOP</h1>
	</div>
	<!-- CONTENT -->
	<div class="ui-content" role="main">
		<ul data-role="listview">
			<?php 
				foreach ($kategori as $key => $value) {
			?>	
  					<li><a href="<?php echo base_url() ?>mb.pos/tab/minstok-detail/<?php echo !empty($value["jumlah_item_stokminim"][1]) ? $value["jumlah_item_stokminim"][1] : "-"; ?>"><span style="float:left"><?php echo $value["m_item_kategori_nama"]; ?></span><span style="float:right" class="box box_minimstok"><?php echo $value["jumlah_item_stokminim"][0]; ?></span></a></li>
			<?php
				}
			?>	
		</ul>
	</div>
	<!-- FOOTER -->
	<div class="ui-footer ui-bar-a custom-bg" data-theme="a" data-role="footer" role="contentinfo" data-position="fixed">
		<div data-role="controlgroup" data-type="horizontal" align="center">
        	<a href="#mainmenu" class="ui-btn ui-corner-all ui-icon-home ui-btn-icon-left" data-transition="fade">Home</a>
	    </div>
	</div>
</div>
