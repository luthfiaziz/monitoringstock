<html>
<link rel="icon" href="<?php echo $icon; ?>" type="image/logo">
<head>
    <title>POS Mobile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <?php
   		echo $js_header;
		echo $css_header;
    ?>
</head>
<body style="background-color:#09C !important">

	<!-- MAIN MENU -->
	<div id="transaction">
		<div class="ui-header ui-bar-inherit custom-bg" data-role="header" role="banner">
			<h1 class="ui-title" role="heading" aria-level="1">MINI STOP</h1>
		</div>
		<div class="ui-content" role="main" style="padding:0px">
			<div class="ui-header ui-bar-inherit" data-role="header" role="banner">
				<input type="search" name="search-mini" id="search-mini" data-mini="true" placeholder="Kode Barcode" style="text-align:center"/>
			</div>
			<div class="ui-grid-a" style="height:100%;">
				<!-- GRID BLOCK -->
				<div class="ui-block-a" style="width:70%;">
					<div id="gridcashier" class="maingrid"></div>
				</div>

				<!-- CASH BLOCK -->
				<div class="ui-block-b" style="width:30%;">
					<div class="ui-grid-a" style="height:100%;">
					    <div class="ui-block-a" style="width:100%">
					    	<div class="ui-bar ui-bar-a blueredup">
					    		<span class="left">Sales Name</span>
					    		<span class="right">Luthfi</span>
					    	</div>
					    </div>
					    <div class="ui-block-a" style="width:100%">
					    	<div class="ui-bar ui-bar-a blueavenue">
					    		<span class="left">Subtotal</span>
					    		<span class="right">0</span>
					    	</div>
					    </div>
					    <div class="ui-block-a" style="width:100%">
					    	<div class="ui-bar ui-bar-a blueavenue">
					    		<span class="left">Discount</span>
					    		<span class="right">0</span>
					    	</div>
					    </div>
					    <div class="ui-block-a" style="width:100%">
					    	<div class="ui-bar ui-bar-a blueavenue">
					    		<span class="left">Tax</span>
					    		<span class="right">0</span>
					    	</div>
					    </div>
					    <div class="ui-block-a" style="width:100%">
					    	<div class="ui-bar ui-bar-a rightres blueavenue">
					    		<div id="grandtotal">
					    			0.00
					    		</div>
					    	</div>
					    </div>
					    <div class="ui-block-a" style="width:100%">
								<div class="ui-grid-b" style="height:100%;">
								    <div class="ui-block-a" style="width:50%">
								    	<div class="ui-bar ui-bar-a rightres cursor">
								    		<div class="blockbutton">
								    			<img style="" src="<?php echo base_url(); ?>assets/img/icon/save2.png" height="25px">
								    			<div style="display:inline; position:relative; top:-5px">SAVE</div>
								    		</div>
								    	</div>
								    </div>
								    <div class="ui-block-b" style="width:50%">
									    <div class="ui-bar ui-bar-a rightres cursor">
								    		<div class="blockbutton">
								    			<img style="" src="<?php echo base_url(); ?>assets/img/icon/more.png" height="25px">
								    			<div style="display:inline; position:relative; top:-5px">MORE</div>
								    		</div>
									    </div>
								    </div>
								    <div class="ui-block-a" style="width:50%">
									    <div class="ui-bar ui-bar-a rightres cursor payment" style="border-right:1px solid #a8b17a">
								    		<div class="blockbutton">
								    			<img style="" src="<?php echo base_url(); ?>assets/img/icon/cash.png" height="25px">
								    			<div style="display:inline; position:relative; top:-5px">CASH</div>
								    		</div>
									    </div>
								    </div>
								    <div class="ui-block-b" style="width:50%">
								    	<div class="ui-bar ui-bar-a rightres cursor payment" style="border-left:1px solid #a8b17a">
								    		<div class="blockbutton">
								    			<img style="" src="<?php echo base_url(); ?>assets/img/icon/credit.png" height="25px">
								    			<div style="display:inline; position:relative; top:-5px">CREDIT</div>
								    		</div>
								    	</div>
								    </div>
					            </div>
					    </div>
		            </div>
				</div>
            </div>
		</div>
		<div class="ui-footer ui-bar-a custom-bg" data-theme="a" data-role="footer" role="contentinfo" data-position="fixed">
			<div data-role="controlgroup" data-type="horizontal" align="center">
	        	<a href="<?php echo base_url(); ?>mb.pos/tab/home" class="ui-btn ui-corner-all ui-icon-home ui-btn-icon-left">Home</a>
		    </div>
		</div>
	</div>
</body>
<script type="text/javascript">
</script>