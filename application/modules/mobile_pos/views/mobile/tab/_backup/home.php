<html>
<link rel="icon" href="<?php echo $icon; ?>" type="image/logo">
<head>
    <title>POS Mobile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <?php
   		echo $js_header;
		echo $css_header;
    ?>
</head>
<body style="background-color:#09C !important">

	<!-- MAIN MENU -->
	<div id="main_menu">
		<div class="ui-header ui-bar-inherit custom-bg" data-role="header" role="banner">
			<h1 class="ui-title" role="heading" aria-level="1">MINI STOP</h1>
		</div>
		<div class="ui-content" role="main">
			<div class="ui-grid-b">
				<div class="ui-block-a">
		        	<a href="<?php echo base_url(); ?>mb.pos/tab/transaction" class="ui-btn ui-custom">
		        		<img src="<?php echo base_url(); ?>assets/img/icon/percentase.png" height="70%;"/>
		        		<div style="margin-top:10px">Transaksi</div>
		        	</a>
				</div>
				<div class="ui-block-b">
    				<a href="#" class="ui-btn ui-custom">
		        		<img src="<?php echo base_url(); ?>assets/img/icon/stock.png" height="70%;"/>
    					<div style="margin-top:10px">Minimum Stok</div>
    				</a>
				</div>
				<div class="ui-block-c">
    				<a href="#" class="ui-btn ui-custom">
		        		<img src="<?php echo base_url(); ?>assets/img/icon/sales.png" height="70%;"/>
    					<div style="margin-top:10px">Laporan Penjualan</div>
    				</a>
				</div>


				<div class="ui-block-a">
    				<a href="#" class="ui-btn ui-custom">
		        		<img src="<?php echo base_url(); ?>assets/img/icon/setting.png" height="70%;"/>
    					<div style="margin-top:10px">Pengaturan</div>
    				</a>
				</div>
				<div class="ui-block-b">
    				<a href="#" class="ui-btn ui-custom">
		        		<img src="<?php echo base_url(); ?>assets/img/icon/logout.png" height="70%;"/>
    					<div style="margin-top:10px">Keluar Aplikasi</div>
    				</a>
				</div>
			</div><!-- /grid-b -->
		</div>
		<div class="ui-footer ui-bar-a custom-bg" data-theme="a" data-role="footer" role="contentinfo" data-position="fixed">
			<div data-role="controlgroup" data-type="horizontal" align="center">
	        	<a href="<?php echo base_url(); ?>mb.pos/tab/home" class="ui-btn ui-corner-all ui-icon-home ui-btn-icon-left">Home</a>
		    </div>
		</div>
	</div>
</body>