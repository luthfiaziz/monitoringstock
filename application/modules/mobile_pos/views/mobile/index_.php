<html>
<link rel="icon" href="<?php echo $icon; ?>" type="image/logo">
<head>
    <title>POS Mobile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1.0,user-scalable=no"> 
    <?php
   		echo $js_header;
		echo $css_header;
    ?>
    <style type="text/css">
		.ui-focus,
		.ui-btn:focus {
		    box-shadow:#CCC 0px 0px 0px 0px !important;
		}
    </style>
</head>
<body>
         
           
           
          
	
	<!-- ----------------------------------------------------------------------------------------------------------------------------------- -->
	<div data-role="page" id="mainmenu">
		<div role="main" style="">
			 <?php $this->load->view("content/beranda");?>
		</div>
	</div>

	<!-------------------------------------------------------------------------------------------------------------------------------------->

           <div data-role="page" id="transaksi">
                <?php echo $this->load->view("content/transaksi"); ?>
           </div>
           
           
           <div data-role="page" id="minimum_stok">
                <?php echo $this->load->view("content/minimum_stok"); ?>
           </div>
           
           
           <div data-role="page" id="lap_penjualan">
                 <?php //echo $this->load->view("content/laporan_penjualan"); ?>
           </div>
           
           
           <div data-role="page" id="pengaturan">
                 <?php echo $this->load->view("content/pengaturan"); ?>
           </div>
           
           
           <div data-role="page" id="pembayaran">
           		<?php echo $this->load->view('content/pembayaran'); ?>
           </div>
           
           
		   <div data-role="page" id="detail_barang">
           		<?php echo $this->load->view('content/detail_barang'); ?>
           </div>
           
           
           <div data-role="page" id="data_barang">
                <?php echo $this->load->view('content/data_barang'); ?>
            </div>
            
            <div data-role="page" id="qty_add">
                <?php echo $this->load->view('content/qty_add'); ?>
            </div>
            
            <div data-role="page" id="cash">
                <?php echo $this->load->view('content/cash'); ?>
            </div>
            
             <div data-role="page" id="debet">
                <?php echo $this->load->view('content/debet'); ?>
            </div>
            
             <div data-role="page" id="kredit">
                <?php echo $this->load->view('content/kredit'); ?>
            </div>
            
            <div data-role="page" id="not_found">
                <?php echo $this->load->view('content/not_found'); ?>
            </div>
            
            <div data-role="page" id="not_found2">
                <?php echo $this->load->view('content/not_found2'); ?>
            </div>
            
            <div data-role="page" id="detail_penjualan">
                <?php echo $this->load->view('content/detail_penjualan'); ?>
            </div>
            
            <div data-role="page" id="detail_min_stok">
                <?php echo $this->load->view('content/detail_min_stok'); ?>
            </div>
            
            <div data-role="page" id="keywords">
                <?php echo $this->load->view('content/keywords'); ?>
            </div>
            
            <div data-role="page" id="laporan">
                <?php echo $this->load->view('content/laporan'); ?>
            </div>
            
            <div data-role="page" id="harian">
                <?php echo $this->load->view('content/laporan/harian'); ?>
            </div>
            
            <div data-role="page" id="bulanan">
                <?php echo $this->load->view('content/laporan/bulanan'); ?>
            </div>
            
            <div data-role="page" id="tahunan">
                <?php echo $this->load->view('content/laporan/tahunan'); ?>
            </div>
            
            <div data-role="page" id="periode">
                <?php echo $this->load->view('content/laporan/periode'); ?>
            </div>
</body>
</html>




<style>
	.header{
		color:#FFF !important;
		background-color:#33aadc;
		background-color:#3c4657 !important;
		height:46px !important;
		box-shadow:none !important;
	}
	
	.ondiv_mob{
		cursor:pointer;
		text-shadow:none !important;
	}
	
	.ondiv_mob:hover{
		background-color:#CFE7E7;
		cursor:pointer;
	}
	
	.ondiv_mob:active{
		background-color:#CFE7E7;
		cursor:pointer;
	}
	
	.circle{
		height:27px;
		width:40px;
		color:#FFF;
		padding-top:13;
		border-radius:25px;
		text-shadow:none;
		background-color:#F97C00;
	}
	
	.circle2{
		height:27px;
		width:40px;
		color:#FFF;
		padding-top:13;
		text-shadow:none;
		border-radius:25px;
		background-color:#16a085;
	}
	
	.circle3{
		height:27px;
		width:40px;
		color:#FFF;
		padding-top:13;
		text-shadow:none;
		border-radius:25px;
		background-color:#F11;
	}
	
	.button_phone{
		height:3em;
		background-color:#3c4657 !important;
		color:#FFF !important;
		text-shadow:none !important;
		border-radius:0px;
	}
	

	
	.input_phone{
		border-radius:0px;
		height:2.5em;
		text-align:center !important;
	}
	
	.input_phone:focus{
		box-shadow:none !important;
	}
	
	.td{
		border-bottom:#f3efec 1px solid;
	}
	
	.input_sales{
		text-align:center !important;
		font-size:12px !important;
		height:2.5em !important;
	}
	
	.input_sales_kredit{
		text-align:center !important;
		font-size:12px !important;
		height:2.5em !important;
	}
	
	.form_active{
		background-color:#EBEBEB !important;
		color:#333 !important;
	}
	
	.tombol_num{
		background-color:#4d4d4d;
		color:#FFF;
		cursor:pointer;
		text-shadow:none !important;
	}
	
	
	.tombol_num:active{
		background-color:#099;
	}
	
	.tombol_num2{
		background-color:#333;
		color:#FFF;
		cursor:pointer;
		text-shadow:none !important;
	}
	
	
	.tombol_num2:active{
		background-color:#099;
	}
	
	.t_transaksi_bayar{
		cursor:pointer;
		background-color:#313948;
		color:#FFF;
		text-shadow:none !important;
		font-size:10px;
	}
	
	.transaksi_bayar:hover{
		background-color:#036;
	}
		
	.t_transaksi_barang{
		cursor:pointer;
		text-shadow:none !important;
		color:#FFF;
		background-color:#536177;
		font-size:10px;
	}
	
	.transaksi_barang:hover{
		background-color:#C0F;
	}
	
		
	.header_title{
		text-shadow:none !important;
		color:#FFF;
		font-size:18px;
	}
	
	.td_list{
		padding-left:10px;
		border-bottom:#f3efec 1px solid;
		font-size:14px;
	}
	
	.td_list{
		cursor:pointer;
		padding-left:10px;
		border-bottom:#f3efec 1px solid;
		font-size:14px;
		height:60px;
	}
	
	.new_div{
		width:100%;
		height:75;
		padding-top:40px;
	}
		
</style>
