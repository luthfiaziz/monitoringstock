<body>	
		<!-- HEADER -->
		<div class="header">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
             <tr>
               <td width="38" style="padding-left:1px">
               		<a href="#pembayaran" data-transition="slide"><img src="<?php echo base_url(); ?>assets/img/icon_dashboard/arrow.png" width="40" height="40">
               </td>
               <td width="1226" align="center" class="header_title">Pembayaran Cash</td>
               <td width="4" align="center" style="color:#FFF">&nbsp;</td>
             </tr>
       </table>
	</div>
    
    <div class="ui-content" style="margin-top:5px">  
            <div id="cash_target">
                
            </div>
            
       	  	<input type="text" id="jumlah_bayar" style="text-align:center" placeholder="Cash" onClick="show_key()">
            <input type="text" id="kembalian" style="text-align:center; border:none !important" readonly placeholder="Kembalian">
            <button class="ui-btn ui-icon-navigation ui-btn-icon-left button_phone" onClick="phone_save()">Bayar</button>
            
            <div style="display:none" id="key_number">
           	  <table width="100%" height="300" border="0" cellpadding="3" cellspacing="3">
            	  <tr>
            	    <td width="35%" align="center" class="btnnumber tombol_num" nb="1">1</td>
            	    <td width="30%" align="center" class="btnnumber tombol_num" nb="2">2</td>
            	    <td width="35%" align="center" class="btnnumber tombol_num" nb="3">3</td>
          	    </tr>
            	  <tr>
            	    <td class="btnnumber tombol_num" nb="4" align="center">4</td>
            	    <td class="btnnumber tombol_num" nb="5" align="center">5</td>
            	    <td class="btnnumber tombol_num" nb="6" align="center">6</td>
          	    </tr>
            	  <tr>
            	    <td class="btnnumber tombol_num" nb="7" align="center">7</td>
            	    <td class="btnnumber tombol_num" nb="8" align="center">8</td>
            	    <td class="btnnumber tombol_num" nb="9" align="center">9</td>
          	    </tr>
            	  <tr>
            	    <td class="btnnumber tombol_num" nb="C" align="center">C</td>
            	    <td class="btnnumber tombol_num" nb="0" align="center">0</td>
            	    <td class="btnnumber tombol_num" nb="del" align="center">x</td>
          	    </tr>
            	  <tr>
            	    <td align="center" class="btnnumber tombol_num" nb="000">000</td>
            	    <td align="center" class="tombol_num" onClick="show_key()">Q</td>
            	    <td align="center" class="tombol_num" onClick="hide_mon()">Cancel</td>
       	        </tr>
            	  <tr>
            	    <td colspan="3">&nbsp;</td>
          	    </tr>
          	  </table>
  			</div>
            
            
      <div style="display:; background-color:#F5F5F5" id="key_mon">
           	  <table width="100%" height="300" border="0" cellpadding="3" cellspacing="3">
            	  <tr>
            	    <td width="50%" align="center" class="btnnumber tombol_num" nb="5000">5.000</td>
            	    <td width="50%" align="center" class="btnnumber tombol_num" nb="50000">50.000</td>
           	    </tr>
            	  <tr>
            	    <td class="btnnumber tombol_num" nb="10000" align="center">10.000</td>
            	    <td class="btnnumber tombol_num" nb="100000" align="center">100.000</td>
           	    </tr>
            	  <tr>
            	    <td class="btnnumber tombol_num" nb="20000" align="center">20.000</td>
            	    <td class="btnnumber tombol_num" nb="200000" align="center">200.000</td>
           	    </tr>
            	  <tr>
            	    <td class="btnnumber tombol_num" nb="C" align="center">C</td>
            	    <td class="tombol_num" align="center" onClick="hide_key()">keynum</td>
           	    </tr>
            	  <tr>
            	    <td colspan="2" align="center" class="tombol_num" onClick="hide_mon()">Cancel</td>
          	    </tr>
            	  <tr>
            	    <td colspan="2">&nbsp;</td>
          	    </tr>
          	  </table>
            </div>	
</div>
</body>

<script>
	var num 		= $("#jumlah_bayar").val();
		
	
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	
	function phone_save(){
		kembalian 	 = $("#kembalian").val();
		total		 = $("#cash_total").val();	
		jumlah_bayar = $("#jumlah_bayar").val();
		status		 = "cash"
		
		
		if(jumlah_bayar == "" || jumlah_bayar == 0){
			alert("Data pembayaran kosong");
			return
		}
		
		if(total > jumlah_bayar){
			alert("Jumlah bayar tidak mencukupi");
			return
		}
		
		$.ajax({
			url:url + "mobile_pos/mobile_pos/phone_save/" + total + "/" + kembalian + "/" + status,
			dataType:"JSON",
			success: function(hasil){
				var no_faktur	= $("#no_faktur").val();
				alert('proses berhasil');
				$("#jumlah_bayar").val('');
				num = '0';
				hide_key();
				$("#kembalian").val('');
				$("#key_number").fadeOut()
				print_klinik(no_faktur);
				location.href = "#transaksi";
				load_transaksi();
			}			
		})
	}
	
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	
	function show_key(){
		$("#key_number").hide();	
		$("#key_mon").show()
	}
	
	function hide_key(){
		$("#key_number").show();
		$("#key_mon").hide();
	}
	
	function hide_mon(){
		$("#key_mon").hide();
		$("#key_number").hide();	
	}
	
	
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	
	$(".btnnumber").each(function(index) {
	    $(this).on("click", function(){
	    	if($(this).attr('nb') != 'del' && $(this).attr('nb') != 'C'){
	    		console.log($(this).attr('nb'));
		    	if(num == '0' || $(this).attr('nb') > 10){
		    		num = $(this).attr('nb');
		    	}else{
		    		if(num.length <= 16){
			    		num = num + $(this).attr('nb');
			    	}
		    	}
	    	}

	    	// CLEAR
	    	if($(this).attr('nb') == 'C'){
	    		num = '0';
	    		$("#jumlah_bayar").val(num);
	    	}

	    	// COMA
	    	if($(this).attr('nb') == 'del'){
	    		num = num.slice(0, (-1));

	    		if(num == ''){
	    			num = '0';
	    		}
	    	}
			
			var total 	  = $("#cash_total").val();	
			var kembalian = num - total; 
			
	    	$("#jumlah_bayar").val(num);
			if(kembalian > 0){
				$("#kembalian").val(kembalian);
			}else{
				$("#kembalian").val(0);
			}
	    });
	});
	
	function print_klinik(no_faktur){
        var arrFak  = no_faktur.split("/");
        no_faktur   = arrFak[0] + "-" + arrFak[1] + "-" + arrFak[2] + "-" + arrFak[3];
        console.log(no_faktur);
        window.open("<?php echo base_url() ?>mobile_pos/tab_pos/print_struk/" + no_faktur,"Struk","width=300,height=400");
    }

</script>

