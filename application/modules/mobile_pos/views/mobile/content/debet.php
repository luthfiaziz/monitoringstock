<body>	
		<!-- HEADER -->
		<div class="header">
                <table width="100%" border="0" cellspacing="0" cellpadding="3">
                 <tr>
                   <td width="38" style="padding-left:1px">
                        <a href="#pembayaran" data-transition="slide"><img src="<?php echo base_url(); ?>assets/img/icon_dashboard/arrow.png" width="40" height="40">
                   </td>
                   <td width="1072" align="center" class="header_title">Pembayaran Debet</td>
                   <td width="214" align="center" class="header_title">&nbsp;</td>
                 </tr>
           </table>
		</div>
        <div class="ui-content" style="margin-top:5px">
        	
            <div id="debet_target">
        	
        	</div>
          	
            <select id="bank_debet">
            	<option value="">Nama Bank</option>
            	<?php foreach($bank->result() as $row){ ?>
                	<option value="<?php echo $row->m_bank_id ?>"><?php echo $row->m_bank_nama ?></option>
                <?php } ?>
            </select>
            
            <input type="text" id="no_kartu_debet" class="input_sales" placeholder="No Kartu Debet" onClick="show_key_debet()">
            <input type="text" id="no_transaksi_kartu_debet" class="input_sales" placeholder="No Transaksi Debet" onClick="show_key_debet()">
            <input type="text" id="jumlah_bayar_debet" class="input_sales" placeholder="Cash" onClick="show_key_debet()">
            <input type="text" id="kembalian_debet" class="input_sales" placeholder="Kembalian" readonly>
            <button class="ui-btn ui-icon-navigation ui-btn-icon-left button_phone" onClick="phone_save_debet()">Bayar</button>
            
            <div style="display:none" id="key_num_debet">
           	  <table width="100%" height="300" border="0" cellpadding="3" cellspacing="3">
            	  <tr>
            	    <td width="34%" align="center" class="btnnumber_debet tombol_num" nd="1">1</td>
            	    <td width="32%" align="center" class="btnnumber_debet tombol_num" nd="2">2</td>
            	    <td width="34%" align="center" class="btnnumber_debet tombol_num" nd="3">3</td>
          	    </tr>
            	  <tr>
            	    <td class="btnnumber_debet tombol_num" nd="4" align="center">4</td>
            	    <td class="btnnumber_debet tombol_num" nd="5" align="center">5</td>
            	    <td class="btnnumber_debet tombol_num" nd="6" align="center">6</td>
          	    </tr>
            	  <tr>
            	    <td class="btnnumber_debet tombol_num" nd="7" align="center">7</td>
            	    <td class="btnnumber_debet tombol_num" nd="8" align="center">8</td>
            	    <td class="btnnumber_debet tombol_num" nd="9" align="center">9</td>
          	    </tr>
            	  <tr>
            	    <td class="btnnumber_debet tombol_num" nd="C" align="center">C</td>
            	    <td class="btnnumber_debet tombol_num" nd="0" align="center">0</td>
            	    <td class="btnnumber_debet tombol_num" nd="del" align="center">BS</td>
          	    </tr>
            	  <tr>
            	    <td align="center" class="btnnumber_debet tombol_num" nd="000">000</td>
            	    <td align="center" class="tombol_num" onClick="hide_key_debet()">Q</td>
            	    <td align="center" class="tombol_num" onClick="hide_mon_debet()">Cancel</td>
       	        </tr>
            	  <tr>
            	    <td colspan="3">&nbsp;</td>
          	    </tr>
          	  </table>
  			</div>
            
            
            <div style="display:; background-color:#F5F5F5" id="key_mon_debet">
           	  <table width="100%" height="300" border="0" cellpadding="3" cellspacing="3">
            	  <tr>
            	    <td width="50%" align="center" class="btnnumber_debet tombol_num" nd="5000">5.000</td>
            	    <td width="50%" align="center" class="btnnumber_debet tombol_num" nd="50000">50.000</td>
           	    </tr>
            	  <tr>
            	    <td class="btnnumber_debet tombol_num" nd="10000" align="center">10.000</td>
            	    <td class="btnnumber_debet tombol_num" nd="100000" align="center">100.000</td>
           	    </tr>
            	  <tr>
            	    <td class="btnnumber_debet tombol_num" nd="20000" align="center">20.000</td>
            	    <td class="btnnumber_debet tombol_num" nd="200000" align="center">200.000</td>
           	    </tr>
            	  <tr>
            	    <td class="btnnumber_debet tombol_num" nd="C" align="center">C</td>
            	    <td class="tombol_num" align="center" onClick="show_key_debet()">keynum</td>
           	    </tr>
            	  <tr>
            	    <td colspan="2" align="center" class="tombol_num" onClick="hide_mon_debet()">Cancel</td>
          	    </tr>
            	  <tr>
            	    <td colspan="2">&nbsp;</td>
          	    </tr>
          	  </table>
            </div>	
</div>

</body>

<script>
	var num_debet 		= $("#jumlah_bayar").val();
	var deb_form  		= "bank_debet";
	
	
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	
	function phone_save_debet(){
		kembalian_debet    = $("#kembalian_debet").val();
		total 			   = $("#cash_total").val();	
		jumlah_bayar_debet = $("#jumlah_bayar_debet").val();
		bank_debet		   = $("#bank_debet").val();
		no_kartu_deb	   = $("#no_kartu_debet").val();
		no_trx_deb		   = $("#no_transaksi_kartu_debet").val();
		status			   = "debit";
		
		if(jumlah_bayar_debet == "" || jumlah_bayar_debet == 0 || bank_debet=="" || no_kartu_deb=="" || no_trx_deb==""){
			alert("Data pembayaran harap diisi lengkap");
			return
		}
		
		if(jumlah_bayar_debet < total){
			alert("Jumlah bayar tidak mencukupi");
			return;
		}
		
		$.ajax({
			url:url + "mobile_pos/mobile_pos/phone_save/" + total  + "/" + kembalian_debet + "/" + status + "/" + bank_debet + "/" + no_kartu_deb + "/" + no_trx_deb ,
			dataType:"JSON",
			success: function(hasil){
				no_faktur = $("#no_faktur").val();
				alert('proses berhasil');
				num_debet = '0';
				$("#bank_debet").val('')
				$("#no_kartu_debet").val('')
				$("#no_transaksi_kartu_debet").val('')
				$("#kembalian_debet").val('');
				$("#key_num_debet").fadeOut();
				$("#jumlah_bayar_debet").val('')
				print_klinik(no_faktur);
				hide_key_debet();
				location.href = "#transaksi";
				load_transaksi();
			}			
		})
	}
	
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	
	function show_key_debet(){
		$("#key_num_debet").show();
		$("#key_mon_debet").hide();	
	}
	
	function hide_key_debet(){
		$("#key_num_debet").hide();
		$("#key_mon_debet").show();
	}
	
	function hide_mon_debet(){
		$("#key_num_debet").hide();
		$("#key_mon_debet").hide();	
	}
	
	
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	
	
	$(".btnnumber_debet").each(function(index) {
	    $(this).on("click", function(){
	    	if($(this).attr('nd') != 'del' && $(this).attr('nd') != 'C'){
	    		console.log($(this).attr('nd'));
		    	if(num_debet == '0' || $(this).attr('nd') > 10){
		    		num_debet = $(this).attr('nd');
		    	}else{
		    		if(num_debet.length <= 16){
			    		num_debet = num_debet + $(this).attr('nd');
			    	}
		    	}
	    	}

	    	// CLEAR
	    	if($(this).attr('nd') == 'C'){
	    		num_debet = '';
	    		$("#" + deb_form).val(num_debet);
	    	}

	    	// COMA
	    	if($(this).attr('nd') == 'del'){
	    		num_debet = num_debet.slice(0, (-1));

	    		if(num_debet == ''){
	    			num_debet = '0';
	    		}
	    	}
			
			var total 	  = $("#cash_total").val();	
			var kembalian_debet = num_debet - total; 
			
	    	$("#" + deb_form).val(num_debet);
			if(deb_form == 'jumlah_bayar_debet'){
				if(kembalian_debet > 0){
					$("#kembalian_debet").val(kembalian_debet);
				}else{
					$("#kembalian_debet").val(0);
				}
			}
	    });
	});
	
	
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	
	$(".input_sales").each(function(index){
		$(this).on("click", function(){
     		
			$(".input_sales").removeClass('form_active');
			$(this).addClass('form_active');
			
			var id = $(this).attr("id");
			if(id == 'bank_debet'){
				deb_form = 'bank_debet';
				num_debet = 0;
			}

			if(id == 'no_kartu_debet'){
				deb_form = 'no_kartu_debet';
				num_debet = 0;
			}

			if(id == 'no_transaksi_kartu_debet'){
				deb_form = 'no_transaksi_kartu_debet';
				num_debet = 0;
			}

			if(id == 'jumlah_bayar_debet'){
				deb_form = 'jumlah_bayar_debet';
				num_debet = 0;
			}

		});
	});
</script>
