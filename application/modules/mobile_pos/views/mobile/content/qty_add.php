<body>	
		<!-- HEADER -->
		<div class="header" data-position="fixed">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
             <tr>
               <td width="3%" style="padding-left:1px">
               		<a href="#transaksi" data-transition="slide"><img src="<?php echo base_url(); ?>assets/img/icon_dashboard/arrow.png" width="40" height="40">
               </td>
               <td width="87%" align="center" class="header_title">Tambah Barang</td>
               <td width="10%" align="center" style="color:#FFF"><img src="<?php echo base_url(); ?>assets/img/icon_dashboard/058b.png" alt="" width="32" height="45" onClick="show_key_add()"></td>
             </tr>
       </table>
		</div>
		
        
        
        <!-- CONTENT -->        
        <div class="ui-content" style="margin-top:5px">
            <div id="data_add_target">
                
            </div>
        	<input type="text" id="qty_add_barang" style="text-align:center" placeholder="Jumlah Barang">
            <button class="ui-btn ui-icon-check ui-btn-icon-left button_phone" onClick="add_barang()">Tambah</button>
            
            <div style="display:none; background-color:#F5F5F5" id="key_number_add">
           	  <table width="100%" height="344" border="0" cellpadding="3" cellspacing="3">
            	  <tr>
            	    <td width="35%" align="center" class="btnumber_detail tombol_num" db="1">1</td>
            	    <td width="30%" align="center" class="btnumber_detail tombol_num" db="2">2</td>
            	    <td width="35%" align="center" class="btnumber_detail tombol_num" db="3">3</td>
          	    </tr>
            	  <tr>
            	    <td class="btnumber_detail tombol_num" db="4" align="center">4</td>
            	    <td class="btnumber_detail tombol_num" db="5" align="center">5</td>
            	    <td class="btnumber_detail tombol_num" db="6" align="center">6</td>
          	    </tr>
            	  <tr>
            	    <td class="btnumber_detail tombol_num" db="7" align="center">7</td>
            	    <td class="btnumber_detail tombol_num" db="8" align="center">8</td>
            	    <td class="btnumber_detail tombol_num" db="9" align="center">9</td>
          	    </tr>
            	  <tr>
            	    <td class="btnumber_detail tombol_num" db="C" align="center">C</td>
            	    <td class="btnumber_detail tombol_num" db="0" align="center">0</td>
            	    <td class="btnumber_detail tombol_num" db="del" align="center">BS</td>
          	    </tr>
            	  <tr>
            	    <td align="center" class="btnumber_detail tombol_num" db="000">000</td>
            	    <td  class="btnumber_detail tombol_num" db="10" align="center">10</td>
            	    <td align="center" class="tombol_num" onClick="hide_mon_add()">Cancel</td>
       	        </tr>
            	  <tr>
            	    <td height="93" colspan="3">&nbsp;</td>
          	    </tr>
          	  </table>
		  </div>
            
            <div style="display:none; background-color:#F5F5F5" id="key_mon_add">
           	  <table width="100%" height="300" border="0" cellpadding="3" cellspacing="3">
            	  <tr>
            	    <td width="50%" align="center" class="btnumber_detail tombol_num" db="5000">5.000</td>
            	    <td width="50%" align="center" class="btnumber_detail tombol_num" db="50000">50.000</td>
           	    </tr>
            	  <tr>
            	    <td class="btnumber_detail tombol_num" db="10000" align="center">10.000</td>
            	    <td class="btnumber_detail tombol_num" db="100000" align="center">100.000</td>
           	    </tr>
            	  <tr>
            	    <td class="btnumber_detail tombol_num" db="20000" align="center">20.000</td>
            	    <td class="btnumber_detail tombol_num" db="200000" align="center">200.000</td>
           	    </tr>
            	  <tr>
            	    <td class="btnumber_detail tombol_num" db="C" align="center">C</td>
            	    <td class="tombol_num" db="0" align="center" onClick="show_key_add()">keynum</td>
           	    </tr>
            	  <tr>
            	    <td colspan="2" align="center" class="tombol_num" onClick="hide_mon_add()">Cancel</td>
          	    </tr>
            	  <tr>
            	    <td colspan="2">&nbsp;</td>
          	    </tr>
          	  </table>
            </div>	
        </div>
</body>

<script>
	var num_add = $("#qty_add_barang").val();
	
	function add_barang(){
		kode_barang = $("#kode_detail").val();
		qty_add		= $("#qty_add_barang").val();
		stok_add	= $("#stok_add").val();
		
				data	= 
					{
						kode_barang : kode_barang,
						qty			: qty_add
					};
		
		if(qty_add==''){
			alert('Qty harus diisi');
			return;
		}
		
		/*if(stok_add > qty_add){
			alert("stok tidak mencukupi");
			return;
		}*/
		
		$.ajax({
			url  : url + "mobile_pos/mobile_pos/scan_barcode",
			data : data,
			type : "POST",
			dataType:"JSON",
			success: function(hasil){
				if(hasil == true){
					location.href = "#transaksi";
					num_add = '0';
					$("#kode_barang").val("");
					$("#qty_add_barang").val("");
					load_transaksi();	
				}
			}
		})
	}
	
	function hide_key_add(){
		$("#key_mon_add").show();
		$("#key_number_add").hide();
	}
	
	function show_key_add(){
		$("#key_mon_add").hide();	
		$("#key_number_add").show();
	}
	
	function hide_mon_add(){
		$("#key_mon_add").hide();	
		$("#key_number_add").hide();	
	}
	
		$(".btnumber_detail").each(function(index) {
	    $(this).on("click", function(){
	    	if($(this).attr('db') != 'del' && $(this).attr('db') != 'C'){
	    		console.log($(this).attr('db'));
		    	if(num_add == '0' || $(this).attr('db') > 10){
		    		num_add = $(this).attr('db');
		    	
				}else{
		    		if(num_add.length <= 16){
			    		num_add = num_add + $(this).attr('db');
			    	}
		    	}
	    	}

	    	// CLEAR
	    	if($(this).attr('db') == 'C'){
	    		num_add = '0';
	    		$("#qty_add_barang").val(num_add);
	    	}

	    	// COMA
	    	if($(this).attr('db') == 'del'){
	    		num_add = num_add.slice(0, (-1));

	    		if(num_add == ''){
	    			num_add = '0';
	    		}
	    	}

	    	$("#qty_add_barang").val(num_add);
	    });
	});
	
	function show_key_add(){
		$("#key_number_add").slideToggle();	
	}
</script>