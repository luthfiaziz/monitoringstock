<body>
<div class="header" style="position:fixed">
  <table width="100%" border="0" cellspacing="0" cellpadding="3">
             <tr>
               <td width="38" style="padding-left:1px">
               		<a href="#mainmenu" data-transition="turn"><img src="<?php echo base_url(); ?>assets/img/icon_dashboard/arrow.png" width="40" height="40">
               </td>
               <td width="1106" align="center" class="header_title">Minimum Stok</td>
               <td width="180" align="center" style="color:#FFF">&nbsp;</td>
             </tr>
       </table>
   </div>
   
   
   <div style="margin-top:46px">
	 <table width="100%" height="381" border="0" cellpadding="3" cellspacing="0">
   		 <?php 
		 	foreach($kategori->result() as $value){ 
		 		$data_min = $this->mpm->jumlah_min_stok($value->m_item_kategori_kode,$kode_toko);
				$kode_kategory	= $value->m_item_kategori_kode;
				
				if($kode_kategory==1){
					$image='016';	
				
				}else if($kode_kategory==2){
					$image='030';
				
				}else if($kode_kategory==3){
					$image='086';
				
				}else if($kode_kategory==4){
					$image='017';
				
				}else if($kode_kategory==5){
					$image='031';
				
				}else if($kode_kategory==6){
					$image='012';
				}
		 ?>
         			
			<tr onClick="min_stok(<?php echo $value->m_item_kategori_kode ?>,<?php echo $data_min ?>)" class="ondiv_mob">
                <td width="14%" align="right" class="td_list"><img src="<?php echo base_url(); ?>assets/img/icon_dashboard/<?php echo $image ?>.png" width="23" height="23"></td>					
                <td width="58%" class="td_list"><?php echo $value->m_item_kategori_nama ?></td>
                <td width="28%" class="td_list"><div align="center" class="circle3"><?php echo $data_min ?></div></td>
             </tr>
   		 <?php } ?> 
   		  
          <tr>
   		    <td colspan="3" class="td_list">&nbsp;</td>
       	  </tr>
     </table>
</div>
</body>

<script>
	function min_stok(kode_kategori,data_min){
		
		if(data_min < 1){
			location.href = "#not_found2";
			return;
		}
		
		location.href = "#detail_min_stok";
		$.ajax({
			url: url +"mobile_pos/mobile_pos/minimum_stok/" + kode_kategori,
			dataType:"JSON",
			beforeSend: function(){
				$("#data_min_stok").html("");
				$("#min_stok_loading").show();
			},
			success: function(data){
				$("#data_min_stok").html(data);	
				$("#min_stok_loading").hide();
			}
		})
	}
</script>