<body>	
		<!-- HEADER -->
		<div class="header">
          <table width="100%" border="0" cellspacing="0" cellpadding="3">
             <tr>s
               <td width="3%" style="padding-left:1px">
               		<a href="#pembayaran" data-transition="slide"><img src="<?php echo base_url(); ?>assets/img/icon_dashboard/arrow.png" width="40" height="40">
               </td>
               <td width="82%" align="center" class="header_title">Pembayaran Credit</td>
               <td width="145" align="center" class="header_title">&nbsp;</td>
             </tr>
      	 </table>
		</div>
        
		<div class="ui-content" style="margin-top:5px">
        	<div id="kredit_target">
        	
  			</div>
           
          	<select id="bank_kredit">
            	<option value="">Nama Bank</option>
            	<?php foreach($bank->result() as $row){ ?>
                	<option value="<?php echo $row->m_bank_id ?>"><?php echo $row->m_bank_nama ?></option>
                <?php } ?>
            </select>
            <input type="text" id="no_kartu_kredit" class="input_sales_kredit" placeholder="No Kartu Kredit" onClick="show_key_kredit()">
            <input type="text" id="no_transaksi_kartu_kredit" class="input_sales_kredit" placeholder="No Transaksi" onClick="show_key_kredit()">
        	<input type="text" id="jumlah_bayar_kredit" class="input_sales_kredit" placeholder="Cash" onClick="show_key_kredit()">
            <input type="text" id="kembalian_kredit"class="input_sales_kredit" placeholder="Kembalian" readonly>
            <button class="ui-btn ui-icon-navigation ui-btn-icon-left button_phone" onClick="phone_save_kredit()">Bayar</button>
            
            <div style="display:none" id="key_num_kredit">
           	  <table width="100%" height="313" border="0" cellpadding="3" cellspacing="3">
            	  <tr>
            	    <td width="34%" align="center" class="btnnumber_kredit tombol_num" nk="1">1</td>
            	    <td width="32%" align="center" class="btnnumber_kredit tombol_num" nk="2">2</td>
            	    <td width="34%" align="center" class="btnnumber_kredit tombol_num" nk="3">3</td>
          	    </tr>
            	  <tr>
            	    <td class="btnnumber_kredit tombol_num" nk="4" align="center">4</td>
            	    <td class="btnnumber_kredit tombol_num" nk="5" align="center">5</td>
            	    <td class="btnnumber_kredit tombol_num" nk="6" align="center">6</td>
          	    </tr>
            	  <tr>
            	    <td class="btnnumber_kredit tombol_num" nk="7" align="center">7</td>
            	    <td class="btnnumber_kredit tombol_num" nk="8" align="center">8</td>
            	    <td class="btnnumber_kredit tombol_num" nk="9" align="center">9</td>
          	    </tr>
            	  <tr>
            	    <td class="btnnumber_kredit tombol_num" nk="C" align="center">C</td>
            	    <td class="btnnumber_kredit tombol_num" nk="0" align="center">0</td>
            	    <td class="btnnumber_kredit tombol_num" nk="del" align="center">x</td>
          	    </tr>
            	  <tr>
            	    <td height="47" align="center" class="btnnumber_kredit tombol_num" nk="000">000</td>
            	    <td height="47" align="center" class="tombol_num" onClick="hide_key_kredit()">Q</td>
            	    <td height="47" align="center" class="tombol_num" onClick="hide_mon_kredit()">Cancel</td>
       	        </tr>
            	  <tr>
            	    <td height="34" colspan="3">&nbsp;</td>
          	    </tr>
          	  </table>
          </div>

            
            
  			<div style="display:; background-color:#F5F5F5" id="key_mon_kredit">
           	  <table width="100%" height="300" border="0" cellpadding="3" cellspacing="3">
            	  <tr>
            	    <td width="50%" align="center" class="btnnumber_kredit tombol_num" nk="5000">5.000</td>
            	    <td width="50%" align="center" class="btnnumber_kredit tombol_num" nk="50000">50.000</td>
           	    </tr>
            	  <tr>
            	    <td class="btnnumber_kredit tombol_num" nk="10000" align="center">10.000</td>
            	    <td class="btnnumber_kredit tombol_num" nk="100000" align="center">100.000</td>
           	    </tr>
            	  <tr>
            	    <td class="btnnumber_kredit tombol_num" nk="20000" align="center">20.000</td>
            	    <td class="btnnumber_kredit tombol_num" nk="200000" align="center">200.000</td>
           	    </tr>
            	  <tr>
            	    <td class="btnnumber_kredit tombol_num" nk="C" align="center">C</td>
            	    <td class="tombol_num" align="center" onClick="show_key_kredit()">keynum</td>
           	    </tr>
            	  <tr>
            	    <td colspan="2" align="center" class="tombol_num" onClick="hide_mon_kredit()">Cancel</td>
          	    </tr>
            	  <tr>
            	    <td colspan="2">&nbsp;</td>
          	    </tr>
          	  </table>
            </div>	
	</div>

</body>

<script>
	var num_kredit  	= $("#jumlah_bayar").val();
	var kre_form    	= "bank_kredit";
	
	
	
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	
	
	function phone_save_kredit(){
		kembalian_kredit 	= $("#kembalian_kredit").val();
		total 				= $("#cash_total").val();
		jumlah_bayar_kredit = $("#jumlah_bayar_kredit").val();
		bank_kredit		    = $("#bank_kredit").val();
		no_kartu_kre	    = $("#no_kartu_kredit").val();
		no_trx_kre		    = $("#no_transaksi_kartu_kredit").val();
		status			    = "credit";
		
		if(jumlah_bayar_kredit == "" || jumlah_bayar_kredit == 0 || bank_kredit=="" || no_kartu_kre=="" || no_trx_kre==""){
			alert("Data pembayaran harap diisi lengkap");
			return
		}
		
		if(total > jumlah_bayar_kredit){
			alert("Jumlah bayar tidak mencukupi");
			return;
		}
			
		$.ajax({
			url:url + "mobile_pos/mobile_pos/phone_save/" + total + "/" + kembalian_kredit + "/" + status + "/" + bank_kredit + "/" + no_kartu_kre + "/" + no_trx_kre,
			dataType:"JSON",
			success: function(hasil){
				no_faktur = $("#no_faktur").val();
				alert('proses berhasil');
				num_kredit = '0';
				hide_key_kredit();
				print_klinik(no_faktur);
				$("#bank_kredit").val('')
				$("#no_kartu_kredit").val('')
				$("#no_transaksi_kartu_kredit").val('')
				$("#kembalian_kredit").val('');
				$("#jumlah_bayar_kredit").val('')
				$("#key_num_kredit").fadeOut()
				location.href = "#transaksi";
				load_transaksi();
			}			
		})
	}
	
	
	function show_key_kredit(){
		$("#key_mon_kredit").hide();
		$("#key_num_kredit").show();	
	}
	
	function hide_key_kredit(){
		$("#key_num_kredit").hide();
		$("#key_mon_kredit").show();
	}
	
	function hide_mon_kredit(){
		$("#key_mon_kredit").hide();
		$("#key_num_kredit").hide();	
	}
	
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	
	$(".btnnumber_kredit").each(function(index) {
	    $(this).on("click", function(){
	    	if($(this).attr('nk') != 'del' && $(this).attr('nk') != 'C'){
	    		console.log($(this).attr('nk'));
		    	if(num_kredit == '0' || $(this).attr('nk') > 10){
		    		num_kredit = $(this).attr('nk');
		    	}else{
		    		if(num_kredit.length <= 16){
			    		num_kredit = num_kredit + $(this).attr('nk');
			    	}
		    	}
	    	}

	    	// CLEAR
	    	if($(this).attr('nk') == 'C'){
	    		num_kredit = '0';
	    		$("#" + kre_form).val(num_kredit);
	    	}

	    	// COMA
	    	if($(this).attr('nk') == 'del'){
	    		num_kredit = num_kredit.slice(0, (-1));

	    		if(num_kredit == ''){
	    			num_kredit = '0';
	    		}
	    	}
			
			var total 	  = $("#cash_total").val();	
			var kembalian_kre = num_kredit - total; 
			
	    	$("#" + kre_form).val(num_kredit);
			if(kre_form == 'jumlah_bayar_kredit'){			
				if(kembalian_kre > 0){
					$("#kembalian_kredit").val(kembalian_kre);
				}else{
					$("#kembalian_kredit").val(0);
				}
			}
	    });
	});
	
	   
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	//----------------------------------------------------------------------------------------------------------------------------------------------->
	
	$(".input_sales_kredit").each(function(index){
		$(this).on("click", function(){
     		
			$(".input_sales_kredit").removeClass('form_active');
			$(this).addClass('form_active');
			
			var id = $(this).attr("id");
			if(id == 'bank_kredit'){
				kre_form = 'bank_kredit';
				num_kredit = 0;
			}

			if(id == 'no_kartu_kredit'){
				kre_form = 'no_kartu_kredit';
				num_kredit = 0;
			}

			if(id == 'no_transaksi_kartu_kredit'){
				kre_form = 'no_transaksi_kartu_kredit';
				num_kredit = 0;
			}

			if(id == 'jumlah_bayar_kredit'){
				kre_form = 'jumlah_bayar_kredit';
				num_kredit = 0;
			}

		});
	});
	
</script>