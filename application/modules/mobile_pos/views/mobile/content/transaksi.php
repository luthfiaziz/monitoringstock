
<div class="header" style="position:fixed">
       	<table width="100%" border="0" cellspacing="0" cellpadding="3">
             <tr>
               <td width="40">
               		<a href="#mainmenu" data-transition="turn"><img src="<?php echo base_url(); ?>assets/img/icon_dashboard/arrow.png" width="40" height="40"></a>
               </td>
               <td width="1283" align="center" class="header_title">Transaksi</td>
               <td width="4" align="right" style="color:#FFF">
       				<a onClick="load_transaksi()"><img src="<?php echo base_url(); ?>assets/img/icon_dashboard/003.png" width="30" height="30"></a>        		
               </td>
             </tr>
       </table>
       
       <div>
   		 <table width="100%" height="70" border="0" cellpadding="3" cellspacing="0">
       		  <tr>
       		    <td width="50%" align="center" class="t_transaksi_barang" onClick="data_barang()">
                	<img src="<?php echo base_url(); ?>assets/img/icon_dashboard/002b.png" width="28" height="28"><br></td>
       		   <td width="50%" align="center" class="t_transaksi_bayar" onClick="total_barang()">
               		<img src="<?php echo base_url(); ?>assets/img/icon_dashboard/046.png" width="35" height="35"></td>
	       </tr>
	     </table>
       </div>
       
  	<div style="background-color:#E9E9E9">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td>
                	<form action="javascript:void(0)" method="post">
      					<input class="input_phone" type="text" id="kode_barang" placeholder="Kode Barcode" data-mini='true' autocomplete="off">
          		 	</form></td>
              </tr>
            </table>
        </div>
	</div>
   
    <div id="barang_target" style="margin-top:180px">         
                
                    
  	</div>
  	
  		
       
		



<script>
	url = "<?php echo base_url() ?>"; 
	
	;(function($){
			$.fn.extend({
				donetyping: function(callback,timeout){
					timeout = timeout || 50; // 1 second default timeout
					var timeoutReference,
						doneTyping = function(el){
							if (!timeoutReference) return;
							timeoutReference = null;
							callback.call(el);
						};
					return this.each(function(i,el){
						var $el = $(el);
						// Chrome Fix (Use keyup over keypress to detect backspace)
						// thank you @palerdot
						$el.is(':input') && $el.on('keyup keypress',function(e){
							// This catches the backspace button in chrome, but also prevents
							// the event from triggering too premptively. Without this line,
							// using tab/shift+tab will make the focused element fire the callback.
							if (e.type=='keyup' && e.keyCode!=8) return;
							
							// Check if timeout has been set. If it has, "reset" the clock and
							// start over again.
							if (timeoutReference) clearTimeout(timeoutReference);
							timeoutReference = setTimeout(function(){
								// if we made it here, our timeout has elapsed. Fire the
								// callback
								doneTyping(el);
							}, timeout);
						}).on('blur',function(){
							// If we can, fire the event since we're leaving the field
							doneTyping(el);
						});
					});
				}
			});
		})(jQuery);
		
	$('#kode_barang').donetyping(function(){
  		kode_barang = $("#kode_barang").val();
		data	= 
					{
						kode_barang : kode_barang,
						qty			: 1
					};
		
		$.ajax({
			url  : url + "mobile_pos/mobile_pos/scan_barcode",
			data : data,
			type : "POST",
			dataType:"JSON",
			success: function(hasil){
				if(hasil == true){
					$("#kode_barang").val("");
					load_transaksi();
									
				}else if(hasil == false){
					$("#kode_barang").val("");
					alert('Maaf kode barcode tidak terdaftar');	
				}
			}
		})
	});
	
	
	/*function scan_barcode(){
		kode_barang = $("#kode_barang").val();
		data	= 
					{
						kode_barang : kode_barang
					};
		
		$.ajax({
			url  : url + "mobile_pos/mobile_pos/scan_barcode",
			data : data,
			type : "POST",
			dataType:"JSON",
			success: function(hasil){
				if(hasil == true){
					$("#kode_barang").val("");
					load_transaksi();	
				}
			}
		})
	}
*/	
	
	function load_transaksi(data){
		$.ajax({
			type: "POST",
			url: url + "mobile_pos/mobile_pos/phone_load_transaksi",
			data: data,
			dataType: "json",
			beforeSend: function() {
				$("#barang_target").html("");
			},
			success: function(data){
				$("#barang_target").html(data);
			}
		})
	}
	
	
	function detail_barang(kode_barang){
		location.href = "#detail_barang";
		$.ajax({
			url  : url + "mobile_pos/mobile_pos/phone_load_detail/" + kode_barang,
			dataType:"JSON",
			beforeSend : function() {
				$("#detail_target").html("");
			},
			success: function(data){
				$("#detail_target").html(data);
			}
		});
	}
	
	
	function total_barang(){
		total_price = $("#total_price").val();
		total_qty 	= $("#total_qty").val();
		
		if(total_price > 0 || total_qty > 0){
			location.href = "#pembayaran";
		}else{
			location.href = "#not_found";
		}
		
		$.ajax({
			url	: url + "mobile_pos/mobile_pos/phone_total/" + total_price + "/" + total_qty,
			dataType:"JSON",
			beforeSend: function(){
				$("#total_target").html("");	
			},
			success:function(data){
				$("#total_target").html(data);
			}
		});
	}
	
	function data_barang(){
		location.href = "#keywords";	
	}

</script>