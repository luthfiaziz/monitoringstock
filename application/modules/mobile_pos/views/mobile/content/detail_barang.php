<body>	
		<!-- HEADER -->
		<div class="header" style="position:fixed">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
             <tr>
               <td width="38" style="padding-left:1px">
               		<a href="#transaksi" data-transition="slide"><img src="<?php echo base_url(); ?>assets/img/icon_dashboard/arrow.png" width="40" height="40">
               </td>
               <td width="1109" align="center" class="header_title">Detail Barang</td>
               <td width="145" align="center" class="header_title">
               <img src="<?php echo base_url(); ?>assets/img/icon_dashboard/058b.png" alt="" width="32" height="45" onClick="show_detail()"></td>
             </tr>
       </table>
		</div>
		
        
        
        <!-- CONTENT -->
        
        <div class="ui-content" style="margin-top:50px">
        	<div id="detail_target">
        	
        	</div>
        	<input type="text" id="qty_barang" style="text-align:center" placeholder="Jumlah Barang">
            <button class="ui-btn ui-icon-check ui-btn-icon-left button_phone" onClick="update_qty()">Ubah</button>
            <button class="ui-btn ui-icon-delete ui-btn-icon-left button_phone" style="background-color:#ff5a56 !important" onClick="delete_qty()">Hapus</button>	
            <div style="display:noe" id="key_number_detail">
           	  <table width="100%" height="303" border="0" cellpadding="3" cellspacing="3">
            	  <tr>
            	    <td width="35%" align="center" class="btnumber_detail tombol_num" nd="1">1</td>
            	    <td width="30%" align="center" class="btnumber_detail tombol_num" nd="2">2</td>
            	    <td width="35%" align="center" class="btnumber_detail tombol_num" nd="3">3</td>
          	    </tr>
            	  <tr>
            	    <td class="btnumber_detail tombol_num" nd="4" align="center">4</td>
            	    <td class="btnumber_detail tombol_num" nd="5" align="center">5</td>
            	    <td class="btnumber_detail tombol_num" nd="6" align="center">6</td>
          	    </tr>
            	  <tr>
            	    <td class="btnumber_detail tombol_num" nd="7" align="center">7</td>
            	    <td class="btnumber_detail tombol_num" nd="8" align="center">8</td>
            	    <td class="btnumber_detail tombol_num" nd="9" align="center">9</td>
          	    </tr>
            	  <tr>
            	    <td class="btnumber_detail tombol_num" nd="C" align="center">C</td>
            	    <td class="btnumber_detail tombol_num" nd="0" align="center">0</td>
            	    <td class="btnumber_detail tombol_num" nd="del" align="center">x</td>
          	    </tr>
            	  <tr>
            	    <td height="99" colspan="3" align="center">&nbsp;</td>
           	    </tr>
          	  </table>
  			</div>
        </div>
</body>

<script>
	var num_detail = $("#qty_barang").val();

	function update_qty(){
		kode_barang = $("#kode_barang_detail").val();
		qty			= $("#qty_barang").val();
		
		if(qty==''){
			alert('Qty harus diisi');
			return;
		}
		
		$.ajax({
			url:url + "mobile_pos/mobile_pos/phone_update_qty/" + kode_barang + "/" + qty,
			dataType:"json",
			success: function(hasil){
				if(hasil==true){
					location.href = "#transaksi";
					load_transaksi();
					$('#qty_barang').val('');
					hide_key_detail();
					num_detail = 0;
				}else{
					alert("Proses Gagal");
					$('#qty_barang').val('');	
				}
			}
		})
	}
	
	
	function delete_qty(){
		kode_barang = $("#kode_barang_detail").val();
		
	
		$.ajax({
			url:url + "mobile_pos/mobile_pos/phone_delete/" + kode_barang,
			dataType:"json",
			success: function(hasil){
				if(hasil==true){
					location.href = "#transaksi";
					load_transaksi();
					$('#qty_barang').val('');
					hide_key_detail()
				}else{
					alert("Proses Gagal");
					$('#qty_barang').val('');	
				}
			}
		})
	}
	
	function show_key_detail(){
		$("#key_number_detail").show();	
	}
	
	function hide_key_detail(){
		$("#key_number_detail").slideUp();
	}
	
		$(".btnumber_detail").each(function(index) {
	    $(this).on("click", function(){
	    	if($(this).attr('nd') != 'del' && $(this).attr('nd') != 'C'){
	    		console.log($(this).attr('nb'));
		    	if(num_detail == '0'){
		    		num_detail = $(this).attr('nd');
		    	}else{
		    		if(num_detail.length <= 16){
			    		num_detail = num_detail + $(this).attr('nd');
			    	}
		    	}
	    	}

	    	// CLEAR
	    	if($(this).attr('nd') == 'C'){
	    		num_detail = '0';
	    		$("#qty_barang").val(num_detail);
	    	}

	    	// COMA
	    	if($(this).attr('nd') == 'del'){
	    		num_detail = num_detail.slice(0, (-1));

	    		if(num_detail == ''){
	    			num_detail = '0';
	    		}
	    	}

	    	$("#qty_barang").val(num_detail);
	    });
	});
	
	function show_keyword_detail_barang(){
		$("#key_number_detail").slideToggle();	
	}
	
	function show_detail(){
		$("#key_number_detail").slideToggle();	
	}
</script>

