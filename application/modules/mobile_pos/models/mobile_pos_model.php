<?php
/**
 * User: Syarief Hidayatullah
 * Date: 30/09/15
 * Time: 17:34
 */

class Mobile_pos_model extends CI_Model {
	
	function insert_pos($tabel='',$data=''){
		$query = $this->db->insert($tabel,$data);
		return $query;
	}
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->
	
	function update_pos($kode_item='',$petugas_id=''){
		$qty = $this->db->query("select trx_tmp_pos_qty as qty from 1nt3x_trx_tmp_pos where trx_tmp_pos_item_kode='".$kode_item."' and trx_tmp_pos_petugas_id='".$petugas_id."'" )->row();
		$up_qty = $qty->qty + 1;
		$data['trx_tmp_pos_qty'] = $up_qty; 
		$query = $this->db->update('trx_tmp_pos',$data,array('trx_tmp_pos_item_kode'=>$kode_item));
		
		return $query;
	}
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->
	
	function update_qty_detail($kode_item='',$qty='',$petugas_id=''){
		$data['trx_tmp_pos_qty'] = $qty;
		$query	= $this->db->update('trx_tmp_pos',$data,array('trx_tmp_pos_item_kode'=>$kode_item,'trx_tmp_pos_petugas_id'=>$petugas_id));
		
		return $query;	
	}
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->
	
	function delete_qty_detail($kode_item='',$petugas_id=''){
		$query	= $this->db->delete('trx_tmp_pos',array('trx_tmp_pos_item_kode'=>$kode_item,'trx_tmp_pos_petugas_id'=>$petugas_id));
		
		return $query;	
	}
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->
	
	function cek_barang($kode_barang){
		$this->db->from("m_item");
		$this->db->where("m_item_barcode",$kode_barang);
		
		$query = $this->db->affected_rows($this->db);
		
		return $query;
	}
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->
	
	public function item($key = ''){
        $this->db->from("m_item b");
		$this->db->join("m_harga c","b.m_item_kode = c.m_item_kode","left");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->
	
	public function data_transaksi($key = ''){
        $this->db->from("trx_tmp_pos a");
		$this->db->join("m_item b","a.trx_tmp_pos_item_kode = b.m_item_kode","left");
		$this->db->join("m_harga c","b.m_item_kode = c.m_item_kode","left");
		$this->db->where(array("a.trx_tmp_pos_petugas_id"=>$key));

        return $this->db;
    }
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->
	
	public function data_detail($kode_barang){
        $this->db->from("m_item b");
		$this->db->join("m_harga c","b.m_item_kode = c.m_item_kode","left");
		$this->db->where(array('m_item_barcode'=>$kode_barang));
		
        return $this->db;
    }
	
	
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->
	
	public function bank(){
		$this->db->from("m_bank");
		return $this->db;
	}
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->
	
	public function laporan_penjualan($petugas_nama='',$tanggal='',$bulan='',$tahun=''){
		if($bulan=='-' && $tahun=='-'){
			$where	= array('trx_pos_petugas'=>$petugas_nama,'trx_pos_tgl_buat'=>$tanggal);
			
		}else if($tanggal=='-' && $bulan !='-'){
			$where	= array('trx_pos_petugas'=>$petugas_nama,'month(trx_pos_tgl_buat)'=>$bulan,'year(trx_pos_tgl_buat)'=>$tahun);
		
		}else if($tanggal='-' && $bulan=='-'){
			$where	= array('trx_pos_petugas'=>$petugas_nama,'year(trx_pos_tgl_buat)'=>$tahun);
		}
		
		$this->db->from("trx_pos");
		$this->db->where($where);
		$this->db->order_by("trx_pos_tgl_buat","desc");
		return $this->db;	
	}
	
	
	public function laporan_penjualan2($petugas_nama='',$tanggal='',$tanggal2){
		$this->db->from("trx_pos");
		$this->db->where(array(
				"trx_pos_petugas"		=>$petugas_nama,
				"trx_pos_tgl_buat >="	=> $tanggal,
				"trx_pos_tgl_buat <="	=> $tanggal2
		));
		$this->db->order_by("trx_pos_tgl_buat","desc");
		return $this->db;	
	}
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->
	
	public function sum_pos($petugas_id){
		$query = $this->db->query("SELECT sum(trx_tmp_pos_harga) as total FROM 1nt3x_trx_tmp_pos where trx_tmp_pos_petugas_id='".$petugas_id."'")->row();
		$data  = $query->total;
		return $data;	
	}
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->

	public function data_min_stok($kode_kategori='',$toko_kode=''){
		$query	= $this->db->query(
					"
					 select a.*,sum(a.stok_berjalan_qty_masuk) as barang_masuk,sum(a.stok_berjalan_qty_keluar) as barang_keluar,b.*
					 from 1nt3x_inv_stok_berjalan a join 1nt3x_m_item b ON a.m_item_kode=b.m_item_kode 
					 where b.m_item_kategori_kode='".$kode_kategori."' and b.m_toko_kode='".$toko_kode."'
					 group by b.m_item_kode
					"
				);
		
		return $query;
	}
	
	//-------------------------------------------------------------------------------------------------->
	//-------------------------------------------------------------------------------------------------->
	
	public function kategori(){
		$this->db->from("m_item_kategori");
		return $this->db;
	}
	
	//-------------------------------------------------------------------------------------------------->
	//-------------------------------------------------------------------------------------------------->
	
	public function jumlah_min_stok($kode_kategori='',$kode_toko=''){
		$query = $this->db->query("SELECT a.m_item_kode,(SUM(a.stok_berjalan_qty_masuk) - SUM(a.stok_berjalan_qty_keluar)) AS stok
						FROM 1nt3x_inv_stok_berjalan a
						LEFT JOIN 1nt3x_m_item b ON a.m_item_kode=b.m_item_kode
						WHERE b.m_item_kategori_kode = '".$kode_kategori."' and b.m_toko_kode='".$kode_toko."'
						GROUP BY m_item_kode
						");
		$qty = 0;
		foreach($query->result() as $value){
			if($value->stok < 20){
				$qty = $qty + 1; 
			}
		}
		
		return $qty;					
	}
	
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->

	public function detail_penjualan($no_faktur=''){
		$this->db->from("trx_pos_detail a");
		$this->db->join("m_item b","a.item_kode=b.m_item_kode");
		$this->db->where(array('trx_pos_no'=>$no_faktur));
		return $this->db;	
	}
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->
	
	public function load_search_barang($keyword='',$kode_toko=''){
		$query	= $this->db->query("select a.*,b.*,(SUM(c.stok_berjalan_qty_masuk) - SUM(c.stok_berjalan_qty_keluar)) AS stok from 1nt3x_m_item a LEFT JOIN 1nt3x_m_harga b ON a.m_item_kode=b.m_item_kode JOIN 1nt3x_inv_stok_berjalan c ON a.m_item_kode=c.m_item_kode where a.m_item_nama like '%$keyword%' and a.m_toko_kode='".$kode_toko."' group by c.m_item_kode");
		return $query;
	}
	
	//-------------------------------------------------------------------------------------------------->
	//-------------------------------------------------------------------------------------------------->
	
	
	 public function load_transaksi($key = ''){
    	$result 		= $this->data_transaksi($key)->get();	
    	$data   		= array();
		$total_qty		= 0; 
		$total_price	= 0;
    	foreach ($result->result() as $value) {
			$total_price = $total_price + ($value->m_harga_jual * $value->trx_tmp_pos_qty);
			$total_qty	 = $total_qty + $value->trx_tmp_pos_qty;
			$html .= "<div style='border-bottom:#f3efec 1px solid' class='ondiv_mob'>";
			$html .= "<a data-transition='slide' onclick='detail_barang(".$value->m_item_barcode.")'>";
    		$html .= '<table height="80px" width="100%" border="0" cellspacing="0" cellpadding="3">'; 
			$html .= '<tr bgcolor="">';
			$html .= '<td width="15px" style="padding-left:20px" align="left">';
			$html .= '<img src="'.base_url().'assets/img/icon_dashboard/023.png" width="25" height="25">';
			$html .= '</td>';
			$html .= '<td style="font-size:14px" width="40%" align="left">';
			$html .= $value->m_item_nama;
			$html .= '</td>';
			$html .= '<td style="font-size:12px; width="20%" align="center"><div align="center" class="circle">';
			$html .= $value->trx_tmp_pos_qty;
			$html .= '</div></td>';
			$html .= '<td style="font-size:12px" width="25%" align="right" style="padding-left:5px; font-size:14px !important; color:#727272">';
			$html .= number_format($value->m_harga_jual * $value->trx_tmp_pos_qty );
			$html .= '</td';
			$html .= '</tr>';
			$html .= '</table>';
			$html .= "</a>";
			$html .= "</div>";
    	}	
			$html .= '<div style="height:30px"></div>';
			$html .= '<input type="hidden" id="total_price" value="'.$total_price.'">';
			$html .= '<input type="hidden" id="total_qty" value="'.$total_qty.'">';

    	return $html;
    }
	
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->
	
	public function load_detail($key = ''){
    	$result = $this->data_detail($key)->get();	
    	$data   = array();

    	foreach ($result->result() as $value) {
			
			$html .= "<div>";
			$html .= '<table height="80px" width="100%" border="0" cellspacing="0" cellpadding="3">'; 
			$html .= '<tr height="60px" bgcolor="">';
			$html .= '<td width="20px" style="padding-left:20px;border-bottom:#f3efec 1px solid" align="left">';
			$html .= '<img src="'.base_url().'assets/img/icon_dashboard/basket8.png" width="20" height="23">';
			$html .= '</td>';
			$html .= '<td style="font-size:12px;border-bottom:#f3efec 1px solid" width="83%" align="left"><input type="hidden" id="kode_barang_detail" value="'.$value->m_item_kode.'">';
			$html .= 'Nama Produk'.'<br><div style="font-weight:bold; font-size:17px">'.$value->m_item_nama.'</div>';
			$html .= '</td>';
			$html .= '</tr>';
			$html .= '<tr height="60px">';
			$html .= '<td width="20px" style="padding-left:20px;border-bottom:#f3efec 1px solid" align="left">';
			$html .= '<img src="'.base_url().'assets/img/icon_dashboard/basket4.png" width="23" height="20">';
			$html .= '</td>';
			$html .= '<td style="font-size:12px;border-bottom:#f3efec 1px solid" width="83%" align="left" style="padding-left:5px; font-size:14px !important; color:#727272">';
			$html .= 'Harga'.'<br><div style="font-weight:bold; font-size:17px">Rp.'.number_format($value->m_harga_jual).'</div>';
			$html .= '</td';
			$html .= '</tr>';
			$html .= '</table>';
			$html .= "</div>";
    	}


    	return $html;
	}
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->
	
	
	public function load_add_item($key = '',$stok=''){
    	$result = $this->data_detail($key)->get();	
    	$data   = array();

    	foreach ($result->result() as $value) {
			
			$html .= "<div>";
			$html .= '<table height="80px" width="100%" border="0" cellspacing="0" cellpadding="3">'; 
			$html .= '<tr height="60px">';
			$html .= '<td width="20px" style="padding-left:20px;border-bottom:#f3efec 1px solid" align="left">';
			$html .= '<img src="'.base_url().'assets/img/icon_dashboard/basket4.png" width="23" height="20">';
			$html .= '</td>';
			$html .= '<td style="font-size:12px;border-bottom:#f3efec 1px solid" width="83%" align="left"><input type="hidden" id="kode_detail" value="'.$value->m_item_barcode.'"><input type="hidden" id="stok_add" value="'.$stok.'">';
			$html .= 'Nama Produk'.'<br><div style="font-weight:bold; font-size:18px">'.$value->m_item_nama.'</div>';
			$html .= '</td>';
			$html .= '</tr>';
			$html .= '<tr height="60px">';
			$html .= '<td width="20px" style="padding-left:20px;border-bottom:#f3efec 1px solid" align="left">';
			$html .= '<img src="'.base_url().'assets/img/icon_dashboard/basket6.png" width="23" height="20">';
			$html .= '</td>';
			$html .= '<td style="font-size:12px;border-bottom:#f3efec 1px solid" width="83%" align="left" style="font-size:14px !important; color:#727272">';
			$html .= 'Harga'.'<br><div style="font-weight:bold; font-size:18px">Rp.'.number_format($value->m_harga_jual).'</div>';
			$html .= '</td';
			$html .= '</tr>';
			$html .= '</table>';
			$html .= "</div>";
    	}


    	return $html;
	}
	
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->
	
	public function load_total($total_price='',$total_qty=''){
		
    		$html .= "<div style='margin-top:20px'>";
			$html .= '<table height="80px" width="100%" border="0" cellspacing="0" cellpadding="3">'; 
			$html .= '<tr height="60px">';
			$html .= '<td width="25px" style="padding-left:25px;border-bottom:#f3efec 1px solid" align="left">';
			$html .= '<img src="'.base_url().'assets/img/icon_dashboard/basket10.png" width="24" height="22">';
			$html .= '</td>';
			$html .= '<td style="font-size:12px;border-bottom:#f3efec 1px solid" width="83%" align="left"><input type="hidden" id="total_sales" value="'.$total_price.'">';
			$html .= 'Total Harga'.'<br><div style="font-weight:bold; font-size:28px">Rp.'.number_format($total_price).'</div>';
			$html .= '</td>';
			$html .= '</tr>';
			$html .= '<tr height="60px">';
			$html .= '<td width="25px" style="padding-left:25px;border-bottom:#f3efec 1px solid" align="left">';
			$html .= '<img src="'.base_url().'assets/img/icon_dashboard/basket7.png" width="23" height="20">';
			$html .= '</td>';
			$html .= '<td width="83%" align="left" style="padding-left:5px; font-size:12px !important;border-bottom:#f3efec 1px solid">';
			$html .= 'Jumlah Barang'.'<br><div style="font-weight:bold; font-size:28px">'.$total_qty.'</div>';
			$html .= '</td';
			$html .= '</tr>';
			$html .= '</table>';
			$html .= "</div>";
			
    	return $html;
	}
	
	//-------------------------------------------------------------------------------------------------->
	//-------------------------------------------------------------------------------------------------->
	
	
	public function load_cash($total_price='',$no_faktur=''){
    		$html .= "<div><input type='hidden' id='no_faktur' value='".$no_faktur."'>";
			$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="3">'; 
			$html .= '<tr height="40px">';
			$html .= '<td width="20px" style="padding-left:20px;border-bottom:#f3efec 1px solid" align="left">';
			$html .= '<img src="'.base_url().'assets/img/icon_dashboard/basket8.png" width="20" height="23">';
			$html .= '</td>';
			$html .= '<td style="font-size:12px;border-bottom:#f3efec 1px solid" width="85%" align="left">';
			$html .= 'Total Harga'.'<br><div style="font-weight:bold; font-size:25px">Rp.'.number_format($total_price).'</div>';
			$html .= '<input id="cash_total" type="hidden" value="'.$total_price.'"></td>';
			$html .= '</tr>';
		
			$html .= '</table>';
			$html .= "</div>";
			
    	return $html;
	}
	
	//-------------------------------------------------------------------------------------------------->
	//-------------------------------------------------------------------------------------------------->
	
	
	public function load_detail_pen($no_faktur=''){
    	$result 		= $this->detail_penjualan($no_faktur)->get();	
    	$data   		= array();
		$total_qty		= 0; 
		$total_price	= 0;
		
		$html .= "<div style='border-bottom:#f3efec 1px solid'>";
		$html .= '<table height="80px" width="100%" border="0" cellspacing="0" cellpadding="3">';
		$html .= '<tr bgcolor="#1976d2" style="text-shadow:none; color:#FFF">';
		$html .= '<td class="td_list" align="center" width="30px">';
		$html .= 'No';
		$html .= '</td>';
		$html .= '<td class="td_list" width="40%" align="left">';
		$html .= 'Nama Barang';
		$html .= '</td>';
		$html .= '<td class="td_list" width="15%" align="center"><div align="center">';
		$html .= 'Jumlah';
		$html .= '</div></td>';
		$html .= '<td class="td_list" width="25%" align="right" style="padding-right:25px">';
		$html .= 'Total Harga';
		$html .= '</td';
		$html .= '</tr>';
		$no	   = 0;
    	foreach ($result->result() as $value) {
			$no++;
			$total_price = $total_price + $value->trx_pos_detail_total;
			$total_qty	 = $total_qty + $value->trx_pos_detail_qty; 
			
			$html .= '<tr>';
			$html .= '<td align="center">';
			$html .= $no;
			$html .= '</td>';
			$html .= '<td class="td_list" width="40%" align="left">';
			$html .= $value->m_item_nama;
			$html .= '</td>';
			$html .= '<td class="td_list" align="center"><div align="center" class="circle2">';
			$html .= $value->trx_pos_detail_qty;
			$html .= '</div></td>';
			$html .= '<td class="td_list" width="25%" align="right" style="padding-right:25px">';
			$html .= number_format($value->trx_pos_detail_total );
			$html .= '</td';
			$html .= '</tr>';	
    	}
			$html .= '<tr height="50" bgcolor="#EEEEEE">';
			$html .= '<td width="15px" class="td_list" align="center">';
			$html .= '<img src="'.base_url().'assets/img/icon_dashboard/002.png" width="25" height="25">';
			$html .= '</td>';
			$html .= '<td class="td_list" width="40%" align="left">';
			$html .= 'Total';
			$html .= '</td>';
			$html .= '<td class="td_list" width="20%" align="center">';
			$html .= number_format($total_qty);;
			$html .= '</td>';
			$html .= '<td class="td_list" width="25%" align="right" style="padding-right:25px">';
			$html .= number_format($total_price);
			$html .= '</td';
			$html .= '</tr>';
			$html .= '</table>';
			$html .= "</div>";
			
			//$html .= '<input type="hidden" id="total_price" value="'.$total_price.'">';
			//$html .= '<input type="hidden" id="total_qty" value="'.$total_qty.'">';

    	return $html;
    }
	
	//-------------------------------------------------------------------------------------------------->
	//-------------------------------------------------------------------------------------------------->
	
	public function load_min_stok($kode_kategori,$kode_toko){
    	$result 		= $this->data_min_stok($kode_kategori,$kode_toko);	
    	$data   		= array();
		
		foreach ($result->result() as $value) {
		 		$kode_kategory	= $value->m_item_kategori_kode;
				if($kode_kategory==1){
					$image='016';	
				
				}else if($kode_kategory==2){
					$image='030';
				
				}else if($kode_kategory==3){
					$image='086';
				
				}else if($kode_kategory==4){
					$image='017';
				
				}else if($kode_kategory==5){
					$image='031';
				
				}else if($kode_kategory==6){
					$image='012';
				}
			
			$barang_masuk	= $value->barang_masuk;
			$barang_keluar	= $value->barang_keluar;
			$total_stok		= $barang_masuk - $barang_keluar;		
			
			if($total_stok < 20){	
				$html .= "<div style='border-bottom:#f3efec 1px solid'>";
				$html .= '<table height="80px" width="100%" border="0" cellspacing="0" cellpadding="3">'; 
				$html .= '<tr>';
				$html .= '<td width="15px" style="padding-left:20px" align="right">';
				$html .= '<img src="'.base_url().'assets/img/icon_dashboard/'.$image.'.png" width="25" height="25">';
				$html .= '</td>';
				$html .= '<td style="font-size:14px" width="40%" align="left">';
				$html .= $value->m_item_nama;
				$html .= '</td>';
				$html .= '<td style="font-size:12px; width="20%" align="center"><div align="center" class="circle3">';
				$html .= $total_stok;
				$html .= '</div></td>';
				$html .= '<td style="font-size:12px" width="25%" align="right" style="padding-left:5px; font-size:14px !important; color:#727272">';
				$html .= '</td';
				$html .= '</tr>';
				$html .= '</table>';
				$html .= "</div>";
			}
    	}
		
		return $html;
    }
	
	//--------------------------------------------------------------------------------------------------->
	//--------------------------------------------------------------------------------------------------->
	
	 public function load_search_barang_detail($keyword='',$kode_toko=''){
    	$result 		= $this->load_search_barang($keyword,$kode_toko);	
    	$data   		= array();
		
    	foreach ($result->result() as $value) {
			$kode_kategory	= $value->m_item_kategori_kode;
				if($kode_kategory==1){
					$image='016';	
				
				}else if($kode_kategory==2){
					$image='030';
				
				}else if($kode_kategory==3){
					$image='086';
				
				}else if($kode_kategory==4){
					$image='017';
				
				}else if($kode_kategory==5){
					$image='031';
				
				}else if($kode_kategory==6){
					$image='012';
				}
				
			if($value->stok > 20){
				$html .= "<div style='border-bottom:#f3efec 1px solid' class='ondiv_mob'>";
				$html .= "<a data-transition='slide' onclick='skip_barang(".$value->m_item_barcode.",".$value->stok.")'>";
				$html .= '<table height="80px" width="100%" border="0" cellspacing="0" cellpadding="3">'; 
				$html .= '<tr>';
				$html .= '<td width="15px" style="padding-left:20px" align="right">';
				$html .= '<img src="'.base_url().'assets/img/icon_dashboard/'.$image.'.png" width="22" height="22">';
				$html .= '</td>';
				$html .= '<td style="font-size:14px;padding-left:20px" width="40%" align="left">';
				$html .= $value->m_item_nama;
				$html .= '</td>';
				$html .= '<td style="font-size:12px; width="20%" align="center"><div align="center" class="circle2">';
				$html .= $value->stok;
				$html .= '</div></td>';
				$html .= '<td style="font-size:12px" width="25%" align="right" style="padding-left:5px; font-size:14px !important; color:#727272">';
				$html .= number_format($value->m_harga_jual);
				$html .= '</td';
				$html .= '</tr>';
				$html .= '</table>';
				$html .= "</a>";
				$html .= "</div>";
			}
    	}
	 	return $html;
    }
	
	//-------------------------------------------------------------------------------------------------->
	//-------------------------------------------------------------------------------------------------->
	
	public function load_harian($sales_name='',$tanggal='',$bulan='',$tahun=''){
		$result	= $this->laporan_penjualan($sales_name,$tanggal,$bulan,$tahun)->get();
		$hitung	= $this->db->affected_rows($result);
		
		$html	.='<table width="100%" height="70" border="0" cellpadding="3" cellspacing="0">';
		$html	.='<tr bgcolor="#2979ff " style="color:#FFF; text-shadow:none">';
		$html	.='<td width="17%" height="39" align="center" class="td_list">No</td>';
		$html	.='<td width="33%" class="td_list" >No Transaksi</td>';
		$html	.='<td width="50%" align="right" class="td_list" style="padding-right:25px">Jumlah Transaksi</td>';
		$html	.='</tr>';
		$no		= 0;
		if($hitung > 0){
			foreach($result->result() as $value){
				$no++;
				$arrFak             = explode("/",$value->trx_pos_no);
        		$no_faktur          = $arrFak[0] . "-" . $arrFak[1] . "-" . $arrFak[2] . "-" . $arrFak[3];
				
				$html .= "<tr onclick='detail_penjualan(\"".$no_faktur."\")' class='ondiv_mob'>";
				$html .= '<td width="17%" align="center" class="td_list">';
				$html .= $no.'</td>';
				$html .= '<td width="33%" class="td_list" >'.$value->trx_pos_no.'</td>';
				$html .= '<td width="50%" align="right" class="td_list" style="padding-right:25px">'.number_format($value->trx_pos_grand_total).'</td>';
				$html .= '</tr>';
			}
		}else{
				$html .= '<tr>';
				$html .= '<td colspan="3" width="17%" align="center" class="td_list">Laporan tidak tersedia</td>';
				$html .= '</tr>';	
		}
		
		return $html;
	}
	
	
	
	public function load_periode($sales_name='',$tanggal='',$tanggal2=''){
		$result	= $this->laporan_penjualan2($sales_name,$tanggal,$tanggal2)->get();
		$hitung	= $this->db->affected_rows($result);
		$no		= 0;
		
		$html	.='<table width="100%" height="70" border="0" cellpadding="3" cellspacing="0">';
		$html	.='<tr  bgcolor="#2979ff " style="color:#FFF; text-shadow:none">';
		$html	.='<td width="17%" height="39" align="right" class="td_list"></td>';
		$html	.='<td width="33%" class="td_list" >No Transaksi</td>';
		$html	.='<td width="50%" align="right" class="td_list" style="padding-right:25px">Jumlah Transaksi</td>';
		$html	.='</tr>';
		
		if($hitung > 0){
			foreach($result->result() as $value){
				$arrFak             = explode("/",$value->trx_pos_no);
        		$no_faktur          = $arrFak[0] . "-" . $arrFak[1] . "-" . $arrFak[2] . "-" . $arrFak[3];
				$no++;
				
				$html .= "<tr onclick='detail_penjualan(\"".$no_faktur."\")' class='ondiv_mob'>";
				$html .= '<td width="17%" align="center" class="td_list">';
				$html .= $no.'</td>';
				$html .= '<td width="33%" class="td_list" >'.$value->trx_pos_no.'</td>';
				$html .= '<td width="50%" align="right" class="td_list" style="padding-right:25px">'.number_format($value->trx_pos_grand_total).'</td>';
				$html .= '</tr>';
			}
		}else{
				$html .= '<tr>';
				$html .= '<td colspan="3" width="17%" align="center" class="td_list">Laporan tidak tersedia</td>';
				$html .= '</tr>';	
		}
		
		return $html;
	}
	
	
	//-------------------------------------------------------------------------------------------------->
	//-------------------------------------------------------------------------------------------------->
	
	 public function getMaxData_phone($kode_toko=''){
       $sql    = $this->db->query("SELECT MAX(RIGHT(trx_pos_no,5)) as noMax FROM 1nt3x_trx_pos WHERE toko_kode = '" . $kode_toko . "'")->row();

        return $sql;
    }
	
	//-------------------------------------------------------------------------------------------------->
	//-------------------------------------------------------------------------------------------------->
	
	public function delete_pos($petugas_id=''){
		$this->db->delete("trx_tmp_pos",array("trx_tmp_pos_petugas_id"=>5));
		return $this->db;	
	}
		
}