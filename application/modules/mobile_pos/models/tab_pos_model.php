<?php
/**
 * User: Syarief Hidayatullah (bayu.nugraha@intex.co.id)
 * Date: 30/09/15
 * Time: 17:34
 */

class tab_pos_model extends CI_Model {
    private $_ktg_table = "m_item_kategori";
    private $_brg_table = "vbarang";
    private $_hpr_table = "vharga_promo";
    
     public function __construct() {
        parent::__construct();
            
        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("a.m_item_kategori_kode" => $key);

        return $key;
    }

    // -----------------------------------------------------------------------------------------------------------
    // DATA KATEGORI
    // -----------------------------------------------------------------------------------------------------------
    public function data_kategori($key = ''){
        $this->db->from($this->_ktg_table . " a");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadDataKategori($key = ''){
    	$result = $this->data_kategori($key)->get();	
    	$data   = array();

    	$html = "<div style='text-align:center'>Data Kategori Kosong</div>";

    	if(!empty($result->result())){
	    	$html = "<ul class='kategoribarang'>";

			$html .= "<li id='smua_kategori' name='smua_kategori' onclick='SearchBarangByKategori(\"#smua_kategori\")' class='true'>";
			$html .= "<p>SMUA KATEGORI</p>";
			$html .= "</li>";

	    	foreach ($result->result() as $value) {

	    		$html .= "<li id='" . $value->m_item_kategori_kode . "' name='" . $value->m_item_kategori_nama . "' onclick='SearchBarangByKategori(\"#" . $value->m_item_kategori_kode . "\")' class='false'>";
	    		$html .= "<p>" . $value->m_item_kategori_nama . "</p>";
	    		$html .= "</li>";
	    	}

	    	$html .= "</ul>";
	    }

    	return $html;
    }

		
    // -----------------------------------------------------------------------------------------------------------
    // DATA BARANG
    // -----------------------------------------------------------------------------------------------------------
    public function data_barang($key = ''){
        $this->db->from($this->_brg_table . " a");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadDataBarang($key = ''){
    	$result = $this->data_barang($key)->get();	
    	$data   = array();

    	$html = "<div style='text-align:center; font-size:12px; padding:20px'>Data Barang Tidak Ada</div>";
    	if(!empty($result->result())){
	    	$html = "<ul class='listbarang'>";

	    	foreach ($result->result() as $value) {

                // $kpromo = array("m_item_kode" => $value->m_item_kode, "m_toko_kode" => $this->SESSION['toko_kode']);
                // $hpromo = !empty(
                //                     $this
                //                     ->HargaPromo($kpromo)
                //                     ->get()
                //                     ->row()
                //                     ->m_harga_promo_harga
                //                 ) ? $this
                //                     ->HargaPromo($kpromo)
                //                     ->get()
                //                     ->row()
                //                     ->m_harga_promo_harga : 0;

                // $hasil  = $value->m_harga_jual - $hpromo;

	    		$html .= "<li 
                                id      = '{$value->m_item_kode}' 
                                item_nm = '{$value->m_item_nama}'
                                onclick = 'AddBarang(this.id)' 
                          >";
	    		$html .= "<p>" . $value->m_item_nama . "</p>";
	    		$html .= "</li>";
	    	}

	    	$html .= "</ul>";
	    }

    	return $html;
    }

    public function HargaPromo($key = ''){
        $this->db->start_cache();
        $this->db->from($this->_hpr_table . " a");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        $this->db->stop_cache();
        $this->db->flush_cache();

        return $this->db;
    }

    public function getMaxData(){
        $sql    = $this->db->query("SELECT MAX(RIGHT(trx_pos_no,5)) as noMax FROM 1nt3x_trx_pos WHERE toko_kode = '" . $this->SESSION['toko_kode'] . "'")->row();

        return $sql;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from("m_bank");

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();
        
        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->m_bank_id] = $row->m_bank_nama;
        }

        return $option;
    }

    public function getDataItem($key = ''){
        $this->db->start_cache();
        $this->db->from("vbarang");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        $this->db->stop_cache();
        $this->db->flush_cache();

        return $this->db;
    }

    public function getInfoStok($key = ''){
        $this->db->start_cache();
        $this->db->select("vbi.*, vb.m_item_nama");
        $this->db->from("vinfostok_blnini vbi");
        $this->db->join("vbarang vb","vb.m_item_kode = vbi.m_item_kode");

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        $this->db->stop_cache();
        $this->db->flush_cache();

        return $this->db;
    }

    public function checkMinimStok($item_kode = '', $sb = 0){

        $this->db->from("m_stok_min");
        $this->db->where("m_item_kode",$item_kode);

        $result = $this->db->get()->row();

        if(!empty($result)){
            if($sb <= $result->m_stok_min){
                return TRUE;
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }
    }

    public function hitungJumlahMinim($kategoribarang = ''){
        $jumlah_minim = 0;

        $data_barang = $this->getDataItem(array("m_item_kategori_kode" => $kategoribarang))->get()->result();
        
        $sb_now = 0;

        $idminim = array();
        if(!empty($data_barang)){
            foreach ($data_barang as $key => $value) {
                $infostok = $this->getInfoStok(array("vbi.m_item_kode"=>$value->m_item_kode,"vbi.m_toko_kode" => $this->SESSION['toko_kode']))->get()->row();
                
                $pa       = !empty($infostok->QTY)? $infostok->QTY : 0;
                $sm       = !empty($infostok->stok_masuk)? $infostok->stok_masuk : 0;
                $sk       = !empty($infostok->stok_keluar)? $infostok->stok_keluar : 0;

                $sb_now   = ($pa + $sm - $sk);

                if($this->checkMinimStok($value->m_item_kode, $sb_now)){
                    $jumlah_minim++;
                    $idminim[] = $value->m_item_kode;
                }
            }
        }

        $item_kode = !empty($idminim) ? implode("-", $idminim) : "";
        return array(0 => $jumlah_minim, 1 => $item_kode);   
    }

    public function getMinimumStokDetail($item_kode = ''){
        $data_kode_item = (trim($item_kode) <> ',') ? explode("-", $item_kode) : array();

        $html .= '<ul class="listcustom">';
        
        if(!empty($data_kode_item)){

            foreach ($data_kode_item as $value) {
                $infostok = $this->getInfoStok(array("vbi.m_item_kode"=>$value,"vbi.m_toko_kode" => $this->SESSION['toko_kode']))->get()->row();

                $pa       = !empty($infostok->QTY)? $infostok->QTY : 0;
                $sm       = !empty($infostok->stok_masuk)? $infostok->stok_masuk : 0;
                $sk       = !empty($infostok->stok_keluar)? $infostok->stok_keluar : 0;

                $sb_now   = ($pa + $sm - $sk);

                $html .= "<li><a class='ministoklink' href='#'><span style='float:left'>" . (!empty($infostok->m_item_nama) ? $infostok->m_item_nama : '') . "</span><span style='float:right' class='box box_minimstok'>". $sb_now . "</span></a></li>";
            }

        }else{
            $html .= "<li><a class='ministoklink' href='#'><span style='float:left'>Tidak Ada Data</span></a></li>";
        }

        $html .= '</ul>';

        return $html;
    }
}