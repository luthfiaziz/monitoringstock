<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Syarief Hidayatullah
 * Create Date : 26/07/2014 - 00:43:05
 * Module Name : Setting App ( For Template )
 */

class Mobile_pos extends MX_Controller {
	private $_module = '';

	public function __construct() {
            parent::__construct();
			
            $this->load->module("auth_phone/app_auth");
            $this->load->module("config/app_setting");
            $this->load->module("crud/app_crud");
			$this->load->model("mobile_pos_model",'mpm');
			$this->load->model('penjualan/pos_model','pos');
            $this->_module = "mobile_pos";
			$this->app_auth->checking_user_session();
			$this->SESSION  = $this->session->userdata(APPKEY);
	}
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>

	public function mobile_index($data = array()){
            $setting = $this->app_setting;

            $setting->set_plugin(array(
                'jquery','jquery-mobile','dhtmlx-skinweb'
            ));

       		$sales_name			= $this->SESSION['user_username'];
            $data["js_header" ] = $setting->get_js();
            $data["css_header"] = $setting->get_css();
			$data["query"]		= $this->mpm->item()->get();
			$data["bank"]		= $this->mpm->bank()->get();
			$data["kategori"]	= $this->mpm->kategori()->get();
			$data["kode_toko"]	= $this->SESSION['toko_kode'];
			//$data["penjualan"]	= $this->mpm->laporan_penjualan($sales_name)->get();
			
			
            $this->load->view($this->_module . "/mobile/index" ,$data);
	}
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	
	
	public function open(){
		 $this->load->view($this->_module . "/mobile/index" ,$data);
	}
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>

	public function mobile_tab_home(){
		    $setting = $this->app_setting;

            $setting->set_plugin(array(
                'jquery','jquery-mobile','mobile_tab','dhtmlx-skinweb','jquery-csv-toarray'
            ));

            $data["title_app"]      = "POS";
            $data["js_header" ]     = $setting->get_js();
            $data["css_header"]     = $setting->get_css();

            $this->load->view($this->_module . "/mobile/tab/index" ,$data);
	}
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>

	public function mobile_tab_transaction(){
		    $setting = $this->app_setting;

            $setting->set_plugin(array(
                'jquery','jquery-mobile','mobile_tab','dhtmlx-skinweb'
            ));

            $data["title_app"]  = "POS";
            $data["js_header" ] = $setting->get_js();
            $data["css_header"] = $setting->get_css();

            $this->load->view($this->_module . "/mobile/tab/transaction" ,$data);
	}
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>

    public function mobile_tes(){
        $setting = $this->app_setting;

        $setting->set_plugin(array(
            'jquery',
            'fullscreen-jquery',
            'select2-jquery','bootstrap'
        ));

        $data["title_app"]  = "POS";
        $data["js_header" ] = $setting->get_js();
        $data["css_header"] = $setting->get_css();

        $this->load->view($this->_module . "/mobile/tes" ,$data);
    }
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>

    public function get_data(){
        $idbarang = $this->input->post("idbarang");

        echo json_encode($idbarang);
    }
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
    
    public function mobile_login(){
        $this->form_validation->set_rules('mobile_username', 'Username', 'required');
        $this->form_validation->set_rules('mobile_password', 'Password', 'required');

        $location       = "auth";

        $data["username"] = $this->input->post("mobile_username");;  
        $data["password"] = $this->input->post("mobile_password");; 

        if($this->form_validation->run()){
            $result = $this->login_process(trim($data["username"]), trim($data["password"]));

            if($result == TRUE){
                $data["status"] = "<p>Login Berhasil</p>";
                $location       = "cpanel";
            }else{
                $data["status"] = "<p>Login Gagal, <u><b>Username</b></u> / <u><b>Password</b></u> yang anda masukan salah</p>";
            }
        }else{
            $data["status"] = validation_errors();
        }
        
        $this->session->set_flashdata($data);
        redirect(base_url($location));
    }
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>

    private function login_process($usr = '', $pass = ''){
        $key = array(
                "a.user_username" => $usr,
                "a.user_password" => md5($pass)
            );

        $data       = $this->set_data_session($this->app_auth_model->data($key)->get());

        $data_count = $this->app_auth_model->data($key)->get();

        if($data_count->num_rows() > 0){
            $this->app_auth_model->register_session($data);

            return TRUE;
        }else{
            return FALSE;
        }
    }
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>

    private function set_data_session($param = array()){
        $data   = $param->result();
        $result = array();

        foreach ($data as $key => $value) {
            // SET SESSION NAME
            $key_id = APPKEY;

            $result[$key_id]["user_id"]               = $value->user_id;
            $result[$key_id]["user_fullname"]         = $value->user_fullname;
            $result[$key_id]["user_username"]         = $value->user_username;
            $result[$key_id]["user_password"]         = $value->user_password;
            $result[$key_id]["user_jk"]               = $value->user_jk;
            $result[$key_id]["user_tgl_lhr"]          = $value->user_tgl_lhr;
            $result[$key_id]["user_alamat"]           = $value->user_alamat;
            $result[$key_id]["user_email"]            = $value->user_email;
            $result[$key_id]["user_tlp"]              = $value->user_tlp;
            $result[$key_id]["user_wrong_pass"]       = $value->user_wrong_pass;
            $result[$key_id]["user_status"]           = $value->user_status;
            $result[$key_id]["role_access_id"]        = $value->role_access_id;
            $result[$key_id]["role_access_nama"]      = $value->role_access_nama;
            $result[$key_id]["role_access_ket"]       = $value->role_access_desc;
            $result[$key_id]["role_id"]               = $value->role_id;
            $result[$key_id]["toko_kode"]             = $value->m_toko_kode;

            $menu_id = $value->menu_id;

            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_view"]     = $value->role_access_view;
            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_add"]      = $value->role_access_add;
            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_edit"]     = $value->role_access_edit;
            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_delete"]   = $value->role_access_delete;
            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_export"]   = $value->role_access_export;
            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_import"]   = $value->role_access_import;
            $result[$key_id]["otoritas_menu"][$menu_id]["menu_id"]              = $value->menu_id;
        }

        $result_final = $result;

        return $result_final;
    }
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>

    public function checking_user_session(){
        $GET_SESSION = $this->session->userdata(APPKEY);
        if(empty($GET_SESSION["user_username"]) || empty($GET_SESSION["user_password"])){
            redirect(base_url('auth'));
        }
    }
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>

    public function sessionCEK(){
        $GET_SESSION = $this->session->userdata(APPKEY);
        $data = array(false);

        if(empty($GET_SESSION["user_username"]) || empty($GET_SESSION["user_password"])){
            $data = array(true);
        }

        echo json_encode($data);
    }
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	
	public function scan_barcode(){
		$kode_barang = $this->input->post('kode_barang');
		$qty_barang	 = $this->input->post('qty');
		$sales_id	 = $this->SESSION['user_id'];
		
		
		$this->db->select('*');
		$this->db->from("m_item");
		$this->db->where(array("m_item_barcode" => $kode_barang));
		$this->db->get()->row();
		$query = $this->db->affected_rows($this->db);
		
		$this->db->select('*');
		$this->db->from('trx_tmp_pos a');
		$this->db->join('m_item b','a.trx_tmp_pos_item_kode = b.m_item_kode');
		$this->db->where(array('m_item_barcode'=>$kode_barang));
		$result = $this->db->get()->row();
		$query2 = $this->db->affected_rows($result); 
		
		$harga	= $this->mpm->data_detail($kode_barang)->get()->row();
						
		
						
		if($query > 0){
			$data['trx_tmp_pos_item_kode'] 	= $harga->m_item_kode;
			$data['trx_tmp_pos_qty']	    = $qty_barang;
			$data['trx_tmp_pos_harga']		= $harga->m_harga_jual;
			$data['trx_tmp_pos_petugas_id']	= $this->SESSION['user_id'];	
			$data['toko_kode']				= $this->SESSION['toko_kode'];
						
			
			if($query2 < 1){	
				$this->mpm->insert_pos('trx_tmp_pos',$data);
			}else{
				$this->mpm->update_pos($harga->m_item_kode,$sales_id);	
			}
			$hasil = true;
			
		}else{
			$hasil = false;	
		}
			
		echo json_encode($hasil);		
	}
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	
	public function phone_load_transaksi(){
		$sales_id	= $this->SESSION['user_id'];
		$toko_kode  = $this->SESSION['toko_kode'];
        $data 		= $this->mpm->load_transaksi($sales_id);
        echo json_encode($data);
    }
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	
	public function phone_search_brg($keyword=''){
		$sales_id	= $this->SESSION['user_id'];
		$toko_kode  = $this->SESSION['toko_kode'];
        $data 		= $this->mpm->load_search_barang_detail($keyword,$toko_kode);
        echo json_encode($data);
    }
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	
	public function phone_detail_pen($no_faktur=""){
		$arrFak             = explode("-",$no_faktur);
        $no_faktur          = $arrFak[0] . "/" . $arrFak[1] . "/" . $arrFak[2] . "/" . $arrFak[3];
        $data		 		= $this->mpm->load_detail_pen($no_faktur);
        echo json_encode($data);
    }
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	
	public function harian($tanggal='',$bulan='',$tahun=''){
		$sales_id	= $this->SESSION['user_username'];
		$data		= $this->mpm->load_harian($sales_id,$tanggal,$bulan,$tahun);
        echo json_encode($data);
    }
	

	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	
	public function periode($tanggal='',$tanggal2=''){
		$sales_id	= $this->SESSION['user_username'];	
		$data		= $this->mpm->load_periode($sales_id,$tanggal,$tanggal2);
        echo json_encode($data);
    }
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	
	public function minimum_stok($kode_kategori=''){
		$toko_kode	= $this->SESSION['toko_kode'];
		$data		= $this->mpm->load_min_stok($kode_kategori,$toko_kode);
		echo json_encode($data);		
	}
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	
	public function phone_load_detail($kode_barang){
        $data = $this->mpm->load_detail($kode_barang);
        echo json_encode($data);
    }
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	
	public function phone_add_item($kode_barang='',$stok=''){
        $data = $this->mpm->load_add_item($kode_barang,$stok);
        echo json_encode($data);
    }
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	
	public function phone_update_qty($kode_barang='',$qty=''){
		$sales_id	= $this->SESSION['user_id'];	
		if($this->mpm->update_qty_detail($kode_barang,$qty,$sales_id)){
			$hasil = true;	
		}else{
			$hasil = false;	
		}
		
		echo json_encode($hasil);
	}
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	
	public function phone_delete($kode_barang=''){
		$sales_id	= $this->SESSION['user_id'];	
		if($this->mpm->delete_qty_detail($kode_barang,$sales_id)){
			$hasil = true;	
		}else{
			$hasil = false;	
		}
		
		echo json_encode($hasil);
	}
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	
	public function phone_total($total_price='',$total_qty=''){
		$sales_id	= $this->SESSION['user_id'];
		$data 		= $this->mpm->load_total($total_price,$total_qty,$sales_id);	
		echo json_encode($data);
	}
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	
	public function cash($total_price=''){
		$no_faktur	= $this->generateNo();
		$data 		= $this->mpm->load_cash($total_price,$no_faktur);	
		echo json_encode($data);
	}
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	
	public function phone_save($total='',$kembalian='',$status='',$bank='',$no_kartu='',$no_trans=''){
		$crud = $this->app_crud;
		
		$no_trx							= $this->generateNo();
		$data['trx_pos_no']				= $no_trx;	
		$data['trx_pos_tgl']			= date('Y-m-d H:i:s');
		$data['trx_pos_ket']			= '';	
		$data['trx_pos_sub_total']		= $total;
		$data['trx_pos_diskon']			= '';
		$data['trx_pos_tax']			= '';
		$data['trx_pos_grand_total']	= $total;
		$data['trx_pos_kembalian']		= $kembalian;	
		$data['trx_pos_tgl_buat']		= date('Y-m-d');
		$data['trx_pos_tgl_ubah']		= '';
		$data['trx_pos_petugas']		= $this->SESSION['user_username'];	
		$data['toko_kode']				= $this->SESSION['toko_kode'];
		
		
		$query = $this->db->query("select * from 1nt3x_trx_tmp_pos");
		if($this->mpm->insert_pos('trx_pos',$data)){		
			
			$data_jenis['trx_pos_jns_pem_status']		= $status;  	
			$data_jenis['trx_pos_jns_pem_no_kartu']		= $no_kartu;
			$data_jenis['trx_pos_jns_pem_no_ref']		= $no_trans;
			
			if($status !='cash'){			
				$data_jenis['trx_pos_jns_pem_bayar']		= $total;
				$data_jenis['trx_pos_no']					= $no_trx;
				$data_jenis['m_bank_id']					= $bank;
			}
			
			$this->mpm->insert_pos('trx_pos_jns_pem',$data_jenis);
			
			foreach($query->result() as $value){
				$dataDetail['trx_pos_detail_qty']		=	$value->trx_tmp_pos_qty;
				$dataDetail['trx_pos_detail_harga']		=	$value->trx_tmp_pos_harga;
				$dataDetail['trx_pos_detail_disc']		=	'';
				$dataDetail['trx_pos_detail_total']		=	$value->trx_tmp_pos_harga * $value->trx_tmp_pos_qty;
				$dataDetail['item_kode']				=	$value->trx_tmp_pos_item_kode;
				$dataDetail['trx_pos_no']				=	$no_trx;
				$this->mpm->insert_pos('trx_pos_detail',$dataDetail);
				
				
				$dataInsSB  = array(
						'stok_berjalan_tgl'     => date('Y-m-d'),
						'stok_berjalan_ket'     => "POS Phone",
						'stok_berjalan_no_trx'  => $no_trx,
						'stok_berjalan_tgl_buat'=> date('Y-m-d'),
						'stok_berjalan_petugas' => $this->SESSION['user_username'],
						'm_toko_kode'           => $this->SESSION['toko_kode'],
						'm_item_kode'           => $value->trx_tmp_pos_item_kode,
						'stok_berjalan_qty_masuk'=> 0,
						'stok_berjalan_qty_keluar'=> $value->trx_tmp_pos_qty
				);
				
				$crud->save_as_new_nrb('inv_stok_berjalan', arrman::StripTagFilter($dataInsSB));
			}
			
			$sales_id	= $this->SESSION['user_id'];
			$this->mpm->delete_pos($sales_id);
			
		}else{
			$hasil = false;	
		}		
		echo json_encode($hasil);
	}
	
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	//------------------------------------------------------------------------------------------------------------------------------------------>
	
	public function generateNo(){
		$kode_toko	= $this->SESSION['toko_kode'];
        $dataMax    = $this->mpm->getMaxData_phone($kode_toko);
        $noNow      = $dataMax->noMax + 1;
		// $noNew      = sprintf("%07s",$noNow)."-".$this->SESSION['toko_kode']."-".date('m')."-".date('y');
        $noNew      = $this->SESSION['toko_kode']."/".date('m')."/".date('Y')."/".sprintf("%05s",$noNow);
		
        return $noNew;
    }
}