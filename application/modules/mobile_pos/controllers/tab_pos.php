<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Luthfi Aziz Nugraha 
 * Create Date : 26/07/2014 - 00:43:05
 * Module Name : Setting App ( For Template )
 */

class tab_pos extends MX_Controller {
	private $_module = '';
    private $layout  = '';

	public function __construct() {
            parent::__construct();

            $this->load->module("auth_mobile/app_auth");
            $this->load->module("config/app_setting");
            $this->load->module("crud/app_crud");
			$this->load->model("tab_pos_model","tpm");
            $this->load->model('penjualan/pos_model','pos');
            $this->load->model('master/list_model','list');
            $this->load->model('master/toko_model','toko');
            $this->load->model('pengaturan/pengguna_model','pengguna');
            $this->load->model("mobile_pos_model",'mpm');

            $this->_module = "mobile_pos";

            $this->app_auth->checking_user_session();
            // echo $this->layout;
            // exit();
            // GET SESSION DATA
            $this->SESSION  = $this->session->userdata(APPKEY);

	}

	public function mobile_tab_home(){
		    $setting = $this->app_setting;

            $setting->set_plugin(array(
                'jquery','jquery-mobile','mobile_tab','dhtmlx-skinweb','jquery-scrollbar','select2-jquery','custom-tab','jquery-csv-toarray'
            ));

            $data["title_app"]      = "POS";
            $data["js_header" ]     = $setting->get_js();
            $data["css_header"]     = $setting->get_css();
            $data["bank"]           = $this->tpm->options("-- PILIH BANK --");
            $data["sales"]          = $this->SESSION['user_username'];

            // Minimum Stok
            $data["kategori"]       = $this->mpm->kategori()->get();
          
            $kategori_mentah    = $this->mpm->kategori()->get()->result();

            $kategori_hasil     = array();

            foreach ($kategori_mentah as $key => $value) {
                $kategori_hasil[] = array(
                        'm_item_kategori_kode'      => $value->m_item_kategori_kode,
                        'm_item_kategori_nama'      => $value->m_item_kategori_nama,
                        'm_item_kategori_ket'       => $value->m_item_kategori_ket,
                        'jumlah_item_stokminim'     => $this->tpm->hitungJumlahMinim($value->m_item_kategori_kode)
                    );
            }

            $data['kategori'] = $kategori_hasil;

            $this->load->view($this->_module . "/mobile/tab/index" ,$data);
	}

	//public function mobile_tab_transaction(){
	//	    $setting = $this->app_setting;
    //
    //       $setting->set_plugin(array(
    //           'jquery','jquery-mobile','mobile_tab','dhtmlx-skinweb'
    //      ));

    //       $data["title_app"]  = "POS";
    //      $data["js_header" ] = $setting->get_js();
    //      $data["css_header"] = $setting->get_css();

    //     $this->load->view($this->_module . "/mobile/tab/transaction" ,$data);
	//}

    public function minimumstok($data = array()){
        $data["kategori"]       = $this->mpm->kategori()->get();
      
        $kategori_mentah    = $this->mpm->kategori()->get()->result();

        $kategori_hasil     = array();

        foreach ($kategori_mentah as $key => $value) {
            $kategori_hasil[] = array(
                    'm_item_kategori_kode'      => $value->m_item_kategori_kode,
                    'm_item_kategori_nama'      => $value->m_item_kategori_nama,
                    'm_item_kategori_ket'       => $value->m_item_kategori_ket,
                    'jumlah_item_stokminim'     => $this->tpm->hitungJumlahMinim($value->m_item_kategori_kode)
                );
        }

        $data['kategori'] = $kategori_hasil;
        
        $this->load->view("mobile_pos/mobile/tab/minimum_stok",$data,true);
    }

    public function minimumstok_detail($item_kode = ""){
        $item_kode = str_replace('-', ',', $item_kode);
        
        $data = $this->tpm->getMinimumStokDetail($item_kode);

        echo json_encode(array("minstok" => $data));
    }

    public function laporanpenjualan($data = array()){

        $this->load->view("mobile_pos/mobile/tab/laporan_penjualan",$data);
    }

    public function mobile_search_kategori(){
        $data = $this->tpm->loadDataKategori();
        echo json_encode($data);
    }

    public function mobile_search_brg(){
        $key = array();

        if(!empty($this->input->post("kd_kategori")) && $this->input->post("kd_kategori") != "smua_kategori")
            $key  = array("m_item_kategori_kode"=>$this->input->post("kd_kategori"));

        $data = $this->tpm->loadDataBarang($key);
        echo json_encode($data);
    }

    public function proses(){
        $crud = $this->app_crud;

        $no_trx                         = $this->generateNo();
        $dataIns                        = $this->input->post('dataIns');
        $dataIns['trx_pos_no']          = $no_trx;
        $dataIns['trx_pos_tgl']         = date('Y-m-d H:i:s');
        $dataIns['trx_pos_ket']         = "Penjualan oleh " . $this->SESSION['user_username'];
        $dataIns['trx_pos_tgl_buat']    = date('Y-m-d');
        $dataIns['trx_pos_petugas']     = $this->SESSION['user_username'];
        $dataIns['toko_kode']           = $this->SESSION['toko_kode'];
        $dataIns['trx_pos_tax']         = $dataIns['trx_pos_sub_total'] * 0.1;
        $dataIns['trx_pos_kembalian']   = $this->input->post('trx_pos_kembalian');

        $this->db->trans_begin();

        $crud->save_as_new_nrb('trx_pos', arrman::StripTagFilter($dataIns));

        $dataGrid   = $this->input->post('dataGrid');
        foreach($dataGrid as $val){
            $dataInsDet = array(
                'trx_pos_detail_qty'    => $val[4],
                'trx_pos_detail_harga'  => $val[3],
                'trx_pos_detail_disc'   => $val[5],
                'trx_pos_detail_total'  => $val[6],
                'item_kode'             => $val[0],
                'trx_pos_no'            => $no_trx
            );
            $crud->save_as_new_nrb('trx_pos_detail', arrman::StripTagFilter($dataInsDet));
        }

        $dataGridPembayaran = $this->input->post('dataGridPembayaran');
        foreach($dataGridPembayaran as $val){
            $dataInsJns = array(
                'trx_pos_jns_pem_status'    => $val[0],
                'trx_pos_jns_pem_bayar'     => $val[1],
                'trx_pos_jns_pem_no_kartu'  => $val[2],
                'trx_pos_jns_pem_no_ref'    => $val[3],
                'trx_pos_no'                => $no_trx
            );
            if($val[0] != "cash"){
                $dataInsJns['m_bank_id']    = $val[5];
            }
            $crud->save_as_new_nrb('trx_pos_jns_pem', arrman::StripTagFilter($dataInsJns));
        }

        $dataLog    = array(
            'log_user'      => $this->SESSION['user_username'],
            'log_kegiatan'  => "Simpan data POS",
            'log_waktu'     => date('Y-m-d H:i:s')
        );
        $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));

        if ($this->db->trans_status() === FALSE) {
            $message = array(false, 'Proses Gagal', 'Proses penyimpanan data gagal ( data di rollback ).', '');
            $this->db->trans_rollback();
        } else {
            $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refreshPos("' . $no_trx . '")');
            $this->db->trans_commit();
        }

        echo json_encode($message,true);
    }

    public function prosesCash(){
        $crud = $this->app_crud;

        $no_trx                         = $this->generateNo();
        $dataIns                        = $this->input->post('dataIns');
        $dataIns['trx_pos_no']          = $no_trx;
        $dataIns['trx_pos_tgl']         = date('Y-m-d H:i:s');
        $dataIns['trx_pos_ket']         = "Penjualan oleh " . $this->SESSION['user_username'];
        $dataIns['trx_pos_tgl_buat']    = date('Y-m-d');
        $dataIns['trx_pos_petugas']     = $this->SESSION['user_username'];
        $dataIns['toko_kode']           = $this->SESSION['toko_kode'];
        $dataIns['trx_pos_tax']         = $dataIns['trx_pos_sub_total'] * 0.1;
        $dataIns['trx_pos_kembalian']   = $this->input->post('trx_pos_kembalian');

        $this->db->trans_begin();

        $crud->save_as_new_nrb('trx_pos', arrman::StripTagFilter($dataIns));

        $dataGrid   = $this->input->post('dataGrid');
        foreach($dataGrid as $val){
            $dataInsDet = array(
                'trx_pos_detail_qty'    => $val[4],
                'trx_pos_detail_harga'  => $val[3],
                'trx_pos_detail_disc'   => $val[5],
                'trx_pos_detail_total'  => $val[6],
                'item_kode'             => $val[0],
                'trx_pos_no'            => $no_trx
            );
            $crud->save_as_new_nrb('trx_pos_detail', arrman::StripTagFilter($dataInsDet));

            $dataInsSB  = array(
                'stok_berjalan_tgl'     => date('Y-m-d'),
                'stok_berjalan_ket'     => "POS Tab",
                'stok_berjalan_no_trx'  => $no_trx,
                'stok_berjalan_tgl_buat'=> date('Y-m-d'),
                'stok_berjalan_petugas' => $this->SESSION['user_username'],
                'm_toko_kode'           => $this->SESSION['toko_kode'],
                'm_item_kode'           => $val[0],
                'stok_berjalan_qty_masuk'=> 0,
                'stok_berjalan_qty_keluar'=> $val[4]
            );
            $crud->save_as_new_nrb('inv_stok_berjalan', arrman::StripTagFilter($dataInsSB));
        }

        $jenispembayaran = '';

        if($this->input->post("paymenttype") == 'cs'){
            $jenispembayaran = 'cash';
        }


        if($this->input->post("paymenttype") == 'cr'){
            $jenispembayaran = 'credit';
        }


        if($this->input->post("paymenttype") == 'db'){
            $jenispembayaran = 'debit';
        }

        $dataInsJns = array(
            'trx_pos_jns_pem_status'  => $jenispembayaran,
            'trx_pos_jns_pem_bayar'   => $this->input->post('trx_pos_bayar'),
            'trx_pos_no'              => $no_trx,
            'm_bank_id'               => $this->input->post('jenisbank')
        );

        if(!empty($this->input->post("tnocard"))){
            $dataInsJns["trx_pos_jns_pem_no_kartu"] = $this->input->post("tnocard");
        }

        if(!empty($this->input->post("tnotrans"))){
            $dataInsJns["trx_pos_jns_pem_no_ref"] = $this->input->post("tnotrans");
        }

        $crud->save_as_new_nrb('trx_pos_jns_pem', arrman::StripTagFilter($dataInsJns));

        $dataLog    = array(
            'log_user'      => $this->SESSION['user_username'],
            'log_kegiatan'  => "Simpan data POS",
            'log_waktu'     => date('Y-m-d H:i:s')
        );
        $crud->save_as_new_nrb('log', arrman::StripTagFilter($dataLog));

        if ($this->db->trans_status() === FALSE) {
            $message = array(false, 'Proses Gagal', 'Proses penyimpanan data gagal ( data di rollback ).', '');
            $this->db->trans_rollback();
        } else {
            $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.',  'refreshPos("' . $no_trx . '")');
            $this->db->trans_commit();
        }

        echo json_encode($message,true);
    }

    public function generateNo(){
        $dataMax    = $this->tpm->getMaxData();

        $noNow      = $dataMax->noMax + 1;
//        $noNew      = sprintf("%07s",$noNow)."-".$this->SESSION['toko_kode']."-".date('m')."-".date('y');
        $noNew      = $this->SESSION['toko_kode']."/".date('m')."/".date('Y')."/".sprintf("%05s",$noNow);

        return $noNew;
    }

    public function print_struk($no_faktur=''){
        $mpdf = new mPDF('utf-8', array(110,400), 2,2,2,2);

        $arrFak             = explode("-",$no_faktur);
        $no_faktur          = $arrFak[0] . "/" . $arrFak[1] . "/" . $arrFak[2] . "/" . $arrFak[3];
        $data['faktur']     = $no_faktur;
        $data['pos']        = $this->pos->dataPos($no_faktur)->get()->row();
        $data["toko"]       = $this->toko->data($this->SESSION['toko_kode'])->get()->row();
        $data['posDetail']  = $this->pos->dataPosDetail($no_faktur)->get()->result();
        $data['posJenis']   = $this->pos->dataPosJenis(array("a.trx_pos_no" => $no_faktur))->get()->result();
        $data['user']       = $this->pengguna->data($this->SESSION['user_id'])->get()->row();

        $html= $this->load->view("mobile_pos/mobile/tab/struk",$data,true);

        // $absolutepath = $_SERVER["DOCUMENT_ROOT"] . "/rbs/assets/css/pdf.css";
        // $stylesheet   = file_get_contents($absolutepath); 
        // $mpdf->WriteHTML($stylesheet,1);

        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
}