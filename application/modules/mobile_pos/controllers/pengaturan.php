<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Luthfi Aziz Nugraha 
 * Create Date : 26/07/2014 - 00:43:05
 * Module Name : Setting App ( For Template )
 */

class Pengaturan extends MX_Controller {
	private $_module = '';

	public function __construct() {
            parent::__construct();

            $this->load->module("auth/app_auth");
            $this->load->module("config/app_setting");
            $this->load->module("crud/app_crud");

            $this->_module = "mobile_pos";
	}

	public function index(){
		$data['button_back']	= true;
		$data['title']			= 'Pengaturan';
		$data['link']			= 'content/pengaturan';
        $this->load->view($this->_module . "/mobile/index",$data);
	}

	
}