<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Luthfi Aziz Nugraha 
 * Create Date : 26/07/2014 - 00:43:05
 * Module Name : Setting App ( For Template )
 */

class Mobile_pos extends MX_Controller {
	private $_module = '';

	public function __construct() {
            parent::__construct();

            $this->load->module("auth/app_auth");
            $this->load->module("config/app_setting");
            $this->load->module("crud/app_crud");

            $this->_module = "mobile_pos";
	}

	public function mobile_index($data = array()){
            $setting = $this->app_setting;

            $setting->set_plugin(array(
                'jquery','jquery-mobile'
            ));

            $data["title_app"]  = "POS";
            $data["js_header" ] = $setting->get_js();
            $data["css_header"] = $setting->get_css();

            $this->load->view($this->_module . "/mobile/index" ,$data);
	}

	public function mobile_index_tab(){
		    $setting = $this->app_setting;

            $setting->set_plugin(array(
                'jquery','jquery-mobile','mobile_tab'
            ));

            $data["title_app"]  = "POS";
            $data["js_header" ] = $setting->get_js();
            $data["css_header"] = $setting->get_css();

            $this->load->view($this->_module . "/mobile/tab/index" ,$data);
	}

    public function mobile_tes(){
        $setting = $this->app_setting;

        $setting->set_plugin(array(
            'jquery',
            'fullscreen-jquery',
            'select2-jquery','bootstrap'
        ));

        $data["title_app"]  = "POS";
        $data["js_header" ] = $setting->get_js();
        $data["css_header"] = $setting->get_css();

        $this->load->view($this->_module . "/mobile/tes" ,$data);
    }

    public function get_data(){
        $idbarang = $this->input->post("idbarang");

        echo json_encode($idbarang);
    }
    
    public function mobile_login(){
        $this->form_validation->set_rules('mobile_username', 'Username', 'required');
        $this->form_validation->set_rules('mobile_password', 'Password', 'required');

        $location       = "auth";

        $data["username"] = $this->input->post("mobile_username");;  
        $data["password"] = $this->input->post("mobile_password");; 

        if($this->form_validation->run()){
            $result = $this->login_process(trim($data["username"]), trim($data["password"]));

            if($result == TRUE){
                $data["status"] = "<p>Login Berhasil</p>";
                $location       = "cpanel";
            }else{
                $data["status"] = "<p>Login Gagal, <u><b>Username</b></u> / <u><b>Password</b></u> yang anda masukan salah</p>";
            }
        }else{
            $data["status"] = validation_errors();
        }
        
        $this->session->set_flashdata($data);
        redirect(base_url($location));
    }

    private function login_process($usr = '', $pass = ''){
        $key = array(
                "a.user_username" => $usr,
                "a.user_password" => md5($pass)
            );

        $data       = $this->set_data_session($this->app_auth_model->data($key)->get());

        $data_count = $this->app_auth_model->data($key)->get();

        if($data_count->num_rows() > 0){
            $this->app_auth_model->register_session($data);

            return TRUE;
        }else{
            return FALSE;
        }
    }

    private function set_data_session($param = array()){
        $data   = $param->result();
        $result = array();

        foreach ($data as $key => $value) {
            // SET SESSION NAME
            $key_id = APPKEY;

            $result[$key_id]["user_id"]               = $value->user_id;
            $result[$key_id]["user_fullname"]         = $value->user_fullname;
            $result[$key_id]["user_username"]         = $value->user_username;
            $result[$key_id]["user_password"]         = $value->user_password;
            $result[$key_id]["user_jk"]               = $value->user_jk;
            $result[$key_id]["user_tgl_lhr"]          = $value->user_tgl_lhr;
            $result[$key_id]["user_alamat"]           = $value->user_alamat;
            $result[$key_id]["user_email"]            = $value->user_email;
            $result[$key_id]["user_tlp"]              = $value->user_tlp;
            $result[$key_id]["user_wrong_pass"]       = $value->user_wrong_pass;
            $result[$key_id]["user_status"]           = $value->user_status;
            $result[$key_id]["role_access_id"]        = $value->role_access_id;
            $result[$key_id]["role_access_nama"]      = $value->role_access_nama;
            $result[$key_id]["role_access_ket"]       = $value->role_access_desc;
            $result[$key_id]["role_id"]               = $value->role_id;
            $result[$key_id]["toko_kode"]             = $value->m_toko_kode;

            $menu_id = $value->menu_id;

            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_view"]     = $value->role_access_view;
            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_add"]      = $value->role_access_add;
            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_edit"]     = $value->role_access_edit;
            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_delete"]   = $value->role_access_delete;
            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_export"]   = $value->role_access_export;
            $result[$key_id]["otoritas_menu"][$menu_id]["role_access_import"]   = $value->role_access_import;
            $result[$key_id]["otoritas_menu"][$menu_id]["menu_id"]              = $value->menu_id;
        }

        $result_final = $result;

        return $result_final;
    }

    public function checking_user_session(){
        $GET_SESSION = $this->session->userdata(APPKEY);
        if(empty($GET_SESSION["user_username"]) || empty($GET_SESSION["user_password"])){
            redirect(base_url('auth'));
        }
    }

    public function sessionCEK(){
        $GET_SESSION = $this->session->userdata(APPKEY);
        $data = array(false);

        if(empty($GET_SESSION["user_username"]) || empty($GET_SESSION["user_password"])){
            $data = array(true);
        }

        echo json_encode($data);
    }
}