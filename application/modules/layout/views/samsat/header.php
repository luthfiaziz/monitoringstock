<div id="pesan" style="display:none"></div>
<div style="padding:2px; height:98px !important; background:#cee3ff" onload="start">
	<table id="table_header" border="0">
		<tr>
			<td width="250">
				<img src="<?php echo base_url(); ?>assets/img/logo/samsat.png" style="height:80px; !important; position:relative; left:-10px"/>
			</td>
			<td>
				<table id="table_icon" border="0">
					<tr>
						<td>
							<?php foreach($result as $r) { ?>
								<div class="icon-shorcut ikon" hovertext='<?php echo $r->m_menu_nama; ?>'>
									<?php 
										$image 	= !empty($r->m_menu_icon)? $r->m_menu_icon : 'man_akses.png';
									?>

									<img src       = "<?php echo base_url(); ?>assets/img/icon/<?php echo $image; ?>" 
										 width	   = "53px" 
										 height    = "53px" 
										 alt       = "gtes"
										 onclick	= "menuClick('<?php echo $r->m_menu_url; ?>')"
										 />
								</div>
							<?php } ?>
						</td>
						<td style="font-size:11px; text-align:right">
							<div id="time" style="font-size:42px; font-weight:bold; margin:0px ;padding:0px; font-family:Arial"></div>
						</td>
					</tr>
					<tr>
						<td height="30" colspan="2">
							<div class="info-header"><div id="toolbar_info"></div></div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
<script type="text/javascript">
	var toolbarnavigation = new dhtmlXToolbarObject("toolbar_info");
    toolbarnavigation.setSkin("dhx_skyblue");
    toolbarnavigation.setIconsPath(url + "assets/img/btn/");
	toolbarnavigation.addText("user",0,"<?php echo $user; ?>");
	toolbarnavigation.setAlign("left");
	toolbarnavigation.addSpacer("user");
	toolbarnavigation.loadXML("<?php echo $toolbar_info_source; ?>");
	toolbarnavigation.attachEvent("onclick",function(id){
		if(id == "infoakun"){
			alert(id);
		}

		if(id == "logout"){
		 	dhtmlx.confirm({                
                type:"confirm",
                text:"Anda yakin meninggalkan aplikasi ini?",
                callback: function(result){
                    if(result){
						logout("cpanel","logout");
                    }
                }
            });
		}
	})

	setInterval(function() {
	    var date = new Date();
	    $('#time').html(
	        checkTime(date.getHours()) + ":" + checkTime(date.getMinutes()) + ":" + checkTime(date.getSeconds())
	        );
	}, 10);


	//Javascript Mouseover
	$('.ikon').mousemove(function(e){
		var hovertext= $(this).attr('hovertext');
		
		$('#pesan').text(hovertext).show();
		$('#pesan').css('left', e.clientX + 10).css('top', e.clientY);	
		//alert("uguhu");
	});

	$('.ikon').mouseout(function(){
		$('#pesan').hide();
	});
</script>