<style>
    #dragDrop{
        z-index: 1000;
        margin-left: 87%;
        position: relative;
        cursor: hand;
        margin-top: 23px;
    }
</style>
<div id="dragDrop">
    <img src="<?php echo $logo; ?>" width="180" height="94">
</div>
<script type="text/javascript">
	var url = "<?php echo base_url(); ?>";
	var dhxLayoutData, layout_core, layout_body, statusbar;
	var lbc_a;
	var treeLayout;

	dhxLayoutData = {
						parent: document.body,
						pattern: "2E",
						cells: [{
						        id: "a",
						        header: false
						    },{
						        id: "b",
						        header: false
						    }]
					};

	myLayout = new dhtmlXLayoutObject(dhxLayoutData);
	myLayout.cells("a").setHeight(110);
	myLayout.cells("a").fixSize(true,true);
	
	/*SET MENU*/
	mainMenu = myLayout.cells("a").attachMenu();
    mainMenu.setIconsPath(url + "assets/img/icon/");
	mainMenu.loadXML("<?php echo $top_source; ?>");
	mainMenu.attachEvent("onClick", menuClick);

	/*SET TOOLBAR*/

	mainTab = myLayout.cells("b").attachTabbar();

	mainTab.setSkin("dhx_skyblue");  
	mainTab.setImagePath(url + "assets/dhtmlx/imgs/");  
	mainTab.addTab("maintabbar", "DASHBOARD", 120); 
	mainTab.cells("maintabbar").attachURL(url + "dashboard/dashboard",true); 
	mainTab.setTabActive("maintabbar");        
	mainTab.enableTabCloseButton(false);

	var statusbar_html = "<center>Copyright &copy; <a href='http://intex.co.id' class='copyFooter' target='_blank'>PT. Marui Intex</a> 2015. All rights reserved.</center>"
	statusbar = myLayout.cells("b").attachStatusBar();
	statusbar.setText(statusbar_html);

	function menuClick(id){
		var i = id.split("|");
		if(i[0]=="#"){
		    return true;
		}else if(i[1] == "logout"){
			dhtmlx.confirm({
			    text:"Anda yakin ingin keluar dari aplikasi ini?",
			    callback:function(result){
			    	if(result){
						logout(i[0],i[1])
			    	}
			    }
			});

			return false;
		}else if(i[1] == "petunjuk_penggunaan"){
			window.open(url + "bantuan/petunjuk_penggunaan",'_blank');
		}else{
		    var randomnumber=Math.floor((Math.random()*1000)+1);
		    var text = mainMenu.getItemText(id);
		    var textLength = text.length;
		    if(parseInt(textLength) >= 12) {
		        var panjang = parseInt(textLength) * 9;
		    } else {
		        var panjang = 100;
		    }

		    target_url = i[0] + "/" + i[1] + "/?id=" + [2];

		    // myLayout.cells("a").attachURL(url + target_url,true);
		    mainTab.clearAll();
			mainTab.addTab("maintabbar", text, panjang); 
			mainTab.cells("maintabbar").attachURL(url + i[0] + "/" + i[1] + "?id=" + i[2],true);
			mainTab.setTabActive("maintabbar");
		}
	}

	function logout(m,c){
		$.post(url + m + "/" + c, function(data) {
			window.location = url + "cpanel";
		});
	}
</script>