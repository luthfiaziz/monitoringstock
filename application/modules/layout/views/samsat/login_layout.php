<!DOCTYPE html>
<html>
<link rel="icon" href="<?php echo $icon; ?>" type="image/logo">
<head>
    <title><?php echo $app_title; ?></title>	
    <?php 
		echo $js_header;
		echo $css_header;
	?>
    <link href="<?php echo base_url(); ?>assets/css/login.css" rel="stylesheet" type="text/css">
</head>
<body class="body">
	<div id="wrapper_login">
		<form action="auth_confirm" method="post">
		<div id="wrapper_inner_login">
			<div id="wrapper_logo">
				<center>
					<img src="<?php echo base_url(); ?>assets/img/logo/cardinallogo.png" id="logo" style="height:70px; width:auto">
				</center>
			</div>
			<table width="100%" class="login_table">
				<tr>
					<td>
						<span>USERNAME</span>
						<?php echo form_input('samsat_username', !empty($this->session->flashdata('username'))? $this->session->flashdata('username') : '', ' class="input-login"  placeholder="ketik disini" id="samsat_username"'); ?>
						<br/><br/>
					</td>
				</tr>
				<tr>
					<td>
						<span>PASSWORD</span>
	      				<?php echo form_password('samsat_password', !empty($this->session->flashdata('password'))? $this->session->flashdata('password') : '', 'class="input-login"  placeholder="ketik disini" id="samsat_password"'); ?>
						<br/><br/>
					</td>
				</tr>
				<tr>
					<td style="text-align:right; padding-right:30px">
						<input type="submit" name="submit" placeholder="password" value="login" id="submit"/><br/><br/>
					</td>
				</tr>
			</table>
		</div>
		</form>
	</div>
	<br/>
	<?php 
		if($this->session->flashdata('status') != '' || $this->session->flashdata('status') != null){
	?>
	<div class="error_box autohidden">
		<div class="error_message">
			<div class="error_title"><u>ERROR</u></div>
		<?php 
			echo $this->session->flashdata('status');
		?>
		</div>
	</div>
	<?php
		}
	?>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$("#samsat_username").select();

		setTimeout(function() {
			$(".autohidden").fadeOut();
		}, 1500);
	});
</script>
	