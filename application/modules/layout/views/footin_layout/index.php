<script type="text/javascript">
	var url = "<?php echo base_url(); ?>";
	var dhxLayoutData, layout_core, layout_body, statusbar;
	var lbc_a, lbc_b;
	var tree;

	dhxLayoutData = {
						parent: document.body,
						pattern: "1C",
						cells: [{
						        id: "a",
						        text: "",
						        header: false,
						    }],
					};
	
	/*SET LAYOUT CORE*/
	layout_core = new dhtmlXLayoutObject(dhxLayoutData);
	
	/*SET MENU*/
	menu = layout_core.attachMenu();
	menu.loadStruct("<?php echo $top_source; ?>",true);
	menu.attachEvent("onClick", menuClick);

	toolbar = layout_core.attachToolbar();

	tab = layout_core.cells("a").attachTabbar();

	tab.setSkin("dhx_skyblue");    
	tab.addTab("dashboard", "DASHBOARD", 120);          
	tab.cells("dashboard").attachURL(url + "<?php echo $dashboard_load; ?>",true);
	tab.cells("dashboard").setActive();
	tab.enableTabCloseButton(true);

	/*SET STATUSBAR*/
	var session_name	    = "<?php echo $this->session->userdata('user_username'); ?>";
	var session_privilage   = "<?php echo $this->session->userdata('grup_nama'); ?>";
	var session_last_login	= "<?php echo $this->session->userdata('user_last_login'); ?>";
	var statusbar_html 		= "2014 &copy; Marui Intex | " +
							  "LOGIN NAME [ <b>" + session_name + "</b> ] - " +
							  "LOGIN AS [ <b>" + session_privilage + "</b> ] - " +
	                          "LAST LOGIN [ <b>" + session_last_login + "</b> ]";
	statusbar = layout_core.attachStatusBar();
	statusbar.setText(statusbar_html);

	function add_space(l){
		var space ="";
		for(var i=0; i<=l; i++){
			space += "&nbsp;";
		}

		return space;
	}
	function menuClick(id){
		// set data from id menu
		var data 		= id.split("|");
		var id_module	= data[0];
		var module		= data[1];
		var controller	= data[2];

		// set text menu item
        var text 		= menu.getItemText(id);
        var textLength 	= text.length;

        if(parseInt(textLength) >= 12) {
            var panjang = parseInt(textLength) * 9;
        } else {
            var panjang = 100;
        }

        if(text.trim() == "Exit"){
			dhtmlx.confirm({
			    text:"Anda yakin ingin keluar dari aplikasi ini?",
			    callback:function(result){
			    	if(result){
	        			logout(module,controller);
			    	}
			    }
			});

        	return false;
        }

        if(tab.getActiveTab() != id){
            var cek=0;
            var ids = tab.getAllTabs();

            for (var q=0; q<ids.length; q++) {
                if(id==ids[q]){
                    cek++;
                }
            }

            if(cek!=0){
				tab.cells(id).setActive();
            }else{
                tab.addTab(id,text,panjang+"px");
				tab.cells(id).setActive();
            }

        }

        tab.enableTabCloseButton(true);

        if(data[1]){
			tab.cells(id).attachURL(url + module + "/" + controller,true);
        }
	}

	function logout(m,c){
		$.post(url + m + "/" + c, function(data) {
			window.location = url + "controlpanel";
		});
	}
	
</script>