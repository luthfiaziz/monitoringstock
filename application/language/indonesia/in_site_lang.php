<?php

// FORM MITRA BISNIS
$lang["mb_kode"] 	= "Kode Mitra";
$lang["mb_mitra"]	= "Nama Mitra";
$lang["mb_tipe"]	= "Tipe Mitra";
$lang["mb_kontak"]	= "Kontak Mitra";
$lang["mb_tlp"]		= "Telepon Mitra";
$lang["mb_hp"]		= "Handphone Mitra";
$lang["mb_email"]	= "Email Mitra";
$lang["mb_website"]	= "Website Mitra";