<?php

// MITRA BISNIS FORM
$lang["mb_kode"] 	= "Mitra Code";
$lang["mb_mitra"]	= "Mitra Name";
$lang["mb_tipe"]	= "Mitra Type";
$lang["mb_kontak"]	= "Mitra Contact";
$lang["mb_tlp"]		= "Mitra Telephone";
$lang["mb_hp"]		= "Mitra Handphone";
$lang["mb_email"]	= "Mitra Email";
$lang["mb_website"]	= "Mitra Site";