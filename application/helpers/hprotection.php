<?php

class hprotection {

    public static function login($status = true) {
        if ($status) {
            if (!self::status_login()) {
                redirect('login');
                exit;
            }
        } else {
            if (self::status_login()) {
                redirect('meeting_management');
                exit;
            }
        }
    }

    public static function status_login() {
        $ci = &get_instance();
        return $ci->session->userdata('login_status');
    }

}