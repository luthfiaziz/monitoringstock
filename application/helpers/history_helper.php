<?php

/**
 * Description of xml_helper
 *
 * @author Luthfi Aziz Nugraha
 */
class arrman {
	public static $history_transaksi = array('pen_bpkb_id', 'pen_history_no_formulir', 'pen_history_no_pendaftaran', 'pen_history_no_bpkb', 'pen_history_no_kohir', 
											 'pen_history_skpd_aktif', 'pen_history_no_identitas', 'pen_history_nama', 'pen_history_alamat', 'pen_history_kebangsaan',
											 'pen_history_tanda_jati_diri', 'pen_history_status_kepemilikan', 'pen_history_no_polisi', 'pen_history_no_rangka', 
											 'pen_history_no_rangka', 'pen_history_no_mesin', 'pen_history_warna_kend', 'pen_history_warna_tnkb', 'pen_history_no_ident',
											 'pen_history_kode_lokasi', 'pen_history_jmlh_sumbu', 'pen_history_jmlh_penumpang', 'pen_history_jmlh_pintu', 'pen_history_kemudi',
											 'pen_history_kemudi', 'pen_history_negara_asal', 'pen_history_thn_reg', 'pen_history_isi_silinder', 'pen_history_tipe_mesin',
											 'pen_history_fungsi', 'pen_history_bhn_bakar', 'pen_history_dasar_penyerahan'
											);
}