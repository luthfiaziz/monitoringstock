<?php

/**
 * Description of xml_helper
 *
 * @author Luthfi Aziz Nugraha
 */
class arrman {
	public static $colotion_data = array(
										""   => "--Interval--",
 										"&gt;"  => ">",
										"&gt;=" => ">=",
										"&lt;"  => "<",
										"&lt;=" => "<=",
										"="  => "=",
									);

	public static $month		 = array(
										1  => "January",
										2  => "February",
										3  => "March",
										4  => "April",
										5  => "May",
										6  => "June",
										7  => "July",
										8  => "August",
										9  => "September",
										10 => "October",
										11 => "November",
										12 => "December"
									);

	public static $day 			 = array(
										0 =>"Minggu",
										1 =>"Senin",
										2 =>"Selasa",
										3 =>"Rabu",
										4 =>"Kamis",
										5 =>"Jum'at",
										6 =>"Sabtu"
									);

	public static $graph_type     = array(
										""     =>"--Type--",
									    "line" 		=> "Line",
									    "bar"  		=> "Bar",
									    "column"	=> "Column"
									);

     // [H] GET URL FORMAT MENU
	 public static function get_url_menu($data = array()){
            $temp     = explode("|", $data);
            $result   = array_splice($temp, 1,(count($temp) - 1));

            return implode("/", $result);
	 }


	public static function get_colotion_name($data){
		return self::$colotion_data[$data];
	}

	public static function get_month(){
		$data   = array();
		$data[''] = "--Month--";

		for($i=1; $i<=12; $i++){
			$data[$i] = self::$month[$i];
		}

		return $data;
	}

	public static function get_year($tahun_awal){
		$data   = array();
		$data[''] = "--Year--";
		for($i = $tahun_awal; $i<= date('Y'); $i++){
			$data[$i] = $i;
		}

		return $data;
	}

	public static function getRequired(){
		return '<div class="required"><span>*</span> wajib diisi</div>';
	}

	public static function showURL($data = ''){
		$data = explode("|", $data);

		return $data[0] . "/" . $data[1];
	}

	public static function csession(){
		return "";
	}

	public static function TglDMY($date){
		if(empty($date)){
			return '';
		}

		return date("d-m-Y", strtotime($date));
	}

	public static function TglYMD($date){
		return date("Y-m-d", strtotime($date));
	}

	public static function NamaHari($date){
		return self::$day[date("w",strtotime($date))];
	}

	public static function LEADINGZERO($num, $zero_number,$format){
 		return str_pad($num, $zero_number, $format, STR_PAD_LEFT);

	}

	public static function bulan($start, $end, $period = "day")
	{
	  	$start1 = strtotime ( '+15 day' , strtotime ( $start ) ) ; 
		$start1 = date("Y-m-d",$start1);

	    $day = 0;
	    $month = 0;
	    $month_array = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	    $datestart = strtotime($start1);
	    $dateend = strtotime($end);
	    $month_start = strftime("%m", $datestart);
	    $current_year = strftime("%y", $datestart);
	    $diff = $dateend - $datestart;
	    $date = $diff / (60 * 60 * 24);
	    $day = $date;

	    $awal = 1;

	    while($date > 0)
	    {
	        if($awal)
	        {
	            $loop = $month_start - 1;
	            $awal = 0;
	        }
	        else
	        {
	            $loop = 0;
	        }
	        for ($i = $loop; $i < 12; $i++)
	        {
	            if($current_year % 4 == 0 && $i == 1)
	                $day_of_month = 29;
	            else
	                $day_of_month = $month_array[$i];

	            $date -= $day_of_month;

	            if($date <= 0)
	            {
	                if($date == 0)
	                    $month++;
	                break;
	            }
	            $month++;
	        }

	        $current_year++;
	    }

	    //jika jumlah tanggal dan jumlah terlambat sama--------------------------------------------------------------
	    $ex_start = explode("-",$start1);
	    $bln = $ex_start[1];
	    $thn = $ex_start[0];
	    $tot_day = 0;
	    for($a=1;$a<=$month;$a++){
	    	$d=cal_days_in_month(CAL_GREGORIAN,$bln,$thn);
	    	$bln++;
	    	if($bln==13){
	    		$thn++;
	    		$bln="01";
	    	}
	    	$bln=sprintf("%02s",$bln);
	    	$tot_day+=$d;
	    }
	    
	    $month=$month+1;
		if($dateend<=$datestart){
			$month=$month-1;
	    }
	    switch($period)
	    {
	        case "day"   : return $day; break;
	        case "month" : return $month; break;
	        case "year"  : return intval($month / 12); break;
	    }
	}

	public function groupDataArray($field_target = array(), $field_temp = array()){
		return $field_temp;
	}

	public static function StripTagFilter($data){
		$result = array();
		foreach ($data as $key => $value) {
			$result[$key] = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;',strip_tags(trim($value)));
		}

		return $result;
	}

	public static function PAS_SYMBOL($char = "*", $length = 0){
		$string = "";
		for($i=1; $i<=$length; $i++){
			$string .= $char;
		}

		return $string;
	}

	public static function rounded($angka = 0){
        $angkaexplode = explode(',', $angka);
        $angkaarray = str_replace(".", "", $angkaexplode[0]);

        return $angkaarray;
    }
}
