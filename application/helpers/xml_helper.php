<?php
/**
 * Description of xml_helper
 *
 * @author Luthfi Aziz Nugraha
 */
class xml {
	 public static function xml_header(){
        if (stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml")) 
        {
            header("Content-type: application/xhtml+xml"); 
        }
        else 
        {
            header("Content-type: text/xml");
        }

        echo '<?xml version="1.0" encoding="iso-8859-1"?>';
	 }
}
