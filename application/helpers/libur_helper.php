<?php

class libur {
    public static function cekLibur($defaultview = '', $tanggal = '', $jamkerja = '',$samsat_id ='') {
        $ci = &get_instance();
        $ci->load->model('master/hari_libur_model','libur');
        $ci->load->model('master/samsat_model','samsat');

        $view = $defaultview;

        if($ci->samsat->cekJamKerja($jamkerja, $samsat_id) <= 0){
            $view = "layout/samsat/hari_libur/jam_kerja";

            if($ci->libur->CekHariLibur($tanggal) > 0){
                $view = "layout/samsat/hari_libur/hari_libur";
            }
        }

        return $view;
    }

}