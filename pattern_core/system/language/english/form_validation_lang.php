<?php

$lang['required']			= "Field <b><b>%s</b></b> wajib diisi";
$lang['isset']				= "Field <b><b>%s</b></b> harus memiliki nilai";
$lang['valid_email']		= "Field <b><b>%s</b></b> harus berisi alamat email yang valid";
$lang['valid_emails']		= "Field <b><b>%s</b></b> harus berisi semua alamat email yang valid";
$lang['valid_url']			= "Field <b><b>%s</b></b> harus berisi URL yang valid";
$lang['valid_ip']			= "Field <b><b>%s</b></b> harus berisi IP yang valid";
$lang['min_length']			= "Field <b>%s</b> harus memiliki sedikitnya <b>%s</b> karakter";
$lang['max_length']			= "Field <b>%s</b> field can not exceed <b>%s</b> characters in length.";
$lang['exact_length']		= "Field <b>%s</b> field must be exactly <b>%s</b> characters in length.";
$lang['alpha']				= "Field <b>%s</b> field may only contain alphabetical characters.";
$lang['alpha_numeric']		= "Field <b>%s</b> field may only contain alpha-numeric characters.";
$lang['alpha_dash']			= "Field <b>%s</b> field may only contain alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= "Field <b>%s</b> harus berisi angka";
$lang['is_numeric']			= "Field <b>%s</b> field must contain only numeric characters.";
$lang['integer']			= "Field <b>%s</b> harus berisi integer";
$lang['regex_match']		= "Field <b>%s</b> field is not in Field correct format.";
$lang['matches']			= "Field <b>%s</b> field does not match Field <b>%s</b> field.";
$lang['is_unique'] 			= "Field <b>%s</b> field must contain a unique value.";
$lang['is_natural']			= "Field <b>%s</b> field must contain only positive numbers.";
$lang['is_natural_no_zero']	= "Field <b>%s</b> field must contain a number greater than zero.";
$lang['decimal']			= "Field <b>%s</b> field must contain a decimal number.";
$lang['less_than']			= "Field <b>%s</b> field must contain a number less than <b>%s</b>.";
$lang['greater_than']		= "Field <b>%s</b> field must contain a number greater than <b>%s</b>.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */