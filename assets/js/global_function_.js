function form_modal(windowDHX,id,twh,toolbar_url,form_url){
	var winid  = "window_" + id;
	var left   = (screen.width - twh[1]) / 2;
    var top    = (screen.height - twh[2]) / 4;
    toolbarWindow = {};

	windowDHX.createWindow(winid,left,top,twh[1],twh[2]);
	windowDHX.window(winid).setText(twh[0]);
	windowDHX.window(winid).setModal(true);

	toolbarWindow["toolbar_" + id] = windowDHX.window(winid).attachToolbar();
    toolbarWindow["toolbar_" + id].setSkin("dhx_skyblue");
 	toolbarWindow["toolbar_" + id].setIconsPath(url + "assets/img/btn/");
	toolbarWindow["toolbar_" + id].loadXML(toolbar_url);

	windowDHX.window(winid).attachURL(form_url,true); 
	windowDHX.window(winid).denyResize();
}

function form_modal_grid(windowDHX,id,twh,toolbar_url, gridprop){
	var winid  = "window_" + id;
	var left   = (screen.width - twh[1]) / 2;
    var top    = (screen.height - twh[2]) / 4;
    toolbarWindowGrid = {};
    GWINDOW = {};

	windowDHX.createWindow(winid,left,top,twh[1],twh[2]);
	windowDHX.window(winid).setText(twh[0]);
	windowDHX.window(winid).setModal(true);
	windowDHX.window(winid).denyResize();
	
	if(toolbar_url !== 'tanpatoolbar'){
		toolbarWindowGrid["toolbar_" + id] = windowDHX.window(winid).attachToolbar();
	    toolbarWindowGrid["toolbar_" + id].setSkin("dhx_skyblue");
	 	toolbarWindowGrid["toolbar_" + id].setIconsPath(url + "assets/img/btn/");
		toolbarWindowGrid["toolbar_" + id].loadXML(toolbar_url);
	}

	GWINDOW["g" + id] = windowDHX.window(winid).attachGrid();
	GWINDOW["g" + id].setHeader(gridprop.header,null,gridprop.headeralign);
	GWINDOW["g" + id].attachHeader(gridprop.attachheader,gridprop.filteralign); 
	GWINDOW["g" + id].setColAlign(gridprop.colalign);
	GWINDOW["g" + id].setColTypes(gridprop.coltypes);
	GWINDOW["g" + id].setInitWidths(gridprop.sizes);
	GWINDOW["g" + id].setImagesPath(url + "assets/dhtmlx/imgs/");
	GWINDOW["g" + id].attachFooter(gridprop.footer);
	GWINDOW["g" + id].init();   

	aksiGrid = gridprop.fungsi;

	for(i=0; i<aksiGrid.length; i++){
		GWINDOW["g" + id].attachEvent(aksiGrid[i][0], aksiGrid[i][1]);
	}

	GWINDOW["g" + id].load(gridprop.urltarget,"json");

	var colhidden = gridprop.hiddencolumn;
	for(i=0; i<colhidden.length; i++){
		GWINDOW["g" + id].setColumnHidden(colhidden[i],true);
	}
}

function close_form_modal(windowDHX,id){
	var winid  = "window_" + id;
	windowDHX.window(winid).close();
}

function refreshForm(field,form,focus_field){
	$(form).trigger("reset");

	for(i=0; i<field.length; i++){
		//------------------------------
		// CLEAR DATA
		//------------------------------
			if(field[i][2] == "combo"){
				$("#" + field[i][0], form).select2('val',field[i][1]);
			}
	}

	if(focus_field[1] == "combo"){
		$("#" + focus_field[0],form).select2('focus');
	}else{
		$("#" + focus_field[0],form).focus();
	}
}


function switchForm(data){
	for(i=0; i<=data.length; i++){
		//------------------------------
		// READONLY FORM
		//------------------------------
			if(data[i][1] == "off"){
				$("#" + data[i][0]).attr("readonly","readonly");
			} 

			if(data[i][1] == "on"){
				$("#" + data[i][0]).removeAttr("readonly","readonly");
			}
	}
}

function EnabledDisabled(data,status){
	for(i=0; i<=data.length; i++){
		//------------------------------
		// READONLY FORM
		//------------------------------
			if(status == "disabled"){
				$("#" + data[i]).attr("disabled","disabled");
			} 

			if(status == "enabled"){
				$("#" + data[i]).removeAttr("disabled","disabled");
			}
	}
}


function createButton(id){
    $('#' + id).linkbutton({
    	iconCls:  "icon-" + $("#" + id).attr("icon")
    });
}

function refreshGrid(grid,url,type){
	grid.clearAll();
	grid.load(url,type);
}

// TOOLBAR POPUP
function SimpanData(form, loading, toolbar, data){
	dhtmlx.confirm({
		text:"Anda yakin ingin melakukan proses ini?",
        ok:"Ya", 
        cancel:"Tidak",
		callback:function(result){
			if(result){
				$(form).ajaxSubmit({
					data:data,
				    beforeSubmit: function(a, f, o) {
				        o.dataType = 'json';
				        toolbarWindowOnOf(toolbar,"disabled");
				        $(loading).show();
				    },
				    success: function(res) {
				        toolbarWindowOnOf(toolbar,"enable");
				        $(loading).hide();	

				        message_type ="alert";
				        judul = "Proses Sukses";

				        if(res[0] == false){
				        	message_type = "alert-error";
				        	judul = "Terdapat Kesalahan";
				        }

				        dhtmlx.message({
				        	title: judul,
	                        text: res[2],
	                        type: message_type,
	                        callback:function(){
						    	if(res[0] == true)
									return eval(res[3]);
	                        }
	                    });
				    },
                    error : function(){
                        toolbarWindowOnOfTF(toolbar,"enable");
                        $(loading).hide();

                        dhtmlx.message({
                            title: "Terdapat Kesalahan",
                            text: 'Proses penyimpanan data gagal.',
                            type: "alert-error"
                        });
                    }
				});
			}
		}
	});
}

// TOOLBAR POPUP
function SimpanDataPembayaran(form, loading, data){
	dhtmlx.confirm({
		text:"Anda yakin ingin melakukan proses ini?",
        ok:"Ya", 
        cancel:"Tidak",
		callback:function(result){
			if(result){

				$(form).ajaxSubmit({
					data:data,
				    beforeSubmit: function(a, f, o) {
				        o.dataType = 'json';
				        $(loading).show();
				    },
				    success: function(res) {
				        $(loading).hide();	

				        message_type ="alert";
				        judul = "Proses Sukses";

				        if(res[0] == false){
				        	message_type = "alert-error";
				        	judul = "Terdapat Kesalahan";
				        }

				        dhtmlx.message({
				        	title: judul,
	                        text: res[2],
	                        type: message_type,
	                        callback:function(){
						    	if(res[0] == true)
									return eval(res[3]);
	                        }
	                    });
				    },
                    error : function(){
                        toolbarWindowOnOfTF(toolbar,"enable");
                        $(loading).hide();

                        dhtmlx.message({
                            title: "Terdapat Kesalahan",
                            text: 'Proses penyimpanan data gagal.',
                            type: "alert-error"
                        });
                    }
				});
			}
		}
	});
}

function SimpanDataPembayaranCicilan(form, loading, data){
    dhtmlx.confirm({
        text:"Anda yakin ingin melakukan proses ini?",
        ok:"Ya",
        cancel:"Tidak",
        callback:function(result){
            if(result){
                $(form).ajaxSubmit({
                    data:data,
                    beforeSubmit: function(a, f, o) {
                        o.dataType = 'json';
                        $(loading).show();
                    },
                    success: function(res) {
                        $(loading).hide();

                        message_type ="alert";
                        judul = "Proses Sukses";

                        if(res[0] == false){
                            message_type = "alert-error";
                            judul = "Terdapat Kesalahan";
                        }

                        dhtmlx.message({
                            title: judul,
                            text: res[2],
                            type: message_type,
                            callback:function(){
                                if(res[0] == true){
                                    return eval(res[3]);
                                }
                            }
                        });
                    }
                });
            }
        }
    });
}

function toolbarWindowOnOf(toolbar, type){
	var dis;
	console.log(toolbarWindow);
	if(type === "disabled"){
		for(dis=0; dis<toolbar.length - 1; dis++){
			console.log(toolbar[0]);
			toolbarWindow["toolbar_" + toolbar[0]].disableItem(toolbar[dis + 1]);
		}
	}

	var enab;
	if(type === "enable"){		
		for(enab=0; enab<toolbar.length - 1; enab++){
			toolbarWindow["toolbar_" + toolbar[0]].enableItem(toolbar[enab + 1]);
		}
	}
}

// TOOLBAR NON POPUP
function SimpanDataTF(form, loading, toolbarobject, toolbar, data){
	dhtmlx.confirm({
		text:"Anda yakin ingin melakukan proses ini?",
        ok:"Ya", 
        cancel:"Tidak",
		callback:function(result){
			if(result){
				$(form).ajaxSubmit({
					data:data,
				    beforeSubmit: function(a, f, o) {
				        o.dataType = 'json';
				        toolbarWindowOnOfTF(toolbar,"disabled");
				        $(loading).show();
				    },
				    success: function(res) {
				        toolbarWindowOnOfTF(toolbar,"enable");
				        $(loading).hide();	

				        message_type ="alert";
				        judul = "Proses Sukses";

				        if(res[0] == false){
				        	message_type = "alert-error";
				        	judul = "Terdapat Kesalahan";
				        }

				        dhtmlx.message({
				        	title: judul,
	                        text: res[2],
	                        type: message_type,
	                        callback:function(){
						    	if(res[0] == true)
									return eval(res[3]);
	                        }
	                    });
				    },
                    error : function(){
                        toolbarWindowOnOfTF(toolbar,"enable");
                        $(loading).hide();

                        dhtmlx.message({
                            title: "Terdapat Kesalahan",
                            text: 'Proses penyimpanan data gagal.',
                            type: "alert-error"
                        });
                    }
				});
			}
		}
	});
}

function toolbarWindowOnOfTF(toolbarObject, toolbar, type){
	var dis;

	if(type === "disabled"){
		for(dis=0; dis<toolbar.length; dis++){
			toolbarObject.disableItem(toolbar[dis]);
		}
	}

	var enab;
	if(type === "enable"){		
		for(enab=0; enab<toolbar.length; enab++){
			toolbarObject.enableItem(toolbar[enab]);
		}
	}
}


function showMessage(tipe,text){
	dhtmlx.message({ 
	    type: tipe, 
	    text: text,
      	ok:"Ok"
	});
}


function PreviewImage(no) {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploadImage"+no).files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById("uploadPreview"+no).src = oFREvent.target.result;
    };
}

function open_popup(url){
	window.open(url, "_blank", "toolbar=no, scrollbars=no, resizable=no, fullscreen=yes");
}

function toolbarOnOff(toolbarObject,toolbar,type){
	var dis;
	if(type === "off"){	
		for(dis=0; dis<toolbar.length; dis++){
			toolbarObject.disableItem(toolbar[dis]);
		}
	}

	var enab;
	if(type === "on"){		
		for(enab=0; enab<toolbar.length; enab++){
			toolbarObject.enableItem(toolbar[enab]);
		}
	}
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

function get_options(target, data, index, form) {
    var st_chosen = false;

    if (typeof chosen !== 'undefined')
        st_chosen = chosen;
        
    var link = $(target,form).attr('data-source');
	
    $(target,form).empty();

    $.post(link, data, function(res) {
        $(target,form).html(generate_option(res));
    	$(target,form).select2('val', index);
    }, 'json');
}

function generate_option(list, selected) {
    var option = '';
    $.each(list, function(key, value) {
        var sel = '';
        if (typeof selected !== 'undefined' && selected === value) {
            sel = 'selected="selected"';
        }

        option += '<option value="' + key + '" ' + sel + '>' + value + '</option>';
    });
    return option;
}



function check_global(url_target, param){
    keluardalam = $.ajax({
        type: "POST",
        url  : url_target,
        data : param,
	    dataType: 'html',
	    context: document.body,
	    global: false,
	    async:false,
        success: function(result){
        	return result;
        }	
    }).responseText;

    return $.parseJSON(keluardalam);
}

function BersihPOPUP(field, focus, text){
	dhtmlx.message({
		title: "Informasi",
		text: text,
	    type: "alert",
	    callback:function(res){
	    	if(res == true){
				for(i=0; i<field.length; i++){
					$("#" + field[i]).val('');
				}

				$("#" + focus).focus();
	    	}
	    }
	});
}

function check_blokir(nopol){
    hasil = $.ajax({
        type: "POST",
        url  : url + "petugas_cbu/blokir_kend/blokir_bro",
        data : {
            nopol : nopol,
        },
	    dataType: 'html',
	    context: document.body,
	    global: false,
	    async:false,
        success: function(result){
        	return result;
        }	
    }).responseText;

    return $.parseJSON(hasil);
}


function bersihPencarianModal(field, focus, nopol, ket){
	dhtmlx.message({
		title: "Informasi",
		text: "Kendaraan Dengan No Polisi <b>" + nopol + "</b> Diblokir <br/><br/><center><b/>.::KETERANGAN::.</b></center>" + ket,
	    type: "alert",
	    callback:function(res){
	    	if(res == true){
				for(i=0; i<field.length; i++){
					$("#" + field[i]).val('');
				}

				$("#" + focus).focus();
	    	}
	    }
	});
}

function check_lunas(nopol){
    hasil = $.ajax({
        type: "POST",
        url  : url + "petugas_cbu/blokir_kend/cekKondisiMutasi",
        data : {
            nopol : nopol,
        },
	    dataType: 'html',
	    context: document.body,
	    global: false,
	    async:false,
        success: function(result){
        	return result;
        }	
    }).responseText;

    return $.parseJSON(hasil);
}

function Lunasi(field, focus){
	dhtmlx.message({
		title: "Informasi",
		text: "Lunasi Biaya Pajak Kendaraan",
	    type: "alert",
	    callback:function(res){
	    	if(res == true){
				for(i=0; i<field.length; i++){
					$("#" + field[i]).val('');
				}

				$("#" + focus).focus();
	    	}
	    }
	});
}

function toRp(angka){
    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
    var rev2    = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += '.';
        }
    }
    return rev2.split('').reverse().join('');
}
