<script type="text/javascript">
	statusEnding();
</script>
<style>
    .select{
        width:320px;
    }
    
    #periode_awal,#periode_akhir{
        border:1px solid #EEEEEE;
        height:20px;
        padding:5px;
        border-radius:5px;
    }
</style>
<div style="padding:10px; border:1px solid #AAAAAA; margin:10px;">
<table border='0' width='100%' cellpadding="0" cellspacing="0">
    <tr>
        <td width="30%" style="padding:30px;" rowspan="2"><div id="report_analisa" style="width:100%; height:450px; overflow: hidden;"></div></td>
        <td style="padding:30px; border-left:3px solid #AAAAAA">
            <form>
                <table>
                    <tr>
                        <td>Lokasi</td>
                        <td>:</td>
                        <td>
                            <?php echo form_dropdown('lokasi_id', $get_lokasi, !empty($default->provinsi_id) ? $default->provinsi_id : '', 'id="lokasi_id" class="select"'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Periode</td>
                        <td>:</td>
                        <td>
                            <?php echo form_input('periode_awal', '', 'class="input" id="periode_awal" placeholder="YYYY-MM-DD"'); ?> s/d
                            <?php echo form_input('periode_akhir', '', 'class="input" id="periode_akhir" placeholder="YYYY-MM-DD"'); ?> 
                        </td>
                    </tr>
                    <tr>
                        <td>Jenis Barang</td>
                        <td>:</td>
                        <td>
                            <?php echo form_dropdown('iditem_type', array("" => "--Pilih Data--","OWN"=>"Own","KSY"=>"Konsinyasi","BRN" => "BRANDED"), '', 'id="iditem_type" class="select"'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Kategori</td>
                        <td>:</td>
                        <td>
                            <?php echo form_dropdown('kategori_id', $get_kategori, '', 'id="kategori_id" class="select"'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Subkategori</td>
                        <td>:</td>
                        <td>
                            <?php echo form_dropdown('sub_kategori_id', $get_subkat, '', 'id="sub_kategori_id" class="select"'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Brand</td>
                        <td>:</td>
                        <td>
                            <?php echo form_dropdown('brand_id', $get_brand, '', 'id="brand_id" class="select"'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Artikel</td>
                        <td>:</td>
                        <td>
                            <?php echo form_dropdown('idbarang', $get_artikel, '', 'id="idbarang" class="select"'); ?>
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
    <tr>
        <td style="padding:30px; border-left:3px solid #AAAAAA">
            <button data-click="priview">PREVIEW</button>
            <!-- <button data-click="cetakpdf">CETAK PDF</button> -->
            <button data-click="cetakexcel">CETAK EXCEL</button>
            <button data-click="refresh">REFRESH FILTER</button>
        </td>
    </tr>
</table>
</div>
<script type="text/javascript">
    $("#idbarang").select2();
    $("#brand_id").select2();
    $("#sub_kategori_id").select2();
    $("#kategori_id").select2();
    $("#iditem_type").select2();
    $("#lokasi_id").select2();
    
    $("button").on("click",function(){
        var idRows;
        var id              = $(this).attr("data-click");
        var selectedID      = dhxGridReportAnalisa.getSelectedRowId();

        var lokasi          = ($("#lokasi_id").val() != "" && $("#lokasi_id").val() != "0") ? $("#lokasi_id").val() : '-';
        var per_awal        = ($("#periode_awal").val() != "" && $("#periode_awal").val() != "0") ? $("#periode_awal").val() : '-';
        var per_akhir       = ($("#periode_akhir").val() != "" && $("#periode_akhir").val() != "0") ? $("#periode_akhir").val() : '-';
        var item_type       = ($("#iditem_type").val() != "" && $("#iditem_type").val() != "0") ? $("#iditem_type").val() : '-';
        var kategori_id     = ($("#kategori_id").val() != "" && $("#kategori_id").val() != "0") ? $("#kategori_id").val() : '-';
        var sub_kategori_id = ($("#sub_kategori_id").val() != "" && $("#sub_kategori_id").val() != "0") ? $("#sub_kategori_id").val() : '-';
        var brand_id        = ($("#brand_id").val() != "" && $("#brand_id").val() != "0") ? $("#brand_id").val() : '-';
        var idbarang        = ($("#idbarang").val() != "" && $("#idbarang").val() != "0") ? $("#idbarang").val() : '-';

        var param = lokasi + "/" + per_awal + "/" + per_akhir + "/" + item_type + "/" + kategori_id + "/" + sub_kategori_id + "/" + brand_id + "/" + idbarang;
        
        try{
            idRows = dhxGridReportAnalisa.cells(selectedID,2).getValue();
        }catch(er){
            idRows = "NULL";
        }

        if(id == "priview"){
           if(idRows == "NULL"){
               alert("Anda belum memilih menu");
               return false;
           }

            window.open("<?php echo base_url(); ?>index.php" + "/" + idRows + param, "_blank", "toolbar=no, scrollbars=yes, resizable=no, fullscreen=yes");
        }

        if(id == "cetakexcel"){
           if(idRows == "NULL"){
               alert("Anda belum memilih menu");
               return false;
           }        

            window.open("<?php echo base_url(); ?>index.php" + "/" + idRows + param + "/excel", "_blank", "toolbar=no, scrollbars=yes, resizable=no, fullscreen=yes");
        }

        if(id == "refresh"){
            var dataenabled  = ["kategori_id","brand_id","idbarang","sub_kategori_id"];

            onoff(dataenabled,"enabled");
            dhxGridReportAnalisa.clearAll();
            dhxGridReportAnalisa.loadXML(base_url+"index.php/module_analisa/reportlist");
        }
    });
    
    //-------------------------------------------------------------------------------
    // GRID
    //-------------------------------------------------------------------------------
     
    window.dhx_globalImgPath = base_url+"assets/codebase_combo/imgs/";

    perawal = new dhtmlxCalendarObject('periode_awal');
    perawal.setDateFormat('%Y-%m-%d');

    perakhir = new dhtmlxCalendarObject('periode_akhir');
    perakhir.setDateFormat('%Y-%m-%d');
    
    dhxGridReportAnalisa = new dhtmlXGridObject('report_analisa');
    dhxGridReportAnalisa.setImagePath(base_url+"assets/codebase_tabbar/imgs/");
    dhxGridReportAnalisa.setHeader("MENU LAPORAN ANALISA,ctr_pdf,ctr_excel");
    dhxGridReportAnalisa.setInitWidths("*,10,10")
    dhxGridReportAnalisa.setColAlign("left,left,left")
    dhxGridReportAnalisa.setColTypes("ro,ro,ro");
    dhxGridReportAnalisa.setColumnHidden(1,true);
    dhxGridReportAnalisa.setColumnHidden(2,true);
    dhxGridReportAnalisa.enableMultiselect(true);
    dhxGridReportAnalisa.attachEvent("onRowSelect", doOnRowSelectedReport);
    dhxGridReportAnalisa.init();
    dhxGridReportAnalisa.setSkin("modern");
    dhxGridReportAnalisa.loadXML(base_url+"index.php/module_analisa/reportlist");



     function doOnRowSelectedReport(id) {
         if(id == "1"){
            var datadisabled = ["sub_kategori_id"];
            var dataenabled  = ["kategori_id","brand_id","idbarang"];

            onoff(datadisabled,"disabled");
            onoff(dataenabled,"enabled");
         }
         
         if(id == "2"){
            var datadisabled = ["sub_kategori_id"];
            var dataenabled  = ["kategori_id","brand_id","idbarang"];
            
            onoff(datadisabled,"disabled");
            onoff(dataenabled,"enabled");
         }
         
         if(id == "3"){
            var datadisabled = ["sub_kategori_id","kategori_id","idbarang"];
            var dataenabled  = ["brand_id"];
            
            onoff(datadisabled,"disabled");
            onoff(dataenabled,"enabled");
         }
         
         if(id == "4"){
            var datadisabled = ["sub_kategori_id","brand_id","idbarang"];
            var dataenabled  = ["kategori_id"];
            
            onoff(datadisabled,"disabled");
            onoff(dataenabled,"enabled");
         }
         
         if(id == "5"){
            var datadisabled = ["kategori_id","idbarang"];
            var dataenabled  = ["sub_kategori_id","brand_id"];
            
            onoff(datadisabled,"disabled");
            onoff(dataenabled,"enabled");
         }
         
         if(id == "6"){
            var datadisabled = ["kategori_id","idbarang","brand_id"];
            var dataenabled  = ["sub_kategori_id"];
            
            onoff(datadisabled,"disabled");
            onoff(dataenabled,"enabled");
         }
         
         if(id == "7"){
            var datadisabled = ["kategori_id","brand_id"];
            var dataenabled  = ["sub_kategori_id","idbarang"];
            
            onoff(datadisabled,"disabled");
            onoff(dataenabled,"enabled");
         }
     }
     
     function onoff(list,type){
         if(type == "disabled"){
            for(var i = 0; i < list.length; i++){
                $("#" + list[i]).attr("disabled","disabled");
                $("#" + list[i]).select2("val",'0');
            }
         }else{
            for(var i = 0; i < list.length; i++){
                $("#" + list[i]).removeAttr("disabled");
            }
         }
     }
</script>